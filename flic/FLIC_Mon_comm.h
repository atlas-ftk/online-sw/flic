#ifndef FLIC_MON_COMM_H
#define FLIC_MON_COMM_H

//////////////////////////////////////// for debugging
#include <iostream>
#include <iterator>
#include <fstream>
#include <string>
//////////////////////////////////////// C library
#include <stdio.h>   // printf()
#include <string.h> // memset()
#include <sys/types.h>
#include <sys/socket.h>   // socket() function
#include <netinet/in.h>   // struct sockaddr_in
#include <arpa/inet.h>   // htonl()
#include <unistd.h> // close()

//#include "flic/FLIC.h"

namespace daq {
  namespace ftk {

    class FLIC_Mon_comm {

    private:
      /*
      char srcipaddress;
      int srcport;
      char dstipaddress;
      int dstport;
      */
      int blade_ETH_sockfd;
      struct sockaddr_in blade_ETH_addr;
      struct sockaddr_in FLIC1_Spybuffer_addr;
      socklen_t addr_len;
      struct sockaddr * addr_ptr;

    public:

      // This will read the ETH setup and put the values to the parameters; based on the value of the parameter, setup the Ethernet connection
      FLIC_Mon_comm(const char* srcipaddress, const int srcport, const char* dstipaddress, const int dstport);

      ~FLIC_Mon_comm() {
	//Only thing to do is close the socket.
	//	close(blade_ETH_sockfd);
      } 

      //      int setConnection();
      int getBladeETHSockfd();
      struct sockaddr * getSockAddrPtr();
      socklen_t getSocklen_t();
      void closesockets() {
	close(blade_ETH_sockfd);
      }

    };

  } // Closing ftk namespace
} // Closing daq namespace

#endif
