#ifndef FLIC_COMM_H
#define FLIC_COMM_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#include "flic/FLIC.h"

namespace daq {
  namespace ftk {

    class flic_comm {

    private:

      int sockfd, portno, clilen, n, status,sfd;
      int broadcast;

      struct sockaddr_in flic_addr,interface_addr;
      struct sockaddr host_addr;
      struct addrinfo hints;
      struct addrinfo *result,*i_res;

      FLIC::UDP_Packet tx;
      FLIC::UDP_Packet rx;

      int lock;
      int num_requesting;
      int num_waiting;
      int get_comm_lock();
      void release_comm_lock();

    public:

      flic_comm(const char* flicIP,const char* flicPort, const char* flicEth, bool checkInUse=true);
      ~flic_comm() {
	//Only thing to do is close the socket.
	close(sockfd);
	close(sfd);
      } 

      int singlewrite(ushort target, ushort command, uint address, ushort value, ushort status = 0);
      int singleread(ushort target, ushort command, uint address, ushort* value, ushort status = 0);
      int singlereadmodwrite(ushort target, uint address, ushort mask, ushort value);
      int blockwrite(ushort target, ushort command, uint address, FLIC::UDP_Data& block, ushort size = 256);
      int blockread(ushort target, ushort command, uint address, ushort* data, ushort size = 256);

      int test();

      void error(const char *msg);
      void warn(const char *msg);
      void closesockets() {
	close(sockfd);
	close(sfd);
      }

      bool IsIPInRange(const char* hostIP, const char* flicIP);
      uint32_t IPtoUInt(const char* ip);
      
    };

  } // Closing ftk namespace
} // Closing daq namespace

#endif
