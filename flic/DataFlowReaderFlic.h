#ifndef DATAFLOWREADERFLIC_H
#define DATAFLOWREADERFLIC_H

#include "ftkcommon/DataFlowReader.h"
#include "flic/dal/FtkDataFlowSummaryFlicNamed.h"
#include "flic/FLIC.h"
#include "flic/flic_comm.h"
#include "flic/FLICController.h"
#include <string>
#include <vector>
#include <memory>
#include <cstdint>

// Namespaces
namespace daq {
  namespace ftk {
    
    /*! \brief  DataFormatter implementation of DataFlowReader
    */
    class DataFlowReaderFlic : public DataFlowReader
      {
        public:
          /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
          *  \param ohRawProvider Pointer to initialized  OHRawProvider object
          *  \param forceIS Force the code to use IS as the source of information (default is 0)
          */
          DataFlowReaderFlic(std::shared_ptr<OHRawProvider<> > ohRawProvider, bool forceIS = false, flic_comm *flic = NULL);
         
          void init(const string& deviceName,
       	  const std::string& partitionName = "FTK",
       	  const std::string& isServerName = "DF");
            
          virtual void getL1id(std::vector<int64_t>& srv) override;
          virtual void getLinkOutStatus(std::vector<int64_t>& srv) override;
          virtual void getLinkInStatus(std::vector<int64_t>& srv) override;
          virtual void getFifoInBusy(std::vector<int64_t>& srv) override;
          virtual void getFifoOutBusy(std::vector<int64_t>& srv) override;
          virtual void getNEventsProcessed(std::vector<int64_t>& srv) override;
          virtual void publishExtraHistos(uint32_t option = 0) override;
      
        private:
          std::vector<std::string> getISVectorNames();
          flic_comm *df_flic;
          uint32_t m_df_fwVersion;
          const std::string& partitionName = "FTK";
          std::shared_ptr<FtkDataFlowSummaryFlicNamed> m_theFlicSummary;
      
      };
  } // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERFLIC_H */
