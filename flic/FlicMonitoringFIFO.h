#ifndef _FLICMONITORINGFIFO_H_
#define _FLICMONITORINGFIFO_H_

#include <stdint.h>

#include "ftkcommon/SpyBuffer.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "ftkcommon/Utils.h"
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

namespace daq { 
  namespace ftk {

    class FlicMonitoringFIFO : public daq::ftk::SpyBuffer
    {

    public:

      FlicMonitoringFIFO( ushort targetFpga , ushort fifoId , uint depth );
      virtual ~FlicMonitoringFIFO() noexcept;

      virtual uint32_t getSpyFreeze() override;
      //bool getSpyOverflow() { return false; }
      //uint32_t getSpyPointer() { return 0; }
      //uint32_t getSpyDimension() { return m_depth; } // monitoring fifos have max depth of 1024

      int read( daq::ftk::flic_comm *flic );

      //Implementation of the method from the base class. See the base for details
      //TODO This is a dummy implementation. Has to be adjusted to FLIC case
      virtual int readSpyStatusRegister( uint32_t& spyStatus ) override; 

    protected:
      //Implementation of the method from the base class. See the base for details
      //TODO This is a dummy implementation. At the moment will stay empty
      //FIXME Can not call FlicMonitoringFIFO::read() as it needs flic_comm object as input 
      virtual int readSpyBuffer() override;
      
    private:

      ushort m_targetFpga;
      ushort m_fifoId;
      uint m_depth;
      
    };

  } // namespace ftk
} // namespace daq

#endif
