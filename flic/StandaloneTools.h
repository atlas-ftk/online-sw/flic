#ifndef STANDALONETOOLS_H
#define STANDALONETOOLS_H

#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICController.h"

#include <vector>
#include <map>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <sys/stat.h>

namespace daq {
  namespace ftk {
    namespace standalone {

      //
      // Functions for grabbing user input
      //

      const std::string line     = "==================================================================";
      const std::string lineSlim = "------------------------------------------------------------------";

      inline static FLIC::Comm::Info getBoardSelectionFromUser() {
        ushort iopt = 0;
        std::map<ushort,FLIC::Comm::Info> option_map;

        std::cout << std::endl
                  << "Please select the board to connect to:" << std::endl
                  << line << std::endl
		  << "                   " << "Src IP, Src Port, Dst Port, Dst IP" << std::endl
                  << "    " << iopt++ << " Exit" << std::endl;

        for( size_t ib = 0 ; ib < FLIC::Comm::nboards ; ib++ ) {
          FLIC::Comm::Info b = FLIC::Comm::boards[ib];
          option_map[iopt] = b;
          std::cout << "    " << iopt++ << " " << b.desc << "   ( " << b.boardIP << " , " << b.port << " , " << b.hostIP << " )" << std::endl;
        }

        uint selection;
        std::cout << "Your choice: ";
        scanf( "%u" , &selection );
        return option_map[selection];
      }

      inline static FLIC::Comm::Info getFlicSelectionFromUser() {
        ushort iopt = 0;
        std::map<ushort,FLIC::Comm::Info> option_map;

        std::cout << std::endl
                  << "Please select the board to connect to:" << std::endl
                  << line << std::endl
                  << "    " << iopt++ << " Exit" << std::endl;

        for( size_t ib = 0 ; ib < FLIC::Comm::nboards ; ib++ ) {
          FLIC::Comm::Info b = FLIC::Comm::boards[ib];
          if( b.isBlade ) continue;
          option_map[iopt] = b;
          std::cout << "    " << iopt++ << " " << b.desc << "   ( " << b.boardIP << " , " << b.port << " , " << b.hostIP << " )" << std::endl;
        }

        uint selection;
        std::cout << "Your choice: ";
        scanf( "%u" , &selection );
        return option_map[selection];
      }

      inline static ushort getFpgaFromUser() {
        uint target = 999;
        printf("Enter target:\n");
        printf("   %2d  FPGA U1               %2d  FPGA U2         \n" , FLIC::targetFpgaU1 , FLIC::targetFpgaU2);
        printf("   %2d  FPGA U3               %2d  FPGA U4         \n" , FLIC::targetFpgaU3 , FLIC::targetFpgaU4);
        printf("   %2d  Flash 1               %2d  Flash 2         \n" , FLIC::targetFlash1 , FLIC::targetFlash2);
        printf("   %2d  Flash 3               %2d  Flash 4         \n" , FLIC::targetFlash3 , FLIC::targetFlash4);
        printf("   %2d  SRAM U1 1             %2d  SRAM U1 2       \n" , FLIC::targetSram1Fpga1 , FLIC::targetSram2Fpga1);
        printf("   %2d  SRAM U1 3             %2d  SRAM U1 4       \n" , FLIC::targetSram3Fpga1 , FLIC::targetSram4Fpga1);
        printf("   %2d  SRAM U2 1             %2d  SRAM U2 2       \n" , FLIC::targetSram1Fpga2 , FLIC::targetSram2Fpga2);
        printf("   %2d  SRAM U2 3             %2d  SRAM U2 4       \n" , FLIC::targetSram3Fpga2 , FLIC::targetSram4Fpga2);
        printf("   %2d  Mgmt FPGA             %2d  Pic             \n" , FLIC::targetMgmtFpga , FLIC::targetPic);
        while( /*target < FLIC::targetFpgaU1 ||*/ target > FLIC::targetPic ) {
          printf( "Choice: " );
          scanf( "%u" , &target );
        }
        return ushort(target);
      }

      inline static uint getSramFromUser() {
        uint target = 999;
        printf( "For which SRAM do you want to write FLASH data?\n" );
        printf( "  %2d  Sram1Fpga1          %2d  Sram2Fpga1\n" , FLIC::targetSram1Fpga1 , FLIC::targetSram2Fpga1 );
        printf( "  %2d  Sram3Fpga1          %2d  Sram4Fpga1\n" , FLIC::targetSram3Fpga1 , FLIC::targetSram4Fpga1 );
        printf( "  %2d  Sram1Fpga2          %2d  Sram2Fpga2\n" , FLIC::targetSram1Fpga2 , FLIC::targetSram2Fpga2 );
        printf( "  %2d  Sram3Fpga2          %2d  Sram4Fpga2\n" , FLIC::targetSram3Fpga2 , FLIC::targetSram4Fpga2 );
        while( target < FLIC::targetSram1Fpga1 || target > FLIC::targetSram4Fpga2 ) {
          printf( "Choice: " );
          scanf( "%u" , &target );
        }
        return target;
      }

      inline static uint getFlashCorrespondingToSram( uint targetSram ) {
        if     ( targetSram == FLIC::targetSram1Fpga1 ) return FLIC::targetFlash1;
        else if( targetSram == FLIC::targetSram2Fpga1 ) return FLIC::targetFlash1;
        else if( targetSram == FLIC::targetSram3Fpga1 ) return FLIC::targetFlash2;
        else if( targetSram == FLIC::targetSram4Fpga1 ) return FLIC::targetFlash2;
        else if( targetSram == FLIC::targetSram1Fpga2 ) return FLIC::targetFlash3;
        else if( targetSram == FLIC::targetSram2Fpga2 ) return FLIC::targetFlash3;
        else if( targetSram == FLIC::targetSram3Fpga2 ) return FLIC::targetFlash4;
        else if( targetSram == FLIC::targetSram4Fpga2 ) return FLIC::targetFlash4;
        std::cout << "Unable to find flash Id corresponding to SRAM " << targetSram << std::endl;
        assert( false );
      }

      inline static FLIC::Flash::Region getFlashRegionCorrespondingToSram( uint targetSram ) {
        if     ( targetSram == FLIC::targetSram1Fpga1 ) return FLIC::Flash::flash_region[FLIC::regionSram1];
        else if( targetSram == FLIC::targetSram2Fpga1 ) return FLIC::Flash::flash_region[FLIC::regionSram2];
        else if( targetSram == FLIC::targetSram3Fpga1 ) return FLIC::Flash::flash_region[FLIC::regionSram1];
        else if( targetSram == FLIC::targetSram4Fpga1 ) return FLIC::Flash::flash_region[FLIC::regionSram2];
        else if( targetSram == FLIC::targetSram1Fpga2 ) return FLIC::Flash::flash_region[FLIC::regionSram1];
        else if( targetSram == FLIC::targetSram2Fpga2 ) return FLIC::Flash::flash_region[FLIC::regionSram2];
        else if( targetSram == FLIC::targetSram3Fpga2 ) return FLIC::Flash::flash_region[FLIC::regionSram1];
        else if( targetSram == FLIC::targetSram4Fpga2 ) return FLIC::Flash::flash_region[FLIC::regionSram2];
        std::cout << "Unable to find flash region corresponding to SRAM " << targetSram << std::endl;
        assert( false );
      }



      //
      // Transfer firmware from a binary file to a Flash region
      //
      inline static int DownloadFirmwareToFlash( flic_comm *flic )
      {
        int bytes     = 0;
        int bytesread = 0;
        int target    = -1;
        char filename[256];
        FLIC::UDP_Data data;
        uint address = 0;
        int j          = 0;
        int fileEnd  = 0;
        char buf[512];
        unsigned char buf1[512];
        struct stat results;

        printf( "Enter Flash Area:\n" );
        printf("   %2d  FPGA U1               %2d  FPGA U2         \n",FLIC::targetFlash1,FLIC::targetFlash2);
        printf("   %2d  FPGA U3               %2d  FPGA U4         \n",FLIC::targetFlash3,FLIC::targetFlash4);
        printf("Target choice: ");
        scanf("%d",&target);

        printf("Firmware File [*.bin]: ");
        scanf("%s",&filename[0]);

        //Check file size is not too big
        std::ifstream file(filename,std::ios::in | std::ios::binary);
        //  std::ifstream file(filename,std::ifstream::binary);
        if (stat(filename, &results) == 0){
          if((uint)results.st_size >
             FLIC::Flash::flash_region[FLIC::regionFpga].blocks * FLIC::Flash::FLASH_BLOCK_SIZE  ){
            printf("FIRMWARE IS TOO BIG! %d vs %d",(uint)results.st_size,FLIC::Flash::flash_region[FLIC::regionFpga].blocks * FLIC::Flash::FLASH_BLOCK_SIZE );
            return 0;
          }
          //Erase what is currently in the Flash area.
          address = FLIC::Flash::flash_region[FLIC::regionFpga].start;
          for( uint iblk = 0 ; iblk < FLIC::Flash::flash_region[FLIC::regionFpga].blocks ; iblk++ ) {
	    printf( "\rErasing %d %%" , (100*iblk)/FLIC::Flash::flash_region[FLIC::regionFpga].blocks ); fflush(stdout); 
            flic->singlewrite( target, FLIC::cmdErase, address, 0x0000 );
            usleep(15000);
            address += FLIC::Flash::FLASH_BLOCK_WORDS;//FLASH_BLOCK_WORDS
          }
	  printf( "\rErasing 100 %%\n" );

          address=FLIC::Flash::flash_region[FLIC::regionFpga].start;
          do {
            // Attempt to read a full page from the file
            //printf("\nread file\n");
            file.read(buf,512);
            for(int i = 0;i<256;i++){
              buf1[2*i]=buf[2*i];
              buf1[2*i+1]=buf[2*i+1];
              //printf(" %x%x",buf1[2*i],buf1[2*i+1]);
              data.word[i]=buf1[2*i+1]*0x100+buf1[2*i];
              //printf(" %04x",data.word[i]);
            }
            bytesread=file.gcount();

            if (bytesread != 512 ) {
              // Fill any remaining bytes in page with zeros
              for (j = bytesread; j < 512/2 ; j++) {
                data.word[j] = 0;
              }
              fileEnd = 1;
            }
            bytes += flic->blockwrite( target , FLIC::cmdArrayWrite , address , data );
            usleep(10000);

            address += 256;
            //printf("\rDownloading %d %% at %x value %04x", (100*address)/(FLIC::Flash::flash_region[FLIC::regionFpga].stop),address,data.word[0]);fflush(stdout);
            printf("\rDownloading %d %%", (100*address)/(FLIC::Flash::flash_region[FLIC::regionFpga].stop));fflush(stdout);
          }
          while (/*(address < FLIC::Flash::flash_region[FLIC::regionFpga].stop) && */ fileEnd == 0 );

          printf("\nWrote %x of %x available bytes.",address,FLIC::Flash::flash_region[FLIC::regionFpga].stop);
          file.close();
        }
        else
          return 0;

        return bytes;
      }


      //
      // Transfer module lookup table from text file to flash region
      //
      inline static int DownloadSramToFlash( flic_comm *flic , uint targetSram = 0 )
      {
        int bytes = 0;

        if( targetSram == 0 )
          targetSram = getSramFromUser();

        uint targetFlash                 = getFlashCorrespondingToSram( targetSram );
        FLIC::Flash::Region targetRegion = getFlashRegionCorrespondingToSram( targetSram );
        std::cout << "Targets: SRAM=" << targetSram << ", FLASH=" << targetFlash << ", StartAddress=" << std::hex << targetRegion.start << ", StopAddress=" << targetRegion.stop << std::dec << std::endl;

        //cout << "Enter Path to SRAM Data [*.bin]: ";
        //scanf( "%s" , &filename[0] );
        std::string filename = "../flic/LUTs/LUT_0x7f005.hex";

        // Input file should have one 16-bit word per line
        // A single SRAM has a 21-bit address (one addr per 16-bit word) ==> 2097152 lines
        std::ifstream file( filename.c_str() );
        if( !file.is_open() ) {
          std::cout << "Unable to open file " << filename << std::endl;
          return 0;
        }

        // File looks okay
        // Start the write by first erasing what is currently in Flash area
        // (setting all bits high)
        std::cout << "Erasing " << std::flush;
        for( uint iblk = 0 , address = targetRegion.start ; iblk < targetRegion.blocks ; iblk++ ) {
          std::cout << "." << std::flush;
          flic->singlewrite( targetFlash , FLIC::cmdErase , address , 0x0000 );
          usleep( 15000 );
          address += FLIC::Flash::FLASH_BLOCK_WORDS;
        }
        std::cout << " done." << std::endl;

        // Now write in the new data from file
        for( uint iblk = 0 , address = targetRegion.start ; address < targetRegion.stop ; address += 0x100 , iblk++ ) {

          FLIC::UDP_Data data;

          for( uint iword = 0 ; iword < 0x100 ; iword++ ) {

            if( file.eof() ) {
              std::cout << "File ended too early! Should have 2^21 lines" << std::endl;
              return bytes;
            }

            std::string word;
            getline( file , word );
            std::istringstream iss( word );
            iss >> std::hex >> data.word[iword];

          }

          bytes += flic->blockwrite( targetFlash , FLIC::cmdArrayWrite , address , data );
          usleep( 10000 );
          std::cout << "\rDownloading " << (100*iblk)/8191 << " %, block # " << iblk << std::flush;

        } std::cout << std::endl;

        file.close();
        return bytes;
      }

      //
      // Transfer module lookup table from text file directory to SRAM
      //
      inline static int DownloadSram( flic_comm *flic , uint targetSram = 0 )
      {
        int bytes = 0;

        if( targetSram == 0 )
          targetSram = getSramFromUser();

        // Set bit in control register to give register I/O to SRAM
        bytes += flic->singlewrite( (targetSram<FLIC::targetSram1Fpga2 ? FLIC::targetFpgaU1 : FLIC::targetFpgaU2) , FLIC::cmdClear , 0x2 , 0x1 );

        std::string filename = "../flic/LUTs/LUT_0x7f005.hex";

        // Input file should have one 16-bit word per line
        // A single SRAM has a 21-bit address (one addr per 16-bit word) ==> 2097152 lines
        std::ifstream file( filename.c_str() );
        if( !file.is_open() ) {
          std::cout << "Unable to open file " << filename << std::endl;
          return 0;
        }

        // Now write in the new data from file
        for( uint iblk = 0 , address = 0 ; address < (1<<21) ; address += 0x100 , iblk++ ) {

          FLIC::UDP_Data data;

          for( uint iword = 0 ; iword < 0x100 ; iword++ ) {

            if( file.eof() ) {
              std::cout << "File ended too early! Should have 2^21 lines" << std::endl;
              return bytes;
            }

            std::string word;
            getline( file , word );
            std::istringstream iss( word );
            iss >> std::hex >> data.word[iword];

          }

          bytes += flic->blockwrite( targetSram , FLIC::cmdArrayWrite , address , data );
          usleep( 10000 );
          std::cout << "\rDownloading " << (100*iblk)/8191 << " %, block # " << iblk << ", target=" << targetSram << std::flush;

        } std::cout << std::endl;

        // Give SRAM access back to lookup process
        bytes += flic->singlewrite( (targetSram<FLIC::targetSram1Fpga2 ? FLIC::targetFpgaU1 : FLIC::targetFpgaU2) , FLIC::cmdSet , 0x2 , 0x1 );

        file.close();
        return bytes;
      }

      //
      // Read module lookup table from Flash and compare to text file to check for errors
      //
      inline static int ReadSramFromFlash( flic_comm *flic , uint targetSram = 0 )
      {
        int bytes = 0;

        if( targetSram == 0 )
          targetSram = getSramFromUser();

        uint targetFlash = getFlashCorrespondingToSram( targetSram );
        FLIC::Flash::Region targetRegion = getFlashRegionCorrespondingToSram( targetSram );
        std::cout << "Targets: SRAM=" << targetSram << ", FLASH=" << targetFlash << ", StartAddress=" << std::hex << targetRegion.start << ", StopAddress=" << targetRegion.stop << std::dec << std::endl;

        std::string inputPath  = "../flic/LUTs/LUT_0x7f005.hex";
        std::string outputPath = "SRAMoutput.hex";

        std::ifstream inputFile( inputPath.c_str() );
        std::ofstream outputFile( outputPath.c_str() );
        outputFile << std::hex;

        uint nMismatches = 0;

        std::cout << "Writing output values here : " << outputPath << std::endl
                  << "Comparing to data here     : " << inputPath << std::endl;

        ushort data[256];
        for( uint iblk = 0 , address = targetRegion.start ; address < targetRegion.stop ; address += 0x100 , iblk++ ) {

          bytes += flic->blockread( targetFlash , FLIC::cmdArrayRead , address , data );

          for( uint iword = 0 ; iword < 0x100 ; iword++ ) {

            // check that the read values in this block match expected values
            std::string expectedWord;
            ushort expectedVal;
            getline( inputFile , expectedWord );
            std::istringstream iss( expectedWord );
            iss >> std::hex >> expectedVal;

            if( expectedVal != data[iword] ) nMismatches++;

            outputFile << std::setfill('0') << std::setw(3) << data[iword] << std::endl;

          }
          std::cout << "\rReading " << 100*iblk/8191 << " %, block # " << iblk << ", nMismatches=" << nMismatches << ", SampleRead=" << std::hex << address+1 << ":" << std::setfill('0') << std::setw(3) << data[1] << std::dec << std::flush;
        } std::cout << std::endl;

        inputFile.close();
        outputFile.close();

        return bytes;
      }


      //
      // Read module lookup table directory from SRAM and compare to text file to check for errors
      //
      inline static int ReadSram( flic_comm *flic , uint targetSram = 0 )
      {
        int bytes = 0;

        if( targetSram == 0 )
          targetSram = getSramFromUser();

        std::cout << "Targets: SRAM=" << targetSram << std::endl;

        // Set bit in control register to give register I/O to SRAM
        bytes += flic->singlewrite( (targetSram<FLIC::targetSram1Fpga2 ? FLIC::targetFpgaU1 : FLIC::targetFpgaU2) , FLIC::cmdClear , 0x2 , 0x1 );

        std::string inputPath  = "../flic/LUTs/LUT_0x7f005.hex";
        std::string outputPath = "SRAMoutput.hex";

        std::ifstream inputFile( inputPath.c_str() );
        std::ofstream outputFile( outputPath.c_str() );
        outputFile << std::hex;

        uint nMismatches = 0;

        std::cout << "Writing output values here : " << outputPath << std::endl
                  << "Comparing to data here     : " << inputPath << std::endl;

        for( uint iblk = 0 , address = 0 ; address < (1<<21) ; address += 0x100 , iblk++ ) {
 
          ushort data[256];
          bytes += flic->blockread( targetSram , FLIC::cmdArrayRead , address , data );
          usleep( 10000 );

          for( uint iword = 0 ; iword < 0x100 ; iword++ ) {

            // check that the read values in this block match expected values
            std::string expectedWord;
            ushort expectedVal;
            getline( inputFile , expectedWord );
            std::istringstream iss( expectedWord );
            iss >> std::hex >> expectedVal;

            if( expectedVal != data[iword] ) nMismatches++;

            outputFile << std::setfill('0') << std::setw(3) << data[iword] << std::endl;

          }
          std::cout << "\rReading " << 100*iblk/8191 << " %, block # " << iblk << ", nMismatches=" << nMismatches << ", SampleRead=" << std::hex << address+1 << ":" << std::setfill('0') << std::setw(3) << data[1] << std::dec << std::flush;
        } std::cout << std::endl;

        // Give SRAM access back to lookup process
        bytes += flic->singlewrite( (targetSram<FLIC::targetSram1Fpga2 ? FLIC::targetFpgaU1 : FLIC::targetFpgaU2) , FLIC::cmdSet , 0x2 , 0x1 );

        inputFile.close();
        outputFile.close();

        return bytes;
      }


      //
      // Change the emulator delay between events using user input
      //
      inline static int ChangeSSBeDelayParam( flic_comm *flic , const ushort &target )
      {
        int bytes       = 0;
        int option      = 1;
        uint randRange  = 4;
        uint fixedDelay = 12;
        uint value      = 0x0;

        printf("\n");
        printf("Select an option:\n\n");
        printf("   1 Random                        2 Fixed  \n\n");
        printf("Your choice: ");
        scanf("%d",&option);

        if( option == 1 ) { //RANDOM OPTIONS
          printf("\nEnter minimum delay [0-4096] : ");
          scanf("%u",&fixedDelay);
          printf("Enter range as power of two [1-7] : ");
          scanf("%u",&randRange);

          printf("\nFLIC SSBe will produce between %d and %d word delay per record. \n",fixedDelay,fixedDelay-2+(int)pow(2,randRange));

          //SEND HERE!
          value=randRange*0x1000;
          value+=fixedDelay;
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x018, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x019, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x01A, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x01B, value);

        } //RANDOM

        if( option == 2 ) { //FIXED OPTIONS
          printf("\nEnter fixed delay [0-4096] : ");
          scanf("%u",&fixedDelay);
          printf("FLIC SSBe will produce %d word delay per record. \n",fixedDelay);
          value=0x8000;
          value+=fixedDelay;
          //SEND HERE
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x018, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x019, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x01A, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x01B, value);

        }

        return bytes;
      }


      //
      // Change emulator number of tracks per event using user input
      //
      inline static int ChangeSSBeTrackParam( flic_comm *flic , const ushort &target )
      {
        int bytes       = 0;
        int option      = 1;
        uint randRange  = 4;
        uint fixedTrack = 12;
        uint value      = 0x0;

        printf("\n");
        printf("Select an option:\n\n");
        printf("   1 Random                        2 Fixed \n\n");
        printf("Your choice: ");
        scanf("%d",&option);

        if( option == 1 ) { //RANDOM OPTIONS
          printf("\nEnter minimum number of tracks [1-16] : ");
          scanf("%u",&fixedTrack);
          printf("Enter range as power of two [1-7] : ");
          scanf("%u",&randRange);

          printf("\nFLIC SSBe will produce between %d and %d tracks per record. \n",fixedTrack,fixedTrack-2+(int)pow(2,randRange));

          //SEND HERE!
          value=randRange*0x1000;
          value+=fixedTrack*0x100;
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x020, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x021, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x022, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x023, value);

        } //RANDOM

        if( option == 2 ) { //FIXED OPTIONS
          printf("\nEnter fixed number of tracks [1-256] : ");
          scanf("%u",&fixedTrack);
          fixedTrack--;
          printf("FLIC SSBe will produce %d tracks per record. \n",fixedTrack+1);
          value=0x8000;
          value+=fixedTrack;
          //SEND HERE
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x020, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x021, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x022, value);
          bytes+=flic->singlewrite(target,FLIC::cmdWrite,0x023, value);
        }

        return bytes;
      }


      //
      // Generic change to SSBe parameters using user input
      //
      inline static int ChangeSSBeRunParam( flic_comm *flic , const ushort &target )
      {
        int bytes  = 0;
        int option = 1;

        while( option != 0 ) {
          printf("\n");
          printf("Select an option:\n");
          printf("   0 Return to main menu\n\n");
          printf("   1 Tracks in record              2 Delay between records  \n\n");
          printf("Your choice: ");
          scanf("%d",&option);
          if( option == 1 ) bytes+=ChangeSSBeTrackParam(flic,target);
          if( option == 2 ) bytes+=ChangeSSBeDelayParam(flic,target);
        }//while option
        return bytes;
      }


      //
      // Dump the processing pipeline counters to terminal
      //
      inline static int DumpProcCounters( flic_comm *flic , ushort target )
      {
        int bytes = 0;

        ushort data[0x40];
        bytes = flic->blockread( target , FLIC::cmdArrayRead , 0x310 , data , 0x40 );

        printf( "\n" );
        printf( "Counts for FPGA %u ...\n" , target );
        printf( "%30s %10s %10s %10s %10s\n" , "Pipeline :" , "1" , "2" , "3" , "4" );
        for( ushort i = 0 ; i < 0x10 ; i++ ) {
          printf( "%30s %10x %10x %10x %10x\n" , FLIC::counter_names[i].c_str() , data[i] , data[i+0x10] , data[i+0x20] , data[i+0x30] );
        }
        printf( "\n" );
        return bytes;
      }


      //
      // Dump the global status of processor FPGA
      //
      inline static int DumpGlobalStatus( flic_comm *flic , ushort target ) {
	
	ushort data[4];
	int bytes = flic->blockread( target , FLIC::cmdArrayRead , 0x13C , data , 4 );

	// Status definitions:
	//   FLIC_GLOBAL_STATUSes(i)(0) <=  SFP_FIFO_PROG_FULLs(i);         --      16: SFP input FIFO Prog Full is set  (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(1) <=  CCMUX_FIFO_PROG_FULLs(i);       --      17: CCMUX FIFO Prog Full is set (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(2) <=  SRAM_FIFO_PROG_FULLs(i);        -- 18: SRAM Lookup FIFO Prog Full is set (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(3) <=  MERGE_FIFO_PROG_FULLs(i);       -- 19: Merge FIFO Prog Full is set (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(4) <=  RTM_FIFO_PROG_FULLs(i); -- 20: RTM FIFO Prog Full is set (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(5) <=  COLLECTED_PIPELINE_PROG_FULLs(i)(5);    -- 21: User test Prog Full is set (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(6) <=  U3_TAGGED_EVENT_FIFO_PROG_FULLs(i);     -- 22: Spy buffer Tag FIFO #1 Prog full is set  (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(7) <=  U4_TAGGED_EVENT_FIFO_PROG_FULLs(i);     -- 23: Spy buffer Tag FIFO #2 Prog full is set  (source of flow control to SSB)
	//   FLIC_GLOBAL_STATUSes(i)(8) <=  SLINK_LDOWN_FLAGs(i);   -- 24: SLINK went down since last event was transmitted
	//   FLIC_GLOBAL_STATUSes(i)(9) <=  SLINK_LFF_FLAGs(i);     -- 25: SLINK FIFO FULL  (LFF)
	//   FLIC_GLOBAL_STATUSes(i)(10) <=  SLINK_XOFF_FLAGs(i);   -- 26: SLINK XOFF
	//   FLIC_GLOBAL_STATUSes(i)(11) <=  NOT_IN_SYNCs(i);       -- 27: NOT_IN_SYNC (Core Crate Receiver not synchronized to input)
	//   FLIC_GLOBAL_STATUSes(i)(12) <=  SLINK_CTL2_REG(i+4);   -- 28: User Defined status bit 1
	//   FLIC_GLOBAL_STATUSes(i)(13) <=  SLINK_CTL2_REG(i+8);   -- 29: User Defined status bit 2
	//   FLIC_GLOBAL_STATUSes(i)(14) <=  '0';   -- 30: Reserved for use as enable/disable of checking the STAT FIFOS
	//   FLIC_GLOBAL_STATUSes(i)(15) <=  '0';   -- 31: Reserved

	printf( "\n" );
	printf( "Global status for FPGA %u ...\n" , target );
	printf( "%35s %10s %10s %10s %10s\n" , "STATUS" , "PIPE0" , "PIPE1" , "PIPE2" , "PIPE3" );
	for( ushort i = 0 ; i < 16 ; i++ ) {
	  printf( "%35s" , FLIC::global_status_names[i].c_str() );
	  for( ushort j = 0 ; j < 4 ; j++ ) {
	    printf( " %10x" , (data[j]>>i)&1 );
	  }
	  printf( "\n" );
	}
	printf( "%35s %10i %10i %10i %10i\n" , "COMBINED" , data[0] , data[1] , data[2] , data[3] );
	printf( "\n" );
	
	return bytes;
      }


      //
      // Dump the global status of processor FPGA
      //
      inline static int DumpRawSLINKStatus( flic_comm *flic , ushort target ) {
	ushort data;
	int bytes = flic->singleread( target , FLIC::cmdRead , 0x10E , &data );

	// RAW_SLINK_STATUSes(i) <= SLINK_LDOWN_N(i) & SLINK_LFF_N(i) & SLINK_FLOWCTLLED(i) & SLINK_ACTIVITYLED(i);

	printf( "\n" );
	printf( "Raw SLINK status for FPGA %u = %10x\n" , target , data );
	printf( "%35s %10s %10s %10s %10s\n" , "STATUS" , "PIPE0" , "PIPE1" , "PIPE2" , "PIPE3" );
	printf( "%35s %10x %10x %10x %10x\n" , "SLINK_ACTIVATED" , (data>>0)&1 , (data>>4)&1 , (data>>8)&1 , (data>>12)&1 );
	printf( "%35s %10x %10x %10x %10x\n" , "SLINK_FLOWCTLLED" , (data>>1)&1 , (data>>5)&1 , (data>>9)&1 , (data>>13)&1 );
	printf( "%35s %10x %10x %10x %10x\n" , "SLINK_LFF_N" , (data>>2)&1 , (data>>6)&1 , (data>>10)&1 , (data>>14)&1 );
	printf( "%35s %10x %10x %10x %10x\n" , "SLINK_LDOWN_N" , (data>>3)&1 , (data>>7)&1 , (data>>11)&1 , (data>>15)&1 );
	printf( "\n" );
	
	return bytes;
      }


      //
      // Dump the IP/Mac address configuration currently used for U3/U4 spybuffers
      //
      inline static int DumpSpybufferIPConfig( flic_comm *flic ) {
	int bytes = 0;

	std::string title[4] = { "U3 Eth1" , "U3 Eth2" , "U4 Eth1" , "U4 Eth2" };
	ushort startAddr[4]  = { 0x17 , 0x37 , 0x17 , 0x37 };
	ushort target[4]     = { FLIC::targetFpgaU3 , FLIC::targetFpgaU3 , FLIC::targetFpgaU4 , FLIC::targetFpgaU4 };
	ushort data[12];

	for( std::size_t i = 0 ; i < 4 ; i++ ) {
	  bytes += flic->blockread( target[i] , FLIC::cmdArrayRead , startAddr[i] , data , 12 );
	  printf( "== %s ==\n" , title[i].c_str() );
	  printf( "Source MAC   %02x:%02x:%02x:%02x:%02x:%02x\n" , data[2]>>8 , data[2]&0xff , data[1]>>8 , data[1]&0xff , data[0]>>8 , data[0]&0xff );
	  printf( "Source IP    %d.%d.%d.%d\n" , data[4]>>8 , data[4]&0xff , data[3]>>8 , data[3]&0xff );
	  printf( "Source Port  %d\n" , data[5] );
	  printf( "Dest MAC     %02x:%02x:%02x:%02x:%02x:%02x\n" , data[8]>>8 , data[8]&0xff , data[7]>>8 , data[7]&0xff , data[6]>>8 , data[6]&0xff );
	  printf( "Dest IP      %d.%d.%d.%d\n" , data[10]>>8 , data[10]&0xff , data[9]>>8 , data[9]&0xff );
	  printf( "Dest Port    %d\n" , data[11] );
	  printf( "\n" );
	}
	
	return bytes;
      }


      //
      // Dump some U3/U4 diagnostic information to help check links to blade, etc.
      //
      inline static int DumpU3U4Diagnostics( flic_comm *flic ) {
	int bytes = 0;

	std::string title[4] = { "U3 Eth1" , "U3 Eth2" , "U4 Eth1" , "U4 Eth2" };
	ushort startAddr[4]  = { 0x110 , 0x130 , 0x110 , 0x130 };
	ushort target[4]     = { FLIC::targetFpgaU3 , FLIC::targetFpgaU3 , FLIC::targetFpgaU4 , FLIC::targetFpgaU4 };
	ushort data[22];

	
	for( std::size_t i = 0 ; i < 4 ; i++ ) {
	  printf( "\n" );
	  printf( "== %s ==\n" , title[i].c_str() );
	  printf( "\n" );

	  // Read all diagnostic registers;
	  bytes += flic->blockread( target[i] , FLIC::cmdArrayRead , startAddr[i] , data , 22 );

	  //
	  // Print info
	  //

	  // XAUI Status Register
	  printf( ".. eth_xaui_status_reg ..\n" );
	  printf( "%-30s %x\n" , "eth_reset" , (data[0]>>15)&0x1 );
	  printf( "%-30s %x\n" , "eth_xaui_txlock" , (data[0]>>14)&0x1 );
	  printf( "%-30s %x\n" , "eth_xaui_mgt_tx_ready" , (data[0]>>13)&0x1 );
	  printf( "%-30s %x\n" , "eth_xaui_align_status" , (data[0]>>12)&0x1 );
	  printf( "%-30s %x\n" , "eth_xaui_sync_status" , (data[0]>>8)&0xf );
	  printf( "%-30s %02x\n" , "eth_xaui_status_vector" , (data[0]>>0)&0xff );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[0]" , (data[0]>>0)&0x1 , "Latches '1' if there is a fault in the transmit path" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[1]" , (data[0]>>1)&0x1 , "Latches '1' if there is a fault in the receive path" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[2]" , (data[0]>>2)&0x1 , "'1' if XAUI RX Lane 0 is synchronized" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[3]" , (data[0]>>3)&0x1 , "'1' if XAUI RX Lane 1 is synchronized" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[4]" , (data[0]>>4)&0x1 , "'1' if XAUI RX Lane 2 is synchronized" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[5]" , (data[0]>>5)&0x1 , "'1' if XAUI RX Lane 3 is synchronized" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[6]" , (data[0]>>6)&0x1 , "'1' if XAUI receiver is aligned over all four lanes" );
	  printf( "%-30s %x   %s\n" , "eth_xaui_status_vector[7]" , (data[0]>>7)&0x1 , "Latches '0' if receiver link is not up" );
	  printf( "\n" );

	  // 10gemac Status Register
	  printf( ".. eth_10gemac_status_reg ..\n" );
	  printf( "%-30s %x\n" , "eth_reset" , (data[1]>>15)&0x1 );
	  printf( "%-30s %x   %s\n" , "14..2" , (data[1]>>2)&0x1fff , "Fixed low" );
	  printf( "%-30s %x   %s\n" , "eth_10gemac_status_vector[0]" , (data[1]>>0)&0x1 , "'1' when reconciliation sublayer is receiving remote fault sequence ordered sets" );
	  printf( "%-30s %x   %s\n" , "eth_10gemac_status_vector[1]" , (data[1]>>1)&0x1 , "'1' when reconciliation sublayer is receiving local fault sequence ordered sets" );
	  printf( "\n" );

	  // 10gemac Tx Statistics
	  printf( ".. eth_10gemac_tx_statistics 1&0 ..\n" );
	  printf( "%-30s %x\n" , "valid" , (data[3]>>15)&0x1 );
	  printf( "%-30s %x\n" , "pause_frame_transmitted" , (data[3]>>14)&0x1 );
	  printf( "%-30s %x\n" , "vlan_frame" , (data[3]>>13)&0x1 );
	  printf( "%-30s %x\n" , "control_frame" , (data[3]>>12)&0x1 );
	  printf( "%-30s %x\n" , "underrun_frame" , (data[3]>>11)&0x1 );
	  printf( "%-30s %x\n" , "multicast_frame" , (data[3]>>10)&0x1 );
	  printf( "%-30s %x\n" , "broadcast_frame" , (data[3]>>9)&0x1 );
	  printf( "%-30s %x\n" , "successful_frame" , (data[3]>>8)&0x1 );
	  printf( "%-30s %x\n" , "bytes_valid[3:0]" , (data[3]>>0)&0xf );
	  printf( "%-30s %04x\n" , "frame_length_count" , (data[2]>>0)&0x7fff );
	  printf( "\n" );

	  // 10gemac Tx Statistics
	  printf( ".. eth_10gemac_rx_statistics 1&0 ..\n" );
	  printf( "%-30s %x\n" , "valid" , (data[5]>>15)&0x1 );
	  printf( "%-30s %x\n" , "out_of_range" , (data[5]>>14)&0x1 );
	  printf( "%-30s %x\n" , "bad_opcode" , (data[5]>>13)&0x1 );
	  printf( "%-30s %x\n" , "flow_control_frame" , (data[5]>>12)&0x1 );
	  printf( "%-30s %x\n" , "vlan_frame" , (data[5]>>11)&0x1 );
	  printf( "%-30s %x\n" , "out_of_bounds" , (data[5]>>10)&0x1 );
	  printf( "%-30s %x\n" , "control_frame" , (data[5]>>9)&0x1 );
	  printf( "%-30s %x\n" , "multicast_frame" , (data[5]>>8)&0x1 );
	  printf( "%-30s %x\n" , "broadcast_frame" , (data[5]>>7)&0x1 );
	  printf( "%-30s %x\n" , "fcs_error" , (data[5]>>6)&0x1 );
	  printf( "%-30s %x\n" , "bad_frame" , (data[5]>>5)&0x1 );
	  printf( "%-30s %x\n" , "good_frame" , (data[5]>>4)&0x1 );
	  printf( "%-30s %x\n" , "bytes_valid[3:0]" , (data[5]>>0)&0xf );
	  printf( "%-30s %04x\n" , "frame_length_count" , (data[4]>>0)&0x7fff );
	  printf( "\n" );
	  
	  // Rx Config
	  printf( ".. PacketReceiver  ..\n" );
	  printf( "rx_dest MAC    %02x:%02x:%02x:%02x:%02x:%02x\n" , data[8]>>8 , data[8]&0xff , data[7]>>8 , data[7]&0xff , data[6]>>8 , data[6]&0xff );
	  printf( "rx_dest IP     %d.%d.%d.%d\n" , data[10]>>8 , data[10]&0xff , data[9]>>8 , data[9]&0xff );
	  printf( "rx_dest Port   %d\n" , data[11] );
	  printf( "rx_src MAC     %02x:%02x:%02x:%02x:%02x:%02x\n" , data[14]>>8 , data[14]&0xff , data[13]>>8 , data[13]&0xff , data[12]>>8 , data[12]&0xff );
	  printf( "rx_src IP      %d.%d.%d.%d\n" , data[16]>>8 , data[16]&0xff , data[15]>>8 , data[15]&0xff );
	  printf( "rx_src Port    %d\n" , data[17] );
	  printf( "rx_udp_value   0x%04x\n" , data[18] );
	  printf( "\n" );

	  // Tx Config
	  printf( ".. PacketSender ..\n" );
	  printf( "checksum       0x%04x\n" , data[19] );
	  printf( "packet_len     0x%04x\n" , data[20] );
	  printf( "packet_num     0x%04x\n" , data[21] );
	  printf( "\n" );

	}

	return bytes;
      }


      //
      // Read CURRENT_L1ID from register map and dump to terminal
      //
      inline static int DumpCurrentL1ID( flic_comm *flic , ushort target ) {
	int bytes = 0;
	ushort l1ids[2];
	bytes += flic->blockread( target , FLIC::cmdArrayRead , 0x14E , l1ids , 2 );
	printf( "CURRENT_L1ID[0] = 0x%04x%04x\n" , l1ids[0] , l1ids[1] );
	return bytes;
      }

      
      //
      // Dump data in monitoring FIFOs to terminal
      //
      inline static int ReadMonitoringFIFOs( flic_comm *flic , ushort target )
      {
        // FIXME : just reading 256 out of available 1024 words here...
        int bytes = 0;

        ushort data[4][256];
        for( ushort i = 0 ; i < 4 ; i++ )
          bytes += flic->blockread( target , FLIC::cmdFifoRead , 0x110+(2*i) , data[i] , 256 );

        printf( "\n" );
        printf( "Monitoring FIFO Data for FPGA %u\n" , target );
        printf( "%10s : %10s %10s %10s %10s\n" , "iAddr" , "FifoM0" , "FifoM1" , "FifoM2" , "FifoM3" );
        for( ushort iword = 0 ; iword < 256 ; iword++ ) {
          printf( "%10x : %10x %10x %10x %10x\n" , iword , data[0][iword] , data[1][iword] , data[2][iword] , data[3][iword] );
        }
        printf( "\n" );

        // Dump output to text files
        for( ushort ififo = 0 ; ififo < 4 ; ififo++ ) {
          char outputPath[50];
          sprintf( outputPath , "MonFifo_Fpga%u_Fifo%u.hex" , target , ififo );
          std::ofstream outputFile( outputPath );
          outputFile << std::hex;
          for( ushort iword = 0 ; iword < 256 ; iword++ ) {
            outputFile << "0x" << std::setfill('0') << std::setw(4) << data[ififo][iword] << std::endl;
          }
          outputFile.close();
        }

        return bytes;
      }

      
      //
      // Dump data in Flow Control monitoring FIFOs to terminal
      //
      inline static int ReadFlowControlFIFOs( flic_comm *flic , ushort target )
      {
        // FIXME : just reading 256 out of available 1024 words here...
        int bytes = 0;

        ushort data[4][256];
        for( ushort i = 0 ; i < 4 ; i++ )
          bytes += flic->blockread( target , FLIC::cmdFifoRead , 0x118+(2*i) , data[i] , 256 );

        printf( "\n" );
        printf( "Flow Control FIFO Data for FPGA %u\n" , target );
        printf( "%10s : %10s %10s %10s %10s\n" , "iAddr" , "FifoM0" , "FifoM1" , "FifoM2" , "FifoM3" );
        for( ushort iword = 0 ; iword < 256 ; iword++ ) {
          printf( "%10x : %10x %10x %10x %10x\n" , iword , data[0][iword] , data[1][iword] , data[2][iword] , data[3][iword] );
	  if( iword < 253 ) {
	    if( data[0][iword] == data[0][iword+1] && data[0][iword] == data[0][iword+2] &&
		data[1][iword] == data[1][iword+1] && data[1][iword] == data[1][iword+2] &&
		data[2][iword] == data[2][iword+1] && data[2][iword] == data[2][iword+2] &&
		data[3][iword] == data[3][iword+1] && data[3][iword] == data[3][iword+2] ) {
	      break;
	    }
	  }
        }
        printf( "\n" );

        // Dump output to text files
        for( ushort ififo = 0 ; ififo < 4 ; ififo++ ) {
          char outputPath[50];
          sprintf( outputPath , "FCFifo_Fpga%u_Fifo%u.hex" , target , ififo );
          std::ofstream outputFile( outputPath );
          outputFile << std::hex;
          for( ushort iword = 0 ; iword < 256 ; iword++ ) {
            outputFile << "0x" << std::setfill('0') << std::setw(4) << data[ififo][iword] << std::endl;
          }
          outputFile.close();
        }

        return bytes;
      }
      

      //
      // Simple single register read from user input
      //
      inline static int ReadRegister( flic_comm *flic )
      {
        int bytes = 0;

        ushort target = getFpgaFromUser();

        uint reg;
        printf("Enter register [hex]: ");
        scanf("%x",&reg);

        ushort value;
        bytes = flic->singleread( target , FLIC::cmdRead , reg , &value );
        printf("\nRegister %x has value: %x\n",reg,value);

        return bytes;
      }


      //
      // Simple single register write from user input
      //
      inline static int WriteRegister( flic_comm *flic )
      {
        int bytes = 0;

        ushort target = getFpgaFromUser();

        int cmd;
        printf( "Enter command:\n" );
        printf("   %2d  Write                 %2d  Set             \n",FLIC::cmdWrite,FLIC::cmdSet);
        printf("   %2d  Clear                 %2d  Erase           \n",FLIC::cmdClear,FLIC::cmdErase);
        printf( "Command choice: " );
        scanf( "%d" , &cmd );

        uint reg;
        printf( "Enter register [hex]: " );
        scanf( "%x" , &reg );

        uint value;
        printf( "Enter value/mask [hex]: " );
        scanf( "%x" , &value );

        bytes += flic->singlewrite( target , cmd , reg , value );
        return bytes;
      }

      
      
    }
  }
}

#endif
