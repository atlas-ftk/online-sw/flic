#ifndef FLIC_MON_EVENT_H
#define FLIC_MON_EVENT_H

#include <inttypes.h>
#include <vector>

//#include <TString.h>
#include <TH1F.h>
#include <TH2F.h>
//#include "TH1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"

#include "flic/halftofloat.h"


namespace daq {
  namespace ftk {
    
    class FLIC_Mon_event {
      
    private:
      
      uint16_t * begin_assembly_header_ptr;
      //      std::vector<TH1F *> Histogram_vector_ptr;

    public:
      
      FLIC_Mon_event(uint16_t *);
      ~FLIC_Mon_event();
      
      int setEventAssemblyHeaderPosition(uint16_t *);
      void getEventProcessingResult();

      //      int setHistogramVector(
      //EventAssemblyHeaderPosition(uint16_t *);

    };

  }
}

#endif
