#ifndef FLICCONTROLLER_H
#define FLICCONTROLLER_H

#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include <vector>
#include <string>


namespace daq {
  namespace ftk {

    class
    FLICController
    {

    private:

      std::vector<ushort> testvectors;
      bool loopbackMode;
      ushort tracksPerRecord;
      ushort delayBetweenRecords;
      bool dumpDataOnTheFloor;
      bool reloadFirmware;

    public:

      FLICController()
	: loopbackMode( true )
	, tracksPerRecord( 10 )
	, delayBetweenRecords( 4000 )
	, dumpDataOnTheFloor( false )
	, reloadFirmware( false )
      {}
      ~FLICController() {}

      void setLoopbackMode( bool v ) { loopbackMode = v; }
      void setTracksPerRecord( ushort v ) { tracksPerRecord = v; }
      void setDelayBetweenRecords( ushort v ) { delayBetweenRecords = v; }
      void setDumpDataOnTheFloor( bool v ) { dumpDataOnTheFloor = v; }
      void setReloadFirmware( bool v ) { reloadFirmware = v; }
      
      //
      // Critical control functions
      //
      int PICPowerOn( daq::ftk::flic_comm *flic );
      int PICPowerOff( daq::ftk::flic_comm *flic );
      int PICPowerFromIPMC( daq::ftk::flic_comm *flic );
      int ProgramFPGAs( daq::ftk::flic_comm *flic , const ushort arr_listReg[16] = FLIC::ListProc::Loopback_NoSRAMs );
      int ProgramFPGAs( daq::ftk::flic_comm *flic , std::vector<ushort> ProcTargets , std::vector<ushort> SSBeTargets , bool loadSRAMs );


      //--------------------------
      //Setup
      //--------------------------
      
      //common
      int SetupIP( daq::ftk::flic_comm *flic );
      int SetupIPTest( daq::ftk::flic_comm *flic );
      int ClearFIFOFullFlags( daq::ftk::flic_comm *flic , const ushort &target );
      int SetupU3U4( daq::ftk::flic_comm *flic,bool useSSBe );
      int EventTagging( daq::ftk::flic_comm *flic,bool useSSBe ); //,uint &mask1,uint &mask2);
      int ResetAssemblyLogic( daq::ftk::flic_comm *flic );
      int SetupTagChAndEth( daq::ftk::flic_comm *flic ); //,uint value);  PW - Feb. 20, 2017: only used by flic_udp

      int PulseFlowControlToSSB( daq::ftk::flic_comm *flic , const ushort &target );
      // Clear all FIFOs at Start--Bing, Aug 18th, 2017
      int ClearAllFIFOs( daq::ftk::flic_comm *flic , const ushort &target);
      bool CheckFIFOStatus( daq::ftk::flic_comm *flic , const ushort &target, ushort linksInUse );
      
      int ResetSERDES_Proc( daq::ftk::flic_comm *flic , const ushort &target );
      int ResetSERDES_SSBe( daq::ftk::flic_comm *flic , const ushort &target );
      int IdleBoard( daq::ftk::flic_comm *flic );
      
      int ResetStateMachines_Proc( daq::ftk::flic_comm *flic , const ushort &target );
      int ResetStateMachines_SSBe( daq::ftk::flic_comm *flic , const ushort &target );
      int ResetStateMachines( daq::ftk::flic_comm *flic );

      int InitStateMachines_Proc( daq::ftk::flic_comm *flic , const ushort &target );
      int InitStateMachines_SSBe( daq::ftk::flic_comm *flic , const ushort &target );
      int InitStateMachines( daq::ftk::flic_comm *flic );

      int ResetSLinks( daq::ftk::flic_comm *flic , const ushort &target );
      int SyncWithSSB( daq::ftk::flic_comm *flic , const ushort &target );

      int SetupDoubleProc( daq::ftk::flic_comm *flic );
      int SetupDoubleSSBe( daq::ftk::flic_comm *flic );


      //--------------------------
      // SSBe controls
      //--------------------------

      int StartSSBeData( daq::ftk::flic_comm *flic , const ushort &target , uint channelMask = 0 , int nevents = -1 ,
			 int tracksPerEvent = -1 , int delayBetweenEvents = -1 , bool sendData = true );
      int RestartSSBeData( daq::ftk::flic_comm *flic , const ushort &target , uint channelMask = 0 ); 
      int StopSSBeData( daq::ftk::flic_comm *flic , const ushort &target , uint channelMask = 0 );
      int SendFIFOData( daq::ftk::flic_comm *flic , const ushort &target , uint channelMask = 0 );
      int ResetSSBe( daq::ftk::flic_comm *flic , const ushort &target );
      int ChangeFixedDelay( daq::ftk::flic_comm *flic , const ushort &target , const ushort &delay );

      // Test vector functions
      void loadTestVectorsFromFile( const char *path , bool verbose = true , int l1id = -1 );
      int WriteTestVectorsOneChannel( daq::ftk::flic_comm *flic , const ushort &target , const ushort &channel ,
				      bool verbose = true );
      int WriteTestVectorsFourChannels( daq::ftk::flic_comm *flic , const ushort &target , bool verbose = true );


      //--------------------------
      // RC Transitions
      //--------------------------
      // Higher level run control state transitions
      // These should simply call the basic control functions

      int setup( daq::ftk::flic_comm *flic );
      int configure( daq::ftk::flic_comm *flic );
      int connect( daq::ftk::flic_comm *flic );
      int prepareForRun( daq::ftk::flic_comm *flic );
      int stopDC( daq::ftk::flic_comm *flic );
      int resynch( daq::ftk::flic_comm *flic );
      

      //--------------------------
      // Debugging / Monitoring
      //--------------------------
      //
      // Debugging functions used for checking the system status, updating
      // firmware, etc...
      //

      int ReadRegister( daq::ftk::flic_comm *flic , const ushort &target , const uint &reg , ushort *val );
      ushort GetRegister( daq::ftk::flic_comm *flic , const ushort &target , const uint &reg );
      int WriteRegister( daq::ftk::flic_comm *flic , const ushort &target , const uint &cmd ,
			 const uint &reg , const uint &val );

      // Set up the clock counters
      int InitClockCounters( daq::ftk::flic_comm *flic , const ushort &target , const bool &rateMode );

      // Set up the pipeline counters
      int InitProcCounters( daq::ftk::flic_comm *flic , const ushort &target , const ushort &mode = 0x0 );

      // Turn on dump-data-on-the-floor mode to ignore ROS s-links and send data regardless of ROS XOFFs
      int DumpDataOnTheFloorMode( daq::ftk::flic_comm *flic , const ushort &target );
      
      // Dump status information from management fpga
      // Returns true if setup is still running, and only prints the
      // new status summary if it's different than "summary"
      bool CheckStatus( daq::ftk::flic_comm *flic , std::string &summary );

      // Simpler version if don't want to compare to previous status summary...
      bool CheckStatus( daq::ftk::flic_comm *flic );

      // Check the clock frequencies
      // Returns true if clock rates match expectation
      bool CheckClockCounters( daq::ftk::flic_comm *flic , const ushort &target );

      // Check the XOFF Counters
      // Returns true if counters match expected counts after sync with SSBe 
      bool CheckXOFFCounters( daq::ftk::flic_comm *flic , const ushort &target );

      // Monitoring FIFO controls
      int ArmMonitoringFIFOs( daq::ftk::flic_comm *flic , const ushort &target );
      int ResetArmMonitoringFIFOs( daq::ftk::flic_comm *flic , const ushort &target , const ushort &channel , uint condition);
      int ResetArmFlowControlFIFOs( daq::ftk::flic_comm *flic , const ushort &target );
      
    };

  }
}

#endif
