/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_FLIC_H
#define READOUT_MODULE_FLIC_H

#include <string>
#include <vector>

#include <TString.h>
#include <TH1F.h>
#include <TH2F.h>

#include "RunControl/Common/RunControlBasicCommand.h"
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/Controllable.h>
#include "RunControl/Common/CommandSender.h"
#include "ipc/partition.h"
#include "ipc/core.h"
#include "ftkcommon/ReadoutModule_FTK.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "flic/dal/FTK_RCD_FLIC_1Named.h"
#include "flic/FLIC.h"
#include "flic/flic_comm.h"
#include "flic/FLICController.h"
#include "flic/FLICSetupTool.h"
#include "flic/StandaloneTools.h"
#include "flic/FlicMonitoringFIFO.h"
#include "flic/DataFlowReaderFlic.h"
#include "oh/OHRawProvider.h"

// EMon
#include "ftkcommon/FtkEMonDataOut.h"
#include "ftkcommon/SourceIDSpyBuffer.h"

namespace daq {
  namespace ftk {
    class ReadoutModule_flic : public ReadoutModule_FTK
      {
        public:  // Constructor and destructor
  
          // Constructor (NB: executed at CONFIGURE transition)
          ReadoutModule_flic();
  
          // Destructor (NB: executed at UNCONFIGURE transition)
          virtual ~ReadoutModule_flic() noexcept;
  
        public: // overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin
  
          // Set internal variables (NB: executed at CONFIGURE transition)
          virtual void setup(DFCountedPointer<ROS::Config> configuration) override;
  
          // Reset internal statistics
          virtual void clearInfo() override;
  
          // Get the list of channels connected to this ReadoutModule
          virtual const std::vector<ROS::DataChannel *> *channels() override;
  
          // Set the Run Parameters
          //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);
  
          // Get values of statistical counters
          // virtual DFCountedPointer<Config> getInfo();
  
          // Get access to statistics stored in ISInfo class
          // virtual ISInfo* getISInfo();
  
        public: // overloaded methods inherited from ReadoutModule_FTK
  
          // RC configure transition
          virtual void doConfigure(const daq::rc::TransitionCmd& cmd) override;
    
          // RC connect transition:
          virtual void doConnect(const daq::rc::TransitionCmd& cmd) override;

      		/// RC startNoDF subtransition
          virtual void startNoDF(const daq::rc::SubTransitionCmd& cmd) override;
  
          // RC start of run transition
          virtual void doPrepareForRun(const daq::rc::TransitionCmd& cmd) override;
  
          // RC stop transition
          virtual void doStopDC(const daq::rc::TransitionCmd& cmd) override {}
          virtual void doStopROIB(const daq::rc::TransitionCmd& cmd) override {}
          virtual void doStopHLT(const daq::rc::TransitionCmd& cmd) override {}
          virtual void doStopRecording(const daq::rc::TransitionCmd& cmd) override;
          virtual void doStopGathering(const daq::rc::TransitionCmd& cmd) override {}
          virtual void doStopArchiving(const daq::rc::TransitionCmd& cmd) override {}
  
          // RC unconfigure transition
          virtual void doUnconfigure(const daq::rc::TransitionCmd& cmd) override;
  
          // RC disconnect transition:
          virtual void doDisconnect(const daq::rc::TransitionCmd& cmd) override;
  
          // RC subtransition (IF NEEDED)
          // virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override;
  
          // Method called periodically by RC
  	      virtual void doPublishFullStats( uint32_t ) override;
          virtual void doPublish( uint32_t, bool ) override;
          void localPublish();
          void reloadFirmware();
          void setupBoards(); 
          void boardReset();      
          void sync(ushort maxAttempt);
          void prepareForRunSSBFLICProtocol();
          void stopRecordingSSBFLICProtocol();
          void FIFOClearAndCheck(); 
  
          // RC HW re-synchronization request
          virtual void resynch(const daq::rc::ResynchCmd& cmd);
  
  	      // RC Disable command
  	      virtual void disable(const std::vector<std::string>& channelList);

          /// Ack usercommand notification sender
          void user_sender(const std::string receiver, const std::string cmq_name, const std::vector<std::string>& cmd_params);

          ///Method used to send the ACK message to the FTK manager application
          void ACKsender(const std::string state);
          void getManager();
      	  // Function that returns the name of the ReadoutModule

      	  std::string name_ftk(){return m_name;};

        protected:
          virtual void doUser( const daq::rc::UserCmd& cmd );


        private:
    
          FTK_RCD_FLIC_1Named          *m_flicNamed;     ///< Access IS via schema
    
          flic_comm *m_flic;
          FLICController m_ctrl;
    
          std::vector<ROS::DataChannel *>  m_dataChannels;
          DFCountedPointer<ROS::Config>    m_configuration; /**< Configuration Object, Map Wrapper */
          std::string                      m_isServerName;      /**< IS Server  */
          IPCPartition                     m_ipcpartition;      /**< Partition  */
          uint32_t                         m_runNumber;                 /**< Run Number */
          bool                             m_dryRun;        ///< Bypass VME calls
          bool                             m_isLoadMode;    ///< Temp variable to keep track of daq::ftk::isLoadMode
    	  bool 				   m_CanPublish;		///< Variable used to control the execution of publish
          OHRootProvider                  *m_ohProvider;    ///< Histogram provider
          std::shared_ptr<OHRawProvider<> >   m_ohRawProviderFlic;  ///< Alternative Histogram provider to publish histos out of arrays    
          std::string     m_appName;                        ///< Online services application name
          std::string           m_manager;                                      ///UID of the Manager Application to which send the ACK 
          // FLIC BOARD Specific Parameters
          std::string     m_name;                           /**< Card name  */
          uint32_t        m_slot;                           /**< Slot       */
          uint32_t        m_boardNumber;                    /**< ID for the board */
          std::string     m_flicip;                         /**< IP of FLIC */
          std::string     m_port;                           /**< FLIC port  */
          std::string     m_hostip;                         /**< IP of Host */
          uint16_t        m_verbose;                        /**< Verbosity of output messages */
          bool            m_loopbackMode;                   /**< Loopback Mode Toggle */
          bool            m_useAsSSBe;                      /**< Configure as SSB emulator */
          bool            m_use32bitL1ID;                   /**< Configure to use 32 bit L1ID */
          unsigned short  m_tracksPerRecord;                /**< For loopback mode tests */
          unsigned short  m_delayBetweenRecords;            /**< For loopback mode tests */
          unsigned short  m_totalRecords;                   /**< For loopback mode tests */
          uint32_t        m_delayBetweenSyncAttempts;       /**< Time between attempts at syncing with SSB [s] */
          unsigned short  m_totalSyncAttempts;              /**< Number of sync attempts before throwing error */
          unsigned short  m_totalFIFOClearanceAttempts;     /**< Number of FIFO clearance attempts before throwing error at stop */
          unsigned short  m_linksInUse;                     /**< 8 bit map indicating which ROS links to check */
          unsigned short  m_totalROSLinkResets;             /**< # of times to try resetting ROS links before error */
          bool            m_ignoreROSLinks;                 /**< Dump-data-on-the-floor Toggle */
          bool            m_reloadFirmware;                 /**< Reload U1-4 firmware on configure */
          bool            m_reloadSRAMs;                    /**< Reload lookup tables to SRAMs on configure */
          uint16_t        m_captureCondition;               /**< When to acture events in monitoring FIFOs*/
          uint16_t        m_monitoringChannel;              /**< Select which pipeline to monitor*/
          
          //Removal control variables
    	  std::atomic<bool>	      m_HWError;						/**< Control for the HWError rising */ 
    
          // FLIC BOARD configuration
          std::vector<unsigned short> m_ProcTargets;        /**< Target FPGAs for processor firmware */
          std::vector<unsigned short> m_SSBeTargets;        /**< Target FPGAs for SSB emulator firmware */
          
          // EMon
          daq::ftk::FtkEMonDataOut    *m_ftkemonDataOut;       /**< Pointer to FtkEMonDataOut for publishing spybuffers */
          //DataFlowReader
          std::unique_ptr<DataFlowReaderFlic>           m_dfReaderFlic; 
     
          // Diagnostics
          ushort m_last_proc_counts[4][0x10]; // keep track of pipeline counter values from previous read
          
          // Histograms
          std::vector<TH1F*> m_hGlobalStatus;
          std::vector<TH1F*> m_hPipelineCounters;
          TH1F *m_hTrackSector;
          TH1F *m_hTrackChi2;
          TH1F *m_hTrackD0;
          TH1F *m_hTrackZ0;
          TH1F *m_hTrackCotth;
          TH1F *m_hTrackPhi;
          TH1F *m_hTrackCurv;
    
      };

    inline const std::vector<ROS::DataChannel *> *ReadoutModule_flic::channels()
      {
        return &m_dataChannels;
      }

  } // namespace ftk
} // namespace daq
#endif 
