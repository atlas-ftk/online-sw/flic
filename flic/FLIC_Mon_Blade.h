#ifndef FLIC_BLADE_MON_H
#define FLIC_BLADE_MON_H
//////////////////////////////////////// for debugging
#include <iostream>
#include <iterator>
#include <fstream>
#include <string>
//////////////////////////////////////// C library
#include <stdio.h>   // printf()
#include <string.h> // memset()
#include <sys/types.h>
#include <sys/socket.h>   // socket() function
#include <netinet/in.h>   // struct sockaddr_in
#include <arpa/inet.h>   // htonl()


//////////////////////////////////////// Constant definition
#define MAXLINE 750 // Max word received from FLIC: 750 words - 1500 bytes


//////////////////////////////////////// Struct definition
struct FLIC_UDP_packet {
  uint16_t mesg[MAXLINE];  // uint16_t -> unsigned short int
};

struct assembly_fragment_position {
  uint16_t * beginning_position;
  std::vector<uint16_t *> ET01_position;
  uint16_t * end_position;
};


//////////////////////////////////////// Class definition
class FLIC_Blade_Mon {

private:
  //////////////////// declare of socket variables
  int blade_ETH_sockfd;
  //  int FLIC1_Spybuffer_sockfd;
  struct sockaddr_in blade_ETH_addr;
  struct sockaddr_in FLIC1_Spybuffer_addr;
  socklen_t addr_len;
  struct sockaddr * addr_ptr;

  //////////////////// declare of storage variable in the blade memory
  std::vector<FLIC_UDP_packet> flic_udp_buffer;
  uint16_t * mesg_ptr = flic_udp_buffer.at(0).mesg;

  
public:
  FLIC_Blade_Mon();

  int readEthernetFromFileAndCreateSocket(std::string location);
  // This method will read the ETH setup from the file and put the values to the parameters; based on the value of the parameter, setup the Ethernet connection

  void setupBuffer();

  // receive data and and put data in the storage structure

  int storeUDPpacket();
  // store UDP packet in to the buffer
  
  ~FLIC_Blade_Mon();
  
};

#endif 

