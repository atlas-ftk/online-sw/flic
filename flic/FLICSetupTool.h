#ifndef FLICSETUPTOOL_H
#define FLICSETUPTOOL_H

#include "flic/flic_comm.h"
#include "flic/FLIC.h"

#include <vector>
#include <map>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <assert.h>


namespace daq {
  namespace ftk {

    class FLICSetupTool
    {

    private:

      flic_comm *flic;

      void SendWrite( ushort target , ushort addr , ushort val ) { flic->singlewrite( target , FLIC::cmdWrite , addr , val ); }
      void SendSet( ushort target , ushort addr , ushort val ) { flic->singlewrite( target , FLIC::cmdSet , addr , val ); }
      void SendClear( ushort target , ushort addr , ushort val ) { flic->singlewrite( target , FLIC::cmdClear , addr , val ); }

    public:

      void standard_u1_init_stage_1( unsigned short int target_fpga ) {

	// Put SFPs in "normal mode" rather than "setup" mode so we don't confuse the SSB with non-comma words
	SendWrite( target_fpga , 0x00000004 , 0x00F0 );

	// U1 Set 0x2 0x6060 #Put U1 112/116 SM in Reset
	SendSet( target_fpga , 0x00000002 , 0x6060 );
        // Set match registers in U1
        // changed 20161012 JTA to set default to nobody matches( all enables off )
        SendWrite( target_fpga , 0x0000002A , 0xFFFF );
        SendWrite( target_fpga , 0x0000002B , 0x01FF );
        SendWrite( target_fpga , 0x0000002C , 0xFFFF );
        SendWrite( target_fpga , 0x0000002D , 0x01FF );
        //20160801: FLIC status "word" registers are now the FLIC ERROR MASK registers , should be initialized but to some as-yet unknown value.
        //until that value is known , make 'em 0.
        SendWrite( target_fpga , 0x00000015 , 0x0000 ); // U1 Write 0x15 0x1515 #FLIC status words
        SendWrite( target_fpga , 0x00000016 , 0x0000 ); // U1 Write 0x16 0x1616 #FLIC status words

        // Put U1-GTX113 and U1-GTX114 and U1-GTX115 SM into reset
        SendSet( target_fpga , 0x00000002 , 0x078C );
        // Set U1-GTX113 and U1-GTX114 and U1-GTX115 to send real data.( commas while idle )
        SendWrite( target_fpga , 0x00000012 , 0x000F ); // GTX113 -- U1<->U2
        SendWrite( target_fpga , 0x00000013 , 0x000F ); // GTX114 -- U1<->U3
        SendWrite( target_fpga , 0x00000014 , 0x000F ); // GTX115 -- U1<->U4
        // Reset U1-GTX113 and U1-GTX114 and U1-GTX115 RX & TX Gracefully
        SendWrite( target_fpga , /*U1_Reg.GTX113_PULSED_CTL_REG_A*/ 0x202 , 0xFFFF );
        SendWrite( target_fpga , /*U1_Reg.GTX114_PULSED_CTL_REG_A*/ 0x204 , 0xFFFF );
        SendWrite( target_fpga , /*U1_Reg.GTX115_PULSED_CTL_REG_A*/ 0x206 , 0xFFFF );
        // Take U1-GTX113 and U1-GTX114 and U1-GTX115 SM out of reset
        SendClear( target_fpga , 0x00000002 , 0x078C );

        // MBO 20150730: End new code , resume Jeremy's prcedure.

        SendWrite( target_fpga , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
        SendWrite( target_fpga , 0x00000200 , 0x8888 ); // U1 Write 0x200 0x8888 #Reset U1 GTX112 RX logic
        SendWrite( target_fpga , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL

        //JTA: now that we have flow control you have to reset the U1 TX , too( 20150805 )
        // Failure to do this means U1 GTX112 TX clock is not present , means SSB( U2 ) can't
        // get any flow control messages or anything else.
        SendWrite( target_fpga , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
        SendWrite( target_fpga , 0x00000200 , 0x2222 ); // U1 Write 0x200 0x2222 #Reset U1 GTX112 TX Logic
        SendWrite( target_fpga , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
        // end add

        SendSet( target_fpga , 0x00000003 , 0x00F0 ); // U1 Set 0x3 0x0F0 #Set RTM TDIS Must be done before GTX116 Resets
        SendWrite( target_fpga , 0x00000208 , 0xFFFF ); // U1 Write 0x208 0xFFFF #Whack U1 GTX116
        SendClear( target_fpga , 0x00000002 , 0x6060 ); // U1 Clear 0x2 0x6060 #Take U1 112/116 SM out of Reset

        // JTA 20150805 you also have to turn on the SFP transmitters at the front of the board( U1 )
        SendClear( target_fpga , 0x00000004 , 0x000F ); //clear TDIS bits in SFP Ctl Reg
        SendSet( target_fpga , 0x00000004 , 0x00F0 ); //Set TX Mux bits of U1 to send flow control data , not PRBS data
        return;
      }

      void standard_u2_init_stage_1( unsigned short int target_fpga )
      {
        SendSet( target_fpga , 0x00000002 , 0x0060 ); // U2 Set 0x2 0x60 #Put U2 112 SM in Reset
        SendWrite( target_fpga , 0x00000200 , 0xFFFF ); // U2 Write 0x200 0xFFFF #Whack U2 GTX112
        SendWrite( target_fpga , 0x0000001F , 0x00F0 ); // U2 Write 0x1F 0x0F0 #Set U2 GTX112 Send Commas

        SendWrite( target_fpga , 0x0000001D , 0xDDDD ); // U2 Write 0x1D 0xDDDD #SSB emulator status words
        SendWrite( target_fpga , 0x0000001E , 0xEEEE ); // U2 Write 0x1E 0xEEEE #SSB emulator status words

        // Put U2-GTX113 and U2-GTX114 and U2-GTX115 SM into reset
        SendSet( target_fpga , 0x00000002 , 0x078C );
        // Set U2-GTX113 and U2-GTX114 and U2-GTX115 to send real data.( commas while idle )
        SendWrite( target_fpga , 0x00000024 , 0x000F ); // GTX113 -- U2<->U3
        SendWrite( target_fpga , 0x00000025 , 0x000F ); // GTX114 -- U2<->U1
        SendWrite( target_fpga , 0x00000026 , 0x000F ); // GTX115 -- U2<->U4
        // Reset U2-GTX113 and U2-GTX114 and U2-GTX115 RX Gracefully
        SendWrite( target_fpga , 0x00000202 , 0x4444 );
        SendWrite( target_fpga , 0x00000202 , 0x8888 );
        SendWrite( target_fpga , 0x00000202 , 0x4444 );
        SendWrite( target_fpga , 0x00000204 , 0x4444 );
        SendWrite( target_fpga , 0x00000204 , 0x8888 );
        SendWrite( target_fpga , 0x00000204 , 0x4444 );
        SendWrite( target_fpga , 0x00000206 , 0x4444 );
        SendWrite( target_fpga , 0x00000206 , 0x8888 );
        SendWrite( target_fpga , 0x00000206 , 0x4444 );
        // Take U2-GTX113 and U2-GTX114 and U2-GTX115 SM out of reset
        SendClear( target_fpga , 0x00000002 , 0x078C );

        SendClear( target_fpga , 0x00000002 , 0x0060 ); // U2 Clear 0x2 0x60 #Take U2 112 SM out of Reset
        // //
        return;
      }

      void standard_u3_u4_init_stage_1( unsigned short int target_fpga )
      {
	// JW - 2017.01.26
	// Set U3 and U4 IP/Mac/Port info so they all match Eth7 on the
	// blade. The hope here is that when we ping the Flic from the
	// blade it is then gaurenteed to respond regardless of the blade
	// slot.
	ushort src_mac[3]  = { 0x07aa , 0xa3b1 , 0x0004 }; // 00-04-A3-B1-07-AA
	ushort src_ip[2]   = { 0x0202 , 0xc0a8 }; // 192.168.2.2
	ushort src_port[1] = { 50000 };
	ushort dst_mac[3]  = { 0xeccb , 0x640a , 0x0030 }; // 00:30:64:0A:EC:CB
	ushort dst_ip[2]   = { 0x0201 , 0xc0a8 }; // 192.168.2.1
	ushort dst_port[1] = { 50001 };
	for( ushort i = 0 ; i < 3 ; i++ ) {
	  SendWrite( target_fpga , 0x0017+i , src_mac[i] );
	  SendWrite( target_fpga , 0x0037+i , src_mac[i] );
	  SendWrite( target_fpga , 0x001d+i , dst_mac[i] );
	  SendWrite( target_fpga , 0x003d+i , dst_mac[i] );
	}
	for( ushort i = 0 ; i < 2 ; i++ ) {
	  SendWrite( target_fpga , 0x001a+i , src_ip[i] );
	  SendWrite( target_fpga , 0x003a+i , src_ip[i] );
	  SendWrite( target_fpga , 0x0020+i , dst_ip[i] );
	  SendWrite( target_fpga , 0x0040+i , dst_ip[i] );
	}
	SendWrite( target_fpga , 0x001c , src_port[0] );
	SendWrite( target_fpga , 0x003c , src_port[0] );
	SendWrite( target_fpga , 0x0022 , dst_port[0] );
	SendWrite( target_fpga , 0x0042 , dst_port[0] );


	
        // Put U3-GTX112 and U3-GTX113 and U3-GTX114 SM into reset
        SendSet( target_fpga , 0x00000002 , 0x01EC );
        // Set U3-GTX112 and U3-GTX113 and U3-GTX114 to send real data.( commas while idle )
        SendWrite( target_fpga , 0x00000004 , 0x000F ); // GTX113 -- U3<->U
        SendWrite( target_fpga , 0x00000005 , 0x000F ); // GTX114 -- U3<->U
        SendWrite( target_fpga , 0x00000006 , 0x000F ); // GTX115 -- U3<->U
        // Reset U3-GTX112 and U3-GTX113 and U3-GTX114 RX Gracefully
        SendWrite( target_fpga , 0x00000200 , 0x4444 );
        SendWrite( target_fpga , 0x00000200 , 0x8888 );
        SendWrite( target_fpga , 0x00000200 , 0x4444 );
        SendWrite( target_fpga , 0x00000202 , 0x4444 );
        SendWrite( target_fpga , 0x00000202 , 0x8888 );
        SendWrite( target_fpga , 0x00000202 , 0x4444 );
	// FIXME - 2017-1-23 - may need to add lines below to set up eth3
        //SendWrite( target_fpga , 0x00000203 , 0x4444 );
        //SendWrite( target_fpga , 0x00000203 , 0x8888 );
        //SendWrite( target_fpga , 0x00000203 , 0x4444 );
        SendWrite( target_fpga , 0x00000204 , 0x4444 );
        SendWrite( target_fpga , 0x00000204 , 0x8888 );
        SendWrite( target_fpga , 0x00000204 , 0x4444 );
        // Take U3-GTX112 and U3-GTX113 and U3-GTX114 SM out of reset
        SendClear( target_fpga , 0x00000002 , 0x01EC );

        return;
      }

      void standard_u1_init_stage_2( unsigned short int target_fpga )
      {
        // should be a Send Set , not a Send Write( JTA 20150805 )
        SendSet( target_fpga , 0x00000002 , 0x0012 ); // U1 Write 0x2 0x12 #Set SRAM access , and enable Core Crate receiver and data merge machines
        //end change


        SendWrite( target_fpga , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
        SendWrite( target_fpga , 0x0000001F , 0xFFFF ); // U1 Write 0x1F 0xFFFF #Hold All
        SendWrite( target_fpga , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
        SendWrite( target_fpga , 0x0000001F , 0x0000 ); // U1 Write 0x1F 0x0000 #Clear all
        SendWrite( target_fpga , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all

        return;
      }

      void standard_u2_init_stage_2( unsigned short int target_fpga )
      {
        SendWrite( target_fpga , 0x00000201 , 0x00FF ); // U2 Write 0x201 0xF #Reset SSB Emulator
	// Ensure that SSBe responds to flow control
	SendSet( target_fpga , 0x0000000B , 0x0003 ); // turn on flow control
	SendSet( target_fpga , 0x0000002D , 0x1111 ); // enable pause
        return;
      }

      void standard_u1_init_stage_3( unsigned short int target_fpga )
      {
        SendClear( target_fpga , 0x00000003 , 0x0F00 ); // U1 Clear 0x3 0xF00 #Clear GO bit of reset machines
        SendWrite( target_fpga , 0x00000209 , 0x0F00 ); // U1 Write 0x209 0xF00 #Request reset machines to re-arm
        //? SendSet( target_fpga , 0x00000002 , 0x0F00 ); // U1 Set 0x2 0xF00 #Set the GO bit to the reset machines
        SendSet( target_fpga , 0x00000002 , 0x0008 ); // U1 Set 0x2 0x8 #Switch SRAM control back to SM
        //end change 20160801
        SendWrite( target_fpga , 0x0000020F , 0xD0F0 ); // U1 Write 0x20F 0xD0F0 #Reset all FIFO flags
        // //

        // MBO 20160928: Bring up all slinks in U1
        //JW SendSet( target_fpga , 0x00000009 , 0x000F ); // SLINK_CTL2_REG
        // MBO 20160928: Set all slinks to data dump mode in U1:
        //JW SendSet( target_fpga , 0x00000009 , 0x00F0 ); // SLINK_CTL2_REG
        //( One can never be too careful. )
        // JW
        SendClear( target_fpga , 0x9 , 0xff );
        SendWrite( target_fpga , 0x209 , 0xf00 );
        SendSet( target_fpga , 0x3 , 0x0f00 );

        return;
      }

      void standard_u3_u4_init_stage_3( unsigned short int target_fpga )
      {
        SendSet( target_fpga , 0x0000020F , 0x0001 ); // Pulse the reset bit.

	// JW - 2017.01.26
	// Try XAUI resets
	// eth_xaui_config_vector bit map...
	//   15   eth_reset
	//  6:5   test pattern rate selection, "00" = high frequency
	//    4   enables test pattern
	//    3   resets Rx Link status
	//    2   resets Tx Link status
	//    1   sets RxTx to power down mode
	//    0   sets RxTx to loopback mode
	SendWrite( target_fpga , 0x10 , 0x8000 ); // eth_reset
	SendWrite( target_fpga , 0x10 , 0x0000 ); // nominal
	SendWrite( target_fpga , 0x10 , 0x000c ); // reset RxTx links
	SendWrite( target_fpga , 0x10 , 0x0000 ); // 

	SendWrite( target_fpga , 0x30 , 0x8000 ); // eth_reset
	SendWrite( target_fpga , 0x30 , 0x0000 ); // nominal
	SendWrite( target_fpga , 0x30 , 0x000c ); // reset RxTx links
	SendWrite( target_fpga , 0x30 , 0x0000 ); // 

        return;
      }

    public:

      FLICSetupTool() {}
      ~FLICSetupTool() {}

      void setFlicComm( flic_comm* _flic ) { flic = _flic; }

      int INIT_U1_U2_U3_U4() {
        standard_u1_init_stage_1( FLIC::targetFpgaU1 );
        standard_u2_init_stage_1( FLIC::targetFpgaU2 );
        standard_u3_u4_init_stage_1( FLIC::targetFpgaU3 );
        standard_u3_u4_init_stage_1( FLIC::targetFpgaU4 );
        standard_u1_init_stage_2( FLIC::targetFpgaU1 );
        standard_u2_init_stage_2( FLIC::targetFpgaU2 );
        standard_u1_init_stage_3( FLIC::targetFpgaU1 );
        standard_u3_u4_init_stage_3( FLIC::targetFpgaU3 );
        standard_u3_u4_init_stage_3( FLIC::targetFpgaU4 );
        return 0;
      }

      int INIT_U1_U1_U3_U4() {
        standard_u1_init_stage_1( FLIC::targetFpgaU1 );
        standard_u1_init_stage_1( FLIC::targetFpgaU2 );
        standard_u3_u4_init_stage_1( FLIC::targetFpgaU3 );
        standard_u3_u4_init_stage_1( FLIC::targetFpgaU4 );
        standard_u1_init_stage_2( FLIC::targetFpgaU1 );
        standard_u1_init_stage_2( FLIC::targetFpgaU2 );
        standard_u1_init_stage_3( FLIC::targetFpgaU1 );
        standard_u1_init_stage_3( FLIC::targetFpgaU2 );
        standard_u3_u4_init_stage_3( FLIC::targetFpgaU3 );
        standard_u3_u4_init_stage_3( FLIC::targetFpgaU4 );
        return 0;
      }

      int INIT_U2_U2_U3_U4() {
        standard_u2_init_stage_1( FLIC::targetFpgaU1 );
        standard_u2_init_stage_1( FLIC::targetFpgaU2 );
        standard_u3_u4_init_stage_1( FLIC::targetFpgaU3 );
        standard_u3_u4_init_stage_1( FLIC::targetFpgaU4 );
        standard_u2_init_stage_2( FLIC::targetFpgaU1 );
        standard_u2_init_stage_2( FLIC::targetFpgaU2 );
        standard_u3_u4_init_stage_3( FLIC::targetFpgaU3 );
        standard_u3_u4_init_stage_3( FLIC::targetFpgaU4 );
        return 0;
      }

      int INIT_SPYBUFFERS( uint u3_match_reg = 0x81FFFFFF , uint u4_match_reg = 0x81FFFFFF , ushort u3_enable = 0xFF , ushort u4_enable = 0xFF ) {

        // Setup matching logic to transfer all.
        SendWrite( FLIC::targetFpgaU1 , 0x0000002A , (u3_match_reg>>0)&0xFFFF );
        SendWrite( FLIC::targetFpgaU1 , 0x0000002B , (u3_match_reg>>16)&0xFFFF );
        SendWrite( FLIC::targetFpgaU1 , 0x0000002C , (u4_match_reg>>0)&0xFFFF );
        SendWrite( FLIC::targetFpgaU1 , 0x0000002D , (u4_match_reg>>16)&0xFFFF );
        SendWrite( FLIC::targetFpgaU2 , 0x0000002A , (u3_match_reg>>0)&0xFFFF );
        SendWrite( FLIC::targetFpgaU2 , 0x0000002B , (u3_match_reg>>16)&0xFFFF );
        SendWrite( FLIC::targetFpgaU2 , 0x0000002C , (u4_match_reg>>0)&0xFFFF );
        SendWrite( FLIC::targetFpgaU2 , 0x0000002D , (u4_match_reg>>16)&0xFFFF );

        // MBO: Register 0x6a contain the 8 channel processing enables bits 7:0 , and the master processing enabled bit 8.
        // This also sets which channels are expected to report fragments.
        // Take U4's Tagged Event Receivers out of reset.
	// 0x31FF ==> U1+U2-all channels
	// 0x31FF ==> U1+U2-all channels
	

	// PW on Feb 20th, 2017: Register 0x6a
	// bit 15: If '1' => disable (enable) even on Eth1 (Eth2)
	// bit 14: If '1' => disable (enable) odd on Eth1 (Eth2)
	// bit 13: If '1' => enable packet generator on Eth1
	// bit 12: If '1' => enable packet generator on Eth2
	// bit 11-9: reserved
	// bit 8: master enable
	// bit 7-4: enable U2 channels
	// bit 3-0: enable U1 channels

        //        SendSet( FLIC::targetFpgaU3 , 0x0000006A , 0xE100+u3_enable ); // 0xE1FF ==> U1+U2-all channels
        //        SendSet( FLIC::targetFpgaU4 , 0x0000006A , 0xE100+u4_enable ); // 0xE1FF ==> U1+U2-all channels

	//        SendSet( FLIC::targetFpgaU3 , 0x0000006A , 0x110f ); // 0x110f ==> U1->U3-all channels
	//        SendSet( FLIC::targetFpgaU4 , 0x0000006A , 0x110f ); // 0x110f ==> U1->U4-all channels
        
        SendSet( FLIC::targetFpgaU3 , 0x0000006A , 0x1101 ); // 0x110f ==> U1->U3-enable 1 channel
	SendSet( FLIC::targetFpgaU4 , 0x0000006A , 0x1101 ); // 0x110f ==> U1->U4-enable 1 channel
        
	return 0;
	
      }

    };

  } // closing ftk namespace
} // closing daq namespace

#endif
