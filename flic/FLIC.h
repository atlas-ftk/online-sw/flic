#ifndef FLIC_H
#define FLIC_H

#include <string>
#include <vector>

namespace daq {
  namespace ftk {

    namespace FLIC {

      union UDP_Data {
        char   byte[512];
        ushort word[256];
        uint   dword[128];
      };

      struct UDP_Header {
        ushort  target;
        ushort  command;
        uint    address;
        ushort  size;
        ushort  status;
      };

      struct UDP_Packet {
        UDP_Header      header;
        UDP_Data        data;
      };
      
      enum targetConstants {
        targetFpgaU1	   = 0,
        targetFpgaU2	   = 1,
        targetFpgaU3	   = 2,
        targetFpgaU4	   = 3,
        targetFlash1	   = 4,
        targetFlash2	   = 5,
        targetFlash3	   = 6,
        targetFlash4	   = 7,
        targetSram1Fpga1   = 8,
        targetSram2Fpga1   = 9,
        targetSram3Fpga1   = 10,
        targetSram4Fpga1   = 11,
        targetSram1Fpga2   = 12,
        targetSram2Fpga2   = 13,
        targetSram3Fpga2   = 14,
        targetSram4Fpga2   = 15,
        targetSramALLFpga1 = 16,
        targetSramALLFpga2 = 17,
        targetMgmtFpga	   = 18,
        targetPic	   = 19,
        targetDDRFpga1	   = 20,
        targetDDRFpga2	   = 21,
        targetDDRFpga3	   = 22,
        targetDDRFpga4	   = 23,
        numTargets	   = 24,
        targetNone	   = -1
      };
      
      enum regionConstants {
        regionFpga    = 0,
        regionUnused1 = 1,
        regionSram    = 2,
        regionReg1    = 3,
        regionUnused2 = 4,
        regionReg2    = 5,
        regionUnused3 = 6,
        numRegions    = 7,
	regionSram1   = 8, // just the first half of the SRAM region
	regionSram2   = 9  // just the second half of the SRAM region
      };
      
      enum commandConstants {
        // Basic Commands
        cmdRead = 0,
        cmdWrite,
        cmdSet,
        cmdClear,
        cmdErase,
        // Array Commands
        cmdArrayRead,
        cmdArrayWrite,
        cmdArraySet,
        cmdArrayClear,
        cmdArrayErase,
        // Fifo Commands
        cmdFifoRead,
        cmdFifoWrite,
	// Request that PIC perform a voltage/current scan of the board and update the ADC data memory block
        cmdAdcScan,  
	// Request that PIC perform a single ADC conversion.  Requires argument byte to select channel.  Updates just that value in the ADC data memory block.
        cmdAdcSingle 
      };
      
      enum statusConstants {
        statusNoError      = 0,
        statusTargetError  = 1,
        statusCommandError = 2,
        statusAddressError = 3,
        statusSizeError    = 4
      };
      
      enum busyConstants {
        busyOff = 0,
        busyOn  = 1
      };
      
      namespace PIC{
        enum picRegConstants {
          picReg_Status = 0,
          picReg_Error,
          picReg_Command,
          picReg_Version,
          picReg_TxPort,
          picReg_RxPort,
          picReg_Ip_0,
          picReg_Ip_1,
          picReg_ErrorADC_0,
          picReg_ErrorADC_1,
          picReg_ErrorADCP5_0,
          picReg_ErrorADCP5_1,
          picReg_ErrorADCM5_0,
          picReg_ErrorADCM5_1,
          picReg_ErrorADCP10_0,
          picReg_ErrorADCP10_1,
          picReg_ErrorADCM10_0,
          picReg_ErrorADCM10_1,
          picReg_ADC0,
          picReg_ADC1,
          picReg_ADC2,
          picReg_ADC3,
          picReg_ADC4,
          picReg_ADC5,
          picReg_ADC6,
          picReg_ADC7,
          picReg_ADC8,
          picReg_ADC9,
          picReg_ADC10,
          picReg_ADC11,
          picReg_ADC12,
          picReg_ADC13,
          picReg_ADC14,
          picReg_ADC15,
          picReg_ADC16,
          picReg_ADC17,
          picReg_ADC18,
          picReg_ADC19,
          picReg_ADC20,
          picReg_ADC21,
          picReg_ADC22,
          picReg_ADC23,
          picReg_ADC24,
          picReg_ADC25,
          picReg_ADC26,
          picReg_ADC27,
          picReg_ADC28,
          picReg_ADC29,
          picReg_ADC30,
          picReg_ADC31,
          numPicRegs
        };
      }
      
      namespace AUX{

        enum auxStatusRegConstants {
          auxStatusReg_PCBRev = 0x0100,
          auxStatusReg_CodeRev,
          auxStatusReg_CodeDateYYYY,
          auxStatusReg_CodeDataMMMDD,
          auxStatusReg_SerialNumber,
          auxStatusReg_SlotNumber,
          auxStatusReg_Reserved0,
          auxStatusReg_Reserved1,
          auxStatusReg_FpgaU1,
          auxStatusReg_FpgaU2,
          auxStatusReg_FpgaU3,
          auxStatusReg_FpgaU4,
          auxStatusReg_Setup,
          auxStatusReg_Unused0,
          auxStatusReg_Unused1,
          auxStatusReg_Unused2,
          numAuxStatusRegs
        };
	
        enum auxControlRegConstants {
          auxControlReg_Control0 = 0x0000,
          auxControlReg_Control1,
          auxControlReg_FlashAddress,
          auxControlReg_FpgaAddress,
          auxControlReg_Unused0,
          auxControlReg_Unused1,
          auxControlReg_Unused2,
          auxControlReg_Unused3,
          auxControlReg_Unused4,
          auxControlReg_Unused5,
          auxControlReg_Unused6,
          auxControlReg_Unused7,
          auxControlReg_Setup0,
          auxControlReg_Setup1,
          auxControlReg_Setup2,
          auxControlReg_Setup3,
          auxControlReg_Setup4,
          auxControlReg_Setup5,
          auxControlReg_Setup6,
          auxControlReg_Setup7,
          auxControlReg_Setup8,
          auxControlReg_Setup9,
          auxControlReg_Setup10,
          auxControlReg_Setup11,
          auxControlReg_Setup12,
          auxControlReg_Setup13,
          auxControlReg_Setup14,
          auxControlReg_Setup15,
          auxControlReg_Setup16,
          auxControlReg_Setup17,
          auxControlReg_Setup18,
          auxControlReg_Setup19,
          numAuxControlRegs
        };
	
        enum auxPulseRegConstants {
          auxPulseReg_Pulse0 = 0x0200,
          auxPulseReg_Pulse1,
          auxPulseReg_Pulse2,
          auxPulseReg_Pulse3,
          numAuxPulseRegs
        };

      }

      
      namespace U1{
        enum U1RegNames {
          U1ControlReg_First = 0,
          U1R_Clock = U1ControlReg_First,                       // 0x0000
          U1R_Led,                                                              // 0x0001
          U1R_General,                                                  // 0x0002
          U1R_Slink,                                                            // 0x0003
          U1R_SfpCtrl,                                                  // 0x0004
          U1R_Prbs0,                                                            // 0x0005
          U1R_Prbs1,                                                            // 0x0006
          U1R_Prbs2,                                                            // 0x0007
          U1R_Prbs3,                                                            // 0x0008
          U1R_RamTestStartAddress,                              // 0x0009
          U1R_RamTestStopAddress,                                       // 0x000A
          U1R_IlaMux,                                                           // 0x000B
          U1R_TxSeed0,                                                  // 0x000C
          U1R_TxSeed1,                                                  // 0x000D
          U1R_TxSeed2,                                                  // 0x000E
          U1R_TxSeed3,                                                  // 0x000F
          U1R_SfpFifoProgEmptyThreshold,                        // 0x0010
          U1R_SfpFifoProgFullThreshold,                 // 0x0011
          U1R_RtmFifoProgEmptyThreshold,                        // 0x0012
          U1R_RtmFifoProgFullThreshold,                 // 0x0013
          U1R_IlaTagMatchVal,                                           // 0x0014
          U1R_FlicStatus1,                                              // 0x0015
          U1R_FlicStatus2,                                              // 0x0016
          U1R_017,                                                              // 0x0017
          U1R_018,                                                              // 0x0018
          U1R_019,                                                              // 0x0019
          U1R_01A,                                                              // 0x001A
          U1R_01B,                                                              // 0x001B
          U1R_01C,                                                              // 0x001C
          U1R_01D,                                                              // 0x001D
          U1R_01E,                                                              // 0x001E
          U1R_HeldReset,                                                        // 0x001F
          U1ControlReg_Last = U1R_HeldReset,

          U1StatusReg_First,
          U1R_CodeRevision = U1StatusReg_First, // 0x0100
          U1R_CodeDateYYYY,                                             // 0x0101
          U1R_CodeDateMMDD,                                             // 0x0102
          U1R_SfpStatus,                                                        // 0x0103
          U1R_RtmStatus,                                                        // 0x0104
          U1R_CCMUX,                                                            // 0x0105
          U1R_SanityCounter1,                                           // 0x0106
          U1R_SanityCounter2,                                           // 0x0107
          U1R_SanityCounter3,                                           // 0x0108
          U1R_SanityCounter4,                                           // 0x0109
          U1R_InternalTimestamp,                                        // 0x010A
          U1R_Gtx112RxErrCount,                                 // 0x010B
          U1R_Gtx113RxErrCount,                                 // 0x010C
          U1R_Gtx114RxErrCount,                                 // 0x010D
          U1R_Gtx115RxErrCount,                                 // 0x010E
          U1R_Gtx116RxErrCount,                                 // 0x010F
          U1StatusReg_Last = U1R_Gtx116RxErrCount,

          U1PulseReg_First,
          U1R_Gtx112Reset = U1PulseReg_First,           // 0x0200
          U1R_Gtx112Pulse,                                              // 0x0201
          U1R_Gtx113Reset,                                              // 0x0202
          U1R_Gtx113Pulse,                                              // 0x0203
          U1R_Gtx114Reset,                                              // 0x0204
          U1R_Gtx114Pulse,                                              // 0x0205
          U1R_Gtx115Reset,                                              // 0x0206
          U1R_Gtx115Pulse,                                              // 0x0207
          U1R_Gtx116Reset,                                              // 0x0208
          U1R_Gtx116Pulse,                                              // 0x0209
          U1R_Pulse20A,                                                 // 0x020A
          U1R_Pulse20B,                                                 // 0x020B
          U1R_Pulse20C,                                                 // 0x020C
          U1R_Pulse20D,                                                 // 0x020D
          U1R_Pulse20E,                                                 // 0x020E
          U1R_PulseReset,                                                       // 0x020F
          U1PulseReg_Last = U1R_PulseReset,

          U1ErrorReg,

          numU1Regs,
          numU1ControlRegs = U1ControlReg_Last - U1ControlReg_First + 1,
          numU1StatusRegs  = U1StatusReg_Last  - U1StatusReg_First  + 1,
          numU1PulseRegs   = U1PulseReg_Last   - U1PulseReg_First   + 1
        };
	
        enum U1Parameters {
          U1P_Null = 0,

          U1P_Clock_Enable,
          U1P_Clock_Reset,
          U1P_Clock_OutputSelect,
          U1P_Clock_OutputDivider,
          U1P_Clock_Prescaler,

          U1P_Led_Led4,
          U1P_Led_Led5,

          U1P_Gen_ClockControlEnable,
          U1P_Gen_EnableCoreReceiver,
          U1P_Gen_ForceSlinkUp,
          U1P_Gen_SramAccessControl,
          U1P_Gen_EnableMergeMachine,
          U1P_Gen_Gtx112_SMResetRx,
          U1P_Gen_Gtx112_SMResetTx,
          U1P_Gen_Gtx112_SMResetRxTx,
          U1P_Gen_Gtx113_SMResetRx,
          U1P_Gen_Gtx113_SMResetTx,
          U1P_Gen_Gtx113_SMResetRxTx,
          U1P_Gen_Gtx114_SMResetRx,
          U1P_Gen_Gtx114_SMResetTx,
          U1P_Gen_Gtx114_SMResetRxTx,
          U1P_Gen_Gtx115_SMResetRx,
          U1P_Gen_Gtx115_SMResetTx,
          U1P_Gen_Gtx115_SMResetRxTx,
          U1P_Gen_Gtx116_SMResetRx,
          U1P_Gen_Gtx116_SMResetTx,
          U1P_Gen_Gtx116_SMResetRxTx,
          U1P_Gen_SMResetMode,

          U1P_Slink_RTM_TDIS_0,
          U1P_Slink_RTM_TDIS_1,
          U1P_Slink_RTM_TDIS_2,
          U1P_Slink_RTM_TDIS_3,
          U1P_Slink_RTM_TDIS_All,
          U1P_Slink_TXMuxSelect_0,
          U1P_Slink_TXMuxSelect_1,
          U1P_Slink_TXMuxSelect_2,
          U1P_Slink_TXMuxSelect_3,
          U1P_Slink_TXMuxSelect_All,
          U1P_Slink_ResetGo_0,
          U1P_Slink_ResetGo_1,
          U1P_Slink_ResetGo_2,
          U1P_Slink_ResetGo_3,
          U1P_Slink_ResetGo_All,
          U1P_Slink_TestEnable_0,
          U1P_Slink_TestEnable_1,
          U1P_Slink_TestEnable_2,
          U1P_Slink_TestEnable_3,
          U1P_Slink_TestEnable_All,

          U1P_SfpCtrl_TDIS_0,
          U1P_SfpCtrl_TDIS_1,
          U1P_SfpCtrl_TDIS_2,
          U1P_SfpCtrl_TDIS_3,
          U1P_SfpCtrl_TDIS_All,

          U1P_Prbs_CommasAfterReset,
          U1P_Prbs_WordsBeforeCommas,
          U1P_Prbs_CommasInBreaks,
          U1P_Prbs_PatternLength,

          U1P_RamTestStartAddress,
          U1P_RamTestStopAddress,

          U1P_IlaMux_Gtx112,
          U1P_IlaMux_Gtx113,
          U1P_IlaMux_Gtx114,
          U1P_IlaMux_Gtx115,
          U1P_IlaMux_Gtx116,
          U1P_IlaMux_DDR,

          U1P_FlicStatus1,
          U1P_FlicStatus2,

          U1P_HeldReset0,
          U1P_HeldReset1,
          U1P_HeldReset2,
          U1P_HeldReset3,
          U1P_HeldReset4,
          U1P_HeldReset5,
          U1P_HeldReset6,
          U1P_HeldReset7,
          U1P_HeldReset8,
          U1P_HeldReset9,
          U1P_HeldReset10,
          U1P_HeldReset11,
          U1P_HeldReset12,
          U1P_HeldReset13,
          U1P_HeldReset14,
          U1P_HeldReset15,
          U1P_HeldResetAll,

          U1P_Gtx112Reset_Serdes0_TxPll,
          U1P_Gtx112Reset_Serdes1_TxPll,
          U1P_Gtx112Reset_Serdes2_TxPll,
          U1P_Gtx112Reset_Serdes3_TxPll,
          U1P_Gtx112Reset_Serdes0_Tx,
          U1P_Gtx112Reset_Serdes1_Tx,
          U1P_Gtx112Reset_Serdes2_Tx,
          U1P_Gtx112Reset_Serdes3_Tx,
          U1P_Gtx112Reset_Serdes0_RxPll,
          U1P_Gtx112Reset_Serdes1_RxPll,
          U1P_Gtx112Reset_Serdes2_RxPll,
          U1P_Gtx112Reset_Serdes3_RxPll,
          U1P_Gtx112Reset_Serdes0_Rx,
          U1P_Gtx112Reset_Serdes1_Rx,
          U1P_Gtx112Reset_Serdes2_Rx,
          U1P_Gtx112Reset_Serdes3_Rx,
          U1P_Gtx112Reset_Serdes0_All,
          U1P_Gtx112Reset_Serdes1_All,
          U1P_Gtx112Reset_Serdes2_All,
          U1P_Gtx112Reset_Serdes3_All,

          U1P_Gtx112Pulse_FGReset_0,
          U1P_Gtx112Pulse_FGReset_1,
          U1P_Gtx112Pulse_FGReset_2,
          U1P_Gtx112Pulse_FGReset_3,
          U1P_Gtx112Pulse_FGReset_All,
          U1P_Gtx112Pulse_FRReset_0,
          U1P_Gtx112Pulse_FRReset_1,
          U1P_Gtx112Pulse_FRReset_2,
          U1P_Gtx112Pulse_FRReset_3,
          U1P_Gtx112Pulse_FRReset_All,
          U1P_Gtx112Pulse_FReset_All,
          U1P_Gtx112Pulse_IlaTag_0,
          U1P_Gtx112Pulse_IlaTag_1,
          U1P_Gtx112Pulse_IlaTag_2,
          U1P_Gtx112Pulse_IlaTag_3,
          U1P_Gtx112Pulse_IlaTag_All,

          U1P_Gtx113Reset_Serdes0_TxPll,
          U1P_Gtx113Reset_Serdes1_TxPll,
          U1P_Gtx113Reset_Serdes2_TxPll,
          U1P_Gtx113Reset_Serdes3_TxPll,
          U1P_Gtx113Reset_Serdes0_Tx,
          U1P_Gtx113Reset_Serdes1_Tx,
          U1P_Gtx113Reset_Serdes2_Tx,
          U1P_Gtx113Reset_Serdes3_Tx,
          U1P_Gtx113Reset_Serdes0_RxPll,
          U1P_Gtx113Reset_Serdes1_RxPll,
          U1P_Gtx113Reset_Serdes2_RxPll,
          U1P_Gtx113Reset_Serdes3_RxPll,
          U1P_Gtx113Reset_Serdes0_Rx,
          U1P_Gtx113Reset_Serdes1_Rx,
          U1P_Gtx113Reset_Serdes2_Rx,
          U1P_Gtx113Reset_Serdes3_Rx,
          U1P_Gtx113Reset_Serdes0_All,
          U1P_Gtx113Reset_Serdes1_All,
          U1P_Gtx113Reset_Serdes2_All,
          U1P_Gtx113Reset_Serdes3_All,

          U1P_Gtx114Reset_Serdes0_TxPll,
          U1P_Gtx114Reset_Serdes1_TxPll,
          U1P_Gtx114Reset_Serdes2_TxPll,
          U1P_Gtx114Reset_Serdes3_TxPll,
          U1P_Gtx114Reset_Serdes0_Tx,
          U1P_Gtx114Reset_Serdes1_Tx,
          U1P_Gtx114Reset_Serdes2_Tx,
          U1P_Gtx114Reset_Serdes3_Tx,
          U1P_Gtx114Reset_Serdes0_RxPll,
          U1P_Gtx114Reset_Serdes1_RxPll,
          U1P_Gtx114Reset_Serdes2_RxPll,
          U1P_Gtx114Reset_Serdes3_RxPll,
          U1P_Gtx114Reset_Serdes0_Rx,
          U1P_Gtx114Reset_Serdes1_Rx,
          U1P_Gtx114Reset_Serdes2_Rx,
          U1P_Gtx114Reset_Serdes3_Rx,
          U1P_Gtx114Reset_Serdes0_All,
          U1P_Gtx114Reset_Serdes1_All,
          U1P_Gtx114Reset_Serdes2_All,
          U1P_Gtx114Reset_Serdes3_All,

          U1P_Gtx115Reset_Serdes0_TxPll,
          U1P_Gtx115Reset_Serdes1_TxPll,
          U1P_Gtx115Reset_Serdes2_TxPll,
          U1P_Gtx115Reset_Serdes3_TxPll,
          U1P_Gtx115Reset_Serdes0_Tx,
          U1P_Gtx115Reset_Serdes1_Tx,
          U1P_Gtx115Reset_Serdes2_Tx,
          U1P_Gtx115Reset_Serdes3_Tx,
          U1P_Gtx115Reset_Serdes0_RxPll,
          U1P_Gtx115Reset_Serdes1_RxPll,
          U1P_Gtx115Reset_Serdes2_RxPll,
          U1P_Gtx115Reset_Serdes3_RxPll,
          U1P_Gtx115Reset_Serdes0_Rx,
          U1P_Gtx115Reset_Serdes1_Rx,
          U1P_Gtx115Reset_Serdes2_Rx,
          U1P_Gtx115Reset_Serdes3_Rx,
          U1P_Gtx115Reset_Serdes0_All,
          U1P_Gtx115Reset_Serdes1_All,
          U1P_Gtx115Reset_Serdes2_All,
          U1P_Gtx115Reset_Serdes3_All,

          U1P_Gtx116Reset_Serdes0_TxPll,
          U1P_Gtx116Reset_Serdes1_TxPll,
          U1P_Gtx116Reset_Serdes2_TxPll,
          U1P_Gtx116Reset_Serdes3_TxPll,
          U1P_Gtx116Reset_Serdes0_Tx,
          U1P_Gtx116Reset_Serdes1_Tx,
          U1P_Gtx116Reset_Serdes2_Tx,
          U1P_Gtx116Reset_Serdes3_Tx,
          U1P_Gtx116Reset_Serdes0_RxPll,
          U1P_Gtx116Reset_Serdes1_RxPll,
          U1P_Gtx116Reset_Serdes2_RxPll,
          U1P_Gtx116Reset_Serdes3_RxPll,
          U1P_Gtx116Reset_Serdes0_Rx,
          U1P_Gtx116Reset_Serdes1_Rx,
          U1P_Gtx116Reset_Serdes2_Rx,
          U1P_Gtx116Reset_Serdes3_Rx,
          U1P_Gtx116Reset_Serdes0_All,
          U1P_Gtx116Reset_Serdes1_All,
          U1P_Gtx116Reset_Serdes2_All,
          U1P_Gtx116Reset_Serdes3_All,

          U1P_Gtx116Pulse_FGReset_0,
          U1P_Gtx116Pulse_FGReset_1,
          U1P_Gtx116Pulse_FGReset_2,
          U1P_Gtx116Pulse_FGReset_3,
          U1P_Gtx116Pulse_FGReset_All,
          U1P_Gtx116Pulse_FRReset_0,
          U1P_Gtx116Pulse_FRReset_1,
          U1P_Gtx116Pulse_FRReset_2,
          U1P_Gtx116Pulse_FRReset_3,
          U1P_Gtx116Pulse_FRReset_All,
          U1P_Gtx116Pulse_FReset_All,
          U1P_Gtx116Pulse_UserReset_0,
          U1P_Gtx116Pulse_UserReset_1,
          U1P_Gtx116Pulse_UserReset_2,
          U1P_Gtx116Pulse_UserReset_3,
          U1P_Gtx116Pulse_UserReset_All,

          U1P_Pulse20E_ClkGenReset,

          U1P_PulseReset0,
          U1P_PulseReset1,
          U1P_PulseReset2,
          U1P_PulseReset3,
          U1P_PulseReset4,
          U1P_PulseReset5,
          U1P_PulseReset6,
          U1P_PulseReset7,
          U1P_PulseReset8,
          U1P_PulseReset9,
          U1P_PulseReset10,
          U1P_PulseReset11,
          U1P_PulseReset12,
          U1P_PulseReset13,
          U1P_PulseReset14,
          U1P_PulseReset15,
          U1P_PulseResetAll,

          numU1Parameters
        };
      }
      
      namespace U2{
        enum U2RegNames {
          U2ControlReg_First = 0,
          U2R_Clock = U2ControlReg_First,                       // 0x0000
          U2R_Led,                                                              // 0x0001
          U2R_General,                                                  // 0x0002
          U2R_Slink,                                                            // 0x0003
          U2R_SfpCtrl,                                                  // 0x0004
          U2R_Prbs0,                                                            // 0x0005
          U2R_Prbs1,                                                            // 0x0006
          U2R_Prbs2,                                                            // 0x0007
          U2R_Prbs3,                                                            // 0x0008
          U2R_RamTestStartAddress,                              // 0x0009
          U2R_RamTestStopAddress,                                       // 0x000A
          U2R_IlaMux,                                                           // 0x000B
          U2R_TxSeed0,                                                  // 0x000C
          U2R_TxSeed1,                                                  // 0x000D
          U2R_TxSeed2,                                                  // 0x000E
          U2R_TxSeed3,                                                  // 0x000F
          U2R_SfpFifoProgEmptyThreshold,                        // 0x0010
          U2R_SfpFifoProgFullThreshold,                 // 0x0011
          U2R_RtmFifoProgEmptyThreshold,                        // 0x0012
          U2R_RtmFifoProgFullThreshold,                 // 0x0013
          U2R_NumRecords0,                                              // 0x0014
          U2R_NumRecords1,                                              // 0x0015
          U2R_NumRecords2,                                              // 0x0016
          U2R_NumRecords3,                                              // 0x0017
          U2R_RecordDelay0,                                             // 0x0018
          U2R_RecordDelay1,                                             // 0x0019
          U2R_RecordDelay2,                                             // 0x001A
          U2R_RecordDelay3,                                             // 0x001B
          U2R_Gtx112MonFifoCtrl,                                        // 0x001C
          U2R_CoreCrateStatus,                                  // 0x001D
          U2R_CoreCrateError,                                           // 0x001E
          U2R_CoreCrateCtrl,                                            // 0x001F
          U2ControlReg_Last = U2R_CoreCrateCtrl,

          U2StatusReg_First,
          U2R_CodeRevision = U2StatusReg_First, // 0x0100
          U2R_CodeDateYYYY,                                             // 0x0101
          U2R_CodeDateMMDD,                                             // 0x0102
          U2R_SfpStatus,                                                        // 0x0103
          U2R_RtmStatus,                                                        // 0x0104
          U2R_UserRunNumber0,                                           // 0x0105
          U2R_UserRunNumber1,                                           // 0x0106
          U2R_UserRunNumber2,                                           // 0x0107
          U2R_UserRunNumber3,                                           // 0x0108
          U2R_UserLevel1ID0,                                            // 0x0109
          U2R_UserLevel1ID1,                                            // 0x010A
          U2R_UserLevel1ID2,                                            // 0x010B
          U2R_UserLevel1ID3,                                            // 0x010C
          U2R_Gtx112Status,                                             // 0x010D
          U2R_Gtx116RxErrCntMux,                                        // 0x010E
          U2R_10F,                                                              // 0x010F
          U2StatusReg_Last = U2R_10F,

          U2PulseReg_First,
          U2R_Gtx112Reset = U2PulseReg_First,           // 0x0200
          U2R_Gtx112Pulse,                                              // 0x0201
          U2R_Gtx113Reset,                                              // 0x0202
          U2R_Gtx113Pulse,                                              // 0x0203
          U2R_Gtx114Reset,                                              // 0x0204
          U2R_Gtx114Pulse,                                              // 0x0205
          U2R_Gtx115Reset,                                              // 0x0206
          U2R_Gtx115Pulse,                                              // 0x0207
          U2R_Gtx116Reset,                                              // 0x0208
          U2R_Gtx116Pulse,                                              // 0x0209
          U2R_Pulse20A,                                                 // 0x020A
          U2R_Pulse20B,                                                 // 0x020B
          U2R_Pulse20C,                                                 // 0x020C
          U2R_Pulse20D,                                                 // 0x020D
          U2R_Pulse20E,                                                 // 0x020E
          U2R_Pulse20F,                                                 // 0x020F
          U2PulseReg_Last = U2R_Pulse20F,

          U2ErrorReg,

          numU2Regs,
          numU2ControlRegs = U2ControlReg_Last - U2ControlReg_First + 1,
          numU2StatusRegs  = U2StatusReg_Last  - U2StatusReg_First  + 1,
          numU2PulseRegs   = U2PulseReg_Last   - U2PulseReg_First   + 1
        };
        enum U2Parameters {
          U2P_Null = 0,

          U2P_Led_Led6,
          U2P_Led_Led7,

          U2P_Gen_Gtx112_SMResetRx,
          U2P_Gen_Gtx112_SMResetTx,
          U2P_Gen_Gtx112_SMResetRxTx,
          U2P_Gen_Gtx113_SMResetRx,
          U2P_Gen_Gtx113_SMResetTx,
          U2P_Gen_Gtx113_SMResetRxTx,
          U2P_Gen_Gtx114_SMResetRx,
          U2P_Gen_Gtx114_SMResetTx,
          U2P_Gen_Gtx114_SMResetRxTx,
          U2P_Gen_Gtx115_SMResetRx,
          U2P_Gen_Gtx115_SMResetTx,
          U2P_Gen_Gtx115_SMResetRxTx,
          U2P_Gen_Gtx116_SMResetRx,
          U2P_Gen_Gtx116_SMResetTx,
          U2P_Gen_Gtx116_SMResetRxTx,
          U2P_Gen_SMResetMode,

          U2P_Slink_RTM_TDIS_0,
          U2P_Slink_RTM_TDIS_1,
          U2P_Slink_RTM_TDIS_2,
          U2P_Slink_RTM_TDIS_3,
          U2P_Slink_RTM_TDIS_All,

          U2P_SfpCtrl_TDIS_0,
          U2P_SfpCtrl_TDIS_1,
          U2P_SfpCtrl_TDIS_2,
          U2P_SfpCtrl_TDIS_3,
          U2P_SfpCtrl_TDIS_All,

          U2P_IlaMux_Gtx112,
          U2P_IlaMux_Gtx113,
          U2P_IlaMux_Gtx114,
          U2P_IlaMux_Gtx115,
          U2P_IlaMux_Gtx116,

          U2P_TxSeed0,
          U2P_TxSeed1,
          U2P_TxSeed2,
          U2P_TxSeed3,

          U2P_NumRecords0,
          U2P_NumRecords1,
          U2P_NumRecords2,
          U2P_NumRecords3,

          U2P_RecordDelay_Val0,
          U2P_RecordDelay_Val1,
          U2P_RecordDelay_Val2,
          U2P_RecordDelay_Val3,
          U2P_RecordDelay_Rnd0,
          U2P_RecordDelay_Rnd1,
          U2P_RecordDelay_Rnd2,
          U2P_RecordDelay_Rnd3,

          U2P_Gtx112MonFifoCtrl,
          U2P_CoreCrateStatus,
          U2P_CoreCrateError,
          U2P_CoreCrateCtrl,

          U2P_Gtx112Reset_Serdes0_TxPll,
          U2P_Gtx112Reset_Serdes1_TxPll,
          U2P_Gtx112Reset_Serdes2_TxPll,
          U2P_Gtx112Reset_Serdes3_TxPll,
          U2P_Gtx112Reset_Serdes0_Tx,
          U2P_Gtx112Reset_Serdes1_Tx,
          U2P_Gtx112Reset_Serdes2_Tx,
          U2P_Gtx112Reset_Serdes3_Tx,
          U2P_Gtx112Reset_Serdes0_RxPll,
          U2P_Gtx112Reset_Serdes1_RxPll,
          U2P_Gtx112Reset_Serdes2_RxPll,
          U2P_Gtx112Reset_Serdes3_RxPll,
          U2P_Gtx112Reset_Serdes0_Rx,
          U2P_Gtx112Reset_Serdes1_Rx,
          U2P_Gtx112Reset_Serdes2_Rx,
          U2P_Gtx112Reset_Serdes3_Rx,
          U2P_Gtx112Reset_Serdes0_All,
          U2P_Gtx112Reset_Serdes1_All,
          U2P_Gtx112Reset_Serdes2_All,
          U2P_Gtx112Reset_Serdes3_All,

          U2P_Gtx112Pulse_FGReset_0,
          U2P_Gtx112Pulse_FGReset_1,
          U2P_Gtx112Pulse_FGReset_2,
          U2P_Gtx112Pulse_FGReset_3,
          U2P_Gtx112Pulse_FGReset_All,
          U2P_Gtx112Pulse_FRReset_0,
          U2P_Gtx112Pulse_FRReset_1,
          U2P_Gtx112Pulse_FRReset_2,
          U2P_Gtx112Pulse_FRReset_3,
          U2P_Gtx112Pulse_FRReset_All,
          U2P_Gtx112Pulse_FReset_All,
          U2P_Gtx112Pulse_EnFIFO_0,
          U2P_Gtx112Pulse_EnFIFO_1,
          U2P_Gtx112Pulse_EnFIFO_2,
          U2P_Gtx112Pulse_EnFIFO_3,
          U2P_Gtx112Pulse_EnFIFO_All,
          U2P_Gtx112Pulse_EnSSB_0,
          U2P_Gtx112Pulse_EnSSB_1,
          U2P_Gtx112Pulse_EnSSB_2,
          U2P_Gtx112Pulse_EnSSB_3,
          U2P_Gtx112Pulse_EnSSB_All,

          U2P_Gtx113Reset_Serdes0_TxPll,
          U2P_Gtx113Reset_Serdes1_TxPll,
          U2P_Gtx113Reset_Serdes2_TxPll,
          U2P_Gtx113Reset_Serdes3_TxPll,
          U2P_Gtx113Reset_Serdes0_Tx,
          U2P_Gtx113Reset_Serdes1_Tx,
          U2P_Gtx113Reset_Serdes2_Tx,
          U2P_Gtx113Reset_Serdes3_Tx,
          U2P_Gtx113Reset_Serdes0_RxPll,
          U2P_Gtx113Reset_Serdes1_RxPll,
          U2P_Gtx113Reset_Serdes2_RxPll,
          U2P_Gtx113Reset_Serdes3_RxPll,
          U2P_Gtx113Reset_Serdes0_Rx,
          U2P_Gtx113Reset_Serdes1_Rx,
          U2P_Gtx113Reset_Serdes2_Rx,
          U2P_Gtx113Reset_Serdes3_Rx,
          U2P_Gtx113Reset_Serdes0_All,
          U2P_Gtx113Reset_Serdes1_All,
          U2P_Gtx113Reset_Serdes2_All,
          U2P_Gtx113Reset_Serdes3_All,

          U2P_Gtx114Reset_Serdes0_TxPll,
          U2P_Gtx114Reset_Serdes1_TxPll,
          U2P_Gtx114Reset_Serdes2_TxPll,
          U2P_Gtx114Reset_Serdes3_TxPll,
          U2P_Gtx114Reset_Serdes0_Tx,
          U2P_Gtx114Reset_Serdes1_Tx,
          U2P_Gtx114Reset_Serdes2_Tx,
          U2P_Gtx114Reset_Serdes3_Tx,
          U2P_Gtx114Reset_Serdes0_RxPll,
          U2P_Gtx114Reset_Serdes1_RxPll,
          U2P_Gtx114Reset_Serdes2_RxPll,
          U2P_Gtx114Reset_Serdes3_RxPll,
          U2P_Gtx114Reset_Serdes0_Rx,
          U2P_Gtx114Reset_Serdes1_Rx,
          U2P_Gtx114Reset_Serdes2_Rx,
          U2P_Gtx114Reset_Serdes3_Rx,
          U2P_Gtx114Reset_Serdes0_All,
          U2P_Gtx114Reset_Serdes1_All,
          U2P_Gtx114Reset_Serdes2_All,
          U2P_Gtx114Reset_Serdes3_All,

          U2P_Gtx115Reset_Serdes0_TxPll,
          U2P_Gtx115Reset_Serdes1_TxPll,
          U2P_Gtx115Reset_Serdes2_TxPll,
          U2P_Gtx115Reset_Serdes3_TxPll,
          U2P_Gtx115Reset_Serdes0_Tx,
          U2P_Gtx115Reset_Serdes1_Tx,
          U2P_Gtx115Reset_Serdes2_Tx,
          U2P_Gtx115Reset_Serdes3_Tx,
          U2P_Gtx115Reset_Serdes0_RxPll,
          U2P_Gtx115Reset_Serdes1_RxPll,
          U2P_Gtx115Reset_Serdes2_RxPll,
          U2P_Gtx115Reset_Serdes3_RxPll,
          U2P_Gtx115Reset_Serdes0_Rx,
          U2P_Gtx115Reset_Serdes1_Rx,
          U2P_Gtx115Reset_Serdes2_Rx,
          U2P_Gtx115Reset_Serdes3_Rx,
          U2P_Gtx115Reset_Serdes0_All,
          U2P_Gtx115Reset_Serdes1_All,
          U2P_Gtx115Reset_Serdes2_All,
          U2P_Gtx115Reset_Serdes3_All,

          U2P_Gtx116Reset_Serdes0_TxPll,
          U2P_Gtx116Reset_Serdes1_TxPll,
          U2P_Gtx116Reset_Serdes2_TxPll,
          U2P_Gtx116Reset_Serdes3_TxPll,
          U2P_Gtx116Reset_Serdes0_Tx,
          U2P_Gtx116Reset_Serdes1_Tx,
          U2P_Gtx116Reset_Serdes2_Tx,
          U2P_Gtx116Reset_Serdes3_Tx,
          U2P_Gtx116Reset_Serdes0_RxPll,
          U2P_Gtx116Reset_Serdes1_RxPll,
          U2P_Gtx116Reset_Serdes2_RxPll,
          U2P_Gtx116Reset_Serdes3_RxPll,
          U2P_Gtx116Reset_Serdes0_Rx,
          U2P_Gtx116Reset_Serdes1_Rx,
          U2P_Gtx116Reset_Serdes2_Rx,
          U2P_Gtx116Reset_Serdes3_Rx,
          U2P_Gtx116Reset_Serdes0_All,
          U2P_Gtx116Reset_Serdes1_All,
          U2P_Gtx116Reset_Serdes2_All,
          U2P_Gtx116Reset_Serdes3_All,

          U2P_Gtx116Pulse_FGReset_0,
          U2P_Gtx116Pulse_FGReset_1,
          U2P_Gtx116Pulse_FGReset_2,
          U2P_Gtx116Pulse_FGReset_3,
          U2P_Gtx116Pulse_FGReset_All,
          U2P_Gtx116Pulse_FRReset_0,
          U2P_Gtx116Pulse_FRReset_1,
          U2P_Gtx116Pulse_FRReset_2,
          U2P_Gtx116Pulse_FRReset_3,
          U2P_Gtx116Pulse_FRReset_All,
          U2P_Gtx116Pulse_FReset_All,

          numU2Parameters
        };
      }
      
      namespace U3 {
        enum U3RegConstants {
          U3RegSfpClockControl = 0,
          U3RegLed,
          U3RegGeneralControl,
          U3RegEthernetClockControl,
          U3Reg004,
          U3Reg005,
          U3Reg006,
          U3Reg007,
          U3Reg008,
          U3Reg009,
          U3Reg00A,
          U3Reg00B,
          U3Reg00C,
          U3Reg00D,
          U3Reg00E,
          U3Reg00F,
          numU3Regs
        };
      }
      
      namespace U4 {}

      namespace Flash {

        struct Region {
          uint          start;
          uint          stop;
          uint          blocks;
        };

        const Region flash_region[] = {
          {0x00000000, 0x002A0000, 42}, // regionFpga           0
          {0x002A0000, 0x00300000,  6}, // regionUnused         1
          {0x00300000, 0x00700000, 64}, // regionSram           2
          {0x00700000, 0x00720000,  2}, // regionReg1           3
          {0x00720000, 0x00780000,  6}, // regionUnused         4
          {0x00780000, 0x007A0000,  2}, // regionReg2           5
          {0x007A0000, 0x00800000,  6}, // regionUnused         6
          {0x00000000, 0x00800000,128}, // regionAll            7
          {0x00300000, 0x00500000, 32}, // regionSram1          8 - just the first half of dedicated SRAM memory
	  {0x00500000, 0x00700000, 32}  // regionSram2          9 - just the second half of dedicated SRAM memory
        };
        const uint FLASH_BLOCK_SIZE = 0x20000;//Number of bytes per block
        const uint FLASH_BLOCK_WORDS = 0x10000;//Number of words per block

      }

      const std::string counter_names[] = {
	"SFP_FIFO_FULL_ERROR_COUNT" ,
	"NOT_IN_SYNC_COUNT" ,
	"HEADER_ERROR_COUNT" ,
	"TRACK_ERROR_COUNT" ,
	"TRAILER_ERROR_COUNT" ,
	"TRUNCATION_ERROR_COUNT" ,
	"CCMUX_FIFO_EVENT_END_COUNT" ,
	"SRAM_FIFO_EVENT_READ_COUNT" ,
	"MERGE_FIFO_EVENT_READ_COUNT" ,
	"U3_TAGGED_EVENT_FIFO_COUNT" ,
	"U4_TAGGED_EVENT_FIFO_COUNT" ,
	"RTM_FIFO_EVENT_END_COUNT" ,
	"FLIC_FLOW_CTL" ,
	"L1_ID_MATCH_U3_COUNT" ,
	"L1_ID_MATCH_U4_COUNT" ,
	"L1_SKIP_ERRORs"
      };

      const std::string global_status_names[] = {
	"SFP_FIFO_PROG_FULL" ,
	"CCMUX_FIFO_PROG_FULL" ,
	"SRAM_FIFO_PROG_FULL" ,
	"MERGE_FIFO_PROG_FULL" ,
	"RTM_FIFO_PROG_FULL" ,
	"COLLECTED_PIPELINE_PROG_FULL" ,
	"U3_TAGGED_EVENT_FIFO_PROG_FULL" ,
	"U4_TAGGED_EVENT_FIFO_PROG_FULL" ,
	"SLINK_LDOWN_FLAG" ,
	"SLINK_LFF_FLAG" ,
	"SLINK_XOFF_FLAG" ,
	"NOT_IN_SYNC" ,
	"SLINK_CTL2_REG(4)" ,
	"SLINK_CTL2_REG(8)" ,
	"RESERVED" ,
	"RESERVED"
      };

      namespace ListProc {

	// Define the different possible transfers
	// that can be added to the list processor
	const ushort ProcToU1 = 0x8001;
	const ushort ProcToU2 = 0x8002;
	const ushort SSBeToU1 = 0x8401;
	const ushort SSBeToU2 = 0x8402;
	const ushort U3	      = 0x8804; // only one firmware build for U3
	const ushort U4	      = 0x8C08; // only one firmware build for U4
	const ushort U1Sram1  = 0x8211;
	const ushort U1Sram2  = 0x9221;
	const ushort U1Sram3  = 0x8631;
	const ushort U1Sram4  = 0x9641;
	const ushort U2Sram1  = 0x8A12;
	const ushort U2Sram2  = 0x9A22;
	const ushort U2Sram3  = 0x8E32;
	const ushort U2Sram4  = 0x9E42;

	// Now define some list proc configurations that we may want to
	// use when testing
	const ushort Loopback_NoSRAMs[16]     = { ProcToU1 , SSBeToU2 , U3 , U4 };
	const ushort Loopback_Reverse_NoSRAMs[16] = { SSBeToU1 , ProcToU2 , U3 , U4 };
	const ushort Loopback_WithSRAMs[16]   = { ProcToU1 , SSBeToU2 , U3 , U4 ,
						  U1Sram1 , U1Sram2 , U1Sram3 , U1Sram4 };
	const ushort DoubleProc_NoSRAMs[16]   = { ProcToU1 , ProcToU2 , U3 , U4 };
	const ushort DoubleProc_WithSRAMs[16] = { ProcToU1 , ProcToU2 , U3 , U4 ,
						  U1Sram1 , U1Sram2 , U1Sram3 , U1Sram4 ,
						  U2Sram1 , U2Sram2 , U2Sram3 , U2Sram4 };
	const ushort DoubleSSBe[16]	      = { SSBeToU1 , SSBeToU2 , U3 , U4 };
	
      }

      
      namespace EventFormat {
	const ushort INPUT_L1ID_POSITION  = 6;
	const ushort OUTPUT_L1ID_POSITION = 10;
      }

      namespace Comm {
      
	struct Info {
	  bool isDefined;
	  std::string boardIP;
	  std::string port;
	  std::string hostIP;
	  std::string desc;
	  bool isBlade;
	  Info( std::string _boardIP , std::string _port , std::string _hostIP , std::string _desc , bool _isBlade )
	    : isDefined( true )
	    , boardIP( _boardIP )
	    , port( _port )
	    , hostIP( _hostIP )
	    , desc( _desc )
	    , isBlade( _isBlade )
	  {}
	  Info()
	    : isDefined( false )
	    , boardIP( "" )
	    , port( "" )
	    , hostIP( "" )
	    , desc( "" )
	    , isBlade( false )
	  {}
	};

	const Info boards[] = {
	  Info( "10.153.37.25" , "50000" , "10.153.37.17" , "P1 FLIC1     " , false ) ,
	  Info( "10.153.37.26" , "50001" , "10.153.37.18" , "P1 FLIC2     " , false ) ,
	  Info( "10.193.32.18" , "50000" , "10.193.32.16" , "Lab4 Spare   " , false ) ,
	  Info( "10.153.37.18" , "50000" , "10.153.37.17" , "Test Bench   " , false ) ,
	  Info( "10.153.37.16" , "50000" , "10.153.37.15" , "With IPMC    " , false ) ,
	  Info( "10.193.32.18" , "50000" , "10.193.32.17" , "Without IPMC " , false ) ,
	  Info( "10.153.37.17" , "50000" , "10.153.37.15" , "Wind Tunnel 3" , false ) ,
	  Info( "10.153.37.17" , "50001" , "10.153.37.15" , "Wind Tunnel 4" , false ) ,
	  Info( "10.145.64.24" , "50000" , ""             , "P1 Blade     " , true ) ,
	  Info( "192.168.1.2"  , "50000" , "192.168.1.1"  , "WT Blade Eth6" , true ) ,
	  Info( "192.168.2.2"  , "50000" , "192.168.2.1"  , "WT Blade Eth7" , true ) ,
	};

	const ushort nboards = 11; // total number of elements in list
	
      }
      
    } // Closing FLIC namespace
  } // Closing ftk namespace
} // Closing daq namespace

#endif
