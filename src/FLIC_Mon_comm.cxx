#include "flic/FLIC_Mon_comm.h"

using namespace daq::ftk;

FLIC_Mon_comm:: FLIC_Mon_comm(const char* srcipaddress, const int srcport, const char* dstipaddress, const int dstport) {


//int FLIC_Mon_comm:: setConnection() {

  //////////////////// setup blade socket and address
  blade_ETH_sockfd = socket(AF_INET, SOCK_DGRAM, 0);//DGRAM, 0);
  // clean blade_ETH_sockfd 
  memset(&blade_ETH_addr, 0, sizeof(blade_ETH_addr));
  blade_ETH_addr.sin_family = AF_INET;
  blade_ETH_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  // bind() of INADDR_ANY DOES NOT "generate a random IP". It binds the socket to all available interfaces.
  // https://stackoverflow.com/questions/16508685/understanding-inaddr-any-for-socket-programming

  char *ip1 = inet_ntoa(blade_ETH_addr.sin_addr);
  std::cout << "varify ip for blade " << ip1 << std::endl;

  blade_ETH_addr.sin_port = htons(dstport);  // blade/SERV_PORT);
  bind(blade_ETH_sockfd, (struct sockaddr *) &blade_ETH_addr, sizeof(blade_ETH_addr));

  std::cout << "----------->" << " cut line: above blade ETH, below flicGTX ETH" << std::endl;

  //////////////////// setup FLIC GTX socket and address
  memset(&FLIC1_Spybuffer_addr, 0, sizeof(FLIC1_Spybuffer_addr));
  FLIC1_Spybuffer_addr.sin_family = AF_INET;
  inet_pton(AF_INET, srcipaddress, &FLIC1_Spybuffer_addr.sin_addr);

  char *ip2 = inet_ntoa(FLIC1_Spybuffer_addr.sin_addr);
  std::cout << "varify ip for flic gtx " << ip2 << std::endl;

  FLIC1_Spybuffer_addr.sin_port = htons(srcport);   // CLI_PORT);
  addr_ptr = (struct sockaddr *) &FLIC1_Spybuffer_addr;
  addr_len = sizeof(FLIC1_Spybuffer_addr);

  //  return 0;
}

int FLIC_Mon_comm::getBladeETHSockfd() {
  return blade_ETH_sockfd;
}

struct sockaddr * FLIC_Mon_comm::getSockAddrPtr() {
  return addr_ptr;
}

socklen_t FLIC_Mon_comm::getSocklen_t() {
  return addr_len;
}

