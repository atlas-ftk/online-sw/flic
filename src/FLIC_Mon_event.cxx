#include "flic/FLIC_Mon_event.h"

using namespace daq::ftk;


FLIC_Mon_event::FLIC_Mon_event(uint16_t *assembly_header_ptr) {
begin_assembly_header_ptr = assembly_header_ptr;
}  

int FLIC_Mon_event::setEventAssemblyHeaderPosition(uint16_t *assembly_header_ptr) {
  begin_assembly_header_ptr = assembly_header_ptr;

  return 0;
}

void FLIC_Mon_event::getEventProcessingResult() {
  
  uint16_t * mesg = begin_assembly_header_ptr;

  int begin_assembly_header = 0;
  int begin_first_record_header ; // -> Refresh -> begin_record_header
  int begin_record_header; // -> begin_first_track_header
  int begin_first_track_header; // -> begin_track_header
  int begin_track_header; // -> begin_track_trailer
  int begin_track_trailer = 0; // -> end for loop -> begin_assembly_trailer

  int begin_assembly_trailer = 0;

  // e0f0 && a5a5 && 5a5a

  int num_of_word_following = 4 + mesg[begin_assembly_header + 1] / 2 ;

  // check the # of words following this word
  // use this # to go to the end of the packet
  // check whether it's the end
  // if it is the process it.

  // e0a3 e0a2 e0a1
  if (mesg[begin_assembly_header + 1 + num_of_word_following ] == 57507 && mesg[begin_assembly_header + 1 + num_of_word_following + 1 ] == 57506 && mesg[begin_assembly_header + 1 + num_of_word_following + 2 ] == 57505)
    {

	
      printf("============start of an assembly header===========\n");
      printf("Number of word following this word:          %d\n", num_of_word_following);

      begin_first_record_header = begin_assembly_header + 5;

      // header be00
      for ( begin_record_header = begin_first_record_header; (mesg[begin_record_header] & 65280) == 48640; begin_record_header = (begin_record_header + 1 + (mesg[begin_record_header+1] / 2) + 1) ) {
	  

	printf("+++++++++++++start of a record header++++++++++++\n");

	int n_of_word_following_et01 = mesg[begin_record_header+1] / 2;
	if ( mesg[begin_record_header + 1 + n_of_word_following_et01 - 4] + 5 == n_of_word_following_et01 )
	  printf("Checksum:                                    passed\n");
	else
	  printf("Checksum:                                    failed\n");


	printf("Mystery number:                              %u\n", mesg[begin_record_header+1]);

	//	printf("0 | RunNumber[30:16], RunNumber=0x3652e  \n");
	//	printf("RunNumber[15:0], RunNumber=0x3652e\n");
	printf("RunNumber:                                   %d\n", ( (mesg[begin_record_header+2] << 16) + mesg[begin_record_header+3] ) );

	//	printf("ExtendedLevel1ID[31:16], word=0x1 \n");
	//	printf("ExtendedLevel1ID[15:0], word=0x1  \n");
	printf("ExtendedLevel1ID:                            %d\n", ( (mesg[begin_record_header+4] << 16) + mesg[begin_record_header+5] ) );

	//	printf("Reserved [15:0] :                            %u\n", mesg[begin_record_header+6]);
	//	printf("Res[3:0] | BCID[11:0], word=0x928 \n");
	printf("Bunch Crossing ID:                           %u\n", mesg[begin_record_header+7] & 4095 );

	//	printf("Reserved [15:0] :                            %u\n", mesg[begin_record_header+8]);
	//	printf("Res[7:0] | L1TriggerType[7:0], word=0x0 \n");
	printf("L1TriggerType:                               %u\n", mesg[begin_record_header+9] & 255);

	//	printf("Res[7:0] | DetectorEventType[7:0] \n");
	printf("Detector Event Type:                         %u\n", mesg[begin_record_header+10] & 255);

	//	printf("Res[11:0] | TIM[3:0] \n");
	printf("TIM:                                         %u\n", mesg[begin_record_header+11] & 15);
	

	printf("+++++++++++++++end of a record header++++++++++++\n");


	begin_first_track_header = begin_record_header + 12;

	begin_track_header = begin_first_track_header;
	// XBDA
	for ( begin_track_header = begin_first_track_header ; (mesg[begin_track_header] & 4095) == 3034; begin_track_header = begin_track_header + 28 ) {

	  printf("--------------start of a track header------------\n");
	
	  //	printf("Res[3:0] | L | 0xb | 0xd | 0xa, word=0xbda4fb9\n");
	  printf("Link ID:                                     %u\n", mesg[begin_track_header] >> 12);

	  //	printf("Res[2:0] | Sector Number \n");
	  printf("Sector Number :                              %u\n", mesg[begin_track_header+1] & 8191);
	  // m_hTrackSector->Fill( temp );

	  //	printf("Res[3:0] | TFNum[3:0] | Res | Tower[6:0], word=0x160fef\n");
	  printf("Tower Number:                                %u\n", mesg[begin_track_header+2] & 127);
	  printf("Track Fitter Number:                         %u\n", (mesg[begin_track_header+2] & 3840) >> 8);

	  //	printf("Res[3:0] | Layermap[11:0] :                  \n");
	  printf("Layermap[11:0] :                  \n");
	  printf("  11SCT 10SCT 09SCT 08SCT 07SCT 06SCT 05SCT 04SCT 03PIX 02PIX 01PIX 00IBL\n");
	  printf("      %u     %u     %u     %u     %u     %u     %u     %u     %u     %u     %u     %u\n", (mesg[begin_track_header+3] & 2048) ? 1 : 0, (mesg[begin_track_header+3] & 1024) ? 1 : 0, (mesg[begin_track_header+3] & 512) ? 1 : 0, (mesg[begin_track_header+3] & 256) ? 1 : 0, (mesg[begin_track_header+3] & 128) ? 1 : 0, (mesg[begin_track_header+3] & 64) ? 1 : 0, (mesg[begin_track_header+3] & 32) ? 1 : 0, (mesg[begin_track_header+3] & 16) ? 1 : 0, (mesg[begin_track_header+3] & 8) ? 1 : 0, (mesg[begin_track_header+3] & 4) ? 1 : 0, (mesg[begin_track_header+3] & 2) ? 1 : 0, (mesg[begin_track_header+3] & 1) ? 1 : 0);
	  //	printf("Res[7:0] | RoadID[23:16], word=0x5fc \n");
	  //	printf("RoadID[15:0] \n");
	  printf("RoadID:                                      %d\n", ( (mesg[begin_track_header+4] & 255) << 16 ) + mesg[begin_track_header+5]);

	  // ( (mesg[begin_track_header+4] << 16) + mesg[begin_track_header+5] ) & 16777215 );
	
	  printf("chi sq :                                     %f\n", HALFToFloat(mesg[begin_track_header+6]));
	  // m_hTrackChi2->Fill( temp );

	  printf("track d0:                                    %f\n", HALFToFloat(mesg[begin_track_header+7]));
	  // m_hTrackD0->Fill( temp );

	  printf("track z0:                                    %f\n", HALFToFloat(mesg[begin_track_header+8]));
	  // m_hTrackZ0->Fill( temp );

	  printf("track cotth:                                 %f\n", HALFToFloat(mesg[begin_track_header+9]));
	  // m_hTrackCotth->Fill( temp );

	  printf("track phi:                                   %f\n", HALFToFloat(mesg[begin_track_header+10]));
	  // m_hTrackPhi->Fill( temp );

	  printf("track curv:                                  %f\n", HALFToFloat(mesg[begin_track_header+11]));
	  // m_hTrackCurv->Fill( temp );

	  printf("---------------end of a track header-------------\n");

	  
	}

	// EODA = 57562                                                  
	begin_track_trailer = begin_track_header;
	if (mesg[begin_track_trailer] == 57562) {
	  printf("+++++++++++++start of a record trailer+++++++++++\n");
	  printf("Begin trailer debug block :                  %04x\n", mesg[begin_track_trailer]);
	  printf("Length of debug block | Indeterminate[3:0] : %u\n", mesg[begin_track_trailer+1]);
	  printf("End debug block :                            %04x\n", mesg[begin_track_trailer+2]);
	  printf("Length of debug block | Indeterminate[3:0] : %u\n", mesg[begin_track_trailer+3]);
	  //	printf("L1ID[31:16] \n");
	  //	printf("L1ID[15:0] \n");
	  printf("L1ID:                                        %d\n", (mesg[begin_track_trailer+4] << 16) + mesg[begin_track_trailer+5]);
	  //	printf("ErrorFlag[31:16]\n");
	  //	printf("ErrorFlag[15:0]\n");
	  printf("ErrorFlag :                                  %d\n", (mesg[begin_track_trailer+6] << 16) + mesg[begin_track_trailer+7]);

	  printf("Module data word count (checksum):           %d\n", (mesg[begin_track_trailer+10] << 16) + mesg[begin_track_trailer+11]);

	  // e0f0 a5a5 5a5a 0e0f
	  printf("End of fragment word:\n");
	  printf("                                             %04x\n", mesg[begin_track_trailer+12]);
	  printf("                                             %04x\n", mesg[begin_track_trailer+13]);
	  printf("                                             %04x\n", mesg[begin_track_trailer+14]);
	  printf("                                             %04x\n", mesg[begin_track_trailer+15]);

	  printf("++++++++++++++end of a record trailer++++++++++++\n");

	} else {
	  printf("error at the location where it should be begining of track trailer\n");
	}


      } // end of for loop for et01 tag



      begin_assembly_trailer = begin_track_trailer + 16;

      // e0a3 e0a2 e0a1
      if ( mesg[begin_assembly_trailer] == 57507 && mesg[begin_assembly_trailer + 1] == 57506 && mesg[begin_assembly_trailer + 2] == 57505 ) {
	printf("===========start of an assembly trailer==========\n");
	printf("Assembly trailer word: \n");
	printf("                                             %04x\n", mesg[begin_assembly_trailer]);
	printf("                                             %04x\n", mesg[begin_assembly_trailer+1]);
	printf("                                             %04x\n", mesg[begin_assembly_trailer+2]);
	printf("Three words after Assembly trailer word: \n");
	printf("(if they are not 0s, error in received data)\n");
	printf("                                             %04x\n", mesg[begin_assembly_trailer+3]);
	printf("                                             %04x\n", mesg[begin_assembly_trailer+4]);
	printf("                                             %04x\n", mesg[begin_assembly_trailer+5]);

      } //if (mesg[ num_of_word_following ] == 57507 && mesg[ num_of_word_following + 1 ] == 57506 && mesg[ num_of_word_following + 2 ] == 57505) 
      else {
	printf("Error in assembly trailer region\n");
	printf("Bad data: error at around line %d\n", begin_assembly_trailer);
      }

    } // end of number of words following
    


  /*
  printf("******************end of UDP packet**********************\n");

  ++udp_packet_counter;

  if (udp_packet_counter % 3 == 0) {

      c1->cd(1);
      m_hTrackSector->Draw();
      c1->cd(2);
      m_hTrackChi2->Draw();
      c1->cd(3);
      m_hTrackD0->Draw();
      c1->cd(4);
      m_hTrackZ0->Draw();
      c1->cd(5);
      m_hTrackCotth->Draw();
      c1->cd(6);
      m_hTrackPhi->Draw();
      c1->cd(7);
      m_hTrackCurv->Draw();

      c1->SaveAs("last_3_udp_packtets.png");
      c1->SaveAs("last_3_udp_packtets.eps");

  }      

  */

}
