// Functions used for spy buffer access using DFApi

#include <assert.h>

#include "flic/FlicMonitoringFIFO.h"

namespace daq { 
  namespace ftk {

    FlicMonitoringFIFO::FlicMonitoringFIFO( ushort targetFpga , ushort fifoId , uint depth )
      : m_targetFpga( targetFpga )
      , m_fifoId( fifoId )
      , m_depth( depth )
    {}
    
    FlicMonitoringFIFO::~FlicMonitoringFIFO() {}
    uint32_t FlicMonitoringFIFO::getSpyFreeze() { return 0; }
    int FlicMonitoringFIFO::readSpyBuffer() { return 0; }
    int FlicMonitoringFIFO::readSpyStatusRegister(uint32_t& spyStatus) { return spyStatus = 0; }
    
    int FlicMonitoringFIFO::read( flic_comm *flic ) {
      int bytes = 0;

      // do the read 256 words at a time
      m_buffer.clear();
      for( ushort nread = 0 ; nread < m_depth ; nread += 256 ) {

        // block read
        ushort data[256];
        bytes += flic->blockread( m_targetFpga , FLIC::cmdFifoRead , 0x110+(2*m_fifoId) , data , 256 );

        // push array data into m_buffer vector
        m_buffer.clear();
        for( ushort i = 0 ; i < 256 ; i++ ) {
          m_buffer.push_back( data[i] );
          cout << data[i] << endl;
        }
      }

      assert( m_buffer.size() == m_depth );

      return bytes;
    }
   

  } // namespace ftk
} // namespace daq


