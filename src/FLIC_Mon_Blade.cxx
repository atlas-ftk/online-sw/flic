#include "../flic/FLIC_Blade_Mon.h"

FLIC_Blade_Mon::FLIC_Blade_Mon() {
}


int FLIC_Blade_Mon::readEthernetFromFileAndCreateSocket(std::string location)
  // This method will read the ETH setup from the file and put the values to the parameters; based on the value of the parameter, setup the Ethernet connection
{
  char ipaddress[50];
  int port;
  char macaddress[50];

  std::cout << " after define IP parameters" << std::endl;
  
  std::ifstream bladeEthernetfile (location);
  //      "/home/peilong/Project/flic/standalone/ethernet_setup_blade.txt");
  
  if (bladeEthernetfile.is_open()) {
    bladeEthernetfile >> ipaddress >> port >> macaddress; 
    std::cout <<"xxx " << ipaddress << "_" << port << "_" << macaddress << std::endl;

    bladeEthernetfile.close();
  }
  else
    std::cout << "Unable to open file" << std::endl;   


  //////////////////// setup blade socket and address
  blade_ETH_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  memset(&blade_ETH_addr, 0, sizeof(blade_ETH_addr));
  blade_ETH_addr.sin_family = AF_INET;
  blade_ETH_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  // bind() of INADDR_ANY DOES NOT "generate a random IP". It binds the socket to all available interfaces.
  // https://stackoverflow.com/questions/16508685/understanding-inaddr-any-for-socket-programming
  
  char *ip1 = inet_ntoa(blade_ETH_addr.sin_addr);
  std::cout << "varify ip for blade " << ip1 << std::endl;

  blade_ETH_addr.sin_port = htons(port);  // SERV_PORT);
  bind(blade_ETH_sockfd, (struct sockaddr *) &blade_ETH_addr, sizeof(blade_ETH_addr));


  std::cout << "----------->" << " cut line: above blade ETH, below flicGTX ETH" << std::endl;
  
  std::ifstream flicGTXEthernetfile ("/home/peilong/Project/flic/standalone/ethernet_setup_flicGTX.txt");
  
  if (flicGTXEthernetfile.is_open()) {
    flicGTXEthernetfile >> ipaddress >> port >> macaddress; 
    std::cout <<"xxx " << ipaddress << "_" << port << "_" << macaddress << std::endl;

    flicGTXEthernetfile.close();
  }
  else
    std::cout << "Unable to open file" << std::endl;   

  //////////////////// setup FLIC GTX socket and address
  memset(&FLIC1_Spybuffer_addr, 0, sizeof(FLIC1_Spybuffer_addr));
  FLIC1_Spybuffer_addr.sin_family = AF_INET;
  inet_pton(AF_INET, ipaddress, &FLIC1_Spybuffer_addr.sin_addr);

  char *ip2 = inet_ntoa(FLIC1_Spybuffer_addr.sin_addr);
  std::cout << "varify ip for flic gtx " << ip2 << std::endl;

  FLIC1_Spybuffer_addr.sin_port = htons(port);   // CLI_PORT);
  addr_ptr = (struct sockaddr *) &FLIC1_Spybuffer_addr;
  addr_len = sizeof(FLIC1_Spybuffer_addr);

  return 0;
}

void FLIC_Blade_Mon::setupBuffer() {

  flic_udp_buffer.resize(100); // 100 UDP packet could be stored
}


void receiveData()
// receive data and and put data in the storage structure
{
  flic_udp_buffer.resize(100); // 100 UDP packet could be stored

  unsigned long int i = 0;
  int n;

  for (i=0; ; ++i) {
    i = i % flic_udp_buffer.size();

    n = recvfrom(blade_ETH_sockfd, &(flic_udp_buffer.at(i).mesg), MAXLINE, 0, addr_ptr, &addr_len);
    // recvfrom() shall return the length of the message in bytes.
    
    if (n < 1500)
      break;
  }
  
}

FLIC_Blade_Mon::~FLIC_Blade_Mon() {

}
