#include "flic/DataFlowReaderFlic.h"
#include "flic/flic_comm.h"
#include "ftkcommon/ReadSRFromISVectors.h"

#include "flic/dal/FtkDataFlowSummaryFlicNamed.h"

#include "ftkcommon/Utils.h"

//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>
#include <memory>

// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"

using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderFlic::DataFlowReaderFlic(std::shared_ptr<OHRawProvider<> > ohRawProvider, bool forceIS, flic_comm *flic) 
   : DataFlowReader(ohRawProvider, forceIS)
/************************************************************/
{
   df_flic = flic; 
}

/***************************************************************************************************************/
void DataFlowReaderFlic::init(const string& deviceName, const string& partitionName, const string& isServerName)
/***************************************************************************************************************/
{
  DataFlowReader::init(deviceName, partitionName, isServerName);
  DataFlowReader::name_dataflow();
  m_theFlicSummary = std::make_shared<FtkDataFlowSummaryFlicNamed>(m_ipcp,name_dataflow());
  m_theSummary= std::static_pointer_cast<FtkDataFlowSummaryNamed>(m_theFlicSummary);
      
  ERS_LOG("Initializing Flic dataflowsummary: "<<name_dataflow()<<" :pointer");
  	
}

/************************************************************/
vector<string> DataFlowReaderFlic::getISVectorNames()
/************************************************************/
{
  vector<string> srv_names;
  /*
  srv_names.push_back("FLIC_l1id_sr_v");
  srv_names.push_back("FLIC_fifo_in_busy_sr_v");
  srv_names.push_back("FLIC_fifo_out_busy_sr_v");
  srv_names.push_back("FLIC_link_in_status_sr_v");
  srv_names.push_back("FLIC_link_out_status_sr_v");
  srv_names.push_back("FLIC_number_events_processed_sr_v");
  */
   return srv_names;
}

/************************************************************/
void DataFlowReaderFlic::getL1id(vector<int64_t>& srv)
/*************************************************************/
{
  srv.clear();
  ushort l1ids[2];
  df_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x14E , l1ids , 2 );   
  srv.push_back(l1ids[1] + (l1ids[0]<<16));
}

/************************************************************/
void DataFlowReaderFlic::getLinkInStatus(vector<int64_t>& srv)
/*************************************************************/
{
  srv.clear();
  ushort xoff_counts[4];
  df_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x013c , xoff_counts , 4 );
  for( ushort link = 0 ; link < 4 ; link++ ) { 
    srv.push_back((xoff_counts[link]>>11) & 0x1);
  }
}

/************************************************************/
void DataFlowReaderFlic::getLinkOutStatus(vector<int64_t>& srv)
/*************************************************************/
{
  srv.clear();
  ushort xoff_counts[4];
  df_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x013c , xoff_counts , 4 );
  for( ushort link = 0 ; link < 4 ; link++ ) { 
    srv.push_back((xoff_counts[link]>>8) & 0x0);
  }
}

/************************************************************/
void DataFlowReaderFlic::getNEventsProcessed(vector<int64_t>& srv)
/*************************************************************/
{
  srv.clear();
  for( ushort link = 0 ; link < 4 ; link++ ) { 
    ushort proc_counts[1];
    df_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x318+(link*0x10) , proc_counts , 0x1 );
    srv.push_back(proc_counts[0]);
  }
}

/************************************************************/
void DataFlowReaderFlic::getFifoInBusy(vector<int64_t>& srv)
/*************************************************************/
{
  srv.clear();
  for( ushort link = 0 ; link < 4 ; link++ ) { 
    ushort proc_counts[1];
    df_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x31c+(link*0x10) , proc_counts , 0x1 );
    srv.push_back(proc_counts[0]);
  }
}

/************************************************************/
void DataFlowReaderFlic::getFifoOutBusy(vector<int64_t>& srv)
/*************************************************************/
{
  srv.clear();
  ushort xoff_counts[4];
  df_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x013c , xoff_counts , 4 );
  for( ushort link = 0 ; link < 4 ; link++ ) { 
    srv.push_back((xoff_counts[link]>>10) & 0x0);
  }
}

/************************************************************/
void DataFlowReaderFlic::publishExtraHistos(uint32_t option)
/************************************************************/
{
  std::vector<int64_t> srv;
  srv.clear();
}

