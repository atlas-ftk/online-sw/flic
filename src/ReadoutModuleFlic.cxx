/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/

//#include <iostream>
#include <string>

// OKS, RC and IS
#include <dal/Segment.h>
#include "flic/ReadoutModuleFlic.h"
#include "flic/dal/FTK_RCD_FLIC_1Named.h"
#include "flic/dal/ReadoutModule_flic.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/Controllable.h>
#include "RunControl/Common/CommandSender.h"

// To include daq::rc::HardwareError
#include "ers/ers.h"
#include "RunControl/Common/UserExceptions.h"

// Common FTK tools
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

// VME
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

// Emon
#include <memory> // shared_ptr
#include <utility> // pair

using namespace daq::ftk;
using namespace std;

/**********************/
ReadoutModule_flic::ReadoutModule_flic()
/**********************/
{
  #include "cmt/version.ixx"
  ERS_LOG("ReadoutModule_flic::constructor: Entered"      << std::endl
    << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl
    << cmtversion );

  m_configuration  = 0;
  m_dryRun         = false;
  m_HWError	   = false;
  m_CanPublish	   = false;

  // JW - Need to add soft reset for this function
  m_isLoadMode = daq::ftk::isLoadMode();
  if( m_isLoadMode ) ERS_LOG( "isLoadMode is True" );

  ERS_LOG("ReadoutModule_flic::constructor: Done");
}

/***********************/
ReadoutModule_flic::~ReadoutModule_flic() noexcept
/***********************/
{
  ERS_LOG("ReadoutModule_flic::destructor: Entered");
  delete m_flic;
  ERS_LOG("ReadoutModule_flic::destructor: Done");
}

/************************************************************/
void ReadoutModule_flic::setup(DFCountedPointer<ROS::Config> configuration)
/************************************************************/
{
  ERS_LOG("ReadoutModule_flic::setup: Entered");

  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  m_appName	      = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

  // Read some info from the DB
  const ftk::dal::ReadoutModule_flic * module_dal = conf->get<ftk::dal::ReadoutModule_flic>(configuration->getString("UID"));
  m_name		       = module_dal->UID();
  m_slot		       = module_dal->get_Slot();
  m_boardNumber		       = module_dal->get_BoardNumber();
  m_flicip		       = module_dal->get_FlicIP();
  m_port		       = module_dal->get_Port();
  m_hostip		       = module_dal->get_HostIP();
  m_verbose		       = module_dal->get_Verbose();
  m_loopbackMode	       = module_dal->get_LoopbackMode();
  m_useAsSSBe                  = module_dal->get_UseAsSSBe();
  m_use32bitL1ID               = module_dal->get_Use32bitL1ID(); 
  m_tracksPerRecord	       = module_dal->get_TracksPerRecord();
  m_delayBetweenRecords	       = module_dal->get_DelayBetweenRecords();
  m_totalRecords               = module_dal->get_TotalRecords();
  m_delayBetweenSyncAttempts   = module_dal->get_DelayBetweenSyncAttempts();
  m_totalSyncAttempts	       = module_dal->get_TotalSyncAttempts();
  m_linksInUse		       = module_dal->get_LinksInUse();
  m_totalROSLinkResets	       = module_dal->get_TotalROSLinkResets();
  m_ignoreROSLinks	       = module_dal->get_IgnoreROSLinks();
  m_reloadFirmware	       = module_dal->get_ReloadFirmware();
  m_reloadSRAMs                = module_dal->get_ReloadSRAMs();
  m_isServerName	       = readOutConfig->get_ISServerName();
  m_dryRun		       = module_dal->get_DryRun();
  m_captureCondition	       = module_dal->get_CaptureCondition();
  m_monitoringChannel	       = module_dal->get_MonitoringChannel();
  m_CanPublish                 = false;

  if(m_dryRun) { 
    std::stringstream message;
    message << "ReadoutModule_flic.DryRun option set in DB, IPBus calls with be skipped!";
    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
    ers::warning(ex);
  }  
  
  // set up the MT infrastructure
  initialize( m_name, module_dal, BOARD_FLIC );

  // Initialize flic_comm object to open a socket for communicating with board
  // using the IP information included in the segment DB
  // (Readout_flic.schema.xml attributes)
  m_flic = new flic_comm( m_flicip.c_str() , m_port.c_str() , m_hostip.c_str() );

  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try 
    { 
      m_flicNamed = new FTK_RCD_FLIC_1Named( m_ipcpartition, isProvider.c_str() ); 
    }
  catch( ers::Issue &ex)
    {
      daq::ftk::ISException e(ERS_HERE, "Error while creating flicNamed object", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try 
    {
      m_ohProvider = new OHRootProvider ( m_ipcpartition , OHServer, OHName );
      m_ohRawProviderFlic = std::shared_ptr<OHRawProvider<> >(new OHRawProvider<>( m_ipcpartition , OHServer, OHName));
      ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << m_appName);
    }
  catch ( daq::oh::Exception & ex)
    {
      // Raise a warning or throw an exception.
      daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Construct the histograms
  for( ushort ich = 0 ; ich < 4 ; ich++ ) {

    TH1F *hGlobalStatus = new TH1F( TString::Format("hGlobalStatus.Channel%i",ich) , TString::Format("GlobalStatus.Channel%i;;Status",ich) , 16 , 0 , 16 );
    hGlobalStatus->GetXaxis()->SetBinLabel( 1 , "SFP_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 2 , "CCMUX_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 3 , "SRAM_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 4 , "MERGE_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 5 , "RTM_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 6 , "COLLECTED_PIPELINE_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 7 , "U3_TAGGED_EVENT_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 8 , "U4_TAGGED_EVENT_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 9 , "SLINK_LDOWN" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 10 , "SLINK_LFF" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 11 , "SLINK_XOFF" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 12 , "NOT_IN_SYNC" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 13 , "SLINK_CTL2_REG1" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 14 , "SLINK_CTL2_REG2" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 15 , "RESERVED" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 16 , "RESERVED" );
    m_hGlobalStatus.push_back( hGlobalStatus );

    TH1F *hPipelineCounters = new TH1F( TString::Format("hPipelineCounters.Channel%i",ich) , TString::Format("PipelineCounters.Channel%i;;Count",ich) , 16 , 0 , 16 );
    hPipelineCounters->GetXaxis()->SetBinLabel( 1 , "SFP_FIFO_FULL_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 2 , "NOT_IN_SYNC_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 3 , "HEADER_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 4 , "TRACK_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 5 , "TRAILER_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 6 , "TRUNCATION_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 7 , "CCMUX_FIFO_EVENT_END_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 8 , "SRAM_FIFO_EVENT_READ_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 9 , "MERGE_FIFO_EVENT_READ_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 10 , "U3_TAGGED_EVENT_FIFO_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 11 , "U4_TAGGED_EVENT_FIFO_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 12 , "RTM_FIFO_EVENT_END_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 13 , "FLIC_FLOW_CTL" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 14 , "L1_ID_MATCH_U3_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 15 , "L1_ID_MATCH_U4_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 16 , "L1_SKIP_ERRORs" );
    m_hPipelineCounters.push_back( hPipelineCounters );
  }
  m_hTrackSector = new TH1F( "hTrackSector" , "TrackSector;Sector;Monitored Ev" , 32 , 0 , 8192 );
  m_hTrackChi2	 = new TH1F( "hTrackChi2" , "TrackChi2;#chi^{2};Monitored Ev" , 25 , 0 , 500 );
  m_hTrackD0	 = new TH1F( "hTrackD0" , "TrackD0;d_{0} [mm];Monitored Ev" , 30 , -3.0 , 3.0 );
  m_hTrackZ0	 = new TH1F( "hTrackZ0" , "TrackZ0;z_{0} [mm];Monitored Ev" , 40 , -200.0 , 200.0 );
  m_hTrackCotth	 = new TH1F( "hTrackCotth" , "TrackCotth;cot#theta;Monitored Ev" , 30 , -6 , 6 );
  m_hTrackPhi	 = new TH1F( "hTrackPhi" , "TrackPhi;#phi_{0};Monitored Ev" , 32 , -8 , 8 );
  m_hTrackCurv	 = new TH1F( "hTrackCurv" , "TrackCurv;1/2*p_{T} [GeV^{-1}];Monitored Ev" , 25 , -0.5 , 0.5 );
    
  //
  // Now for the actual board setup commands
  //

  //
  // Figure out which FW to put on each FPGA based on user input
  //
  m_ProcTargets.clear();
  m_SSBeTargets.clear();
  if( m_loopbackMode ) {
    m_ProcTargets.push_back( FLIC::targetFpgaU1 );
    m_SSBeTargets.push_back( FLIC::targetFpgaU2 );
  } else if( m_useAsSSBe ) {
    m_SSBeTargets.push_back( FLIC::targetFpgaU1 );
    m_SSBeTargets.push_back( FLIC::targetFpgaU2 );
  } else {
    m_ProcTargets.push_back( FLIC::targetFpgaU1 );
    m_ProcTargets.push_back( FLIC::targetFpgaU2 );
  }

  ERS_LOG( "Processing FPGA targets: " );
  for( ushort target : m_ProcTargets ) ERS_LOG( "    " << target );
  ERS_LOG( "SSB emulator FPGA targets: " );
  for( ushort target : m_SSBeTargets ) ERS_LOG( "    " << target );
  
  ERS_LOG("ReadoutModule_flic::setup: Done");
}


/********************************/
void ReadoutModule_flic::doConfigure(const daq::rc::TransitionCmd& cmd)
/********************************/
{
  ERS_LOG(cmd.toString());

  //
  // The program firmware and initialize links/state machines code below
  // was initially in "setup", but moved here on 2017.1.19 to adhere to the
  // guidelines suggested here:
  //   https://twiki.cern.ch/twiki/bin/view/Atlas/FtkRcTransitions
  //
  
  reloadFirmware();  

  setupBoards();

  ERS_LOG(cmd.toString() << " Done");
}


/**************************/
void ReadoutModule_flic::doUnconfigure(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void ReadoutModule_flic::doConnect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  boardReset(); 

  // Sync FLIC Processing state machines with SSBs or SSB emulator FPGAs
  // When running with ATLAS, FLIC should not be in the loopBack mode,
  // therefore we do not have to call anything about SSBe in the stopless recovery
  
  if( m_SSBeTargets.size() > 0 ) {
    //Setup 32-bit or ECR + 24-bit level-1 ID in the emulatior
    if(m_use32bitL1ID)m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x002D , 0xbbbb ); 
    else m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x002D , 0x3333 ); 

    // When running with SSB emulator in loopback, the emulator just needs to send a single dummy
    // event to the processor, and then reset the L1ID back to zero
    for( ushort target : m_SSBeTargets ) {
      m_ctrl.StartSSBeData( m_flic , target , 0xf /*channel mask*/ , 1 /*event*/ , 0 /*tracks*/ , 4000 /*commas*/ );
      m_ctrl.ResetSSBe( m_flic , target );
    }
  }

  if (!m_loopbackMode) sync(m_totalSyncAttempts); 
  
  ERS_LOG(cmd.toString() << " done");
}
  

/**************************/
void ReadoutModule_flic::doDisconnect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}

/**********************************/
void ReadoutModule_flic::startNoDF(const daq::rc::SubTransitionCmd& cmd)
/**********************************/
{
  ERS_LOG(cmd.toString());

  prepareForRunSSBFLICProtocol();

  //for( ushort target : m_ProcTargets ) {
  //   m_ctrl.PulseFlowControlToSSB( m_flic , target);
  // } 

  // Add SSBe reset to enable stop-start transition in loopback mode
  // Stopless recovery does not need the following functionalities
  for( ushort target : m_SSBeTargets ) {
    m_ctrl.ResetSSBe( m_flic , target );
  }

  // Start sending continuous data on all of the SSB emualators
  for( ushort target : m_SSBeTargets ) {
    m_ctrl.StartSSBeData( m_flic , target , 0xf /*channel map*/ , m_totalRecords , m_tracksPerRecord , m_delayBetweenRecords );
  } 
  ERS_LOG(cmd.toString() << " Done");
}

/**********************************/
void ReadoutModule_flic::doPrepareForRun(const daq::rc::TransitionCmd& cmd)
/**********************************/
{
  ERS_LOG(cmd.toString());
  //Enable the automatic execution of publish
  //The publish method is independent from the state transitions so that it is safe to enable at start and disable at stop
  m_CanPublish = true;
  ERS_LOG(cmd.toString() << " Done");
}


/////////////////////////////////////////////////////////////////
// FTK agreed on moving stop transition to stopRecording       //
// The standard procedure is: Send xoff to the SSB, publish the// 
// last time, clear all FIFOs, check if all FIFOs are cleared  //
/////////////////////////////////////////////////////////////////

/**************************/
void ReadoutModule_flic::doStopRecording(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  // Publish final numbers
  m_CanPublish    = false;
  localPublish();
  //stopping the automatic publish execution

  // Stop sending continuous data on all SSB emulators
  // Stopless recovery does not need the following functionalities
  for( ushort target : m_SSBeTargets )
    m_ctrl.StopSSBeData( m_flic , target , 0xf ); // 0xf = stop continuous data on all 4 channels
  
  stopRecordingSSBFLICProtocol();
  FIFOClearAndCheck();

  ERS_LOG(cmd.toString() << " done");
}



/**************************/
void ReadoutModule_flic::reloadFirmware()
/**************************/
{
  // Start by reloading the FW if the user requested it. If isLoadMode is true, reload the firmware and LUTs regardless of user configs
  if( m_reloadFirmware || m_isLoadMode) {
    ERS_LOG( "Transfering firmware from FLASH to FPGAs" );
    // Ensure that the FPGAs are powered on...
    m_ctrl.PICPowerOn( m_flic );
    // Transfer firmware and lookup tables binaries in Flash to FPGAs and SRAMs
    bool SRAM_reload = m_isLoadMode || m_reloadSRAMs;
    if( m_ctrl.ProgramFPGAs( m_flic , m_ProcTargets , m_SSBeTargets , SRAM_reload) < 0 )
      ERS_LOG( "Unable to load firmware for some reason..." );
  }
}
  
/**************************/
void ReadoutModule_flic::setupBoards()
/**************************/
{
  // Ensure that the clock rates are all correct.
  // We just check clocks on the processing FPGAs here. Make sure it is in rate mode
  for( ushort target : m_ProcTargets )
    m_ctrl.CheckClockCounters( m_flic , target );
  // Put SFPs in "normal mode" rather than "setup" mode so we don't confuse the SSB with non-comma words
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0004 , 0x00F0 );

  m_ctrl.IdleBoard( m_flic );
  m_ctrl.InitStateMachines( m_flic );

  // If OKS specified to ignore ROS links, do that here...
  if( m_ignoreROSLinks ) {
    for( ushort target : m_ProcTargets ) {
      m_ctrl.DumpDataOnTheFloorMode( m_flic , target );
    }
  }
}

/**************************/
void ReadoutModule_flic::boardReset() 
/**************************/
{
  // Whack the Rx's again on both U1 and U2 since we're
  // really supposed to do this in "connect"
  // Alignment should be automatically handled in Rx state machine
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );

  //  Reset flow control FIFOs now that Tx's and Rx's are initlialized
  // This could in theory be done after just initializing the Tx's. However, when
  // we reset the Rx's the state machine automatically sends a flow control signal
  // to SSB to temporarily pause data, so it's cleaner to reset the flow control
  // FIFOs after the Rx reset is complete.
  for( ushort target : m_ProcTargets )
    m_ctrl.ResetArmFlowControlFIFOs( m_flic , target );
  
  // Try to initialize EMon data handler
  m_ftkemonDataOut = reinterpret_cast<daq::ftk::FtkEMonDataOut*>(ROS::DataOut::sampled());
  if( !m_ftkemonDataOut ) {
    std::stringstream message;
    message << "EMonDataOut plugin not found! SpyBuffer data will not be published in emon";
    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
    ers::warning(ex);
  } else {
    m_ftkemonDataOut->setScalingFactor(1);
    ERS_LOG("Found EMonDataOut plugin");
  }
  
  // Reset and initialize the processor counters
  // Also reset and arm the monitoring FIFOs, spying on channel 0 by default
  for( ushort target : m_ProcTargets ) {
    m_ctrl.InitProcCounters( m_flic , target , 0x0000 ); // 0 => Count mode; 0xffff => Rate mode
    m_ctrl.ResetArmMonitoringFIFOs( m_flic , target , m_monitoringChannel , m_captureCondition);
  }
}


/**************************/
void ReadoutModule_flic::sync(ushort maxAttempt) 
/**************************/
{
  bool linksHappy = true;
  if( m_ProcTargets.size() > 0 && !m_loopbackMode) {
    for( ushort iAttempt = 0 ; true ; iAttempt++ ) {
      linksHappy = true;
   
      // When running with the actual SSB, the processor FPGAs need to send a sync control word to each SSB link.
      // The SSBs will then respond by sending back a dummy event. (I don't need to do this exchange when testing the
      // SSBe, since the SSBe sends a dummy event regardless of whether it sees the sync control word.)
    
      // The control word was pushed to prepareForRun to fix synchronization timing, but this created more
      // problems because data would start flowing before the boards sync'd. Lets try adding
      // the sync request here, but continuing to request until the FLIC is happy with the sync.
      // Throw FATAL error if we have exceeded the max sync attempts allowed by user
      if( iAttempt >= maxAttempt ){ 
        std::stringstream message;
        message << "FLIC exceeded max sync attempts with the SSB!" << m_appName.c_str();
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(),message.str());
        throw(ex);
        return; 
      }
  
      if( m_verbose > 0 ) ERS_LOG( "FLIC attempting to sync with SSB [attempt " << (iAttempt+1) << " / " << maxAttempt << "]" );
    
      // Send sync word to SSB
      for( ushort target : m_ProcTargets )
        m_ctrl.SyncWithSSB( m_flic , target );
  
      // Wait a bit before checking if the sync worked
      usleep( m_delayBetweenSyncAttempts * 1000000 );
  
      // Now check to see if the FLIC thinks the boards are sync'd
      for( ushort target : m_ProcTargets ) {
        ushort global_status[4];
        m_flic->blockread( target , FLIC::cmdArrayRead , 0x13C , global_status , 4 );
        for( ushort iLink = 0 ; iLink < 4 ; iLink++ ) {
          ushort globalLink = iLink + (target*4);
          if( ((m_linksInUse>>globalLink)&1) == 0 ) continue; // We don't care about this link
          if( ((global_status[iLink]>>11)&1) == 1 ) {
            linksHappy = false;
            if( m_verbose > 0 ) {
              ERS_LOG( "Link " << globalLink << " did NOT sync, GLOBAL_STATUS = " << std::hex << global_status[iLink] << std::dec );
  	      standalone::DumpGlobalStatus( m_flic , target );
  	      standalone::DumpRawSLINKStatus( m_flic , target );
  	    }
          } 
          else {
            if( m_verbose > 0 ) ERS_LOG( "Link " << globalLink << " is happy" ); 
          }
        }
      }
      if (linksHappy) break;
    }
  }  
  // Stop making sync attempts if all the links are happy

  // FIXME - Do a full reconigure of the FLIC before trying to synch again with SSB
  // This was added to help fix synch with SSB at P1, but really should be updated and
  // removed once the problem is understood from the SSB end!!

  // check if the SLink is up
  for ( ushort target : m_ProcTargets ) {
    ushort global_status[4];
    m_flic->blockread( target, FLIC::cmdArrayRead, 0x13C, global_status, 4 );
    for ( ushort iLink = 0; iLink < 4; iLink++ ) {
      ushort globalLink = iLink + (target*4);
      if ( ((m_linksInUse>>globalLink)&1) == 0 ) continue; // We don't care about this link
      if ( ((global_status[iLink]>>8)&1) == 1 ) {
        std::stringstream message;
        message << "SLink is down in link " << iLink << " on FPGA " << target << m_appName.c_str();
        daq::ftk::FTKIssue ex(ERS_HERE, m_name,message.str());
        throw(ex);
      }
    }
  }

  ERS_LOG( "Doing hard reset of all FLIC state machines. This seems to be the only way to recover comm with SSB at P1" );

  /*
  FLICSetupTool t;
  t.setFlicComm( m_flic );
  if( m_loopbackMode ) {
    t.INIT_U1_U2_U3_U4();
  } 
  else if( m_useAsSSBe ) {
    t.INIT_U2_U2_U3_U4();
  } 
  else {
    t.INIT_U1_U1_U3_U4();
  }
  */
  
  m_ctrl.IdleBoard( m_flic );
  m_ctrl.InitStateMachines( m_flic );
 
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  
  if( linksHappy && m_verbose)
    ERS_LOG( "All of the specified links have sync'd" );
}

/**************************/
void ReadoutModule_flic::prepareForRunSSBFLICProtocol()
/**************************/
{
  m_flic->singlewrite( FLIC::targetFpgaU1,   FLIC::cmdSet,  0x0004, 0x00F0);
  m_flic->singlewrite( FLIC::targetFpgaU1,   FLIC::cmdClear,  0x0004, 0x2000);
  // Enable FAKE FIFO empty signal
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x0004 , 0x1000 );
  // Send fake FIFO empty signal 
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x2F , 0x000F );   
  // Disable FAKE FIFO empty signal
  //m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x0004 , 0x1000 );
  //m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x2F , 0x000F );   
}

/**************************/
void ReadoutModule_flic::stopRecordingSSBFLICProtocol()
/**************************/
{
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //            The agreement with SSB at stop is to send FIFO full signal to the SSB                       //
  //               The solution is to use the Fake FIFO Full signal FLIC cand send                          //
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
  // Enable FAKE FIFO full signal
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x0004 , 0x1000 );
  // Send fake FIFO full signal 
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x2F , 0x00F0 );   
  // Disable FAKE FIFO full signal
  //m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x0004 , 0x1000 );
  // Disable flow control
  m_flic->singlewrite( FLIC::targetFpgaU1,   FLIC::cmdClear,  0x0004, 0x00F0);
  //m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x2F , 0x00F0 );   
}

/**************************/
void ReadoutModule_flic::FIFOClearAndCheck()
/**************************/
{
  // Clear all FIFOs
  for( ushort target : m_ProcTargets ) {
    m_ctrl.ClearAllFIFOs( m_flic , target);
  }
 
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //              Check if all the FIFOs are cleared if not clear them again for up to 3 times              //  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  for (ushort iAttempt = 0; true; iAttempt++) {
    if (iAttempt > 3) {
      //Rising a Fatal in case the FIFOs are not cleaned 
      std::stringstream message;
      message << "FLIC exceeded max attempts when clearing FIFOs" << m_appName.c_str();
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(),message.str());
      throw(ex);
      break;
    } 
    bool FIFOCleared = true;
    for( ushort target : m_ProcTargets ) {
      ushort linksInUse = m_linksInUse >> (target*4);
      FIFOCleared = FIFOCleared && m_ctrl.CheckFIFOStatus( m_flic , target, linksInUse);
    }   
    if (FIFOCleared) break;
    else {     
      for( ushort target : m_ProcTargets ) {
        m_ctrl.ClearAllFIFOs( m_flic , target);
      }
    }
  }
}

/**************************/
void ReadoutModule_flic::doPublish( uint32_t flag, bool finalPublish  )
/**************************/
{
  ERS_LOG("Publishing...");
  if (!m_CanPublish) {
    ERS_LOG("Not in a good state yet, skipping");
    return;
  }
 
  localPublish();
  // Collect the relevant information from VME
  // TODO: fill

  // Publish in IS with schema, i.e. fill ISInfo object

  // PIC registers
}

void ReadoutModule_flic::doPublishFullStats( uint32_t flag  )
{
  ERS_LOG("Publishing Full Statistics...");
  if (!m_CanPublish) {
    ERS_LOG("Not in a good state yet, skipping");
    return;
  }
  // Monitoring FIFOs
  vector< pair< shared_ptr<SpyBuffer> , uint32_t > > vec_MonFifos;
  for( ushort imon = 0 ; imon < 4 /*Number of FIFOs in a single pipeline*/ ; imon++ ) {
    shared_ptr< FlicMonitoringFIFO > monFifo = make_shared< FlicMonitoringFIFO >( FLIC::targetFpgaU1 , imon , 256 );
    monFifo->read( m_flic );
    uint32_t sourceId = encode_SourceIDSpyBuffer( BoardType::Flic , m_boardNumber , imon , Position::OTHER_POSITION );
    vec_MonFifos.push_back( make_pair(monFifo,sourceId) );

    // Put the first 10 words from each fifo in IS
    for( ushort iword = 0 ; iword < 10 ; iword++ ) {
      if( imon==0 )      m_flicNamed->MonCCMUX[iword] = monFifo->getBuffer()[iword];
      else if( imon==1 ) m_flicNamed->MonSRAM[iword] = monFifo->getBuffer()[iword];
      else if( imon==2 ) m_flicNamed->MonMERGE[iword] = monFifo->getBuffer()[iword];
      else if( imon==3 ) m_flicNamed->MonRTM[iword] = monFifo->getBuffer()[iword];
    }

    // Scan MonFifo0 and extract some track information
    if( imon==0 ) {
	for( ushort iword = 0 ; iword < 256 - 12 ; iword++ ) {
	  if( ((monFifo->getBuffer()[iword]) & 0xfff) == 0xbda ) {
	    m_hTrackSector->Fill( monFifo->getBuffer()[iword+1] );
	    m_hTrackChi2->Fill( monFifo->getBuffer()[iword+6] );
	    m_hTrackD0->Fill( monFifo->getBuffer()[iword+7] );
	    m_hTrackZ0->Fill( monFifo->getBuffer()[iword+8] );
	    m_hTrackCotth->Fill( monFifo->getBuffer()[iword+9] );
	    m_hTrackPhi->Fill( monFifo->getBuffer()[iword+10] );
	    m_hTrackCurv->Fill( monFifo->getBuffer()[iword+11] );
	  }
	}
    }
    
    // Scan MonFifo0 for an example RunNumber and Lvl1ID to put in IS
    /*
    if( imon==0 ) {
      for( ushort iword = 0 ; iword < 256 - 7 ; iword++ ) {
        if( monFifo->getBuffer()[iword] != 0xB0F0 ) continue;
        if( monFifo->getBuffer()[iword+1] != 0xCAF0 ) continue;
        if( monFifo->getBuffer()[iword+2] != 0xFF12 ) continue;
        if( monFifo->getBuffer()[iword+3] != 0xFF34 ) continue;
        break;
      }
    }
    */
    
  }

  // Arm MonFifos for next call to publish
  m_ctrl.ArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 );


  // Ship Mon FIFO data to EMon
  if( m_ftkemonDataOut ) {
    ERS_LOG( "Shipping Monitoring FIFO data to EMonDataOut ..." );
    m_ftkemonDataOut->sendData( vec_MonFifos );
    ERS_LOG( "Shipment complete" );
  }
}

/**************************/
void ReadoutModule_flic::localPublish()
/**************************/
{

  // Other attributes to be monitored in IS
  // Resize

  // XOFF counters
  m_flicNamed->Pipeline0.resize(4);
  m_flicNamed->SSB_XOFF.resize(4);
  m_flicNamed->SFP_FIFO.resize(4);
  m_flicNamed->CCMUX_FIFO.resize(4);
  m_flicNamed->SRAM_FIFO.resize(4);
  m_flicNamed->MERGE_FIFO.resize(4);
  m_flicNamed->RTM_FIFO.resize(4);

  // GLOBAL status words
  m_flicNamed->GLOBAL_STATUS.resize(4);
  m_flicNamed->Not_In_Sync.resize(4);
  m_flicNamed->SLink_Down.resize(4);
  m_flicNamed->XOFF_ROS.resize(4);
  m_flicNamed->RAW_SLINK_STATUS.resize(4);

  // Pipeline counters
  m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT.resize(4);
  m_flicNamed->NOT_IN_SYNC_COUNT.resize(4);
  m_flicNamed->HEADER_ERROR_COUNT.resize(4);
  m_flicNamed->TRACK_ERROR_COUNT.resize(4);
  m_flicNamed->TRAILER_ERROR_COUNT.resize(4);
  m_flicNamed->TRUNCATION_ERROR_COUNT.resize(4);
  m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT.resize(4);
  m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT.resize(4);
  m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT.resize(4);
  m_flicNamed->RTM_FIFO_EVENT_END_COUNT.resize(4);
  m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT.resize(4);
  m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT.resize(4);
  m_flicNamed->FLIC_FLOW_CTL.resize(4);
  m_flicNamed->L1_ID_MATCH_U3_COUNT.resize(4);
  m_flicNamed->L1_ID_MATCH_U4_COUNT.resize(4);
  m_flicNamed->L1_SKIP_ERRORs.resize(4);

  // Monitoring fifos
  m_flicNamed->MonCCMUX.resize(10);
  m_flicNamed->MonSRAM.resize(10);
  m_flicNamed->MonMERGE.resize(10);
  m_flicNamed->MonRTM.resize(10);

  // We need to still decide on this one
  // This is under development
  m_flicNamed->Pipeline0[0] = m_ctrl.GetRegister( m_flic , FLIC::targetFpgaU1 , 0x0105 ); // CCMUX_STATUS_REG

  // XOFFs from various parts of the pipeline
  ushort xoff_counts[28];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x0124 , xoff_counts , 28 );
  for( ushort i = 0 ; i < 4 ; i++ ) {
    // Loop over the four channels in U1
    m_flicNamed->SSB_XOFF[i]   = xoff_counts[0+i]; // SSB_XOFF
    m_flicNamed->SFP_FIFO[i]   = xoff_counts[4+i]; // SFP_FIFO
    m_flicNamed->CCMUX_FIFO[i] = xoff_counts[8+i]; // CCMUX_FIFO
    m_flicNamed->SRAM_FIFO[i]  = xoff_counts[12+i]; // SRAM_FIFO
    m_flicNamed->MERGE_FIFO[i] = xoff_counts[16+i]; // MERGE_FIFO
    m_flicNamed->RTM_FIFO[i]   = xoff_counts[20+i]; // RTM_FIFO

    // The global status word is read from registers 0x13C - 0x13F, immediately after the
    // XOFF counts. We grab it in the same block read to save time.
    m_flicNamed->GLOBAL_STATUS[i] = xoff_counts[24+i]; // GLOBAL STATUS words

    for( ushort flag = 0 ; flag < 16 ; flag++ ) {
      m_hGlobalStatus[i]->SetBinContent( flag+1 , (xoff_counts[24+i]>>flag) & 0x1 );
    }
    
    m_flicNamed->Not_In_Sync[i] = (xoff_counts[24+i]>>11) & 0x1;
    m_flicNamed->SLink_Down[i]  = (xoff_counts[24+i]>>8) & 0x1;
    m_flicNamed->XOFF_ROS[i]    = (xoff_counts[24+i]>>10) & 0x1;
    // Now check to see if the FLIC thinks the boards are sync'd
    for( ushort iLink = 0 ; iLink < 4 ; iLink++ ) {
      if( ((m_linksInUse>>iLink)&1) == 0 ) continue; // We don't care about this link
      if( ((xoff_counts[24+iLink]>>11)&1) == 1 ) {
        if ( m_verbose > 1 ) {
          std::stringstream message;
          message << "Link " << iLink << " did NOT sync, GLOBAL_STATUS = " << xoff_counts[24+iLink];
          daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
          ers::warning(ex);
        }
        else {
          ERS_LOG("Link " << iLink << " did NOT sync, GLOBAL_STATUS = " << xoff_counts[24+iLink] );
        }
      }
      if( ((xoff_counts[24+iLink]>>8)&1) == 1 ) {
        if ( m_verbose > 1 ) {
          std::stringstream message;
          message << "Slink " << iLink << " is down! ";
          daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
          ers::warning(ex);
        }
        else {
          ERS_LOG( "Slink " << iLink << " is down! ");
        }
      }
    }
    
    // Status definitions:
    //   FLIC_GLOBAL_STATUSes(i)(0) <=  SFP_FIFO_PROG_FULLs(i);         --      16: SFP input FIFO Prog Full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(1) <=  CCMUX_FIFO_PROG_FULLs(i);       --      17: CCMUX FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(2) <=  SRAM_FIFO_PROG_FULLs(i);        -- 18: SRAM Lookup FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(3) <=  MERGE_FIFO_PROG_FULLs(i);       -- 19: Merge FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(4) <=  RTM_FIFO_PROG_FULLs(i); -- 20: RTM FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(5) <=  COLLECTED_PIPELINE_PROG_FULLs(i)(5);    -- 21: User test Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(6) <=  U3_TAGGED_EVENT_FIFO_PROG_FULLs(i);     -- 22: Spy buffer Tag FIFO #1 Prog full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(7) <=  U4_TAGGED_EVENT_FIFO_PROG_FULLs(i);     -- 23: Spy buffer Tag FIFO #2 Prog full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(8) <=  SLINK_LDOWN_FLAGs(i);   -- 24: SLINK went down since last event was transmitted
    //   FLIC_GLOBAL_STATUSes(i)(9) <=  SLINK_LFF_FLAGs(i);     -- 25: SLINK FIFO FULL  (LFF)
    //   FLIC_GLOBAL_STATUSes(i)(10) <=  SLINK_XOFF_FLAGs(i);   -- 26: SLINK XOFF
    //   FLIC_GLOBAL_STATUSes(i)(11) <=  NOT_IN_SYNCs(i);       -- 27: NOT_IN_SYNC (Core Crate Receiver not synchronized to input)
    //   FLIC_GLOBAL_STATUSes(i)(12) <=  SLINK_CTL2_REG(i+4);   -- 28: User Defined status bit 1
    //   FLIC_GLOBAL_STATUSes(i)(13) <=  SLINK_CTL2_REG(i+8);   -- 29: User Defined status bit 2
    //   FLIC_GLOBAL_STATUSes(i)(14) <=  '0';   -- 30: Reserved for use as enable/disable of checking the STAT FIFOS
    //   FLIC_GLOBAL_STATUSes(i)(15) <=  '0';   -- 31: Reserved

  }

  // In addition to the global status, we can get the raw status of the SLINKs
  ushort slink_status;
  m_flic->singleread( FLIC::targetFpgaU1 , FLIC::cmdRead , 0x10E , &slink_status );
  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_flicNamed->RAW_SLINK_STATUS[i] = (slink_status>>(i*4))&0xf;
  }

  // Get the most recent L1IDs
  ushort l1ids[2];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x14E , l1ids , 2 );
  m_flicNamed->CURRENT_L1ID = l1ids[1] + (l1ids[0]<<16);
  
  // Processor counters
  for( ushort i = 0 ; i < 4 ; i++ ) {
    ushort proc_counts[0x10];
    m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x310+(i*0x10) , proc_counts , 0x10 );
    m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT[i]   = proc_counts[0x0];
    m_flicNamed->NOT_IN_SYNC_COUNT[i]           = proc_counts[0x1];
    m_flicNamed->HEADER_ERROR_COUNT[i]          = proc_counts[0x2];
    m_flicNamed->TRACK_ERROR_COUNT[i]           = proc_counts[0x3];
    m_flicNamed->TRAILER_ERROR_COUNT[i]         = proc_counts[0x4];
    m_flicNamed->TRUNCATION_ERROR_COUNT[i]      = proc_counts[0x5];
    m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[i]  = proc_counts[0x6];
    m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT[i]  = proc_counts[0x7];
    m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT[i] = proc_counts[0x8];
    m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT[i]  = proc_counts[0x9];
    m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT[i]  = proc_counts[0xa];
    m_flicNamed->RTM_FIFO_EVENT_END_COUNT[i]    = proc_counts[0xb];
    m_flicNamed->FLIC_FLOW_CTL[i]               = proc_counts[0xc];
    m_flicNamed->L1_ID_MATCH_U3_COUNT[i]        = proc_counts[0xd];
    m_flicNamed->L1_ID_MATCH_U4_COUNT[i]        = proc_counts[0xe];
    m_flicNamed->L1_SKIP_ERRORs[i]              = proc_counts[0xf];

    // Fill counter histogram
    for( ushort icounter = 0 ; icounter < 16 ; icounter++ ) {
      m_hPipelineCounters[i]->Fill( icounter , proc_counts[icounter] - m_last_proc_counts[i][icounter] );
      m_last_proc_counts[i][icounter] = proc_counts[icounter];
    }
 
    if( ((m_linksInUse>>i)&1) == 0 ) continue;
    usleep( 1000 ); 
    ushort proc_counts_compare[0x10];
    m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x310+(i*0x10) , proc_counts_compare , 0x10 );
    if (proc_counts[0x6] == proc_counts_compare[0x6]) {
      if ( m_verbose > 1 ) {
        std::stringstream message;
        message << "Link " << i << " may not be sending events to ROS, MERGE counter is: " << std::dec << proc_counts_compare[0x6];
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(),message.str());
        ers::warning(ex);
      }
      else ERS_LOG("Link " << i << " may not be sending events to ROS, MERGE counter is: " << std::dec << proc_counts_compare[0x6];);
    }
    if (proc_counts[0x8] == proc_counts_compare[0x8]) {
      if ( m_verbose > 1 ) {
        std::stringstream message;
        message << "Link " << i << " may not be processing events in CCR, CCMUX counter is: " << std::dec << proc_counts_compare[0x8];
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::warning(ex);
      }
      else ERS_LOG("Link " << i << " may not be processing events in CCR, CCMUX counter is: " << std::dec << proc_counts_compare[0x8]);
    }
    if (proc_counts_compare[0x1] != proc_counts[0x1]) {
      if ( m_verbose > 1 ) {    
        std::stringstream message;
        message << "Link " << i << " have not-in-sync errors, "<< std::dec << proc_counts_compare[0x1] << " over the past 0.1 second.";
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(),message.str());
        ers::warning(ex);
      }
      else ERS_LOG("Link " << i << " have not-in-sync errors, "<< std::dec << proc_counts_compare[0x1] << " over the past 0.1 second.");
    }
  }
  // Dump new words that show up in channel 0 flow control monitoring fifo
  if( m_verbose > 0 ) {
    // First read the FIFO
    ushort data[256];
    m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x118 , data , 256 );
    //for( size_t iword = 0 ; iword < 100 ; iword++ ) {
      // temporary dump of FIFO for added debugging
      //ERS_LOG( "Data flow word " << iword << " = " << std::hex << data[iword] << std::dec );
    //}
    for( size_t iword = 0 ; iword < 255 ; iword += 3 ) {
      if( data[iword] == data[iword+1] && data[iword]==data[iword+2] ) break;
      // otherwise we have found a new flow control word and time stamp
      uint word = data[iword];
      uint time = data[iword+2] + (data[iword+1]<<16);
      ERS_LOG( "FLIC sent flow control word " << std::hex << word << " at timestamp=" << std::hex << time << std::dec );
    }
    // Reset the FIFO
    m_ctrl.ResetArmFlowControlFIFOs( m_flic , FLIC::targetFpgaU1 );
  }

  // Dump some monitoring FIFO data to the log file
  if( m_verbose > 1 ) {
    
    ushort data[4][256];
    for( ushort i = 0 ; i < 4 ; i++ )
      m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x110+(2*i) , data[i] , 256 );

    if( data[0][0]==0x0 && data[0][1]==0x0 && data[0][2]==0x0 && data[0][3]==0x0 && data[0][4]==0x0 ) {
      // Looks like FIFOs are empty so don't bother printing
      // a bunch of zeros
    } else  {
      std::cout << "Monitoring FIFO Data for FPGA " << FLIC::targetFpgaU1 << std::endl;
      std::cout << std::setw(10) << "iAddr" << " : " << std::setw(10) << "M0" << std::setw(10) << "M1" << std::setw(10) << "M2" << std::setw(10) << "M3" << std::endl;
      std::cout << std::hex;
      for( ushort iword = 0 ; iword < 256 ; iword++ )
	std::cout << std::setw(10) << iword << " : "
		  << std::setw(10) << data[0][iword]
		  << std::setw(10) << data[1][iword]
		  << std::setw(10) << data[2][iword]
		  << std::setw(10) << data[3][iword]
		  << std::endl;
      std::cout << std::dec;
    }
    
    // Re-arm the monitoring FIFOs so they will catch to next available event, which
    // will get printed on the next call to publish
    m_ctrl.ArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 );
    
  }

  // Check in the IS info
  try { m_flicNamed->checkin(); }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception.
      daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Fill and publish histograms
  // Example:
  //m_histogram->Fill( rand()%100 - 50 );
  try {
    //m_ohProvider->publish( *m_hGlobalStatus , m_hGlobalStatus->GetName() , true );
    for( ushort i = 0 ; i < 4 ; i++ ) {
      m_ohProvider->publish( *m_hGlobalStatus[i] , m_hGlobalStatus[i]->GetName() , true );
      m_ohProvider->publish( *m_hPipelineCounters[i] , m_hPipelineCounters[i]->GetName() , true );
    }
    m_ohProvider->publish( *m_hTrackSector , m_hTrackSector->GetName() , true );
    m_ohProvider->publish( *m_hTrackChi2 , m_hTrackChi2->GetName() , true );
    m_ohProvider->publish( *m_hTrackD0 , m_hTrackD0->GetName() , true );
    m_ohProvider->publish( *m_hTrackZ0 , m_hTrackZ0->GetName() , true );
    m_ohProvider->publish( *m_hTrackCotth , m_hTrackCotth->GetName() , true );
    m_ohProvider->publish( *m_hTrackPhi , m_hTrackPhi->GetName() , true );
    m_ohProvider->publish( *m_hTrackCurv , m_hTrackCurv->GetName() , true );
  }
  catch( daq::oh::Exception & ex) { // Raise a warning or throw an exception.
    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
    ers::warning(e);  //or throw e;
  }
  //Stopless removal procedure
  if(m_HWError)
    {
      ERS_LOG("Trying to raise an hardware error; Application to be notified: " + m_appName);
      daq::rc::HardwareError removal( ERS_HERE , "FTK_Dummy_Busy_Channel" , m_appName.c_str());
      ers::error(removal);
      m_HWError=false; 
    }
  // Before ending publish, do some "stopless removal" checks...
  // FIXME!
  //
  // Check that ROS links are up, i.e. that LDOWN = 0 on channel 0
  /*
  if( (m_flicNamed->GLOBAL_STATUS[0] >> 8) & 1 ) {
    daq::rc::HardwareError e( ERS_HERE , (m_name+".Channel0").c_str() , m_appName.c_str() );
    ers::error( e );
  }
  */
  
  /*
  if( m_flicNamed->CCMUX_EVENTS_CHANNEL0 > 10000 ) {
    daq::rc::HardwareError e( ERS_HERE , (m_name+".DummyChannel").c_str() , m_appName.c_str() );
    ers::error( e );
  }
  */
  //usleep( 100000000 ); 
  //if(true){ 
  //  daq::rc::HardwareError e( ERS_HERE , "FTK_Dummy_Busy_Channel" , m_appName.c_str() );
  //  ers::error( e );
  //}     
  //New DataFlowReader
  m_dfReaderFlic = std::unique_ptr<DataFlowReaderFlic>(new DataFlowReaderFlic(m_ohRawProviderFlic, true, m_flic));
  m_dfReaderFlic->init(m_name, m_ipcpartition.name(), m_isServerName);
  try {
    m_dfReaderFlic->readIS();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  try {
    m_dfReaderFlic->publishAllHistos();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
}

//Function used to receive and parse the Usercommand
/**************************/
void ReadoutModule_flic::doUser(const daq::rc::UserCmd& cmd) 
/**************************/
{
  const std::string& cmdName = cmd.commandName();
  ERS_LOG("Received user command \"" + cmdName + "\" while in state \"" + cmd.currentFSMState() + "\"");
  
  if(cmdName == "raiseHWError") { 
    m_HWError=true;
  } 
  else {
    ERS_LOG("Not registered command:" + cmdName + "!");
  }
}

void ReadoutModule_flic::disable(const std::vector<std::string>& channelList) {
  //Sending the Disable_BP usercommand to FTK segment (will be caught by IM/DF)
  ERS_LOG("Received DISABLE command from Chip! Taking the proper actions...");
  const std::string& command_name = "Disable_BP";
  std::vector<std::string> params;
  rc::UserCmd command(command_name, params);
  rc::UserBroadcastCmd usrbc(command);
  rc::CommandSender sendr(m_ipcpartition.name(), m_appName);
  sendr.makeTransition("FTK", usrbc);
  ERS_LOG("Disable_BP usercommand sent to the FTK segment...");
  //
  // TAKE HERE THE PROPER FLIC ACTIONS
  //
}

void ReadoutModule_flic::user_sender(const std::string receiver, const std::string cmd_name, const std::vector<std::string>& cmd_params) { 
  ERS_LOG("Sending the command " + cmd_name);
  rc::UserCmd command(cmd_name, cmd_params);
  rc::UserBroadcastCmd usrbc(command);
  rc::CommandSender sendr(m_ipcpartition.name(), m_appName);
  sendr.makeTransition(receiver, usrbc);
  ERS_LOG("usercommand " + cmd_name + " sent to " + receiver);
}

void ReadoutModule_flic::ACKsender(const std::string state) {
  const std::string cmd_name = "ACK";
  std::vector<std::string> cmd_params;
  cmd_params.push_back(m_name);
  cmd_params.push_back(state);
  ERS_LOG("Sending the ACK: Transition " << state << " completed!");
  user_sender(m_manager, cmd_name, cmd_params);
}

void ReadoutModule_flic::getManager() {
  //Reading the Configuration..Trying to identify the Manager Application from the segment
  Configuration& confi=daq::rc::OnlineServices::instance().getConfiguration();
  const daq::core::Partition & partition = daq::rc::OnlineServices::instance().getPartition();
  const daq::core::Segment & segment = daq::rc::OnlineServices::instance().getSegment();
  std::vector<const daq::core::ResourceBase*> rb = segment.get_Resources();
  std::vector<const daq::core::Component*> disabledComponents = partition.get_Disabled();
  //Performing the check on all found applications what are the RM and what are the active ones
  for (std::vector<const daq::core::ResourceBase*>::iterator it = rb.begin(); it != rb.end(); it++) {
    const daq::df::RCD* rcdAppl = confi.cast<daq::df::RCD>(*it);
		if ( rcdAppl == 0 ) continue;
    std::vector<const daq::core::ResourceBase*> RMs = rcdAppl->get_Contains();
    for (std::vector<const daq::core::ResourceBase*>::iterator cit = RMs.begin(); cit != RMs.end(); cit++) {
      const daq::df::ReadoutModule* roModule = confi.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*cit);
      if (roModule==0) { //check if the object is a RM
              continue;
      }
      else if ((*cit)->disabled(partition)) { //check if the RM is enabled
        continue;
      } 
      else if (roModule->class_name()== "ReadoutModule_Manager") { //check if the RM is the manager Application
        ERS_LOG( "ReadoutModule " << roModule->UID()<< " is the Manager application!\n The Recovery ACKs will be sent to RCD " << rcdAppl->UID());
	m_manager=rcdAppl->UID();
        break;
      }
    }
  }
  if(m_manager == "") {
    std::stringstream message;
    message << "FTK WARNING: the RM " << m_name << " was not able to find the FTK Manager application. The Recovery procedure will fail!!";
    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
    ers::warning(ex);
  }
}

/**************************/
void ReadoutModule_flic::resynch(const daq::rc::ResynchCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  // Try to resynch each Proc with SSBs
  for( ushort target : m_ProcTargets )
    m_ctrl.SyncWithSSB( m_flic , target );

  ERS_LOG(cmd.toString() << " done");
}

/**************************/
void ReadoutModule_flic::clearInfo()
/**************************/
{
  ERS_LOG("Clear histograms and counters");

  // Reset histograms

  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_hGlobalStatus[i]->Reset();
    m_hPipelineCounters[i]->Reset();
  }
  m_hTrackSector->Reset();
  m_hTrackChi2->Reset();
  m_hTrackD0->Reset();
  m_hTrackZ0->Reset();
  m_hTrackCotth->Reset();
  m_hTrackPhi->Reset();
  m_hTrackCurv->Reset();
    
  // Reset the IS counters

  m_flicNamed->Pipeline0[0] = 0 ;


  for( ushort i = 0 ; i < 4 ; i++ ) {
    // Loop over the four pipelines in U1
    m_flicNamed->SSB_XOFF[i]	     = 0 ;
    m_flicNamed->SFP_FIFO[i]	     = 0 ;
    m_flicNamed->CCMUX_FIFO[i]	     = 0 ;
    m_flicNamed->SRAM_FIFO[i]	     = 0 ;
    m_flicNamed->MERGE_FIFO[i]	     = 0 ;
    m_flicNamed->RTM_FIFO[i]	     = 0 ;
    m_flicNamed->GLOBAL_STATUS[i]    = 0 ;
    m_flicNamed->RAW_SLINK_STATUS[i] = 0 ;
  }

  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT[i]   = 0 ;
    m_flicNamed->NOT_IN_SYNC_COUNT[i]           = 0 ;
    m_flicNamed->HEADER_ERROR_COUNT[i]          = 0 ;
    m_flicNamed->TRACK_ERROR_COUNT[i]           = 0 ;
    m_flicNamed->TRAILER_ERROR_COUNT[i]         = 0 ;
    m_flicNamed->TRUNCATION_ERROR_COUNT[i]      = 0 ;
    m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[i]  = 0 ;
    m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT[i]  = 0 ;
    m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT[i] = 0 ;
    m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT[i]  = 0 ;
    m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT[i]  = 0 ;
    m_flicNamed->RTM_FIFO_EVENT_END_COUNT[i]    = 0 ;
    m_flicNamed->FLIC_FLOW_CTL[i]               = 0 ;
    m_flicNamed->L1_ID_MATCH_U3_COUNT[i]        = 0 ;
    m_flicNamed->L1_ID_MATCH_U4_COUNT[i]        = 0 ;
    m_flicNamed->L1_SKIP_ERRORs[i]              = 0 ;
    // last counter currently unused
  }
  m_flicNamed->CURRENT_L1ID = 0 ;

  for( ushort i = 0 ; i < 10 ; i++ ) {
    m_flicNamed->MonCCMUX[i] = 0 ;
    m_flicNamed->MonSRAM[i] = 0 ;
    m_flicNamed->MonMERGE[i] = 0 ;
    m_flicNamed->MonRTM[i] = 0 ;
  }
}

//FOR THE PLUGIN FACTORY
extern "C" {
  extern ROS::ReadoutModule* createReadoutModule_flic();
}

ROS::ReadoutModule* createReadoutModule_flic() {
  return (new ReadoutModule_flic());
}
