/*****************************************************************/
/* Class: FLICController                                         */
/*  synopsis: For performing state transitions & debugging       */
/*   the FLIC production/prototype boards                        */
/*  author:  Jordan Webster jwebster@anl.gov                     */
/*  created: 2016.02.08                                          */
/*****************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <string>
#include <vector>
#include <limits>
#include <unistd.h>
#include <assert.h>

#include "flic/FLICController.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;



// --------------------- //
// Basic Setup Functions //
// --------------------- //

int FLICController::PICPowerOn( flic_comm *flic ) {
  return flic->singlewrite( FLIC::targetPic , FLIC::cmdWrite , FLIC::PIC::picReg_Command , 0x0001 , FLIC::statusNoError );
}

int FLICController::PICPowerOff( flic_comm *flic ) {
  return flic->singlewrite( FLIC::targetPic , FLIC::cmdWrite , FLIC::PIC::picReg_Command , 0x0000 , FLIC::statusNoError );
}

int FLICController::PICPowerFromIPMC( flic_comm *flic ) {
  return flic->singlewrite( FLIC::targetPic , FLIC::cmdWrite , FLIC::PIC::picReg_Command , 0x0002 , FLIC::statusNoError );
}

int FLICController::ProgramFPGAs( flic_comm *flic , const ushort arr_listReg[16] ) {
  int bytes = 0;

  // Setup list processor to program all 4 fpgas and maybe SRAMs
  // FIXME - move controls to another function

  //        15   14   13   12   11   10   09   08   07   06   05   04   03   02   01   00
  //      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  //      | EN | unused  |A/B |FLSH CS  |TRANSTYP |SRAM SELECT CODE   |  FPGA SELECT CODE |
  //      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  //
  // When you tell the FPGA to process the list, it reads every list register to see what it should do.  The bit-wise encoding is:
  //
  // EN : set if list element is enabled to do something, clear if it is not.  List processor will read and skip past non-enabled elements
  // until it hits the end.
  //
  // A/B : sub-selector bit associated with FLSH CS and TRANS_TYP.  FLSH CS selects which flash RAM (0,1,2,3) you want the list element
  // to use.  A/B, plus the TRANS_TYP, ends up selecting *where* in the selected flash you want to pull data from.  The coding is:
  //
  //      TRANS_TYPE : two-bit code indicating the type of transfer that will occur:
  //
  //              00: Read flash data and serialize to the FPGA as a programming operation.
  //              01: Read flash data as a list of (address,data) value pairs; write the data to the selected addresses.
  //              10: Read flash data as DMA transfer (full length of an SRAM), write the data in blocks of 64K
  //                      to the SRAM behind the selected FPGA.
  //              11: Reserved for future misuse.
  //
  //              The TRANS_TYPE, plus the A/B bit, defines what the starting address and length of the transfer will be:
  //
  //      TRANS_TYPE              A/B                     Addresses used within selected flash memory
  //              00                       X                      0x000000 - 0x29FFFF is the FPGA image area of the flash
  //              01                       0                      0x700000 - 0x71FFFF is reserved for register setup (pre-SRAM load)
  //              01                       1                      0x780000 - 0x79FFFF is reserved for register setup (post-SRAM load)
  //              10                       0                      0x300000 - 0x4FFFFF is SRAM data area #1
  //              10                       1                      0x500000 - 0x6FFFFF is SRAM data area #2
  //
  // addresses 0x2A0000 - 0x2FFFFF are a spare region of the flash
  // addresses 0x720000 - 0x77FFFF are a spare region of the flash
  // addresses 0x7A0000 - 0x7FFFFF are a spare region of the flash

  // FIXME - In SRAM transfer I could replace the (1,2,3,4) in second LS hexit with 'A' to simultaneously write all four LUTs using same FLASH source
  //         but this functionality is not yet tested
  FLIC::UDP_Data listReg;
  for( ushort i = 0 ; i < 256 ; i++ ) {
    if( i < 16 )
      listReg.word[i] = arr_listReg[i];
    else
      listReg.word[i] = 0x0000;
  }
  bytes += flic->blockwrite( FLIC::targetMgmtFpga , FLIC::cmdArrayWrite , FLIC::AUX::auxControlReg_Setup0 , listReg , 16 );

  // New control handler for mgmt FPGA tri-state control of FPGA bus.
  // Generally you don't want to enable the tri-state drivers until the FPGA has been powered.
  // Bit 2 of register 0 controls whether the drivers are enabled or not.
  bytes += flic->singlereadmodwrite( FLIC::targetMgmtFpga , FLIC::AUX::auxControlReg_Control0 , 0xFFFB , 0x1<<2 );

  // Read, clear bits 4,3,1, then set bits 4,3,1 to desired value of 01 and 1 respectively,
  // yielding all control over both flash and fpga buses to the management fpga.
  bytes += flic->singlereadmodwrite( FLIC::targetMgmtFpga , FLIC::AUX::auxControlReg_Control0 , 0xFFE5 , 0x000A );

  // Pulse the setup sequence bit
  bytes += flic->singlewrite( FLIC::targetMgmtFpga , FLIC::cmdSet , FLIC::AUX::auxPulseReg_Pulse0 , 0x0001 );

  // Check if everything is loaded
  bool isStarted  = false;
  bool isFinished = false;
  string summaryStr = "";
  while( !(isStarted && isFinished) ) {
    usleep( 100000 );
    bool setupRunning = CheckStatus( flic , summaryStr );
    if( setupRunning ) {
      isStarted = true;
    } else if( isStarted ) {
      isFinished = true;
    }
  }
  cout << summaryStr << endl;

  // Read, clear bits 4,3,1, then set bits 4,3,1 to desired value of 00 and 0 respectively.
  // PIC reclaims control of both buses.
  bytes += flic->singlereadmodwrite( FLIC::targetMgmtFpga , FLIC::AUX::auxControlReg_Control0, 0xFFE5, 0x0000);

  //bytes += CheckClockCounters( flic , FLIC::targetFpgaU1 );

  return bytes;
}

int FLICController::ProgramFPGAs( flic_comm *flic , vector<ushort> ProcTargets , vector<ushort> SSBeTargets , bool loadSRAMs ) {

  if( ProcTargets.size()==2 && SSBeTargets.size()==0 )
    return ProgramFPGAs( flic , (loadSRAMs ? FLIC::ListProc::DoubleProc_WithSRAMs : FLIC::ListProc::DoubleProc_NoSRAMs) );

  if( ProcTargets.size()==0 && SSBeTargets.size()==2 )
    return ProgramFPGAs( flic , FLIC::ListProc::DoubleSSBe );

  if( ProcTargets.size()==1 && SSBeTargets.size()==1 )
    // FIXME - not checking here to ensure that U1 is Proc and U2 is SSBe
    return ProgramFPGAs( flic , (loadSRAMs ? FLIC::ListProc::Loopback_WithSRAMs : FLIC::ListProc::Loopback_NoSRAMs) );
  
  return -1;
  
}


bool FLICController::CheckStatus( flic_comm *flic , string &summary ) {

  // Read the status words from U1,2,3,4 and MGMT FPGA
  // Then return a string summarizing the status

  ushort UiStatus[4];
  ushort AuxStatus;

  flic->singleread( FLIC::targetMgmtFpga , FLIC::cmdRead , FLIC::AUX::auxStatusReg_Setup , &AuxStatus );
  flic->blockread( FLIC::targetMgmtFpga , FLIC::cmdArrayRead , FLIC::AUX::auxStatusReg_FpgaU1 , UiStatus , 4 );

  // Dump to terminal for debugging

  string UiLabels[16] = {
    "Done Pin" ,
    "Init Pin" ,
    "Unused" ,
    "Unused" ,
    "Config Complete" ,
    "Done Error" ,
    "Init Low Error" ,
    "Init High Error" ,
    "SRAM 1 Loaded" ,
    "SRAM 2 Loaded" ,
    "SRAM 3 Loaded" ,
    "SRAM 4 Loaded" ,
    "Registers Complete" ,
    "Registers Error" ,
    "Config Flag" ,
    "RegLoad Bus Req"
  };

  string AuxLabels[16] = {
    "Flash 1 Status" ,
    "Flash 2 Status" ,
    "Flash 3 Status" ,
    "Flash 4 Status" ,
    "Setup Running" ,
    "Unused" ,
    "Unused" ,
    "Unused" ,
    "HW Address 0" ,
    "HW Address 1" ,
    "HW Address 2" ,
    "HW Address 3" ,
    "HW Address 4" ,
    "HW Address 5" ,
    "HW Address 6" ,
    "HW Parity"
  };

  ostringstream oss;
  oss << right << setw(5) << "U1" << setw(5) << "U2" << setw(5) << "U3" << setw(5) << "U4" << "   "
      << left << setw(30) << ""
      << right << setw(5) << "MGMT" << "   "
      << left << setw(30) << "" << endl;
  for( ushort j = 0 ; j < 16 ; j++ ) {
    oss << right << setw(5) << ((UiStatus[0]>>j)&0x1) << setw(5) << ((UiStatus[1]>>j)&0x1) << setw(5) << ((UiStatus[2]>>j)&0x1) << setw(5) << ((UiStatus[3]>>j)&0x1) << "   "
        << left << setw(30) << UiLabels[j]
        << right << setw(5) << ((AuxStatus>>j)&0x1) << "   "
        << left << setw(30) << AuxLabels[j] << endl;
  }

  string newSummary = oss.str();
  // if the status summary is different than input "summary" string,
  // then print it to the terminal and update the summary string.
  if( newSummary != summary ) {
    cout << newSummary << endl;
    summary = newSummary;
  }

  // return true if setup is running
  return (AuxStatus>>4) & 0x1;
}

bool FLICController::CheckStatus( flic_comm *flic ) {
  string tmp = "";
  return CheckStatus( flic , tmp );
}

int FLICController::InitClockCounters( flic_comm *flic , const ushort &target , const bool &rateMode ) {
  int bytes = 0;
  
  // Set the clock counters in the proper mode
  //  1 => rate mode
  //  0 => count
  // GTX112 is bit 12 in register A
  // GTX116/GEN is bit 14 in register A
  if( rateMode ) {
    bytes += flic->singlewrite( target , FLIC::cmdSet , 0x000A , 0x1000 );
    bytes += flic->singlewrite( target , FLIC::cmdSet , 0x000A , 0x4000 );
  } else {
    bytes += flic->singlewrite( target , FLIC::cmdClear , 0x000A , 0x1000 );
    bytes += flic->singlewrite( target , FLIC::cmdClear , 0x000A , 0x4000 );
  }

  return bytes;
}

int FLICController::InitProcCounters( flic_comm *flic , const ushort &target , const ushort &mode ) {
  int bytes = 0;

  // Turn all 16 counters (in each pipeline) to count mode.
  // (Rate mode = 1, Count mode = 0)
  bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x30 , mode );

  // The rate interval can be set by writing to 0x31 and 0x32, as follows...
  // case PIPELINE_RATE_INTERVALs((CHANNEL_COUNT*2)+1 downto (CHANNEL_COUNT*2)) is
  //   --32 bit vector in pairs 31/30, 29/28, ..., 3/2, 1/0
  //   when "00" =>
  //   INTERVAL_SAMP1(PIPELINE_COUNT)(CHANNEL_COUNT) <= INTERVAL_1SEC;
  //   when "01" =>
  //   INTERVAL_SAMP1(PIPELINE_COUNT)(CHANNEL_COUNT) <= INTERVAL_10MSEC;
  //   when "10" =>
  //   INTERVAL_SAMP1(PIPELINE_COUNT)(CHANNEL_COUNT) <= INTERVAL_100USEC;
  //   when "11" =>
  //   INTERVAL_SAMP1(PIPELINE_COUNT)(CHANNEL_COUNT) <= INTERVAL_1USEC;
  //   when others =>
  //   INTERVAL_SAMP1(PIPELINE_COUNT)(CHANNEL_COUNT) <= INTERVAL_1SEC;
  // end case;
  bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x31 , 0x0000 );
  bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x32 , 0x0000 );

  // Changing the U3/U4 L1_ID match counters to edge mode 
  bytes += flic->singlewrite( target , FLIC::cmdSet , 0x33 , 0x6000 );
  
  // Reset counters on each of the four pipelines using the reset pulsed
  // control reg 0x20E, one bit for each pipeline
  bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x20E , 0x000F );

  return bytes;
}

int FLICController::DumpDataOnTheFloorMode( daq::ftk::flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  // If the s-link is not up than data can get backed up and FIFOs will
  // get saturated after the first few events. To avoid this problem we set
  // a control register that tells the Flic to pretend that the s-link is up
  // so it will always send data out from the output FIFO
  bytes += flic->singlewrite( target , FLIC::cmdSet , 0x9 , 0xf ); // set the "force s-link up" bit in each of the four pipelines
  bytes += flic->singlewrite( target , FLIC::cmdSet , 0x209 , 0xf00 ); // reset s-link controller to acknowledge the above control
  return bytes;
}

bool FLICController::CheckClockCounters( flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  ushort counts[8];

  // Set all counters to "rate" mode
  //  1 => rate mode
  //  0 => count
  bytes += flic->singlewrite( target , FLIC::cmdSet , 0x000A , 0x1000 ); // GTX112 mode is bit 12
  bytes += flic->singlewrite( target , FLIC::cmdSet , 0x000A , 0x4000 ); // "GEN"/GTX116 modes are bit 14
  usleep( 10000 );

  // Now read and print the counter values
  bytes += flic->blockread( target , FLIC::cmdArrayRead , 0x0106 , counts , 8 );

  string CountLabels[8] = {
    "SRAM_REFCLK" ,
    "DDR_REFCLK" ,
    "GTX112_REFCLK" ,
    "GTX112_TXCLK" ,
    "GTX112_RXCLK" ,
    "GTX116_REFCLK" ,
    "GTX116_TXCLK" ,
    "GTX116_RXCLK"
  };

  int CountsExpected[8] = { 80 , 200 , 125 , 100 , 100 , 125 , 100 , 100 };

  // Print clock rates
  printf( "%20s %10s %20s\n" , "COUNTER" , "COUNT" , "EXPECTED (±2 MHz)" );
  for( ushort i = 0 ; i < 8 ; i++ ) {
    printf( "%20s %10i %20i\n" , CountLabels[i].c_str() , counts[i] , CountsExpected[i] );
  }

  // Check if rates match expectation
  bool countsMatch = true;
  for( ushort i = 0 ; i < 8 ; i++ ) {
    if( abs(counts[i]-CountsExpected[i]) > 2 ) {
      countsMatch = false;
      printf( "WARNING : %s does not match expected rate within threshold (%i != %i)\n" , CountLabels[i].c_str() , counts[i] , CountsExpected[i] );
    }
  }

  // If counts do not match, try resetting the GTX112/116 clocks and then redo the check
  if( !countsMatch ) {
    flic->singlewrite( target , FLIC::cmdWrite , 0x200 , 0xffff ); // Kick GTX112
    flic->singlewrite( target , FLIC::cmdWrite , 0x208 , 0xffff ); // Kick GTX116
    return CheckClockCounters( flic , target );
  }

  // returns true if all clock rates match expecation
  return countsMatch;

}


bool FLICController::CheckXOFFCounters( flic_comm *flic , const ushort &target ) {

  // Read XOFF registers
  ushort xoff[24];
  flic->blockread( target , FLIC::cmdArrayRead , 0x0124 , xoff , 24 );

  // XOFF signals are in groups of four, one signal for each pipe.
  // The groups of four are ordered as follows...
  string XOFFLabels[6] = {
    "SSB_XOFF" ,
    "SFP_FIFO" ,
    "CCMUX_FIFO" ,
    "SRAM_FIFO" ,
    "MERGE_FIFO" ,
    "RTM_FIFO"
  };

  // XOFF Counts expected after the board is configured and synced with SSBe
  ushort XOFFExpected[6] = { 5 , 0 , 0 , 0 , 0 , 0 };

  bool matchesExpectation = true;
  cout << setw(15) << "XOFF" << setw(15) << "CHANNEL0" << setw(15) << "CHANNEL1" << setw(15) << "CHANNEL2" << setw(15) << "CHANNEL3" << endl;
  for( ushort ixoff = 0 ; ixoff < 6 ; ixoff++ ) {
    cout << setw(15) << XOFFLabels[ixoff];
    for( ushort ipipe = 0 ; ipipe < 4 ; ipipe++ ) {
      ushort i = (ixoff*4) + ipipe;
      cout << setw(15) << xoff[i];
      if( xoff[i] != XOFFExpected[ixoff] ) matchesExpectation = false;
    }
    cout << endl;
  }

  // return high if any of the counters don't match counts that we expect after
  // configuring the board and syncing with SSBe
  return matchesExpectation;

}


//==================================================================
//                       Common setup
//==================================================================

int FLICController::SetupIP( flic_comm *flic ){
  int bytes = 0;
  //setup U3 and U4 eth 1&2 MAC to
  // 00-04-A3-B1-07-AA // 00-04-A3-B1-07-AB // 00-04-A3-B1-07-AC // 00-04-A3-B1-07-AD
  // 00-04-A3-B1-07-AE // 00-04-A3-B1-07-AF // 00-04-A3-B1-07-BA // 00-04-A3-B1-07-BB
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0017 , 0x07AA );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0018 , 0xA3B1 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0019 , 0x0004 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0037 , 0x07AB );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0038 , 0xA3B1 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0039 , 0x0004 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0017 , 0x07AC );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0018 , 0xA3B1 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0019 , 0x0004 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0037 , 0x07AD );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0038 , 0xA3B1 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0039 , 0x0004 );

  //setup U3 and U4 eth 1&2 IP to
  //10.153.37.27
  // 10.153.37.28
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x001A , 0x251F );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x001B , 0x0A99 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x003A , 0x251E );
  bytes+=flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x003B , 0x0A99 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x001A , 0x251D );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x001B , 0x0A99 );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x003A , 0x251B );
  bytes+=flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x003B , 0x0A99 );

  //setup U3 and U4 UDP port to
  //50010 // 50011 // 50012 // 50013
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x001C,0xC35A);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x003C,0xC35B);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x001C,0xC35C);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x003C,0xC35D);

  //setup destination MAC for U3 and U4 eth 1&2 to
  // 00:30:64:10:6A:F0 // 00:30:64:10:6A:F1 // 00:30:64:10:6A:F3 // 00:30:64:10:6A:F4
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x001D,0x6AF0);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x001E,0x6410);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x001F,0x0030);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x003D,0x6AF1);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x003E,0x6410);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x003F,0x0030);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x001D,0x6AF2);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x001E,0x6410);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x001F,0x0030);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x003D,0x6AF3);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x003E,0x6410);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x003F,0x0030);

  //setup destination IP for U3 and U4 eth 1&2 to
  //10.153.37.20 // 10.153.37.21 // 10.153.37.22 // 10.153.37.23
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0020,0x2514);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0021,0x0A99);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0040,0x2515);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0041,0x0A99);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0020,0x2516);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0021,0x0A99);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0040,0x2517);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0041,0x0A99);

  //setup destination port for U3 and U4 UDP to
  //50010 // 50011 // 50012 // 50013
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0022,0xC35A);
  bytes+=flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0042,0xC35B);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0022,0xC35C);
  bytes+=flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0042,0xC35D);

  return bytes;
}


int FLICController::SetupIPTest( flic_comm *flic ){
  int bytes = 0;

  // Test function for configuring U4/U3 IPs/MACs/Ports with different values
  // to see if we can establish communication with the WT blade.
  //
  // All of the source/destination addresses get set to the same values for now.
  
  ushort src_mac[3]  = { 0x07aa , 0xa3b1 , 0x0004 }; // 00-04-A3-B1-07-AA
  ushort src_ip[2]   = { 0x0202 , 0xc0a8 }; // 192.168.2.2
  ushort src_port[1] = { 50000 };

  ushort dst_mac[3]  = { 0xeccb , 0x640a , 0x0030 }; // 00:30:64:0A:EC:CB
  //ushort dst_mac[3]  = { 0xecca , 0x640a , 0x0030 };  // 00:30:64:0A:EC:CA
  //ushort dst_ip[2]   = { 0x0101 , 0xc0a8 }; // 192.168.1.1
  ushort dst_ip[2]   = { 0x0201 , 0xc0a8 }; // 192.168.2.1
  ushort dst_port[1] = { 50000 };

  //
  // Source (FLIC) info
  //
  
  for( ushort i = 0 ; i < 3 ; i++ ) {
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0017+i , src_mac[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0037+i , src_mac[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0017+i , src_mac[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0037+i , src_mac[i] );
  }

  for( ushort i = 0 ; i < 2 ; i++ ) {
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x001a+i , src_ip[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x003a+i , src_ip[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x001a+i , src_ip[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x003a+i , src_ip[i] );
  }

  bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x001c , src_port[0] );
  bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x003c , src_port[0] );
  bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x001c , src_port[0] );
  bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x003c , src_port[0] );

  //
  // Destination (Blade) info
  //
  
  for( ushort i = 0 ; i < 3 ; i++ ) {
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x001d+i , dst_mac[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x003d+i , dst_mac[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x001d+i , dst_mac[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x003d+i , dst_mac[i] );
  }

  for( ushort i = 0 ; i < 2 ; i++ ) {
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0020+i , dst_ip[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0040+i , dst_ip[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0020+i , dst_ip[i] );
    bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0040+i , dst_ip[i] );
  }

  bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0022 , dst_port[0] );
  bytes += flic->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x0042 , dst_port[0] );
  bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0022 , dst_port[0] );
  bytes += flic->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x0042 , dst_port[0] );

  return bytes;

}


int FLICController::SetupU3U4( flic_comm *flic,bool useSSBe) {
  int bytes=0;
  //reset RXTX
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x0002,0x078C);
  if(!useSSBe) bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x0002,0x078C);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdSet,0x0002,0x01EC);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdSet,0x0002,0x01EC);
  //switch to real data mode
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0012,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0013,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0014,0x000F);
  if(!useSSBe){
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0012,0x000F);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0013,0x000F);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0014,0x000F);
  }
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0004,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0005,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0006,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0004,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0005,0x000F);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0006,0x000F);
  //Reset GTX control logic
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0202,0x0FFF);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0204,0x0FFF);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0206,0x0FFF);
  if(!useSSBe){
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0202,0x0FFF);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0204,0x0FFF);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0206,0x0FFF);
  }
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0200,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0200,0xAAAA);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0200,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0202,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0202,0xAAAA);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0202,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0204,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0204,0xAAAA);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0204,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0200,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0200,0xAAAA);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0200,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0202,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0202,0xAAAA);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0202,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0204,0x5555);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0204,0xAAAA);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x0204,0x5555);
  //Enable GTX RX/TX logic
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x0002,0x078C);
  if(!useSSBe) bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdClear,0x0002,0x078C);
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdClear,0x0002,0x01EC);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdClear,0x0002,0x01EC);
  return bytes;
}

int FLICController::EventTagging( flic_comm *flic,bool useSSBe){//,unsigned int &mask1,unsigned int &mask2){
  int bytes=0;
  int mask1=0x0010, mask2=0x0010;
  //Config the routing of event to U3/U4 using L1ID
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x002A,mask1);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x002B,mask2);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x002C,mask1);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x002D,mask2);
  if(!useSSBe){
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0012,mask1);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0013,mask2);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0014,mask1);
    bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0014,mask2);
  }
  return bytes;
}

int FLICController::ResetAssemblyLogic(flic_comm *flic){
  int bytes=0;
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdSet,0x0001,0x020F);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdSet,0x0001,0x020F);
  return bytes;
}

// PW - Feb. 20, 2017: this function is only used by flic_udp
int FLICController::SetupTagChAndEth( flic_comm *flic){//,unsigned int value=0xB1FF){
  int bytes=0;
  //int value=0x1101;
  bytes += flic->singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x006A,0x1001);
  bytes += flic->singlewrite(FLIC::targetFpgaU4,FLIC::cmdWrite,0x006A,0xE101);
  return bytes;
}


int FLICController::ResetSERDES_Proc( flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x2,0x6060);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x200,0x4444);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x200,0x8888);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x200,0x4444);
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x3,0xf0);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x208,0x4444);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x208,0x8888);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x208,0x4444);
  bytes += flic->singlewrite(target,FLIC::cmdClear,0x2,0x6060);
  return bytes;
}

int FLICController::ResetSERDES_SSBe( flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x2,0x60);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x200,0xffff);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1f,0xf0);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1f,0x00);
  bytes += flic->singlewrite(target,FLIC::cmdClear,0x2,0x60);
  return bytes;
}


int FLICController::PulseFlowControlToSSB( flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  // Pulse GTX112 reset
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x2,0x60);
  bytes += flic->singlewrite(target,FLIC::cmdClear,0x2,0x60);
  return bytes;
}


/////////////////////////////////////////////////
//               Clear all FIFOs               //
/////////////////////////////////////////////////
/* Register descriptions received from Michael  
   Need to update this for the next relase
   Register descriptions from Michael regarding the FIFO clearance check
   Register 0x20F
     Bit 8 (0x0100): This is the one and only SRAM reset.
     Bit 9 (0x0200): This resets the inter-FPGA transceiver FIFOs.  That’s GTX 113, 114 and 115.
     Bit 13 (0x2000): Looks like this bits has been doubly conscribed.  In addition to being assigned to the SLINK logic, is also resets the three inter-FPGA transceiver state machines.
   To check if the FIFOs are cleared:
     FIFO_EMPTY_REG (Address 0x140):
       3..0: SFP FIFOs
       7..4: CCMUX FIFOs
       11..8: MERGE FIFOs
       15..12: RTM FIFOs
     Additional status registers for other state flags.  These follow the same pattern as the FIFO_EMPTY_REG above.
     FIFO_PROG_EMPTY_REG (Address 0x141) 
     FIFO_PROG_FULL_REG (Address 0x142)
     FIFO_FULL_REG (Address 0x143) 
   Register 0x20E
     This clears the counters in the 4 data pipelines.
   To check if the FIFOs are cleared
     Registers 0x300 - 0x33F
       0: SFP_FIFO_FULL_ERROR_COUNT increments each time the SFP FIFO goes full.
       1: NOT_IN_SYNC_COUNT increments each time the CoreCrateRcvr goes out of sync. 
       2: HEADER_ERROR_COUNT increments each time the CoreCrateRcvr detects a header error.
       3: TRACK_ERROR_COUNT increments each time the CoreCrateRcvr detects a track error.
       4: TRAILER_ERROR_COUNT increments each time the CoreCrateRcvr detects a trailer error.
       5: TRUNCATION_ERROR_COUNT increments each time the CoreCrateRcvr truncates an event.
       6: CCMUX_FIFO_EVENT_END_COUNT increments each time the CoreCrateRcvr finishes processing an event.
       7: SRAM_FIFO_EVENT_READ_COUNT increments each time one event of SRAM FIFO data has been read by the merge process.
       8: MERGE_FIFO_EVENT_READ_COUNT increments each time one event of the merge data has been read by the SLINK_PRIMITIVE process.
       9: U3_TAGGED_EVENT_FIFO_COUNT increments for each event sent by the pipeline to FPGA "U3".
       10: U4_TAGGED_EVENT_FIFO_COUNT increments for each event sent by the pipeline to FPGA "U4".
       11: RTM_FIFO_EVENT_END_COUNT increments for each event is sent to the Hola Core logic by SLINK_PRIMITIVE.
       12: FLIC_FLOW_CTL measures the percentage of time flow control is being asserted by any cause OTHER than S-Link.
       13: reserved for future misuse
       14: reserved for future misuse
       15: reserved for future misuse
   Register 0x20C 
     This resets the 8 monitor FIFOs in the FLIC.  These FIFOs are only read via register access.
   To check if the FIFOs are cleared
     Register 0x121
        Bits 7..0: These bits report the state of the 8 separate FIFO empty flags.
         Bits 15..8: These bits report the state of the 8 separate FIFO full flags.  
*/
int FLICController::ClearAllFIFOs( flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  //*************************************************************************************
  //Clear individual FIFOs on 20F--See registr map page 49 SUBSECTION_PULSED_RESETS_REG *
  //*************************************************************************************
  //CCMUX FIFO resets
  //printf( "Clear CCMUX FIFOs!" );
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20F,0x00F0);
  //Resets all Merge state machines and all Merge FIFOs
  //printf( "Clear Merge FIFOs!" );
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20F,0x1000);
  //Resets all GTX 116 event FIFOs
  //printf( "Clear GTX 116 FIFOs!" );
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20F,0x4000);
  //Resets all GTX 116 FIFO reader machines
  //printf( "Clear GTX 116 reader machines!" );
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20F,0x8000);
  //Clear the above all at once
  printf( "Clear FIFOs in 0x20F." );
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20F,0xD0F0);
  //*************************************************************************************
  //Clear DDR/SRAM clock frequency counters. See register map page 28 COUNTER_CTL_REG   *
  //*************************************************************************************
  //They are for diagonistic purposes, not eseential--FIXME
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0xA,  0x8000);
  //*************************************************************************************
  //Clear per-pipeline counters. See register map page 49 PULSED_CTL_REG_20E            *
  //*************************************************************************************
  //Bit 0,1,2,3 correspond to pipeline 1,2,3,4
  printf( "Clear Pipeline FIFOs!" );
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20E,0x000F);
  //*************************************************************************************
  //Clear monitor FIFOs. See register map page 48 PULSED_CTL_REG_20C                    *
  //*************************************************************************************
  //Lower 8 bits clear monitoring FIFOs
  printf( "Clear Monitoring FIFOs!" );
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20C,0x00FF);
  return bytes;
}

//Old implementation can be deleted after testing the new one thoroughly 
int FLICController::ClearFIFOFullFlags( flic_comm *flic, const ushort &target ) {
  return flic->singlewrite(target,FLIC::cmdWrite,0x020f,0xD0F0);
}

bool FLICController::CheckFIFOStatus(flic_comm *flic, const ushort &target, ushort linksInUse ) {
  bool FIFOCleared = true;
  
  //Turning off for now: Check all pipeline counters, 4 * 16, 64 in total  
  /*
    for( ushort i = 0 ; i < 4 ; i++ ) {
    ushort proc_counts[0x10];
    flic->blockread( target , FLIC::cmdArrayRead , 0x310+(i*0x10) , proc_counts , 0x10 );
    for (ushort j = 0; j < 16; j++) { 
      cout<< "Register "<< 0x310+(i*0x10) << " counter "<<j<<proc_counts[j] <<endl; 
      //if (proc_counts[j] != 0x0) {
      //  FIFOCleared = false;
      //  return FIFOCleared;
      //} 
    }
  }
  */
  //Check the monitor FIFOs
  ushort MonitorFIFOEmpty;
  for (ushort iAttempt = 0; true; iAttempt++) {
    if (iAttempt > 3) return FIFOCleared;
    flic->singleread( target , FLIC::cmdRead , 0x121 , &MonitorFIFOEmpty );
    if (MonitorFIFOEmpty != 0x00ff) {
      FIFOCleared = false;
      return FIFOCleared;
    }
  }  
  
  ushort FIFORegEmpty;
  for (ushort iAttempt = 0; true; iAttempt++) {
    if (iAttempt > 3) return FIFOCleared;
    flic->singleread( target , FLIC::cmdRead , 0x140 , &FIFORegEmpty);
    if (!FIFORegEmpty && (0xfff0 + linksInUse)) {
      FIFOCleared = false;
    }  
  }
  
  ushort FIFORegProgEmpty; 
  for (ushort iAttempt = 0; true; iAttempt++) {
    if (iAttempt > 3) return FIFOCleared;
    flic->singleread( target , FLIC::cmdRead , 0x141 , &FIFORegProgEmpty);
    if (!FIFORegProgEmpty && (0xfff0 + linksInUse)) {
      FIFOCleared = false;
      return FIFOCleared;
    }
  }  
  
  ushort FIFORegProgFull; 
  for (ushort iAttempt = 0; true; iAttempt++) {
    if (iAttempt > 3) return FIFOCleared;
    flic->singleread( target , FLIC::cmdRead , 0x142 , &FIFORegProgFull);
    if (FIFORegProgFull != 0x0000) {
      FIFOCleared = false;
      return FIFOCleared;
    }
  }  
  
  ushort FIFORegFull;
  for (ushort iAttempt = 0; true; iAttempt++) {
    if (iAttempt > 3) return FIFOCleared;
    flic->singleread( target , FLIC::cmdRead , 0x143 , &FIFORegFull);
    if (FIFORegFull != 0x0000) {
      FIFOCleared = false;
      return FIFOCleared;
    }
  }  

  return FIFOCleared;
}
  
//==================================================================
//                       Processor + Processor
//==================================================================



//==================================================================
//                       Processor + SSBe
//==================================================================

/*
  int FLICController::ResetSERDES_SSBe( flic_comm *flic ) {
  int bytes=0;
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x2,0x60);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x2,0x6060);
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x200,0xffff);
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0xf0);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x4444);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x8888);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x4444);
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0x00);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x3,0xf0);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x4444);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x8888);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x4444);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x2,0x6060);
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdClear,0x2,0x60);
  return bytes;
  }
*/

int FLICController::IdleBoard( flic_comm *flic ) {
  // Optionally check if board is programmed?
  // By reading addresses from AUX and looking at bit 0.
  //return ResetSERDES_SSBe( flic );
  int bytes = 0;
  //bytes += ResetSERDES_Proc( flic , FLIC::targetFpgaU1 );
  //bytes += ResetSERDES_SSBe( flic , FLIC::targetFpgaU2 );

  //
  // temp changes to match Setup function in flicware
  //

  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x2,0x60); // put U2 112 SM in Reset
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x2,0x6060); // Put U1 112/116 SM in Reset
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x200,0xffff); // Whack U2 GTX112
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0xf0); // Set U2 GTX112 to Send Commas

  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x4444); // Reset U1 GTX112 RX PLL
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x8888); // Reset U1 GTX112 RX logic
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x4444); // Reset U1 GTX112 RX PLL

  // new
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x1111); // Reset U1 GTX112 TX PLL
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x2222); // Reset U1 GTX112 TX logic
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x1111); // Reset U1 GTX112 TX PLL

  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0x00); // ?
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x3,0xf0); // Set RTM TDIS Must be done before GTX116 Resets
  //bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x4444);
  //bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x8888);
  //bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x4444);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0xffff); // Whack U1 GTX116
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x2,0x6060); // Take U1 112/116 SM out of Reset

  // new
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x4,0xf); // Clear TDIS bits in SFT Ctl Reg
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0xf,0xf0); // Set TX Mux bits of U1 to send flow control data, not PRBS data

  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdClear,0x2,0x60); // Take U2 112 SM out of Reset

  return bytes;
}

int FLICController::ResetStateMachines_Proc( flic_comm *flic , const ushort &target ) {
  //Pulse reset of all SMs on U1.
  int bytes = 0;
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20f,0xffff);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1f,0xffff);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20f,0xffff);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1f,0x0000);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x20f,0xffff);
  return bytes;
}

int FLICController::ResetStateMachines_SSBe( flic_comm *flic , const ushort &target ) {
  //Pulse reset of SSB Emulator or U2
  return flic->singlewrite(target,FLIC::cmdWrite,0x201,0xff);
}

int FLICController::ResetStateMachines( flic_comm *flic ) {
  int bytes = 0;

  //Pulse reset of all SMs on U1.
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x1f,0xffff);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x1f,0x0000);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);

  //Pulse reset of SSB Emulator or U2
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201,0xff);
  //same as GTX112 reset. Should be done. Here I reset the PRBS machine...

  return bytes;
}

int FLICController::ResetSLinks( flic_comm *flic , const ushort &target ) {
  int bytes=0;

  // For setup of processing FPGAs

  // Clear the GO bits to the reset machines
  bytes += flic->singlewrite(target,FLIC::cmdClear,0x00000003, 0x0F00);

  // Request the reset machines to re-arm
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x00000209, 0x0F00);

  // Set the GO bits to the reset machines
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x00000003, 0x0F00);

  return bytes;
}

int FLICController::InitStateMachines_Proc( flic_comm *flic , const ushort &target ) {
  int bytes = 0;

  // Set SRAM access to state machine (bit 3 of GENERAL_CONTROL clear) in U1 (should already be so)
  // Enable Core Crate receiver machines (set bit 1 of GENERAL_CONTROL) in U1
  // Enable data merge machines (set bit 4 of GENERAL_CONTROL) in U1
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x00000002,0x0012);
  bytes += ResetStateMachines_Proc( flic , target );

  // Enable the SLINK reset state machine to go
  bytes += ResetSLinks( flic , target );

  // Switch SRAM control back to state machine by setting bit 3 of GENERAL_CONTROL register
  // (FIXME: is this still relevent? The bit may be out of date)
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x00000002, 0x0008);

  // Finally, set the FLIC status words in U1 to 0x1515 and 0x1616, respectively
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0x00000015, 0x0000);
  //bytes += flic->singlewrite(target,FLIC::cmdWrite,0x00000016, 0x0000);

  // Set bits 7:4 of the SFP_CTL_REG in U1 to have the FLIC sends flow control messages rather than PRBS data to the SSB.
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x0004, 0x00f0);
  //  bytes += flic->singlewrite(target,FLIC::cmdSet,0x0004, 0x1000);//enbale fake fifo
  //  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x002F, 0x000f);//reset fake fifo

  // Make sure all FIFOs are empty.
  bytes+=ClearFIFOFullFlags( flic , target );
  //Change to the new FIFO clear function.
  //bytes+=ClearAllFIFOs( flic , target );

  // Change SRAM access to the lookup process
  bytes += flic->singlewrite( target , FLIC::cmdSet , 0x2 , 0x1);

  return bytes;
}

int FLICController::InitStateMachines_SSBe( flic_comm *flic , const ushort &target ) {
  int bytes = 0;

  bytes += ResetStateMachines_SSBe( flic , target );

  // Set the SSB emulator in U2 to have status words of 0xDDDD and 0xEEEE
  // FIXME: for runs with real SSBe
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x0000001D, 0xDDDD);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x0000001E, 0xEEEE);

  // Set bits 1:0 of the ILA_MUX_CTL_REG in U2 to have the SSBe response to the flow control messages
  bytes += flic->singlewrite(target,FLIC::cmdSet,0x000B, 0x0003);

  return bytes;
}

int FLICController::InitStateMachines( flic_comm *flic ) {
  int bytes = 0;

  InitStateMachines_Proc( flic , FLIC::targetFpgaU1 );
  InitStateMachines_SSBe( flic , FLIC::targetFpgaU2 );

  /*
  // Set SRAM access to state machine (bit 3 of GENERAL_CONTROL clear) in U1 (should already be so)
  // Enable Core Crate receiver machines (set bit 1 of GENERAL_CONTROL) in U1
  // Enable data merge machines (set bit 4 of GENERAL_CONTROL) in U1
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000002,0x0012);
  bytes += ResetStateMachines(flic);
  // Enable the SLINK reset state machine to go
  bytes += ResetSLinks(flic);
  // Switch SRAM control back to state machine by setting bit 3 of GENERAL_CONTROL register
  // (FIXME: is this still relevent? The bit may be out of date)
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000002, 0x0008);
  // Set the SSB emulator in U2 to have status words of 0xDDDD and 0xEEEE
  // FIXME: for runs with real SSBe
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0000001D, 0xDDDD);
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0000001E, 0xEEEE);
  // Finally, set the FLIC status words in U1 to 0x1515 and 0x1616, respectively
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000015, 0x0000);
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000016, 0x0000);
  // Set bits 7:4 of the SFP_CTL_REG in U1 to have the FLIC sends flow control messages rather than PRBS data to the SSB.
  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x0004, 0x00f0);
  //  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x0004, 0x1000);//enbale fake fifo
  //  bytes += flic->singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x002F, 0x000f);//reset fake fifo
  //set bits 1:0 of the ILA_MUX_CTL_REG in U2 to have the SSBe response to the flow control messages
  bytes += flic->singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x000B, 0x0003);
  // Make sure all FIFOs are empty.
  bytes+=ClearFIFOFullFlags(flic,FLIC::targetFpgaU1);
  //Change to the new FIFO clear function
  bytes+=ClearAllFIFOs( flic , target );
  // Change SRAM access to the lookup process
  bytes += flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x2 , 0x1);
  */

  // NOW READY TO SEND DATA FROM SSBe
  return bytes;
}

int FLICController::SyncWithSSB( flic_comm *flic , const ushort &target ) {

  int bytes=0;
  // Request the reset machines to re-arm
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x00000209, 0x0F00); // remove? -JW
  bytes += flic->singlewrite(target,FLIC::cmdClear,0x0004, 0x4000); // flow control mode normal // remove? -JW
  //bytes += flic->singlewrite(target,FLIC::cmdClear,0x0004, 0x2000); // based on JW's comment this should be acting on bit 13
  // Send one word of 0x0010 to the SSB asking for one word for resync
  bytes += flic->singlewrite(target, FLIC::cmdWrite, 0x00000201, 0x0F00);

  // Reset GTX116 event FIFO (0x4000)
  // Reset the SRAM address FIFO (0x0100)
  // Reset Entire back end (0xcd00)
  //bytes += flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xcd00 );

  return bytes;

}


//==================================================================
//                    Trigger SSBe Data
//==================================================================

int FLICController::StartSSBeData( flic_comm *flic , const ushort &target , uint channelMask , int nevents , int tracksPerEvent , int delayBetweenEvents , bool sendData ) {

  int bytes = 0;

  // Ask user to input SSBe parameters if they are not set
  // by default in function call

  while( channelMask < 0x1 || channelMask > 0xf ) {
    printf( "Enter channel mask in hex [e.g. 0x1=first channel only, 0xf=all channels] : " );
    scanf( "%x" , &channelMask );
  }

  while( nevents < 0 || nevents > 32767 ) {
    printf( "Enter number of events you would like to run [1-32767, 0=continuous] : " );
    scanf( "%d" , &nevents );
  }

  while( tracksPerEvent < 0 || tracksPerEvent > 255 ) {
    printf( "Enter fixed number of tracks per event [0-255] : " );
    scanf( "%d" , &tracksPerEvent );
  }

  while( delayBetweenEvents < 1 || delayBetweenEvents > 4095 ) {
    printf( "Enter fixed delay between events [1-4095] : " );
    scanf( "%d" , &delayBetweenEvents );
  }

  // Set control parameters
  if( nevents > 0 ) {
    for( ushort channel = 0 ; channel < 4 ; channel++ ) {
      if( (channelMask>>channel)&0x1 ) {
        bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x14+channel , nevents-1 );
      }
    }
  } else {
    // continuous data
    bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x1f , channelMask<<8 );
  }

  for( ushort channel = 0 ; channel < 4 ; channel++ ) {
    if( (channelMask>>channel)&0x1 ) {
      bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x20+channel , 0x8000+tracksPerEvent );
      bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x18+channel , 0x8000+delayBetweenEvents );
    }
  }

  // Turn on "ENABLE_PAUSE" mode so SSBe pays attention to flow control
  bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x2d , 0x1111 );
  // To turn on long l1id mode, I think we need to set bits 0x8888 in 0x2d as well
  // and set bits 0xf000 in 0x1f
  // e.g.
  //bytes += flic->singlewrite( target , FLIC::cmdSet , 0x1f , 0xf000 ); // 0x0f00 should be high for continuous data
  //bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x2d , 0x9999 );

  // Send
  if( sendData )
    bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x201 , channelMask<<12 );

  return bytes;
}

int FLICController::RestartSSBeData( flic_comm *flic , const ushort &target , uint channelMask ) {
  // Restart the SSBe data without making any modifications to nevents, ntracks, or delay,
  // i.e. keeping the latest configuration
  while( channelMask < 0x1 || channelMask > 0xf ) {
    printf( "Enter channel mask in hex [e.g. 0x1=first channel only, 0xf=all channels] : " );
    scanf( "%x" , &channelMask );
  }
  return flic->singlewrite( target , FLIC::cmdWrite , 0x201 , channelMask<<12 );
}

int FLICController::StopSSBeData( flic_comm *flic , const ushort &target , uint channelMask ) {
  // Stop continuous dataflow on the designated channels
  while( channelMask < 0x1 || channelMask > 0xf ) {
    printf( "Enter channel mask in hex [e.g. 0x1=first channel only, 0xf=all channels] : " );
    scanf( "%x" , &channelMask );
  }
  return flic->singlewrite( target , FLIC::cmdClear , 0x1f , channelMask<<8 );
}

int FLICController::SendFIFOData( flic_comm *flic , const ushort &target , uint channelMask ) {
  while( channelMask < 0x1 || channelMask > 0xf ) {
    printf( "Enter channel mask in hex [e.g. 0x1=first channel only, 0xf=all channels] : " );
    scanf( "%x" , &channelMask );
  }
  return flic->singlewrite( target , FLIC::cmdWrite , 0x201 , channelMask<<8 );
}

int FLICController::ResetSSBe( flic_comm *flic , const ushort &target ) {
  int bytes = 0;
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1f,0x0); // turn off continuous events
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x201,0x0ff); // rwang f
  return bytes;
}

int FLICController::ChangeFixedDelay( flic_comm *flic , const ushort &target , const ushort &delay ) {
  assert( delay < 0x1000 );
  int bytes = 0;
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x18,0x8000+delay);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x19,0x8000+delay);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1a,0x8000+delay);
  bytes += flic->singlewrite(target,FLIC::cmdWrite,0x1b,0x8000+delay);
  return bytes;
}

//
// Load a testvector file and use 16-bit words to populate "testvectors" variable,
// which can be used to write FIFOs on the SSBe
//
void FLICController::loadTestVectorsFromFile( const char *path , bool verbose , int l1id ) {
  testvectors.clear();
  ushort word;
  ifstream inputFile( path );
  assert( inputFile.is_open() );
  inputFile >> hex;
  ushort iline = 0;
  while( inputFile >> word ) {
    // If l1id is non-negative, then assume the user wants to replace the Level1ID in the event
    // with the input argument
    if( l1id >= 0 ) {
      if(      iline == FLIC::EventFormat::INPUT_L1ID_POSITION )   word = (l1id >> 16);
      else if( iline == FLIC::EventFormat::INPUT_L1ID_POSITION+1 ) word = (l1id & 0xffff);
    }
    testvectors.push_back( word );
    // skip the remainder of the line
    inputFile.ignore( numeric_limits<streamsize>::max() , '\n' );
    iline++;
  }
  inputFile.close();
  assert( testvectors.size() <= 8192 && "Testvector file is too long to fit in FIFOs" );
  if( verbose ) std::cout << "testvectors.size() = " << testvectors.size() << std::endl;
}

int FLICController::WriteTestVectorsOneChannel( flic_comm *flic , const ushort &target , const ushort &channel , bool verbose ) {
  int bytes = 0;
  ushort nBlockWrites = (testvectors.size() / 256) + 1;
  for( size_t iBlock = 0 , iWord = 0 ; iBlock < nBlockWrites ; iBlock++ ) {
    FLIC::UDP_Data block;
    ushort size = 0;
    for( ; iWord < testvectors.size() && size < 256 ; size++ , iWord++ ) {
      // Fill the next block of 256 words. End the loop once the block is full
      // or we are out of testvector data
      block.word[size] = testvectors[iWord];
    }
    if( verbose ) cout << "\rWriting Channel " << channel << " Block " << iBlock + 1 << " / " << nBlockWrites << " (" << iWord << " total words)" << flush;
    bytes += flic->blockwrite( target , FLIC::cmdFifoWrite /*FLIC::cmdArrayWrite*/ , 0x80+channel , block , size );
    usleep(10000);
  }
  if( verbose ) cout << endl;
  return bytes;
}

int FLICController::WriteTestVectorsFourChannels( flic_comm *flic , const ushort &target , bool verbose ) {
  int bytes = 0;
  bytes += WriteTestVectorsOneChannel( flic , target , 0 , verbose );
  bytes += WriteTestVectorsOneChannel( flic , target , 1 , verbose );
  bytes += WriteTestVectorsOneChannel( flic , target , 2 , verbose );
  bytes += WriteTestVectorsOneChannel( flic , target , 3 , verbose );
  return bytes;
}




// ----------------------//
// Run Control Functions //
// ----------------------//

int FLICController::setup( flic_comm *flic ) {
  int bytes = 0;
  
  while( reloadFirmware ) {

    bytes += PICPowerOn( flic );
    bytes += ProgramFPGAs( flic );

    // ensure that clocks come up correctly after program
    // if they don't then reprogram
    if( CheckClockCounters( flic , FLIC::targetFpgaU1 ) ) break;

    // Just kick the GTX112 clocks in case they don't come up correctly
    //flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
    
    bytes += PICPowerOff( flic );
    usleep( 100000 );

  }

  // Initialize SFP_CTL_REG in normal mode rather than setup mode so that we
  // don't confuse the SSB with a bunch of non-comma words.
  flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0004 , 0x00F0 );
  
  /*
  bytes += IdleBoard( flic );
  bytes += InitStateMachines( flic );
  if( dumpDataOnTheFloor )
    bytes += DumpDataOnTheFloorMode( flic , FLIC::targetFpgaU1 );
  bytes += ResetArmFlowControlFIFOs( flic , FLIC::targetFpgaU1 );
  */

  FLICSetupTool t;
  t.setFlicComm( flic );
  t.INIT_U1_U2_U3_U4();
  
  return bytes;
}

int FLICController::configure( flic_comm *flic ) {
  int bytes = 0;
  
  //bytes += IdleBoard( flic );
  //bytes += InitStateMachines( flic );

  // Whack the Rx's on both processor and SSBe
  flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  
  if( dumpDataOnTheFloor )
    bytes += DumpDataOnTheFloorMode( flic , FLIC::targetFpgaU1 );

  bytes += ResetArmFlowControlFIFOs( flic , FLIC::targetFpgaU1 );
  
  return bytes;
}

int FLICController::connect( flic_comm *flic ) {
  int bytes = 0;

  // Set all counters to "rate" mode
  //  1 => rate mode
  //  0 => count
  bytes += flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x000A , 0x1000 ); // GTX112 mode is bit 12
  bytes += flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x000A , 0x4000 ); // "GEN"/GTX116 modes are bit 16
  
  if( loopbackMode ) {

    //
    // When running with SSB emulator, we just need to send a single event from SSBe
    // and then reset the L1ID counter
    //

    bytes += StartSSBeData( flic , FLIC::targetFpgaU2 ,
                            0xf ,   // 0xf = all 4 channels
                            1 ,     // nevents
                            0 ,     // tracks per event
                            4000 ); // number of commas between events
    bytes += ResetSSBe( flic , FLIC::targetFpgaU2 );

    // Reset and initialize the processor counters
    bytes += InitProcCounters( flic , FLIC::targetFpgaU1 , 0x0000 ); // 0 => Count mode; 0xffff => Rate mode
    
  } else {

    //
    // When running with actual SSB use this function...
    //
    //   This was pushed to prepareForRun to fix synchronization timing, but this created more
    // problems because data would start flowing before the boards sync'd. Now we try adding
    // the sync request here but using a preceeding sleep statement.
    //
    // Wait for SSB to be ready
    usleep( 30000000 );
    // Send sync word to SSB
    bytes += SyncWithSSB( flic , FLIC::targetFpgaU1 );
    // Reset and initialize the processor counters
    bytes += InitProcCounters( flic , FLIC::targetFpgaU1 , 0x0000 ); // 0 => Count mode; 0xffff => Rate mode

  }

  // Set monitoring FIFOs to watch channel 0 on U1, reset & arm
  bytes += ResetArmMonitoringFIFOs( flic , FLIC::targetFpgaU1 , 0 , 4);

  return bytes;
}

int FLICController::prepareForRun( flic_comm *flic ) {
  int bytes = 0;

  if( loopbackMode ) {
    bytes += StartSSBeData( flic , FLIC::targetFpgaU2 ,
                            0xf ,                  // 0xf = all 4 channels
                            0 ,                    // 0 =  continuous data
                            tracksPerRecord ,      // tracks per event
                            delayBetweenRecords ); // number of commas between events
  } else {
    // Send sync word to SSB
    // Moved to "connect"
    //bytes += SyncWithSSB( flic , FLIC::targetFpgaU1 );
    // Reset and initialize the processor counters
    //bytes += InitProcCounters( flic , FLIC::targetFpgaU1 , 0x0000 ); // 0 => Count mode; 0xffff => Rate mode
  }
  
  return bytes;
}

int FLICController::stopDC( flic_comm *flic ) {
  if( loopbackMode ) {
    return StopSSBeData( flic , FLIC::targetFpgaU2 , 0xf ); // 0xf = stop continuous data on all 4 channels
  }
  return 0;
}

int FLICController::resynch( flic_comm *flic ) {
  return SyncWithSSB( flic , FLIC::targetFpgaU1 );
}





// --------------------//
// Debugging Functions //
// --------------------//

int FLICController::ReadRegister( flic_comm *flic , const ushort &target , const unsigned int &reg , ushort *val ) {
  return flic->singleread(target,FLIC::cmdRead,reg,val);
}

ushort FLICController::GetRegister( flic_comm *flic , const ushort &target , const unsigned int &reg ) {
  ushort tmp;
  flic->singleread(target,FLIC::cmdRead,reg,&tmp);
  return tmp;
}

int FLICController::WriteRegister( flic_comm *flic , const ushort &target , const unsigned int &cmd , const unsigned int &reg , const unsigned int &val ) {
  return flic->singlewrite(target,cmd,reg,val,FLIC::statusNoError);
}



// -------------------------------- //
// Monitoring FIFOs and Spy Buffers //
// -------------------------------- //

int FLICController::ArmMonitoringFIFOs( flic_comm *flic , const ushort &target ) {
  // Send signal to pulsed control register to reset & arm the fifo. One bit is set high for each of
  // the monitoring fifos.
  return flic->singlewrite( target , FLIC::cmdWrite , 0x20C , 0x000F );
}

int FLICController::ResetArmMonitoringFIFOs( flic_comm *flic , const ushort &target , const ushort &channel, uint condition) {
  int bytes = 0;

  // Set the control register to specify read out condition (bits[3..2]) and pipeline (bits[1..0]).
  // We currently reading out pipeline as soon as data arrives at the merge machine. There are
  // four different monitoring FIFOs to arm and each has a different control register
  FLIC::UDP_Data data;
  for( uint i = 0 ; i < 4 ; i++ ) data.word[i] = channel + condition;
  // JW - 2017.2.28 - Only capture events in mon fifos when CCR flags an error - bits 3..2 need to be '11'
  //for( uint i = 0 ; i < 4 ; i++ ) data.word[i] = 12 + channel;
  bytes += flic->blockwrite( target , FLIC::cmdArrayWrite , 0x17 , data , 4);
  //bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x17 , channel );
  //bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x18 , channel );
  //bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x19 , channel );
  //bytes += flic->singlewrite( target , FLIC::cmdWrite , 0x1a , channel );

  bytes += ArmMonitoringFIFOs( flic , target );

  return bytes;
}


int FLICController::ResetArmFlowControlFIFOs( flic_comm *flic , const ushort &target ) {
  int bytes = 0;

  // Send signal to pulsed control register to reset flow control fifos. One bit is set high for each of
  // the monitoring fifos.
  return flic->singlewrite( target , FLIC::cmdWrite , 0x20C , 0x00F0 );

  // For these fifos I don't need to set up any fancy triggering.
  // However, if we want to reset the time stamp then we need to write to the SFP_CTL reg.
  
  return bytes;
}

