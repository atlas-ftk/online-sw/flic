#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "flic_comm.h"
#include "FLIC.h"
int HelloWorld(flic_comm flictalk){
  int bytes=0;
  int data=0;
  printf("Enter data[dec?]: ");
  scanf("%d",&data);
  bytes+=flictalk.singlewrite(FLIC::targetNone,FLIC::cmdRead,0x0,data,0);
  printf("Wrote %d words to FLIC\n\n",bytes);
  return bytes;
}

int ReadWorld(flic_comm flictalk){
  int bytes=0;
  ushort data=0;
  bytes+=flictalk.singleread(FLIC::targetNone,FLIC::cmdRead,0x0,&data,0);
  printf("Read string: %d",data);
  return bytes;
}
int main(){
  int bytesent=0;
  int option=1;
  flic_comm flic1("192.168.1.2","50000");
  
  while(option!=0){
    printf("\n");
    printf("Select an option:\n");
    printf("   0 Exit                                                   \n");
    printf("   1 Send Message                                           \n");
    printf("   2 Read Message                                           \n");
    scanf("%d",&option);
    
    if (option == 1) bytesent=HelloWorld(flic1);
    if (option == 2) bytesent=ReadWorld(flic1);
    
  }//while option
  
  flic1.closesockets();
  return bytesent;
}
