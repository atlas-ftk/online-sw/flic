#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

typedef union _UDP_Data {
  char	 byte[512];
  ushort word[256];
  uint	 dword[128];
} UDP_Data;

typedef struct _UDP_Header {
  ushort	target;
  ushort	command;
  uint	        address;
  ushort	size;
  ushort	status;
} UDP_Header;

typedef struct _UDP_Packet {
  UDP_Header	header;
  UDP_Data	data;
} UDP_Packet;


class flic_comm {
  int sockfd, portno, clilen, n, status,sfd;
  int broadcast;

  struct sockaddr_in flic_addr;
  struct sockaddr host_addr;
  struct addrinfo hints;
  struct addrinfo *result,*i_res;

  UDP_Packet tx;
  UDP_Packet rx;
  
 public:
  flic_comm(const char* flicIP,const char* flicPort);
  
  int singlewrite(ushort target, ushort command, uint address, ushort value);
  int singlewrite(ushort target, ushort command, uint address, ushort value,ushort status);
  int singleread(ushort target, ushort command, uint address, ushort* value);
  int singleread(ushort target, ushort command, uint address, ushort* value,ushort status);
  int blockwrite(ushort target, ushort command, uint address, UDP_Data& block);
  int blockread(ushort target, ushort command, uint address, ushort* data);
  int test();
  void error(const char *msg);
  void closesockets(){close(sockfd);close(sfd);};
  int adcScan (ushort target, ushort command, uint address, ushort* value);
  int adcSingle (ushort target, ushort command, uint address, ushort* value);
  ~flic_comm(){};//Only thing to do is close the socket.
};

