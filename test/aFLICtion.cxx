#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fstream>
#include <getopt.h>

#include "flic_comm.h"
#include "FLIC.h"

int PicPowerOn(flic_comm flictalk);
int ProgramAll(flic_comm flictalk,unsigned int mask);
int FlashAddRemap(flic_comm flictalk);
int SetClocks(flic_comm flictalk,unsigned int mask);
int SERDESReset(flic_comm flictalk);
int Idle(flic_comm flictalk, unsigned int mask);
int SetupProc(flic_comm flictalk, unsigned int fpga, unsigned int mask);
int SetupSSBe(flic_comm flictalk, unsigned int fpga, unsigned int mask, unsigned int tracks, unsigned int delay);
int ResetSLink(flic_comm flictalk, unsigned int fpga, unsigned int mask);
int Dump(flic_comm flictalk, unsigned int fpga, unsigned int mask);
int SendEvent(flic_comm flictalk, unsigned int fpga, unsigned int mask);
int SendCont(flic_comm flictalk, unsigned int fpga, unsigned int mask);
int ResetSSBe(flic_comm flictalk, unsigned int fpga, unsigned int mask);

int main(int argc, char **argv){
  int status=0;
  int index;
  int iarg=0;
  
  //Initialize to default values.
  bool powerOn=false;
  unsigned int progMask=0x0000;
  bool isProc=false;
  unsigned int procMask=0x00;
  bool doSSBe=false;
  int ssbeMask=0x00;
  bool isVerbose=false;
  bool doIdle=false;
  unsigned int  clockMask=0x13;//2Gb/s S-Link 3Gb/s SERDES to SSB
  bool doRemap=false;
  unsigned int slinkMask=0x0;
  unsigned int dumpMask=0x0; 
  bool doSync=false;
  bool runCont=false;
  bool resetSSBe=false;
  unsigned int tracks=0;
  unsigned int delay=0;
  bool setup=false;
  
  //To be added. Functionality to be included.
  //...//char *progIP="192.168.1.101";
  //...//char *ssbeIP="192.168.1.101";
  //...//
  //unsigned int numTracks=0x00;
  //unsigned int trackDelay=0x00;
  //...//
  
  //...//
  //...// Define and Read arguments, modify defaults as necessary
  //...//

  //Define options
  const struct option longopts[] = 
    {
      {"prog",           optional_argument, 0,'p'},
      {"power",          no_argument, 0, 'P'},
      {"proc",           required_argument, 0, 'C'},
      {"ssbe",           required_argument, 0, 'S'},
      {"idle",           no_argument, 0, 'i'},
      {"clocks",         required_argument, 0, 'c'},
      {"verbose",        no_argument, 0, 'v'},
      {"help",           no_argument, 0, 'h'},
      {"remap",          no_argument, 0,'a'},
      {"slink",          optional_argument, 0,'l'},
      {"sync",           no_argument, 0, 'y'},
      {"dump",           required_argument, 0, 'd'},
      {"cont",           no_argument, 0, 'o'},
      {"reset",           no_argument, 0, 'r'},
      {"setup",           no_argument, 0, 'u'},
      {0,0,0,0},
    };
  
  //Describe options for --help message
  const char options_usage[] =
    "\n"
    "The default behavior of aFLICtion is to do nothing. Here are the options that cause it to do something.\n\n"
    "Options:\n"
    "-h  --help\n"
    "    Print this usage message and exit.\n"
    "-v  --verbose\n"
    "    Turn on verbose output.\n"    
    "***********************************************************************\n"
    "Board Configuration Options\n"
    "***********************************************************************\n"
    "-p mask, --prog=mask\n"
    "    Modify default behavior to program FPGAs from on board FLASH.\n"
    "    Argument is a hex mask of firmware for FPGA.\n"
    "    The encoding is:\n"
    "             0 = off do not reprogram\n"
    "             1 = data processing\n"
    "             2 = SSB emulation\n"
    "             3 = 10 Gbe + clock control\n"
    "             4 = 10 Gbe\n"
    "    Default: 0x4321.\n"
    "-P, --power\n"
    "    Turn power on to the board.\n"
    "    Default: false.\n"
    "-u,  --setup]n"
    "    Setup firmware state machines.\n"
    "    Default: false.\n"
    "-i,  --idle\n"
    "    Turn on low level set up.\n"
    "    Default: If programming FPGAs, or changing the clock mask.\n"
    "-c mask, --clocks=mask\n"
    "    Mask for clocks. LSB is Front Panel Link Speed, MSB is RTM S-Link Speed.\n" 
    "    Default: 0x13\n"
    "-a,  --remap\n"
    "    Perform Flash address remap. Only necessary on prototype 2.\n"
    "    Default: false.\n"
    "***********************************************************************\n"
    "FLIC Processing Options\n"
    "***********************************************************************\n"
    "-C mask, --proc=mask\n"
    "    Process events with channel mask.\n"
    "    Default: 0x01\n"
    "-l mask, --slink mask\n"
    "    Reset S-Link for given mask.\n"
    "    Default: same as --proc\n"
    "-d \n"
    "    Dump data on the floor for given mask. Ignore downstream S-Link devices if they exist.\n"
    "    Default: Off. If set the difference between the Proc and S-Link masks will be dumped.\n"
    "***********************************************************************\n"
    "SSB Emulation Options\n"
    "***********************************************************************\n"
    "-S mask, --ssbe=mask\n"
    "    Emulate SSB with channel mask.\n"
    "    Default: 0x10\n"
    "-y, --sync\n"
    "    Send one event to synchronize.\n"
    "    Default: done for SSBe mask.\n"
    "-o,  --cont\n"
    "    Run continuously and forever.\n"
    "    Default: false"
    "-r,  --reset\n"
    "    Reset counters and configuration of SSBe.\n"
    "    Default: false"
    "\n";
  
  //Read Options
  while(iarg!=-1){
    optarg=0;
    iarg=getopt_long(argc,argv,"hvioyadruPC:S:p::c:l::", longopts, &index);
    switch(iarg){
    case 'P' :
      powerOn=true;
      break;
    case 'p' :
      if(optarg !=0)
	progMask = strtol(optarg, NULL, 16);
      else
	progMask = 0x4321;
      break;
    case 'C' :
      procMask = strtol(optarg, NULL, 16);
      break;
    case 'u' :
      setup=true;
      break;
    case 'S' :
      ssbeMask = strtol(optarg, NULL, 16);
      doSSBe=true;
      break;
    case 'c' :
      clockMask=strtol(optarg,NULL, 16);
      break;
    case 'i' :
      doIdle=true;
      break;
    case 'a' :
      doRemap=true;
      break;
    case 'l' :
      slinkMask=strtol(optarg, NULL, 16);
      break;
    case 'd' :
      dumpMask=0xff;
      break;
    case 'y' :
      doSync=true;
      break;
    case 'o' :
      runCont=true;
      break;
    case 'r' :
      resetSSBe=true;
      doSSBe=false;
      break;
    case 'v' :
      isVerbose=true;
      break;
    case 'h' :
      printf(options_usage);
      exit(0);
    case '?' :
      printf(options_usage);
      exit(0);
      
    }//switch on iarg
  }//while(reading options)
  
  if(isVerbose)
    printf("The Verbose option does nothing currently... this message is to remove the compiler warning for an unused variable.");
  
  //Configure FLIC
  flic_comm flic1("192.168.1.101","50000");//Hard coded. Add functionality to set IPs.
  if(powerOn)
    PicPowerOn(flic1);
  
  if(doRemap)
    FlashAddRemap(flic1);
  
  if(progMask!=0){
    ProgramAll(flic1,progMask);
    Idle(flic1, clockMask);
  }
  
  if(doIdle || clockMask != 0x13)
    Idle(flic1, clockMask);
  
  if(doSSBe && setup ){
    if((ssbeMask&0xf0) > 1){
      ResetSSBe(flic1,FLIC::targetFpgaU2,(ssbeMask &0xf0)/0x10);      
    }
    if((ssbeMask&0xf) > 1){
      ResetSSBe(flic1,FLIC::targetFpgaU1,(ssbeMask &0xf));
    }
  }
  
  if(isProc && setup){
    dumpMask=(procMask & ~slinkMask);    
    if((procMask&0xf0) > 1){
      SetupProc(flic1,FLIC::targetFpgaU2,(procMask &0xf0)/0x10);      
    }
    if((procMask&0xf) > 1){
      SetupProc(flic1,FLIC::targetFpgaU1,(procMask &0xf));
    }
  }
  
  if(slinkMask > 0 && (setup || doSync) ){
    if((slinkMask&0xf0) > 1){
      ResetSLink(flic1,FLIC::targetFpgaU2,(slinkMask &0xf0)/0x10); // 
    }
    if((slinkMask&0xf) > 1){
      ResetSLink(flic1,FLIC::targetFpgaU1,(slinkMask &0xf));
    }
  }
  if(dumpMask>0){
    if((dumpMask&0xf0) > 1){
      Dump(flic1,FLIC::targetFpgaU2,(dumpMask &0xf0)/0x10);      
    }
    if((dumpMask&0xf) > 1){
      Dump(flic1,FLIC::targetFpgaU1,(dumpMask &0xf));
    }    
  }
  
  if(doSSBe && (setup || resetSSBe)){
    if((ssbeMask&0xf0) > 1){
      SetupSSBe(flic1,FLIC::targetFpgaU2,(ssbeMask &0xf0)/0x10, tracks, delay);      
    }
    if((ssbeMask&0xf) > 1){
      SetupSSBe(flic1,FLIC::targetFpgaU1,(ssbeMask &0xf), tracks, delay);
    }
  }
  
  if(doSync){
    if((ssbeMask&0xf0) > 1){
      SendEvent(flic1,FLIC::targetFpgaU2,(ssbeMask &0xf0)/0x10);      
      SetupSSBe(flic1,FLIC::targetFpgaU2,(ssbeMask &0xf0)/0x10, tracks, delay);      
    }
    if((ssbeMask&0xf) > 1){
      SendEvent(flic1,FLIC::targetFpgaU1,(ssbeMask &0xf));
      SetupSSBe(flic1,FLIC::targetFpgaU1,(ssbeMask &0xf), tracks, delay);
    }
  }
  
  if(runCont){
    if((ssbeMask&0xf0) > 1){
      SendCont(flic1,FLIC::targetFpgaU2,(ssbeMask &0xf0)/0x10);
    }
    if((ssbeMask&0xf) > 1){
      SendCont(flic1,FLIC::targetFpgaU1,(ssbeMask &0xf));
    }
  }
    
  
  //Clean up after one's self
     
  flic1.closesockets();
  return status;
}

//Function to send power to the board
int PicPowerOn(flic_comm flictalk){
  return flictalk.singlewrite(FLIC::targetPic,FLIC::cmdWrite,FLIC::PIC::picReg_Command, 0x0001, FLIC::statusNoError);
};

//Function to turn on address remap
int FlashAddRemap(flic_comm flictalk){
  return flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdSet,FLIC::AUX::auxControlReg_Control1,0x1);
}

//Function to program the FPGAs from on board memory
int ProgramAll(flic_comm flictalk, unsigned int mask){
  int bytes=0;
  const int nDEST=4;
  int isDone=0;
  ushort status[nDEST];//number of things being programmed for now just FPGA's  
  
  //Set configuration
  unsigned int j=0xf;
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdWrite,FLIC::AUX::auxControlReg_Setup0,
			      ((mask & j)?8:0)*0x1000+(1<<((mask & j)%0xf-1))*(0x100)+0x11);
  j*=0x10;
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdWrite,FLIC::AUX::auxControlReg_Setup1,
  			      ((mask & j)?8:0)*0x1000+(1<<((mask & j)%0xf-1))*0x100+0x12);
  j*=0x10;
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdWrite,FLIC::AUX::auxControlReg_Setup2,
  			      ((mask & j)?8:0)*0x1000+(1<<((mask & j)%0xf-1))*0x100+0x14);
  j*=0x10;
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdWrite,FLIC::AUX::auxControlReg_Setup3,
  			      ((mask & j)?8:0)*0x1000+(1<<((mask & j)%0xf-1))*0x100+0x18);
  //Set control
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdWrite,FLIC::AUX::auxControlReg_Control0, 0x0004);
  
  //Start programming
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdSet,FLIC::AUX::auxControlReg_Control0, 0x000A);
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdSet,FLIC::AUX::auxPulseReg_Pulse0, 0x0001);
  
  //Check if FPGA firmware has been loaded.
  while(isDone < nDEST){
    usleep(100000);
    isDone=0;
    for(int i =0;i<nDEST;i++){
      bytes+=flictalk.singleread(FLIC::targetMgmtFpga,FLIC::cmdRead,0x108+i,&status[i]);
      if((status[i] & (1<<0)))
  	isDone++;
    }
  }//end while
  
  //Return control
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdClear,FLIC::AUX::auxControlReg_Control0, 0x000A, FLIC::statusNoError);
  
  return bytes;
}

int SetClocks(flic_comm flictalk, unsigned int mask){
  int bytes=0;  

  //RTM First.
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0, ((mask & 0xf0)%0xf)); //MSB
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x20E,0x0001);
  //Front Panel second.
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU3,FLIC::cmdWrite,0x0, (mask & 0xf));//LSB
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU3,FLIC::cmdSet,0x200,0x0001);

  return bytes;
}

int SERDESReset(flic_comm flictalk){
  int bytes=0;
  //For the time being reset only GTX lines to the Front Panel or the RTM.
  //Reset of internal and backplane GTX lines to be implemented.
  //
  
  //GTX112
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x200,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x208,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x200,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0xffff);
  
  ///
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x208,0xffff);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x209,0xffff);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x209,0xffff);
  /////bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x002, 0x0000);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x00000200, 0x4444);//reset the GTX116 TLK wrappers
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x00000200, 0x8888);//reset the GTX116 hola core logic
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x00000200, 0x4444);//reset the GTX116 TLK wrappers
  ///
  ///
  /////U1 GTX112,116 reset
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0xffff);
  /////bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x3,0xf0);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0xffff);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x209,0xFF);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x209,0xF00);
  /////Need 'GO' Bit.
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x002, 0x0000);
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000200, 0x4444);//reset the GTX116 TLK wrappers
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000200, 0x8888);//reset the GTX116 hola core logic
  ///bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000200, 0x4444);//reset the GTX116 TLK wrappers
  
  
  return bytes;
}

int SMResets(flic_comm flictalk){
  int bytes=0;
  //Pulse reset of all SMs on U1.
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  //Pulse reset of SSB Emulator or U2
  //same as GTX112 reset. Should be done. Here I reset the PRBS machine...
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  return bytes;
}

int ClearFIFOFullFlags(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xD0F0);
  return bytes;
}

int Idle(flic_comm flictalk, unsigned int mask){
  int bytes=0;  

  //Turn on SFPs.
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000004, 0x0000);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x00000004, 0x0000);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000003, 0x00F0);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x00000003, 0x00F0);

  if(mask != 0x13)
    bytes+=SetClocks(flictalk, mask);
  bytes+=SERDESReset(flictalk);
  return bytes;
}

int SendEvent(flic_comm flictalk, unsigned int fpga, unsigned int mask){
  return flictalk.singlewrite(fpga,FLIC::cmdWrite,0x201, mask*0x1000 );
}

int SendCont(flic_comm flictalk, unsigned int fpga, unsigned int mask){  
  int bytes=0;
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x1f, mask*0x100);
  bytes+=SendEvent(flictalk,fpga,mask);
  return bytes;
}
  
 int ResetSSBe(flic_comm flictalk, unsigned int fpga, unsigned int mask){
   return flictalk.singlewrite(fpga,FLIC::cmdWrite,0x1f,mask*0x10);
}


int SetupSSBe(flic_comm flictalk,unsigned int fpga, unsigned int mask, unsigned int tracks, unsigned int delay){
  int bytes=0;
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x1f,0xf0);
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x1f,0x00);
  //set the SSB emulator in U2 to have status words of 0xDDDD and 0xEEEE
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000001D, 0xDDDD);	
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000001E, 0xEEEE);	
  
  //Set Tracks and Delay
  if(tracks !=0){
    if((mask&0x1)==0x1)
      bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x20,tracks+0x8000);
    if((mask&0x2)==0x2)
      bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x21,tracks+0x8000);
    if((mask&0x4)==0x4)
      bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x22,tracks+0x8000);
    if((mask&0x8)==0x8)
      bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x23,tracks+0x8000);
  }
  if(delay !=0){
    if((mask&0x1)==0x1)
      bytes+=flictalk.singlewrite(fpga, FLIC::cmdWrite,0x18,delay);
    if((mask&0x2)==0x2)
      bytes+=flictalk.singlewrite(fpga, FLIC::cmdWrite,0x19,delay);
    if((mask&0x4)==0x4)
      bytes+=flictalk.singlewrite(fpga, FLIC::cmdWrite,0x1A,delay);
    if((mask&0x8)==0x8)
      bytes+=flictalk.singlewrite(fpga, FLIC::cmdWrite,0x1B,delay);
  }
  return bytes;
}

int SetupProc(flic_comm flictalk,unsigned int fpga,unsigned int mask){
  int bytes=0;
  
  printf("Setting up FPGA %x with mask %x\n",fpga,mask);

  //...//
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x00000004, 0x0000);
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdSet,0x00000003, mask*0x10);//S-LINK over RTM
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdSet,0x00000002, 0x0012);//Enable CCR DMM
  //need to enable look up process.
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x8000);//reset the GTX116 FIFO reader
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x4000);//reset the GTX116 data FIFO
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x2000);//reset the SLINK primitives
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x1000);//reset the merge machines
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x0300);//reset the SRAM machines
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x00F0);//reset the CCMUX logic
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x000F);//reset the core crate receivers
  
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x4444);//reset the GTX116 TLK wrappers
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x8888);//reset the GTX116 hola core logic
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x0000020F, 0x4444);//reset the GTX116 TLK wrappers
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdClear,0x0000000003, mask*0x100);
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x00000209,  mask*0x100);
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdSet, 0x000000003,  mask*0x100);
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x00000201, 0x0001);

  bytes+=flictalk.singlewrite(fpga,FLIC::cmdSet,0x00000002, 0x0008);	//set bit 3
  
  //as last steps, set the FLIC status words in U1 to 0x1515 and 0x1616, respectively
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x00000015, 0x1515);	
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x00000016, 0x1616);	
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdWrite,0x00000209, mask*0x100);//request the GTX116 reset machines to re-arm
  
  return bytes;
}
int ResetSLink(flic_comm flictalk, unsigned int fpga, unsigned int mask){
  int bytes=0;

  printf("Resetting S-Link of FPGA %x with mask %x\n",fpga,mask);
  
  // now enable the SLINK reset state machine to go
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdClear,0x00000003, mask*0x100);//clear the GO bits to the reset machines
  //
  //usleep(1000);
  bytes+=flictalk.singlewrite(fpga,FLIC::cmdSet,0x00000003, mask*0x100);//set the GO bits to the reset machines
  
  return bytes;
}
int Dump(flic_comm flictalk, unsigned int fpga, unsigned int mask){
  int bytes=0;
  printf("Setting FPGA %x to dump data from channels %x\n",fpga,mask);
  return bytes;
}
