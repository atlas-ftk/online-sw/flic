# running daq dummy partition on _ustb13_; dir tdaq_ftk_flic_test_ustb13_20180724

## first thing first
1. vnc connect to ustb13
2. do the setup
3. remember to clean up the threads after you run the tests, like ```ps -efw | grep pmglauncher | awk '{ print $2 }' | xargs kill```

## setup
```
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-07-01-00/installed/setup.sh tdaq-07-01-00 x86_64-slc6-gcc62-opt
pm_part_hlt.py -p dummy
touch ipc_root.ref; TDAQ_IPC_INIT_REF=file:${PWD}/ipc_root.ref
```

then change the location of the log files in dummy.data.xml.

```
 <attr name="LogRoot" type="string">"/tmp/ac.peilongw/logs/${TDAQ_VERSION}"</attr>
 <attr name="WorkingDirectory" type="string">"/tmp/ac.peilongw/logs"</attr>
```
Then 
```
pm_farm.py --safe-hw-tags --add='ustb[17]' farm.data.xml
```
and copy "Computer" and "Interface" section from farm.data.xml to dummy.data.xml.

You can also change the host computer to ustb17 in the block of dummy
```
 <rel name="DefaultHost">"Computer" "ustb17.hep.anl.gov"</rel>
```

```
setup_daq -p dummy -d dummy.data.xml 
```
then disable HLT from IGUI



## VNC through SSH tunnel
connect to ustb13 through SSH
```
ssh -tCY -L 5901:localhost:5901 ac.peilongw@alogin.hep.anl.gov ssh -tCY -L 5901:localhost:5901 ac.peilongw@nathan ssh -tCY -L 5901:localhost:5901 ac.peilongw@ustb13
```
the port number is 5901 because the VNCserver we started has the default port number of 5901. It can be checked with the command later after start VNC server "ps -aux | grep peilongw | grep vnc"

in the terminal of ustb13, start vncserver by 
```
vncserver
```
on the local machine (my laptop in this case), start the VNC viewer and connect to "localhost:5901".

To kill vnc sessions:
```
vncserver -kill :1
vncserver -kill :2
```
more on https://superuser.com/questions/549386/what-is-the-correct-way-to-kill-a-vncsession-in-linux

VNC athentication: sometimes like 123456
