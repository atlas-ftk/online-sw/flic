#!/bin/bash

# rm *.xml *.ref
# rm -rf daq/

source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-07-01-00/installed/setup.sh tdaq-07-01-00 x86_64-slc6-gcc62-opt
pm_part_hlt.py -p dummy
touch ipc_root.ref; TDAQ_IPC_INIT_REF=file:${PWD}/ipc_root.ref
# pm_farm.py --safe-hw-tags --add='ustb[10,11,12,13,14,15,17,20,21]' farm.data.xml
pm_farm.py --safe-hw-tags --add='ustb[17]' farm.data.xml
# create daq/hw and daq/segments and "mv farm.data.xml daq/hw/"
# mkdir daq
# mkdir daq/hw daq/segments
setup_daq -p dummy -d dummy.data.xml 
