#!/bin/bash

source ${PWD}/setup_ftk_anl.sh FTK-03-01-00
# FTK-02-01-01
# export TDAQ_DB_PATH=/users/ac.peilongw/flic_partition_test_20180507/flic_partition_ustb:/users/ac.peilongw/flic_partition_test_20180612/oks_ftk:$TDAQ_DB_PATH
export TDAQ_IPC_INIT_REF=file:${PWD}/ipc_root.ref
# file:/users/ac.peilongw/flic_partition_test_20180612/ipc_root.ref
# pm_farm.py --safe-hw-tags --add='ustb[13]' farm.data.xml
# touch ipc_root.ref; TDAQ_IPC_INIT_REF=file:${PWD}/ipc_root.ref
