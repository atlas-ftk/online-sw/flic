# Script to be sourced to setup FTK running environment @afs
# Andrea.Negri@cern.ch

# COLOR VARIABLES
NO_COL="\033[0m"
REDFG="\033[1;31m"
BLUFG="\033[1;34m"

#Check on the user group. Not needed anymore (the oks file folders are forced to belong to the zp group)
<<comment
GRP=zp
TBED="tbed"
HOST=$(hostname)

if [[ $HOST == *$TBED* ]]; then
  if [ $(id -gn) != $GRP ]; then
    echo -e "${REDFG} FTK source script for release $1...${NO_COL}"
    echo -e "${REDFG}--- \tYou are not logged as zp member!! \t--- \nPlease do ''${NO_COL}${BLUFG}newgrp zp${NO_COL}${REDFG}'' before source this script..${NO_COL}"
    return 1;
  fi
fi
comment

# FTK VARIABLES
TDAQRELEASE=tdaq-07-01-00
FTK_BASE_PATH=/users/ac.peilongw/ftk_cmake_from_lxplus/inst
# /users/ac.peilongw/flic_partition_test_20180612/inst
# /afs/cern.ch/user/f/ftk/inst
FTK_RELEASE_PATH=${FTK_BASE_PATH}/releases
FTK_RELEASE=

if [ $# -lt 1 ]
then
  echo "FTK release setup script."
  echo "Usage: source setup_ftk.sh FtkReleaseName"
  echo "Available releases:"
  cd $FTK_RELEASE_PATH
  /bin/ls -lrt -d FTK*/ | grep -v "patches" | sed -e 's/^d/   d/' | tr "/" " "
  cd -
  return
fi
export FTK_RELEASE=$1


if [[ ${FTK_RELEASE} == FTK-00* ]]
then
  echo "No, you can not source ${FTK_RELEASE} at Argonne"
  TDAQRELEASE=tdaq-05-05-00
fi

if [[ ${FTK_RELEASE} == FTK-01* ]]
then
  echo "No, you can not source ${FTK_RELEASE} at Argonne"
  TDAQRELEASE=tdaq-06-01-01
fi

if [[ ${FTK_RELEASE} == FTK-nightly* ]]||[[ ${FTK_RELEASE} == FTK-m* ]]
then 
  echo "No, you can not source nightly at Argonne"
  cd $FTK_RELEASE_PATH
  LAST_RELEASE=$(/bin/ls -t -d FTK*/ | grep -v "patches" | grep -v "nightly" | tr -d "/" | sed "1q;d")
  FTK_DB_PATH=/tbed/oks/${TDAQRELEASE}/FTK/${LAST_RELEASE}
  cd -
else FTK_DB_PATH=/tbed/oks/${TDAQRELEASE}/FTK/${FTK_RELEASE}
fi

# TODO
# - DYNAMICALLY MAP FTK RELEASE TO TDAQ ONE


# TDAQ RELEASE

echo "Setting up TDAQ release $TDAQRELEASE"

if [[ ${FTK_RELEASE} == FTK-00* ]]
then
  source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh ${TDAQRELEASE}
elif [[ ${FTK_RELEASE} == FTK-01* ]]
then
  source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh ${TDAQRELEASE}
else
  echo "Source release from CVMFS at Argonne"
  ARCH=x86_64-slc6-gcc62-opt
  source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-07-01-00/installed/setup.sh ${TDAQRELEASE} ${ARCH}
fi

echo "Setting up FTK release $FTK_RELEASE"
export FTK_INST_PATH=${FTK_RELEASE_PATH}/${FTK_RELEASE}/installed
export FTK_INST_PATH_PATCH=${FTK_RELEASE_PATH}/${FTK_RELEASE}.patches/installed
export TDAQ_DB_PATH=${FTK_RELEASE_PATH}/${FTK_RELEASE}.patches/installed/share/data:${FTK_INST_PATH}/share/data:${FTK_DB_PATH}:${TDAQ_DB_PATH}
export CMAKE_PREFIX_PATH=${FTK_RELEASE_PATH}/${FTK_RELEASE}/installed:$CMAKE_PREFIX_PATH

# /users/ac.peilongw/flic_partition_test_20180612/inst/releases/FTK-02-01-01/installed
# /afs/cern.ch/user/f/ftk/inst/releases/${FTK_RELEASE}/installed/:$CMAKE_PREFIX_PATH
# CMAKE_PREFIX_PATH=/afs/cern.ch/user/f/ftk/inst/releases/FTK-02-01-01/installed/:/afs/cern.ch/atlas/project/tdaq/inst:/afs/cern.ch/atlas/project/tdaq/inst/tdaq-common/tdaq-common-02-02-00/installed/share/cmake_tdaq/cmake

export SVNROOT=svn+ssh://svn.cern.ch/reps/atlastdaq
#export TDAQ_DB_REPOSITORY=${FTK_BASE_PATH}/oks/${FTK_RELEASE}:${TDAQ_DB_REPOSITORY}

 # For command line tool
echo "Updating PATH, LD_LIBRARY_PATH and PYTHONPATH"
export PATH=${FTK_INST_PATH_PATCH}/${CMTCONFIG}/bin:${FTK_INST_PATH_PATCH}/share/bin:${FTK_INST_PATH}/${CMTCONFIG}/bin:${FTK_INST_PATH}/share/bin:${PATH}
export LD_LIBRARY_PATH=${FTK_INST_PATH_PATCH}/${CMTCONFIG}/lib:${FTK_INST_PATH}/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=${FTK_INST_PATH_PATCH}/share/lib/python:${FTK_INST_PATH}/share/lib/python:${PYTHONPATH}
if [[ ${FTK_RELEASE} == FTK-01* ]] || [[ ${FTK_RELEASE} == FTK-00* ]]
then
  echo -e "${REDFG}--- Configuration summary --- ${NO_COL}"
  echo -e "CMTCONFIG       = ${REDFG}${CMTCONFIG}${NO_COL}"
  echo -e "TDAQ_RELEASE    = ${REDFG}${TDAQRELEASE}${NO_COL}"
  echo -e "FTK_RELEASE     = ${REDFG}${FTK_RELEASE}${NO_COL}"
  echo -e "${BLUFG}[Reminder]${NO_COL} To run a partition:"
  echo -e "  - Set \$TDAQ_DB_DATA to your partition file and set \$TDAQ_PARTITION accordingly"
  echo -e "  - To use a local OKS repository, prepend it to \$TDAQ_DB_PATH"
  echo -e "  - To force RunControl to load binaries/libraries from your 'installed' area, modify Partition.RepositoryRoot field in your partition file"
  # Add FTK install repository to the CMT project path 
  export CMTPROJECTPATH=${FTK_BASE_PATH}/:${CMTPROJECTPATH}
else
  echo -e "${REDFG}--- Configuration summary --- ${NO_COL}"
  echo -e "CMAKECONFIG     = ${REDFG}${CMTCONFIG}${NO_COL}"
  echo -e "TDAQ_RELEASE    = ${REDFG}${TDAQRELEASE}${NO_COL}"
  echo -e "FTK_RELEASE     = ${REDFG}${FTK_RELEASE}${NO_COL}"
  echo -e "${BLUFG}[Reminder]${NO_COL} To run a partition:"
  echo -e "  - Set \$TDAQ_DB_DATA to your partition file and set \$TDAQ_PARTITION accordingly"
  echo -e "  - To use a local OKS repository, prepend it to \$TDAQ_DB_PATH"
  echo -e "  - To force RunControl to load binaries/libraries from your 'installed' area, modify Partition.RepositoryRoot field in your partition file" 
fi

