#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flic_ssbe , flic_comm *flic_proc , FLICController ctrl );
int Setup( flic_comm *flic_ssbe , flic_comm *flic_proc );

int StartTestVectorRun( flic_comm *flic_proc, flic_comm *flic_ssbe , FLICController ctrl , int nevents=0 , bool verbose=false );

int main() {
  int bytes = 0;
  FLICController ctrl;

  while( true ) {

    cout << endl;
    cout << "--- SSBe Board ---" << endl;
    cout << "------> default 6" << endl;
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic_ssbe = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );

    cout << endl;
    cout << "--- Processor Board ---" << endl;
    cout << "------> default 5" << endl;
    b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic_proc = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );

    bytes += Option( flic_ssbe , flic_proc , ctrl );

    delete flic_ssbe;
    delete flic_proc;
  }

  return bytes;
}


int Option( flic_comm *flic_ssbe , flic_comm *flic_proc , FLICController ctrl ) {
  int option = 1;
  int bytes  = 0;
  while( option != 0 ) {
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On ALL" << endl
         << " 100 Power Off ALL" << endl
      //<< " 101 Power On SSBe   102 Power Off SSBe" << endl
      //<< " 103 Power On Proc   104 Power Off Proc" << endl
         << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
         << " Basic initialization controls..." << endl
         << "   2 Program All FPGAs" << endl
	 << " 201 Program Proc U1 and U2 " << endl
	 << " 202 Program SSBe U1 and U2 " << endl
         << "   3 Setup SSBe board" << endl
	 << "   4 Setup Proc board" << endl
	 << " 401 Setup Proc board - 1" << endl
	 << " 402 Setup SSBe board - 2" << endl
         << "   5 Send sync event from SSBe board to Proc board" << endl
         << "   6 Reset SSBe" << endl
	 << standalone::line << endl

	 << "   7 Setup Spy buffers for all events on all channels" << endl
         << " Test Vector controls..." << endl
         << "  170 Fill TV FIFOs, 4 channels      171 Send TVs from FIFOs, 4 channels" << endl
         << "  172 Reset & arm flowcontrol FIFOs  173 Read flowcontrol FIFOs" << endl
         << "  174 Reset & arm monitoring FIFOs   175 Read monitoring FIFOs" << endl
         << "  176 Fancy TV test" << endl

	 << standalone::line << endl
	 << "   8 Read register on SSBe board   9 Write register on SSBe board" << endl
	 << "  10 Read register on Proc board  11 Write register on Proc board" << endl
	 << standalone::line << endl
	 << "  75 FLIC and Spybuffer one button setup" << endl
	 << "  76 Send single event" << endl
	 << "  760 Send single event with options" << endl
	 << "  761 Send infinity loop of single event" << endl
         << "  77 Start SSBe data" << endl
         << "  78 Stop SSBe data" << endl
         << standalone::line << endl
         << " Diagnostics..." << endl
         << "  84 Reset and arm monitoring FIFOs" << endl
         << "  85 Dump monitoring FIFOs on U1" << endl
         << "  86 Dump monitoring FIFOs on U2" << endl
	 << "  87 Reset and arm flow control FIFOs" << endl
	 << "  88 Dump flow control FIFOs on U1" << endl
	 << "  89 Dump flow control FIFOs on U2" << endl
         << "  90 Initialize counters" << endl
         << "  91 Dump counters" << endl
	 << "  92 Dump global status" << endl
	 << "  93 Dump raw SLINK status" << endl
         << " 222 Check program status" << endl
         << " 223 Check clock counters" << endl
         << " 224 Check XOFF counters" << endl
         << " 225 Change to \"dump data on the floor\" mode" << endl
	 << " 226 Dump spybuffer IP configuration" << endl
         << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) {
      bytes += ctrl.PICPowerOn( flic_ssbe );
      bytes += ctrl.PICPowerOn( flic_proc );
    }
    if (option == 100) {
      bytes += ctrl.PICPowerOff( flic_ssbe );
      bytes += ctrl.PICPowerOff( flic_proc );
    }
    if (option == 101 ) bytes += ctrl.PICPowerOn( flic_ssbe );
    if (option == 102 ) bytes += ctrl.PICPowerOff( flic_ssbe );
    if (option == 103 ) bytes += ctrl.PICPowerOn( flic_proc );
    if (option == 104 ) bytes += ctrl.PICPowerOff( flic_proc );
    if (option == 200) {
      bytes+=ctrl.PICPowerFromIPMC( flic_ssbe );
      bytes+=ctrl.PICPowerFromIPMC( flic_proc );
    }
    if (option == 2) {
      // Program proc board
      ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::DoubleProc_NoSRAMs );
      // Kick GTX112 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x200 , 0xffff );
      // Kick GTX116 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x208 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x208 , 0xffff );
      // Then check the clocks
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) );
      // Program ssbe board
      ctrl.ProgramFPGAs( flic_ssbe , FLIC::ListProc::DoubleSSBe );
    }

    //	 << " 201 Program Proc U1 and U2 " << endl
    if (option == 201) {
      // Program proc board
      ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::DoubleProc_NoSRAMs );
      // Kick GTX112 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x200 , 0xffff );
      // Kick GTX116 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x208 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x208 , 0xffff );
      // Then check the clocks
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) );

    }

    //	 << " 202 Program SSBe U1 and U2 " << endl
    if (option == 202) {
      // Program ssbe board
      ctrl.ProgramFPGAs( flic_ssbe , FLIC::ListProc::DoubleSSBe );
    }


    if (option == 3) {
      FLICSetupTool t;
      t.setFlicComm( flic_ssbe );
      t.INIT_U2_U2_U3_U4();
    }
    if (option == 4) {
      FLICSetupTool t;
      t.setFlicComm( flic_proc );
      t.INIT_U1_U1_U3_U4();
      // Whack the Rx's on both processor and SSBe boards
      /*
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      */
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
    }

    if (option == 401) {
      FLICSetupTool t;
      t.setFlicComm( flic_proc );
      t.INIT_U1_U1_U3_U4();
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
    }

    if (option == 402) {
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
    }

    if (option == 5) {
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU2 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    }
    if (option == 6) {
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU2 );
    }
    if (option == 7) {
      FLICSetupTool t;
      t.setFlicComm( flic_proc );
      t.INIT_SPYBUFFERS();
    }

    if (option == 170) {
      ctrl.loadTestVectorsFromFile( "/users/ac.peilongw/FLIC_TV/FlicInputTVFile_1.7.1_ev0.txt");
//../testvectors/events/FlicInputTVFile_1.7.1_ev0.txt" );
      bytes += ctrl.WriteTestVectorsFourChannels( flic_ssbe,FLIC::targetFpgaU1 );
      bytes += ctrl.WriteTestVectorsFourChannels( flic_ssbe,FLIC::targetFpgaU2 );

    }
    if (option == 171) {
      bytes += ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }


    if (option == 1701) {

      uint evid = rand() % 100; // random between 0 and 99
      evid = rand() % 100; // random between 0 and 99

      while (true) {

	//	srand(1);

	evid = rand() % 100; // random between 0 and 99

	char inputTVPath[100];
	sprintf( inputTVPath , "/users/ac.peilongw/FLIC_TV/FlicInputTVFile_1.7.1_ev%u.txt" , evid );

	// Fill FIFO with test vector
	ctrl.loadTestVectorsFromFile( "/users/ac.peilongw/FLIC_TV/FlicInputTVFile_1.7.1_ev0.txt");
	//../testvectors/events/FlicInputTVFile_1.7.1_ev0.txt" );
	bytes += ctrl.WriteTestVectorsFourChannels( flic_ssbe,FLIC::targetFpgaU1 );
	bytes += ctrl.WriteTestVectorsFourChannels( flic_ssbe,FLIC::targetFpgaU2 );

	// Send FIFO 
	bytes += ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
	bytes += ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
      }
    }

    if (option == 172) {
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 173) {
      bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 174) {
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0x0 , 4); // JW - 2017.2.28 - temporarily monitoring channel 1 instead of 0
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 , 0x0 , 4); // JW - 2017.2.28 - temporarily monitoring channel 1 instead of 0
    }
    if (option == 1741) bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0x1 , 4); 
    if (option == 1742) bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0x2 , 4); 
    if (option == 1743) bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0x3 , 4); 

    if (option == 175) {
      bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::ReadMonitoringFIFOs( flic_proc, FLIC::targetFpgaU2 );
    }

    if (option == 176) {
      bytes += StartTestVectorRun( flic_proc, flic_ssbe , ctrl , 0 , false );
      //      bytes += StartTestVectorRun( flictalk , ctrl , 0 , false );
    }


    if (option == 8) standalone::ReadRegister( flic_ssbe );
    if (option == 9) standalone::WriteRegister( flic_ssbe );
    if (option == 10) standalone::ReadRegister( flic_proc );
    if (option == 11) standalone::WriteRegister( flic_proc );
    //	 << "  75 FLIC and Spybuffer one button setup" << endl
    if (option == 75) {
      //    if (option == 2) {
      // Program proc board
      ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::DoubleProc_NoSRAMs );
      // Kick GTX112 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x200 , 0xffff );
      // Kick GTX116 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x208 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x208 , 0xffff );
      // Then check the clocks
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) );
      // Program ssbe board
      ctrl.ProgramFPGAs( flic_ssbe , FLIC::ListProc::DoubleSSBe );


      //    if (option == 3) {
      FLICSetupTool t;
      t.setFlicComm( flic_ssbe );
      t.INIT_U2_U2_U3_U4();



      //    if (option == 4) {
      t.setFlicComm( flic_proc );
      t.INIT_U1_U1_U3_U4();
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );


      //    if (option == 5) {
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU2 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
   

      /// if (option == 6) {
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU2 );

      //    if (option == 7) {
      t.setFlicComm( flic_proc );
      t.INIT_SPYBUFFERS();

    }
    if (option == 76) {
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 10 , 100 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 10 , 100 );
    }

    //	 << "  760 Send single event with options"
    if (option == 760) {

      //`uint channelMask = 0 ;
      int nevents = 0; 
      int tracksPerEvent = 0; 
      int delayBetweenEvents = 0;
      /*
      while( channelMask < 0x1 || channelMask > 0xf ) {
      printf( "Enter channel mask in hex [e.g. 0x1=first channel only, 0xf=all channels] : " );
      scanf( "%x" , &channelMask );
      }
      */
      printf( "Enter number of events you would like to run [1-32767, 0=continuous] : " );
      scanf( "%d" , &nevents );

      printf( "Enter fixed number of tracks per event [0-255] : " );
      scanf( "%d" , &tracksPerEvent );

      printf( "Enter fixed delay between events [1-4095] : " );
      scanf( "%d" , &delayBetweenEvents );

      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , nevents , tracksPerEvent, delayBetweenEvents );
      //      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 5 );
    }


    //	 << "  761 Send infinity loop of single event" << endl
    if (option == 761) {
      while (true) {
        bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 3 , 5 );
        bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 5 );
      }
    }

    if (option == 77) {
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 78) {
      bytes += ctrl.StopSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.StopSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 84) {
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0 , 4);
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 , 0 , 4);
    }
    if (option == 85) bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 );
    if (option == 86) bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 );
    if (option == 87) {
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 88) bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
    if (option == 89) bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    if (option == 90) {
      bytes += ctrl.InitProcCounters( flic_proc , FLIC::targetFpgaU1 , 0x0 );
      bytes += ctrl.InitProcCounters( flic_proc , FLIC::targetFpgaU2 , 0x0 );
    }
    if (option == 91) {
      bytes += standalone::DumpProcCounters( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpProcCounters( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 92) {
      bytes += standalone::DumpGlobalStatus( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpGlobalStatus( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 93) {
      bytes += standalone::DumpRawSLINKStatus( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpRawSLINKStatus( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 222) {
      cout << "--- SSBe Board ---" << endl
           << ctrl.CheckStatus( flic_ssbe ) << endl
           << "--- Processor Board ---" << endl
           << ctrl.CheckStatus( flic_proc ) << endl;
    }
    if (option == 223) {
      cout << ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 224) {
      cout << ctrl.CheckXOFFCounters( flic_proc , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckXOFFCounters( flic_proc , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 225) {
      bytes += ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU2 );
    }
    if( option == 226 ) bytes += standalone::DumpSpybufferIPConfig( flic_proc );
    //option=0;//One time!
  }//while option
  return bytes;
}


// PW - Feb. 20, 2017: I wonder whether this Setup() function is used in setting up FPGA. I checked the code above and also other code in the "trunk" directory. My inference is that this function is not used anywhere.
int Setup( flic_comm *flic_ssbe , flic_comm *flic_proc ) {

  int bytes = 0;

  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x0060 ); // U2 Set 0x2 0x60 #Put U2 112 SM in Reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x0060 ); // U2 Set 0x2 0x60 #Put U2 112 SM in Reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x6060 ); // U1 Set 0x2 0x6060 #Put U1 112/116 SM in Reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x6060 ); // U1 Set 0x2 0x6060 #Put U1 112/116 SM in Reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0xFFFF ); // U2 Write 0x200 0xFFFF #Whack U2 GTX112
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0xFFFF ); // U2 Write 0x200 0xFFFF #Whack U2 GTX112
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001F , 0x00F0 ); // U2 Write 0x1F 0x0F0 #Set U2 GTX112 Send Commas
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001F , 0x00F0 ); // U2 Write 0x1F 0x0F0 #Set U2 GTX112 Send Commas
  //20150805 : would be prudent here to ensure U2 TDIS is OFF , to ensure that the photons are going out...

  // MBO 20150730: New Code
  // Ok , at this point , we need to look at configuring the internal quads. This is an intial stab and the ideal placement in the setup may be elsewhere.
  //
  // #if 0

  /** suspected bugs in this section **/

  /** No bugs found... **/

  // Put U1-GTX113 and U1-GTX114 and U1-GTX115 SM into reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x078C );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x078C );
  // Put U2-GTX113 and U2-GTX114 and U2-GTX115 SM into reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x078C );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x078C );
  // Put U3-GTX112 and U3-GTX113 and U3-GTX114 SM into reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdSet , 0x00000002 , 0x01EC );
  // Put U4-GTX112 and U4-GTX113 and U4-GTX114 SM into reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdSet , 0x00000002 , 0x01EC );
  // Set U1-GTX113 and U1-GTX114 and U1-GTX115 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000012 , 0x000F ); // GTX113 -- U1<->U2
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000013 , 0x000F ); // GTX114 -- U1<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000014 , 0x000F ); // GTX115 -- U1<->U4
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000012 , 0x000F ); // GTX113 -- U1<->U2
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000013 , 0x000F ); // GTX114 -- U1<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000014 , 0x000F ); // GTX115 -- U1<->U4
  // Set U2-GTX113 and U2-GTX114 and U2-GTX115 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000024 , 0x000F ); // GTX113 -- U2<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000025 , 0x000F ); // GTX114 -- U2<->U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000026 , 0x000F ); // GTX115 -- U2<->U4
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000024 , 0x000F ); // GTX113 -- U2<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000025 , 0x000F ); // GTX114 -- U2<->U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000026 , 0x000F ); // GTX115 -- U2<->U4
  // Set U3-GTX112 and U3-GTX113 and U3-GTX114 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U3<->U
  // Set U4-GTX112 and U4-GTX113 and U4-GTX114 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U4<->U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U4<->U1
  // Reset U1-GTX113 and U1-GTX114 and U1-GTX115 RX & TX Gracefully
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0202 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0204 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0206 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0202 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0204 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0206 , 0xFFFF );
  // Reset U2-GTX113 and U2-GTX114 and U2-GTX115 RX Gracefully
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000206 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  //
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000206 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  // Reset U3-GTX112 and U3-GTX113 and U3-GTX114 RX Gracefully
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000200 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  // Reset U4-GTX112 and U4-GTX113 and U4-GTX114 RX Gracefully
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000200 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  // Take U1-GTX113 and U1-GTX114 and U1-GTX115 SM out of reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x078C );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x078C );
  // Take U2-GTX113 and U2-GTX114 and U2-GTX115 SM out of reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x078C );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x078C );
  // Take U3-GTX112 and U3-GTX113 and U3-GTX114 SM out of reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdClear , 0x00000002 , 0x01EC );
  // Take U4-GTX112 and U4-GTX113 and U4-GTX114 SM out of reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdClear , 0x00000002 , 0x01EC );


  // DEBUG USE ONLY:
  // Set match registers in U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002A , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002B , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002C , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002D , 0xFFFF );
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002A , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002B , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002C , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002D , 0xFFFF );

  // MBO 20150730: End new code , resume Jeremy's prcedure.

  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x8888 ); // U1 Write 0x200 0x8888 #Reset U1 GTX112 RX logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x8888 ); // U1 Write 0x200 0x8888 #Reset U1 GTX112 RX logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL

  //JTA: now that we have flow control you have to reset the U1 TX , too( 20150805 )
  // Failure to do this means U1 GTX112 TX clock is not present , means SSB( U2 ) can't
  // get any flow control messages or anything else.
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x2222 ); // U1 Write 0x200 0x2222 #Reset U1 GTX112 TX Logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x2222 ); // U1 Write 0x200 0x2222 #Reset U1 GTX112 TX Logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  // end add

  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000003 , 0x00F0 ); // U1 Set 0x3 0x0F0 #Set RTM TDIS Must be done before GTX116 Resets
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000208 , 0xFFFF ); // U1 Write 0x208 0xFFFF #Whack U1 GTX116
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x6060 ); // U1 Clear 0x2 0x6060 #Take U1 112/116 SM out of Reset
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000003 , 0x00F0 ); // U1 Set 0x3 0x0F0 #Set RTM TDIS Must be done before GTX116 Resets
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000208 , 0xFFFF ); // U1 Write 0x208 0xFFFF #Whack U1 GTX116
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x6060 ); // U1 Clear 0x2 0x6060 #Take U1 112/116 SM out of Reset

  // JTA 20150805 you also have to turn on the SFP transmitters at the front of the board( U1 )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000004 , 0x000F ); //clear TDIS bits in SFP Ctl Reg
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000004 , 0x00F0 ); //Set TX Mux bits of U1 to send flow control data , not PRBS data
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000004 , 0x000F ); //clear TDIS bits in SFP Ctl Reg
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000004 , 0x00F0 ); //Set TX Mux bits of U1 to send flow control data , not PRBS data
  // end add



  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x0060 ); // U2 Clear 0x2 0x60 #Take U2 112 SM out of Reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x0060 ); // U2 Clear 0x2 0x60 #Take U2 112 SM out of Reset
  // //
  // // 4. Setup for processing and emulation.


  // should be a Send Set , not a Send Write( JTA 20150805 )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x0012 ); // U1 Write 0x2 0x12 #Set SRAM access , and enable Core Crate receiver and data merge machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x0012 ); // U1 Write 0x2 0x12 #Set SRAM access , and enable Core Crate receiver and data merge machines
  //end change


  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001F , 0xFFFF ); // U1 Write 0x1F 0xFFFF #Hold All
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001F , 0x0000 ); // U1 Write 0x1F 0x0000 #Clear all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001F , 0xFFFF ); // U1 Write 0x1F 0xFFFF #Hold All
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001F , 0x0000 ); // U1 Write 0x1F 0x0000 #Clear all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  // //( One can never be too careful. )
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000201 , 0x000F ); // U2 Write 0x201 0xF #Reset SSB Emulator
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000201 , 0x000F ); // U2 Write 0x201 0xF #Reset SSB Emulator
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000003 , 0x0F00 ); // U1 Clear 0x3 0xF00 #Clear GO bit of reset machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000209 , 0x0F00 ); // U1 Write 0x209 0xF00 #Request reset machines to re-arm
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000003 , 0x0F00 ); // U1 Clear 0x3 0xF00 #Clear GO bit of reset machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000209 , 0x0F00 ); // U1 Write 0x209 0xF00 #Request reset machines to re-arm
  //? SendSet( targetFpgaU1 , 0x00000002 , 0x0F00 ); // U1 Set 0x2 0xF00 #Set the GO bit to the reset machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x0008 ); // U1 Set 0x2 0x8 #Switch SRAM control back to SM
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x0008 ); // U1 Set 0x2 0x8 #Switch SRAM control back to SM
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001D , 0xDDDD ); // U2 Write 0x1D 0xDDDD #SSB emulator status words
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001D , 0xDDDD ); // U2 Write 0x1D 0xDDDD #SSB emulator status words
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001E , 0xEEEE ); // U2 Write 0x1E 0xEEEE #SSB emulator status words
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001E , 0xEEEE ); // U2 Write 0x1E 0xEEEE #SSB emulator status words

  //20160801: FLIC status "word" registers are now the FLIC ERROR MASK registers , should be initialized but to some as-yet unknown value.
  //until that value is known , make 'em 0.
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000015 , 0x0 ); // U1 Write 0x15 0x1515 #FLIC status words
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000016 , 0x0 ); // U1 Write 0x16 0x1616 #FLIC status words
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000015 , 0x0 ); // U1 Write 0x15 0x1515 #FLIC status words
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000016 , 0x0 ); // U1 Write 0x16 0x1616 #FLIC status words
  //end change 20160801
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xD0F0 ); // U1 Write 0x20F 0xD0F0 #Reset all FIFO flags
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xD0F0 ); // U1 Write 0x20F 0xD0F0 #Reset all FIFO flags
  // //

  // MBO 20160928: Bring up all slinks in U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000009 , 0x000F ); // SLINK_CTL2_REG
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000009 , 0x000F ); // SLINK_CTL2_REG
  // MBO 20160928: Set all slinks to data dump mode in U1:
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000009 , 0x00F0 ); // SLINK_CTL2_REG
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000009 , 0x00F0 ); // SLINK_CTL2_REG

  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdSet , 0x0000020F , 0x0001 ); // Pulse the reset bit.
  // MBO: Register 0x6a contain the 8 channel processing enables bits 7:0 , and the master processing enabled bit 8.
  // This also sets which channels are expected to report fragments.
  // Take U4's Tagged Event Receivers out of reset.
  // SendSet( targetFpgaU4 , 0x0000006A , 0x0101 ); // U1-ch1
  // SendSet( targetFpgaU4 , 0x0000006A , 0x0103 ); // U1-ch1+ch2
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdSet , 0x0000006A , 0x010F ); // U1-all
  // SendSet( targetFpgaU4 , 0x0000006A , 0x01F0 ); // U2-all

  return bytes;

}



int StartTestVectorRun( flic_comm *flic_proc, flic_comm *flic_ssbe , FLICController ctrl , int nevents , bool verbose ) {
  int bytes = 0;

  ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU1 );
  ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU2 );

  // tell U2 to pay attention to "uase_data" signals coming from U1 in case
  // things get backed up
  flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x2d , 0x1111 );
  flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x2d , 0x1111 );

  // Just to be extra careful, dump the monitoring FIFOs a couple times to make sure they're empty,
  // then re-reset.
  ushort tmparray[256];
  for( ushort i = 0 ; i < 4 ; i++ ) { // reading full FIFO requires 4 block reads
    for( ushort j = 0 ; j < 4 ; j++ ) {
      bytes += flic_proc->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x110+(2*i) , tmparray , 256 );
      bytes += flic_proc->blockread( FLIC::targetFpgaU2 , FLIC::cmdFifoRead , 0x110+(2*i) , tmparray , 256 );
      usleep(10000);
    }
  }

  cout << "Loading lookup tables onto SRAMs" << endl;
  /*
    DownloadSram( flictalk , FLIC::targetSram1Fpga1 );
    DownloadSram( flictalk , FLIC::targetSram2Fpga1 );
    DownloadSram( flictalk , FLIC::targetSram3Fpga1 );
    DownloadSram( flictalk , FLIC::targetSram4Fpga1 );
  */

  // initialize random number generator with seed=1
  srand( 1 );

  const ushort nEventsInTVDB = 100;

  uint totalMon0Errors = 0;
  uint totalMon3Errors = 0;
  uint totalMon0ErrorsByPipe[4] = {0,0,0,0};
  uint totalMon3ErrorsByPipe[4] = {0,0,0,0};
  uint totalMon0ErrorsByEvid[nEventsInTVDB];
  uint totalMon3ErrorsByEvid[nEventsInTVDB];

  uint totalEventsByPipe[4] = {0,0,0,0};
  uint totalEventsByEvid[nEventsInTVDB];

  uint totalEventsSkipped0 = 0;
  uint totalEventsSkipped1 = 0;

  for( ushort i = 0 ; i < nEventsInTVDB ; i++ ) {
    totalMon0ErrorsByEvid[i] = 0;
    totalMon3ErrorsByEvid[i] = 0;
    totalEventsByEvid[i] = 0;
  }

  while( nevents < 1 ) {
    printf( "Enter number of events you would like to run : " );
    scanf( "%d" , &nevents );
  }

  int mypipe = 4;
  printf( "Which pipeline do you want to monitor? [0-3, 4=>choose random each event] : " );
  scanf( "%d" , &mypipe );
  printf( "\n" );

  // As we pass data through the system, save the Flic output to a file.
  // This can be used to compare to robin data as an additional cross-check.
  ofstream robinExpectedInputData( "robinExpectedInputData.txt" );
  robinExpectedInputData << hex;

  for( uint ievent = 0 , L1ID = 0 ; ievent < uint(nevents) ; ievent++ , L1ID++ ) {

    uint eventMon0Errors = 0;
    uint eventMon3Errors = 0;

    // Use random number generator to determine the TV event that we want to load
    // and process. Also randomly choose the pipeline that we want to monitor.
    uint pipe = mypipe;
    if( pipe > 3 ) pipe = rand() % 4; // random between 0 and 3
    uint evid = rand() % nEventsInTVDB; // random between 0 and 99

    // Load input test vectors into U2 SSBe FIFO, all four pipes
    char inputTVPath[100];
    char outputTVPath[100];
    sprintf( inputTVPath , "/users/ac.peilongw/FLIC_TV/FlicInputTVFile_1.7.1_ev%u.txt" , evid );
    sprintf( outputTVPath , "/users/ac.peilongw/FLIC_TV/outputFromExperiment/FlicOutputTVFile_1.7.1_ev%u.txt" , evid );
    ctrl.loadTestVectorsFromFile( inputTVPath , false , L1ID );
    ctrl.WriteTestVectorsOneChannel( flic_ssbe , FLIC::targetFpgaU1 , pipe , false ); // (faster than writing all four channels)

    // Make sure all monitoring FIFOs are watching the desired pipeline
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x17 , (pipe<<12) + (pipe<<8) + (pipe<<4) + pipe );
    bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x17 , pipe ); // set mon0 to pipe
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x18 , pipe ); // set mon1 to pipe
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x19 , pipe ); // set mon2 to pipe
    bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x1a , pipe ); // set mon3 to pipe

    // Now reset & arm the monitoring FIFOs
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x20c , 0x1111 );
    bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x20c , 0xf ); // one bit for each of four mon fifos

    // Send event through the system
    ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU1 , 0x1<<pipe );

    // Load data in monitoring FIFOs
    ushort dataMon0[1024];
    ushort dataMon3[1024];
    for( ushort i = 0 ; i < 4 ; i++ ) { // reading full FIFO requires 4 block reads
      bytes += flic_proc->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x110 , &dataMon0[i*256] , 256 );
      usleep(1000);
      bytes += flic_proc->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x116 , &dataMon3[i*256] , 256 );
      usleep(1000);
    }

    // Temporary fix to skip over cases where pseudo-random event is sent instead of the
    // event in the SSBe FIFO
    bool skipEvent = false;
    if( dataMon0[0] == 0x0 && dataMon0[1] == 0x0 ) {
      totalEventsSkipped0++;
      printf( "Event %5u : Loaded TV ID %2u, Monitoring pipe %1u : Skipping & resetting because header = 0x0 !!\n" ,
              ievent , evid , pipe );
      skipEvent = true;
    } else if( dataMon0[2] == 0x1234 && dataMon0[3] == 0x5678 ) {
      // try sending the event again in order to empty out the SSBe FIFO and be ready for next event
      ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      ctrl.SendFIFOData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      totalEventsSkipped1++;
      printf( "Event %5u : Loaded TV ID %2u, Monitoring pipe %1u : Skipping & resetting because RuNumber = 0x12345678 !!\n" ,
              ievent , evid , pipe );
      skipEvent = true;
    }

    // comment out by Peilong on Oct. 23 2017
    
    if( skipEvent ) {
      /*
      // need to reset all the state machines to ensure we don't sabotage the following event :(
      ctrl.IdleBoard(flictalk);
      ctrl.InitStateMachines(flictalk);
      ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 0 , 4000 );
      ctrl.ResetSSBe(flictalk,FLIC::targetFpgaU2);
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x9 , 0xf ); // set the "force s-link up" bit in each of the four pipelines
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x209 , 0xf00 ); // reset s-link controller to acknowledge the above control
      return bytes;
      continue;
      */
    }
    
    totalEventsByPipe[pipe]++;
    totalEventsByEvid[evid]++;

    // Open files with input/output TVs and check to make sure the data in monitoring FIFOs
    // matches the expected data in the input/output TVs
    ifstream inputTVFile( inputTVPath );
    ifstream outputTVFile( outputTVPath );
    if( !inputTVFile.is_open() || !outputTVFile.is_open() ) {
      cout << "Unable to open TVs at one of the following paths, ending loop: " << endl;
	// Sep 28 2017 Peilong: comment out for cmake compiling
	//           << "   " << inputTVFile << endl
	//           << "   " << outputTVFile << endl;
      return bytes;
    }

    string tmpword;
    ushort tmpnum;
    uint tmpnum_robin = 0;

    ofstream mon0file( "tmp_mon0.txt" );
    ofstream mon3file( "tmp_mon3.txt" );
    mon0file << hex;
    mon3file << hex;

    // drop the event header from inputTVFile since it doesn't show up in mon fifo 0
    getline( inputTVFile , tmpword );
    getline( inputTVFile , tmpword );
    getline( inputTVFile , tmpword );
    getline( inputTVFile , tmpword );

    for( ushort iword = 0 ; iword < 1024 && !inputTVFile.eof() ; iword++ ) {
      getline( inputTVFile , tmpword );
      istringstream iss( tmpword );
      if( tmpword.length() == 0 ) break; // in case there is a trailing empty line in file
      iss >> hex >> tmpnum;

      // Replace L1ID in the TV file with the one loaded for this event
      //if(      iword == FLIC::EventFormat::INPUT_L1ID_POSITION )   tmpnum = (L1ID >> 16);
      //else if( iword == FLIC::EventFormat::INPUT_L1ID_POSITION+1 ) tmpnum = (L1ID & 0xffff);
      // updated iword values here after dropping event header from input TV file
      if(      iword == 2 ) tmpnum = (L1ID >> 16);
      else if( iword == 3 ) tmpnum = (L1ID & 0xffff);

      bool match = ( dataMon0[iword] == tmpnum );
      if( !match ) eventMon0Errors++;

      char outstr[120];
      sprintf( outstr , "%4x : 0x%04x  %s  %s\n" , iword , dataMon0[iword] , (match?"   ==   ":"XX != XX") , tmpword.c_str() );
      mon0file << outstr;
      if( verbose ) cout << outstr << flush;

      // In every block read (of 256 words) we accidentally miss the last word due to a sync bug
      // Temporary workaround...
      if( (iword+1) % 256 == 0 && !inputTVFile.eof() ) getline( inputTVFile , tmpword );
    }

    for( ushort iword = 0 ; iword < 1024 && !outputTVFile.eof() ; iword++ ) {
      getline( outputTVFile , tmpword );
      istringstream iss( tmpword );
      if( tmpword.length() == 0 ) break; // in case there is a trailing empty line in file
      iss >> hex >> tmpnum;

      // Replace L1ID in the TV file with the one loaded for this event
      if(      iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION )   tmpnum = (L1ID >> 16);
      else if( iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION+1 ) tmpnum = (L1ID & 0xffff);

      bool match = ( dataMon3[iword] == tmpnum );
      if( !match ) eventMon3Errors++;

      char outstr[120];
      sprintf( outstr , "%4x : 0x%04x  %s  %s\n" , iword , dataMon3[iword] , (match?"   ==   ":"XX != XX") , tmpword.c_str() );
      mon3file << outstr;
      if( verbose ) cout << outstr << flush;

      // In every block read (of 256 words) we accidentally miss the last word due to a sync bug
      // Temporary workaround...
      if( (iword+1) % 256 == 0 && !outputTVFile.eof() ) getline( outputTVFile , tmpword );
    }

    mon0file.close();
    mon3file.close();

    // Save the expected flic data to file so we can do additional cross-checks
    // against the robin data
    outputTVFile.clear();
    outputTVFile.seekg( 0 , ios::beg );
    for( ushort iword = 0 ; true ; iword++ ) {
      getline( outputTVFile , tmpword );
      istringstream iss( tmpword );
      if( tmpword.length() == 0 ) break;
      iss >> hex >> tmpnum;

      // Replace L1ID in the TV file with the one loaded for this event
      if(      iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION )   tmpnum = (L1ID >> 16);
      else if( iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION+1 ) tmpnum = (L1ID & 0xffff);

      // Write to file, 32 bits at a time
      if( iword % 2 == 0 ) tmpnum_robin = (tmpnum << 16);
      else {
        tmpnum_robin += (tmpnum & 0xffff);
        robinExpectedInputData << setw(8) << setfill('0') << tmpnum_robin << endl;
      }
    }

    inputTVFile.close();
    outputTVFile.close();

    totalMon0Errors += eventMon0Errors;
    totalMon3Errors += eventMon3Errors;
    totalMon0ErrorsByPipe[pipe] += eventMon0Errors;
    totalMon3ErrorsByPipe[pipe] += eventMon3Errors;
    totalMon0ErrorsByEvid[evid] += eventMon0Errors;
    totalMon3ErrorsByEvid[evid] += eventMon3Errors;

    printf( "Event %5u : Loaded TV ID %2u, Monitoring pipe %1u : nInputErrors = %4u (total = %u), nOutputErrors = %4u (total = %u)\n" ,
            ievent , evid , pipe , eventMon0Errors , totalMon0Errors , eventMon3Errors , totalMon3Errors );

  }

  robinExpectedInputData.close();
  printf( "Expected FLIC output saved to robinExpectedInputData.txt\n" );

  printf( "\n=== Summary ===\n\n" );
  printf( "%20s %2s %15s %15s %15s %15s %15s\n" , "" , "" , "nEvents" , "nErrorsIn" , "Avg(nErrorsIn)" , "nErrorsOu" , "Avg(nErrorsOu)" );
  for( ushort pipe = 0 ; pipe < 4 ; pipe++ )
    printf( "%20s %2u %15u %15u %15g %15u %15g\n" ,
            "Pipeline" , pipe , totalEventsByPipe[pipe] ,
            totalMon0ErrorsByPipe[pipe] , double(totalMon0ErrorsByPipe[pipe])/double(totalEventsByPipe[pipe]) ,
            totalMon3ErrorsByPipe[pipe] , double(totalMon3ErrorsByPipe[pipe])/double(totalEventsByPipe[pipe]) );
  for( ushort evid = 0 ; evid < nEventsInTVDB ; evid++ )
    printf( "%20s %2u %15u %15u %15g %15u %15g\n" ,
            "Event Index" , evid , totalEventsByEvid[evid] ,
            totalMon0ErrorsByEvid[evid] , double(totalMon0ErrorsByEvid[evid])/double(totalEventsByEvid[evid]) ,
            totalMon3ErrorsByEvid[evid] , double(totalMon3ErrorsByEvid[evid])/double(totalEventsByEvid[evid]) );
  printf( "%2s %20s %15u %15u %15g %15u %15g\n" ,
          "" , "Total" , nevents ,
          totalMon0Errors , double(totalMon0Errors)/double(nevents) ,
          totalMon3Errors , double(totalMon3Errors)/double(nevents) );
  printf( "%2s %20s %15u\n" , "" , "Skipped(0x0000)" , totalEventsSkipped0 );
  printf( "%2s %20s %15u\n" , "" , "Skipped(0x1234)" , totalEventsSkipped1 );
  printf( "\n" );

  return bytes;
}

