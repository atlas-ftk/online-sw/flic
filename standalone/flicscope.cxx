#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flictalk , FLICController ctrl );

int StartTestVectorRun( flic_comm *flictalk , FLICController ctrl , int nevents=0 , bool verbose=false );

int main() {
  int bytes = 0;
  FLICController ctrl;

  while( true ) {
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic1 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    bytes += Option( flic1 , ctrl );
    delete flic1; // sockets closed in destructor
  }

  return bytes;
}


int Option( flic_comm *flictalk , FLICController ctrl ) {
  FLICSetupTool t;
  t.setFlicComm( flictalk );
  int option=1;
  int bytes=0;
  while(option!=0){
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On                      100 Power Off" << endl
         << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
         << " Basic initialization controls..." << endl
         << "   2 Program All FPGAs               3 Idle Board" << endl
         << "   4 Setup One Channel Proc.         5 Send sync word to SSB" << endl
	 << " 300 FLICWare setup" << endl
         << "   6 Reset SSBe                     20 Send single event from SSBe on all channels" << endl
         << standalone::line << endl
	 << " Spy buffer testing option 1000 - 100x" << endl
	 << endl
	 << "1201 Program FLIC U1 and U2 as processor - 5" << endl
	 << "1202 Program FLIC U1 and U2 as emulator - 6" << endl
	 << "1203 LoopBack Mode" << endl
	 << "1003 Setup SSBe board (U1 and U2)-6" << endl
	 << "1401 Setup U1 and U2 on processor FLIC - 5" << endl
	 << "1402 Setup SSBe board again (U1 and U2) - 6" << endl
	 << "1501 Send sync word from Proc to SSBe (U1 to U1) - 5" << endl
	 << "1502 Start SSBe Data after receive sync word on U1 - 6" <<endl
	 << "1503 Send sync word from Proc to SSBe (U2 to U2) - 5" << endl
	 << "1504 Start SSBe Data after receive sync word on U2 - 6" << endl
	 << "1006 Reset SSBe - 6" << endl
	 << "1007 Setup Spy buffers on processor FLIC - 5" << endl

	 << "1010 Setup Spy buffers on processor FLIC" << endl
	 << "1011 Send single event from SSBe on all channels(from both U1 and U2)" << endl
	 << "1012 Start SSBe on all channels(only U1)" << endl
	 << "1002 Resetup flic for spybuffer (step 3)" << endl

         << standalone::line << endl
         << "  77 Start SSBe data with new configuration" << endl
         << "  78 Stop SSBe data but leave configuration unchanged" << endl
         << standalone::line << endl
         << " Restart SSBe data on channel X using latest SSBe configuration..." << endl
         << "  21 Channel 1                      22 Channel 2" << endl
         << "  21 Channel 3                      22 Channel 4" << endl
         << "  25 All Channels                   26 Use your own channel mask" << endl
         << standalone::line << endl
         << "   7 Read a register                 8 Write a register" << endl
         << "   9 Download Firmware" << endl
         << standalone::line << endl
         << "  11 Pulse flow control to SSB      12 Reset S-Link" << endl
         << "  13 Change Emulation Parameters" << endl
         << standalone::line << endl
         << " Test Vector controls..." << endl
         << "  70 Fill TV FIFOs, 4 channels      71 Send TVs from FIFOs, 4 channels" << endl
         << "  72 Reset & arm flowcontrol FIFOs  73 Read flowcontrol FIFOs" << endl
         << "  74 Reset & arm monitoring FIFOs   75 Read monitoring FIFOs" << endl
         << "  76 Fancy TV test" << endl
         << standalone::line << endl
         << " Lookup table controls..." << endl
         << "  80 Write lookup table to FLASH    81 Read lookup table from FLASH to validate" << endl
         << "  82 Write ALL SRAM data to FLASH   83 Read & Validate ALL SRAM data in FLASH" << endl
         << "  84 Write lookup table to SRAM     85 Read lookup table from SRAM" << endl
         << "  86 Write ALL SRAM data            87 Read & Validate ALL SRAM data" << endl
         << "  88 Write U1 SRAM data" << endl
         << standalone::line << endl
         << " Diagnostics..." << endl
         << "  90 Init counters to COUNT MODE    91 Init counters to RATE MODE" << endl
         << "  92 Dump counters" << endl
	 << "  93 Dump global status" << endl
	 << "  94 Dump SLINK status" << endl
         << " 222 Check program status          223 Check U1 clock counters" << endl
         << " 224 Check XOFF counters           225 Change to \"dump data on the floor\" mode" << endl
	 << " 226 Dump U3/U4 spybuffer IP settings" << endl
	 << " 227 Dump U3/U4 diagnostics" << endl
	 << " 228 Dump CURRENT_L1ID" << endl
         << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) bytes+=ctrl.PICPowerOn( flictalk );
    if (option == 100) bytes+=ctrl.PICPowerOff( flictalk );
    if (option == 200) bytes+=ctrl.PICPowerFromIPMC( flictalk );
    if (option == 2) {
      ctrl.ProgramFPGAs( flictalk);
      // kick GTX112 in case clocks didn't come up correctly
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff ); 
    }
    if (option == 300) t.INIT_U1_U2_U3_U4();
    /*
    if (option == 301) t.standard_u1_init_stage_1( FLIC::targetFpgaU1 );
    if (option == 302) t.standard_u2_init_stage_1( FLIC::targetFpgaU2 );
    if (option == 303) t.standard_u3_u4_init_stage_1( FLIC::targetFpgaU3 );
    if (option == 304) t.standard_u3_u4_init_stage_1( FLIC::targetFpgaU4 );
    if (option == 305) t.standard_u1_init_stage_2( FLIC::targetFpgaU1 );
    if (option == 306) t.standard_u2_init_stage_2( FLIC::targetFpgaU2 );
    if (option == 307) t.standard_u1_init_stage_3( FLIC::targetFpgaU1 );
    if (option == 308) t.standard_u3_u4_init_stage_3( FLIC::targetFpgaU3 );
    if (option == 309) t.standard_u3_u4_init_stage_3( FLIC::targetFpgaU4 );
    */
    if (option == 3) bytes += ctrl.IdleBoard(flictalk);
    if (option == 4) bytes += ctrl.InitStateMachines(flictalk);
    if (option == 5) bytes += ctrl.SyncWithSSB( flictalk , FLIC::targetFpgaU1 );
    if (option == 6) bytes += ctrl.ResetSSBe( flictalk , FLIC::targetFpgaU2 );
    if (option == 20) bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 1 , 4000 );

    //	 << "1201 Program FLIC U1 and U2 as processor - 5" << endl
    if (option == 1201) {
    // Program proc board
      ctrl.ProgramFPGAs( flictalk , FLIC::ListProc::DoubleProc_NoSRAMs );
      // Kick GTX112 to make sure clocks come up correctly
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
      flictalk->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x200 , 0xffff );
      // Kick GTX116 to make sure clocks come up correctly
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x208 , 0xffff );
      flictalk->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x208 , 0xffff );
      // Then check the clocks
      //assert( ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU1 ) );
      //assert( ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU2 ) );
    }

    //	 << "1202 Program FLIC U1 and U2 as emulator - 6" << endl
    if (option == 1202) {
      // Program ssbe board
      ctrl.ProgramFPGAs( flictalk , FLIC::ListProc::DoubleSSBe );
    }
    if (option == 1203) {
      ctrl.ProgramFPGAs( flictalk , {FLIC::targetFpgaU1}, {FLIC::targetFpgaU2}, false);
    }
    //	 << "1003 Setup SSBe board (U1 and U2)-6" << endl
    if (option == 1003) {
      FLICSetupTool t;
      t.setFlicComm( flictalk );
      t.INIT_U2_U2_U3_U4();
    }

    //	 << "1401 Setup U1 and U2 on processor FLIC - 5" << endl
    if (option == 1401) {
      FLICSetupTool t;
      t.setFlicComm( flictalk );
      t.INIT_U1_U1_U3_U4();
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flictalk->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
    }

    //	 << "1402 Setup SSBe board again (U1 and U2) - 6" << endl
    if (option == 1402) {
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flictalk->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
    }

    //	 << "1501 Send sync word from Proc to SSBe (U1 to U1) - 5" << endl
    if (option == 1501) {
      bytes += ctrl.SyncWithSSB( flictalk , FLIC::targetFpgaU1 );
    }

    //	 << "1502 Start SSBe Data after receive sync word on U1 - 6" <<endl
    if (option == 1502) {
            bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
    }
    
    //	 << "1503 Send sync word from Proc to SSBe (U2 to U2) - 5" << endl
    if (option == 1503) {
      bytes += ctrl.SyncWithSSB( flictalk , FLIC::targetFpgaU2 );
    }
    
    //	 << "1504 Start SSBe Data after receive sync word on U2 - 6" << endl
    if (option == 1504) {
      bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    }
    
    //	 << "1006 Reset SSBe - 6" << endl
    if (option == 1006) {
      bytes += ctrl.ResetSSBe( flictalk , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetSSBe( flictalk , FLIC::targetFpgaU2 );
    }

    //	 << "1007 Setup Spy buffers on processor FLIC - 5" << endl
    if (option == 1007) {
      FLICSetupTool t;
      t.setFlicComm( flictalk );
      t.INIT_SPYBUFFERS();
    }


    //	 << "1010 Setup Spy buffers on processor FLIC" << endl
    if (option == 1010) {
      FLICSetupTool t;
      t.setFlicComm( flictalk );
      t.INIT_SPYBUFFERS();
    }

    //	 << "1011 Send single event from SSBe on all channels(from both U1 and U2)" << endl
    if (option == 1011) {
      bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 , 0xf , 1 , 3 , 100 );
      bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 100 );
    }

    if (option == 1012) {

      bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 );
    }

    //	 << "1002 Resetup flic for spybuffer (step 3)" << endl
   if (option == 1002) {
     FLICSetupTool t;
     t.setFlicComm( flictalk );
     t.INIT_U1_U1_U3_U4();

     flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
     flictalk->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
 
   }

    if (option == 77) bytes += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 );
    if (option == 78) bytes += ctrl.StopSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf );
    if (option == 21) bytes += ctrl.RestartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x1 );
    if (option == 22) bytes += ctrl.RestartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x2 );
    if (option == 23) bytes += ctrl.RestartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x4 );
    if (option == 24) bytes += ctrl.RestartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x8 );
    if (option == 25) bytes += ctrl.RestartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf );
    if (option == 26) bytes += ctrl.RestartSSBeData( flictalk , FLIC::targetFpgaU2 , -1 );
    if (option == 7) bytes += standalone::ReadRegister( flictalk );
    if (option == 8) bytes += standalone::WriteRegister( flictalk );
    if (option == 9) bytes += standalone::DownloadFirmwareToFlash( flictalk );
    if (option == 11) bytes += ctrl.PulseFlowControlToSSB( flictalk , FLIC::targetFpgaU1 );
    if (option == 12) bytes += ctrl.ResetSLinks( flictalk , FLIC::targetFpgaU1 );
    if (option == 13) bytes += standalone::ChangeSSBeRunParam( flictalk , FLIC::targetFpgaU2 );
    if (option == 70) {
      ctrl.loadTestVectorsFromFile("../testvectors/events/FlicInputTVFile_1.7.1_ev0.txt");
//../testvectors/events/FlicInputTVFile_1.7.1_ev0.txt" );
      bytes += ctrl.WriteTestVectorsFourChannels( flictalk,FLIC::targetFpgaU2 );
    }
    if (option == 71) bytes += ctrl.SendFIFOData( flictalk , FLIC::targetFpgaU2 , 0xf );
    if (option == 72) bytes += ctrl.ResetArmFlowControlFIFOs( flictalk , FLIC::targetFpgaU1 );
    if (option == 73) bytes += standalone::ReadFlowControlFIFOs( flictalk , FLIC::targetFpgaU1 );
    if (option == 74) bytes += ctrl.ResetArmMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 , 0x0 , 0); // JW - 2017.2.28 - temporarily monitoring channel 1 instead of 0
    if (option == 741) bytes += ctrl.ResetArmMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 , 0x1 , 4); 
    if (option == 742) bytes += ctrl.ResetArmMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 , 0x2 , 4); 
    if (option == 743) bytes += ctrl.ResetArmMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 , 0x3 , 4); 
    if (option == 75) bytes += standalone::ReadMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 );
    if (option == 76) bytes += StartTestVectorRun( flictalk , ctrl , 0 , false );
    if (option == 80) bytes += standalone::DownloadSramToFlash( flictalk );
    if (option == 81) bytes += standalone::ReadSramFromFlash( flictalk );
    if (option == 82) for( uint isram = FLIC::targetSram1Fpga1 ; isram <= FLIC::targetSram4Fpga2 ; isram++ ) bytes += standalone::DownloadSramToFlash( flictalk , isram );
    if (option == 83) for( uint isram = FLIC::targetSram1Fpga1 ; isram <= FLIC::targetSram4Fpga2 ; isram++ ) bytes += standalone::ReadSramFromFlash( flictalk , isram );
    if (option == 84) bytes += standalone::DownloadSram( flictalk );
    if (option == 85) bytes += standalone::ReadSram( flictalk );
    if (option == 86) for( uint isram = FLIC::targetSram1Fpga1 ; isram <= FLIC::targetSram4Fpga2 ; isram++ ) bytes += standalone::DownloadSram( flictalk , isram );
    if (option == 87) for( uint isram = FLIC::targetSram1Fpga1 ; isram <= FLIC::targetSram4Fpga2 ; isram++ ) bytes += standalone::ReadSram( flictalk , isram );
    if (option == 88) for( uint isram = FLIC::targetSram1Fpga1 ; isram <= FLIC::targetSram4Fpga1 ; isram++ ) bytes += standalone::DownloadSram( flictalk , isram );
    if (option == 90) bytes += ctrl.InitProcCounters( flictalk , FLIC::targetFpgaU1 , 0x0000 );
    if (option == 91) bytes += ctrl.InitProcCounters( flictalk , FLIC::targetFpgaU1 , 0xFFFF );
    if (option == 92) bytes += standalone::DumpProcCounters( flictalk , FLIC::targetFpgaU1 );
    if (option == 93) bytes += standalone::DumpGlobalStatus( flictalk , FLIC::targetFpgaU1 );
    if (option == 94) bytes += standalone::DumpRawSLINKStatus( flictalk , FLIC::targetFpgaU1 );
    if (option == 222) cout << ctrl.CheckStatus( flictalk ) << endl;
    if (option == 223) cout << ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU1 ) << endl;
    if (option == 224) cout << ctrl.CheckXOFFCounters( flictalk , FLIC::targetFpgaU1 ) << endl;
    if (option == 225) bytes += ctrl.DumpDataOnTheFloorMode( flictalk , FLIC::targetFpgaU1 );
    if (option == 226) bytes += standalone::DumpSpybufferIPConfig( flictalk );
    if (option == 227) bytes += standalone::DumpU3U4Diagnostics( flictalk );
    if (option == 228) bytes += standalone::DumpCurrentL1ID( flictalk , FLIC::targetFpgaU1 );
    //option=0;//One time!
  }//while option
  return bytes;
}


int StartTestVectorRun( flic_comm *flictalk , FLICController ctrl , int nevents , bool verbose ) {
  int bytes = 0;

  ctrl.DumpDataOnTheFloorMode( flictalk , FLIC::targetFpgaU1 );

  // tell U2 to pay attention to "uase_data" signals coming from U1 in case
  // things get backed up
  flictalk->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x2d , 0x1111 );

  // Just to be extra careful, dump the monitoring FIFOs a couple times to make sure they're empty,
  // then re-reset.
  ushort tmparray[256];
  for( ushort i = 0 ; i < 4 ; i++ ) { // reading full FIFO requires 4 block reads
    for( ushort j = 0 ; j < 4 ; j++ ) {
      bytes += flictalk->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x110+(2*i) , tmparray , 256 );
      usleep(10000);
    }
  }

  cout << "Loading lookup tables onto SRAMs" << endl;
  /*
    DownloadSram( flictalk , FLIC::targetSram1Fpga1 );
    DownloadSram( flictalk , FLIC::targetSram2Fpga1 );
    DownloadSram( flictalk , FLIC::targetSram3Fpga1 );
    DownloadSram( flictalk , FLIC::targetSram4Fpga1 );
  */

  // initialize random number generator with seed=1
  srand( 1 );

  const ushort nEventsInTVDB = 100;

  uint totalMon0Errors = 0;
  uint totalMon3Errors = 0;
  uint totalMon0ErrorsByPipe[4] = {0,0,0,0};
  uint totalMon3ErrorsByPipe[4] = {0,0,0,0};
  uint totalMon0ErrorsByEvid[nEventsInTVDB];
  uint totalMon3ErrorsByEvid[nEventsInTVDB];

  uint totalEventsByPipe[4] = {0,0,0,0};
  uint totalEventsByEvid[nEventsInTVDB];

  uint totalEventsSkipped0 = 0;
  uint totalEventsSkipped1 = 0;

  for( ushort i = 0 ; i < nEventsInTVDB ; i++ ) {
    totalMon0ErrorsByEvid[i] = 0;
    totalMon3ErrorsByEvid[i] = 0;
    totalEventsByEvid[i] = 0;
  }

  while( nevents < 1 ) {
    printf( "Enter number of events you would like to run : " );
    scanf( "%d" , &nevents );
  }

  int mypipe = 4;
  printf( "Which pipeline do you want to monitor? [0-3, 4=>choose random each event] : " );
  scanf( "%d" , &mypipe );
  printf( "\n" );

  // As we pass data through the system, save the Flic output to a file.
  // This can be used to compare to robin data as an additional cross-check.
  ofstream robinExpectedInputData( "robinExpectedInputData.txt" );
  robinExpectedInputData << hex;

  for( uint ievent = 0 , L1ID = 0 ; ievent < uint(nevents) ; ievent++ , L1ID++ ) {

    uint eventMon0Errors = 0;
    uint eventMon3Errors = 0;

    // Use random number generator to determine the TV event that we want to load
    // and process. Also randomly choose the pipeline that we want to monitor.
    uint pipe = mypipe;
    //if( pipe > 3 ) pipe = rand() % 4; // random between 0 and 3
    pipe = 0;
    uint evid = rand() % nEventsInTVDB; // random between 0 and 99
    evid = 0;

    // Load input test vectors into U2 SSBe FIFO, all four pipes
    char inputTVPath[100];
    char outputTVPath[100];
    sprintf( inputTVPath , "../testvectors/events/FlicInputTVFile_1.7.1_ev%u.txt" , evid );
    sprintf( outputTVPath , "../testvectors/events/FlicOutputTVFile_1.7.1_ev%u.txt" , evid );
    ctrl.loadTestVectorsFromFile( inputTVPath , false , L1ID );
    ctrl.WriteTestVectorsOneChannel( flictalk , FLIC::targetFpgaU2 , pipe , false ); // (faster than writing all four channels)

    // Make sure all monitoring FIFOs are watching the desired pipeline
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x17 , (pipe<<12) + (pipe<<8) + (pipe<<4) + pipe );
    bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x17 , pipe ); // set mon0 to pipe
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x18 , pipe ); // set mon1 to pipe
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x19 , pipe ); // set mon2 to pipe
    bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x1a , pipe ); // set mon3 to pipe

    // Now reset & arm the monitoring FIFOs
    //bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x20c , 0x1111 );
    bytes += flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x20c , 0xf ); // one bit for each of four mon fifos

    // Send event through the system
    ctrl.SendFIFOData( flictalk , FLIC::targetFpgaU2 , 0x1<<pipe );

    // Load data in monitoring FIFOs
    ushort dataMon0[1024];
    ushort dataMon3[1024];
    for( ushort i = 0 ; i < 4 ; i++ ) { // reading full FIFO requires 4 block reads
      bytes += flictalk->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x110 , &dataMon0[i*256] , 256 );
      usleep(1000);
      bytes += flictalk->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x116 , &dataMon3[i*256] , 256 );
      usleep(1000);
    }

    // Temporary fix to skip over cases where pseudo-random event is sent instead of the
    // event in the SSBe FIFO
    /*
    bool skipEvent = false;
    if( dataMon0[0] == 0x0 && dataMon0[1] == 0x0 ) {
      totalEventsSkipped0++;
      printf( "Event %5u : Loaded TV ID %2u, Monitoring pipe %1u : Skipping & resetting because header = 0x0 !!\n" ,
              ievent , evid , pipe );
      skipEvent = true;
    } else if( dataMon0[2] == 0x1234 && dataMon0[3] == 0x5678 ) {
      // try sending the event again in order to empty out the SSBe FIFO and be ready for next event
      ctrl.SendFIFOData( flictalk , FLIC::targetFpgaU2 , 0xf );
      ctrl.SendFIFOData( flictalk , FLIC::targetFpgaU2 , 0xf );
      totalEventsSkipped1++;
      printf( "Event %5u : Loaded TV ID %2u, Monitoring pipe %1u : Skipping & resetting because RuNumber = 0x12345678 !!\n" ,
              ievent , evid , pipe );
      skipEvent = true;
    }
    if( skipEvent ) {
      // need to reset all the state machines to ensure we don't sabotage the following event :(
      ctrl.IdleBoard(flictalk);
      ctrl.InitStateMachines(flictalk);
      ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 0 , 4000 );
      ctrl.ResetSSBe(flictalk,FLIC::targetFpgaU2);
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x9 , 0xf ); // set the "force s-link up" bit in each of the four pipelines
      flictalk->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x209 , 0xf00 ); // reset s-link controller to acknowledge the above control
      return bytes;
      continue;
    }
    */

    totalEventsByPipe[pipe]++;
    totalEventsByEvid[evid]++;

    // Open files with input/output TVs and check to make sure the data in monitoring FIFOs
    // matches the expected data in the input/output TVs
    ifstream inputTVFile( inputTVPath );
    ifstream outputTVFile( outputTVPath );
    if( !inputTVFile.is_open() || !outputTVFile.is_open() ) {
      cout << "Unable to open TVs at one of the following paths, ending loop: " << endl;
	// Sep 28 2017 Peilong: comment out for cmake compiling
	//           << "   " << inputTVFile << endl
	//           << "   " << outputTVFile << endl;
      return bytes;
    }

    string tmpword;
    ushort tmpnum;
    uint tmpnum_robin = 0;

    ofstream mon0file( "tmp_mon0.txt" );
    ofstream mon3file( "tmp_mon3.txt" );
    mon0file << hex;
    mon3file << hex;

    // drop the event header from inputTVFile since it doesn't show up in mon fifo 0
    getline( inputTVFile , tmpword );
    getline( inputTVFile , tmpword );
    getline( inputTVFile , tmpword );
    getline( inputTVFile , tmpword );

    for( ushort iword = 0 ; iword < 1024 && !inputTVFile.eof() ; iword++ ) {
      getline( inputTVFile , tmpword );
      istringstream iss( tmpword );
      if( tmpword.length() == 0 ) break; // in case there is a trailing empty line in file
      iss >> hex >> tmpnum;

      // Replace L1ID in the TV file with the one loaded for this event
      //if(      iword == FLIC::EventFormat::INPUT_L1ID_POSITION )   tmpnum = (L1ID >> 16);
      //else if( iword == FLIC::EventFormat::INPUT_L1ID_POSITION+1 ) tmpnum = (L1ID & 0xffff);
      // updated iword values here after dropping event header from input TV file
      if(      iword == 2 ) tmpnum = (L1ID >> 16);
      else if( iword == 3 ) tmpnum = (L1ID & 0xffff);

      bool match = ( dataMon0[iword] == tmpnum );
      if( !match ) eventMon0Errors++;

      char outstr[120];
      sprintf( outstr , "%4x : 0x%04x  %s  %s\n" , iword , dataMon0[iword] , (match?"   ==   ":"XX != XX") , tmpword.c_str() );
      mon0file << outstr;
      if( verbose ) cout << outstr << flush;

      // In every block read (of 256 words) we accidentally miss the last word due to a sync bug
      // Temporary workaround...
      if( (iword+1) % 256 == 0 && !inputTVFile.eof() ) getline( inputTVFile , tmpword );
    }

    for( ushort iword = 0 ; iword < 1024 && !outputTVFile.eof() ; iword++ ) {
      getline( outputTVFile , tmpword );
      istringstream iss( tmpword );
      if( tmpword.length() == 0 ) break; // in case there is a trailing empty line in file
      iss >> hex >> tmpnum;

      // Replace L1ID in the TV file with the one loaded for this event
      if(      iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION )   tmpnum = (L1ID >> 16);
      else if( iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION+1 ) tmpnum = (L1ID & 0xffff);

      bool match = ( dataMon3[iword] == tmpnum );
      if( !match ) eventMon3Errors++;

      char outstr[120];
      sprintf( outstr , "%4x : 0x%04x  %s  %s\n" , iword , dataMon3[iword] , (match?"   ==   ":"XX != XX") , tmpword.c_str() );
      mon3file << outstr;
      if( verbose ) cout << outstr << flush;

      // In every block read (of 256 words) we accidentally miss the last word due to a sync bug
      // Temporary workaround...
      if( (iword+1) % 256 == 0 && !outputTVFile.eof() ) getline( outputTVFile , tmpword );
    }

    mon0file.close();
    mon3file.close();

    // Save the expected flic data to file so we can do additional cross-checks
    // against the robin data
    outputTVFile.clear();
    outputTVFile.seekg( 0 , ios::beg );
    for( ushort iword = 0 ; true ; iword++ ) {
      getline( outputTVFile , tmpword );
      istringstream iss( tmpword );
      if( tmpword.length() == 0 ) break;
      iss >> hex >> tmpnum;

      // Replace L1ID in the TV file with the one loaded for this event
      if(      iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION )   tmpnum = (L1ID >> 16);
      else if( iword == FLIC::EventFormat::OUTPUT_L1ID_POSITION+1 ) tmpnum = (L1ID & 0xffff);

      // Write to file, 32 bits at a time
      if( iword % 2 == 0 ) tmpnum_robin = (tmpnum << 16);
      else {
        tmpnum_robin += (tmpnum & 0xffff);
        robinExpectedInputData << setw(8) << setfill('0') << tmpnum_robin << endl;
      }
    }

    inputTVFile.close();
    outputTVFile.close();

    totalMon0Errors += eventMon0Errors;
    totalMon3Errors += eventMon3Errors;
    totalMon0ErrorsByPipe[pipe] += eventMon0Errors;
    totalMon3ErrorsByPipe[pipe] += eventMon3Errors;
    totalMon0ErrorsByEvid[evid] += eventMon0Errors;
    totalMon3ErrorsByEvid[evid] += eventMon3Errors;

    printf( "Event %5u : Loaded TV ID %2u, Monitoring pipe %1u : nInputErrors = %4u (total = %u), nOutputErrors = %4u (total = %u)\n" ,
            ievent , evid , pipe , eventMon0Errors , totalMon0Errors , eventMon3Errors , totalMon3Errors );

  }

  robinExpectedInputData.close();
  printf( "Expected FLIC output saved to robinExpectedInputData.txt\n" );

  printf( "\n=== Summary ===\n\n" );
  printf( "%20s %2s %15s %15s %15s %15s %15s\n" , "" , "" , "nEvents" , "nErrorsIn" , "Avg(nErrorsIn)" , "nErrorsOu" , "Avg(nErrorsOu)" );
  for( ushort pipe = 0 ; pipe < 4 ; pipe++ )
    printf( "%20s %2u %15u %15u %15g %15u %15g\n" ,
            "Pipeline" , pipe , totalEventsByPipe[pipe] ,
            totalMon0ErrorsByPipe[pipe] , double(totalMon0ErrorsByPipe[pipe])/double(totalEventsByPipe[pipe]) ,
            totalMon3ErrorsByPipe[pipe] , double(totalMon3ErrorsByPipe[pipe])/double(totalEventsByPipe[pipe]) );
  for( ushort evid = 0 ; evid < nEventsInTVDB ; evid++ )
    printf( "%20s %2u %15u %15u %15g %15u %15g\n" ,
            "Event Index" , evid , totalEventsByEvid[evid] ,
            totalMon0ErrorsByEvid[evid] , double(totalMon0ErrorsByEvid[evid])/double(totalEventsByEvid[evid]) ,
            totalMon3ErrorsByEvid[evid] , double(totalMon3ErrorsByEvid[evid])/double(totalEventsByEvid[evid]) );
  printf( "%2s %20s %15u %15u %15g %15u %15g\n" ,
          "" , "Total" , nevents ,
          totalMon0Errors , double(totalMon0Errors)/double(nevents) ,
          totalMon3Errors , double(totalMon3Errors)/double(nevents) );
  printf( "%2s %20s %15u\n" , "" , "Skipped(0x0000)" , totalEventsSkipped0 );
  printf( "%2s %20s %15u\n" , "" , "Skipped(0x1234)" , totalEventsSkipped1 );
  printf( "\n" );

  return bytes;
}

