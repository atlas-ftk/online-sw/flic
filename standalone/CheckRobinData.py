#!/usr/bin/python2

"""
Descriptions:
Script compares a Robin data stream (produced using `robin_slink_dst -f XXX.bin`) to the expected data
determined in flicscope testvector runs (nominally saved to robinExpectedInputData.txt") and checks
for bit errors.
"""

import sys, os
import argparse

parser = argparse.ArgumentParser( description = "Check robin byte stream for bit errors" )
parser.add_argument( '-e' , '--expected_data' , required=False , default="robinExpectedInputData.txt" ,
                     help="Path to file containing the expected robin data, produced using flicscope" )
parser.add_argument( '-o' , '--observed_data' , required=True ,
                     help="Path to .bin or .txt file with robin bytestream, produced using robin_slink_dst" )

args = parser.parse_args()

print "Your settings:"
for key,value in vars(args).iteritems():
    print "%-20s : %s" % (key,value)



#
# Open the input files that we want to compare
#

expectedDataFile = open( args.expected_data , 'r' )

observedDataPath = args.observed_data
if args.observed_data[-3:] == "bin":
    # We need to convert the binary to text before making comparison
    observedDataPath += ".txt"
    os.system( "od -X -w4 -An -v %s > %s" % (args.observed_data,observedDataPath) )
observedDataFile = open( observedDataPath , 'r' )



#
# Loop over the observed data, compare to the expected data as we go.
# We need to be a bit careful because the robin bytestream contains additional event
# header and trailer words that aren't saved to the robinExpectedInputData.txt file.
# For the time being we just skip over words until we arrive at our recognizable header,
# and then start ignoring the observed data again when we spot a trailer.
#

nErrors      = 0
nComparisons = 0
nEvents      = 0

exitLoop = False

print "Looping over observed data..."
while not(exitLoop):

    observedLine = observedDataFile.readline()
    if not(observedLine):
        break
    observedWord = int(observedLine.rstrip(),16)
    
    if observedWord == 0xee1234ee:
        # Found the start of a new event
        nEvents += 1
        
        expectedLine = expectedDataFile.readline()
        if not(expectedLine):
            print "ERROR : expected_data file ended too early"
            sys.exit()
        expectedWord = int(expectedLine.rstrip(),16)

        if not(expectedWord==0xee1234ee): nErrors += 1
        nComparisons += 1
        
        # Loop over event until we find the trailer
        while True:
            
            observedLine = observedDataFile.readline()
            expectedLine = expectedDataFile.readline()

            # print nComparisons,observedLine.rstrip(),expectedLine.rstrip()

            if not(observedLine) or not(expectedLine):
                print "ERROR : At least one of the files ended in the middle of an event"
                if not(observedLine): print "        ---> observed_data file is guilty"
                if not(expectedLine): print "        ---> expected_data file is guilty"
                sys.exit()

            observedWord , expectedWord = int(observedLine.rstrip(),16) , int(expectedLine.rstrip(),16)
            if not( observedWord == expectedWord ): nErrors += 1
            nComparisons += 1
                
            if observedWord == 0xeeeedddd:
                # end of event trailer
                break
print "finished"
            
expectedDataFile.close()
observedDataFile.close()

print
print "=== SUMMARY ==="
print "nEvents      =",nEvents
print "nComparisons =",nComparisons
print "nErrors      =",nErrors
print

