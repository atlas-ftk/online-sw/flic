#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>

#include "flic/flic_comm.h"
#include "flic/FLICController.h"
#include "flic/FLIC.h"
#include "flic/StandaloneTools.h"

using namespace daq::ftk;
using namespace std;

int OptionFLIC( flic_comm *flictalk , FLICController ctrl );
int OptionBLADE( flic_comm *flictalk , FLICController ctrl );

int HelloWorld( flic_comm *flictalk ) {
  int bytes=0;
  int data=0;
  printf("Enter data[dec?]: ");
  scanf("%d",&data);
  bytes+=flictalk->singlewrite(FLIC::targetNone,FLIC::cmdRead,0x0,data,0);
  printf("Wrote %d words to FLIC\n\n",bytes);
  return bytes;
}

int ReadWorld( flic_comm *flictalk ) {
  int bytes=0;
  ushort data=0;
  bytes+=flictalk->singleread(FLIC::targetNone,FLIC::cmdRead,0x0,&data,0);
  printf("Read string: %d",data);
  return bytes;
}


int main() {
  int bytesent=0;
  FLICController ctrl;

  while( true ) {

    FLIC::Comm::Info b = standalone::getBoardSelectionFromUser();
    if( ! b.isDefined ) break;

    flic_comm *flictalk = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    if( b.isBlade ) {
      bytesent += OptionBLADE( flictalk , ctrl );
    } else {
      bytesent += OptionFLIC( flictalk , ctrl );
    }
    delete flictalk; // sockets closed in destructor

  }

  return bytesent;
}

int OptionFLIC( flic_comm *flictalk , FLICController ctrl ) {
  int option=1;
  int optionSSBe=1;
  int bytesent=0;
  bool useSSBe=true;
  bool checkSetup=true;
  while(checkSetup!=0){
    cout << endl
         << standalone::line << endl
         << "   0 Exit  " << endl
         << standalone::line << endl
         << "Using SSBe?" << endl
         << "   1 Yes                           2 No" <<endl
         << standalone::line << endl;
    scanf("%d",&optionSSBe);
    if(optionSSBe==1){useSSBe=true;checkSetup=0;}
    else if (optionSSBe==2){useSSBe=false;checkSetup=0;}
    else checkSetup=1;
  }
  while(option!=0){
    cout << endl
         << standalone::line << endl
         << "Select an option:"<< endl
         << "   0 Exit                                                   "<< endl
         << standalone::line << endl
         << "   1 Power On                    100 Power Off" << endl
         << standalone::line << endl
         << "   2 Default setup using SSBe      3 Default setup using SSB(TBD)" <<endl
         << standalone::lineSlim << endl
         << "   4 Program All FPGAs             5 Spybuffer Setup" << endl
         << "   6 Event tagging                 7 Idle Board"<< endl
         << standalone::lineSlim << endl
         << "   8 Setup SSBe                    9 Setup Channel and Eth"<< endl
         << standalone::line << endl
         << "   21 Send event to channel zero  22 Send event to channel one" << endl
         << "   23 Send event to channel two   24 Send event to channel three" << endl
         << standalone::lineSlim << endl
         << "   14 Run Continuously, 1 channel 15 Run Continuously, 4 channels" << endl
         << "                                  18 Stop event sending" << endl
         << "Your choice: ";

    scanf("%d",&option);

    if (option == 1){
      bytesent+=ctrl.PICPowerOn(flictalk);
    }
    if (option == 100) bytesent+=ctrl.PICPowerOff(flictalk);
    if (option == 4){
      bytesent+=ctrl.ProgramFPGAs(flictalk);
    }
    if (option == 5) {
      //bytesent += ctrl.SetupIP(flictalk);
      //bytesent += ctrl.SetupU3U4(flictalk,useSSBe);
      bytesent += ctrl.SetupIPTest(flictalk);
      bytesent += ctrl.SetupU3U4(flictalk,useSSBe);
      bytesent += standalone::DumpSpybufferIPConfig(flictalk);
    }
    if (option == 6) {
      /*      unsigned int mask1=0x0,mask2=0x0;
              cout << endl
              << "   Please give the first mask:" <<endl;
              scanf("%x",&mask1);
              cout << endl
              << "   Please give the second mask:" <<endl;
              scanf("%x",&mask2);
      */      bytesent+=ctrl.EventTagging(flictalk,useSSBe);
    }
    if (option == 7) if(useSSBe)bytesent+=ctrl.IdleBoard(flictalk);
    if (option == 8) {
      bytesent+=ctrl.InitStateMachines(flictalk);
      bytesent+=ctrl.SyncWithSSB(flictalk,FLIC::targetFpgaU1);
      bytesent+=ctrl.ResetSSBe(flictalk,FLIC::targetFpgaU2);
    }
    if (option == 9) {
      /*      unsigned int value1=0x0,value2=0x0;
              cout << endl
              << "   Please give the first mask" <<endl;
              scanf("%d",&value1);
              cout << endl
              << "   Please give the second mask" <<endl;
              scanf("%d",&value2);
      */      bytesent+=ctrl.SetupTagChAndEth(flictalk);
    }

    if (option == 20) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    if (option == 21) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x1 , 1 , 20 , 4000 );
    if (option == 22) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x2 , 1 , 20 , 4000 );
    if (option == 23) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x3 , 1 , 20 , 4000 );
    if (option == 24) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x8 , 1 , 20 , 4000 );

    if (option == 14) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0x1 , 0 , 20 , 4000 );
    if (option == 15) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 0 , 20 , 4000 );
    if (option == 18) bytesent += ctrl.StopSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf );

    if (option == 2) {
      //      unsigned int value1=0x0010,value2=0x0000;
      cout << endl
           << "Default testing setup of Spybuffer using SSBe.\n Tag every 1 in 4 events to U1, 1 in 4 events to U2.\n Send U1 channel1 to eth2 in U4.\n" << endl;
      bytesent+=ctrl.ProgramFPGAs(flictalk);
      bytesent+=ctrl.SetupIP(flictalk);
      bytesent+=ctrl.SetupU3U4(flictalk,true);
      bytesent+=ctrl.EventTagging(flictalk,true);//, value1, value2);
      bytesent+=ctrl.IdleBoard(flictalk);
      bytesent+=ctrl.InitStateMachines(flictalk);
      bytesent+=ctrl.SyncWithSSB(flictalk,FLIC::targetFpgaU1);
      bytesent+=ctrl.ResetSSBe(flictalk,FLIC::targetFpgaU2);
      bytesent+=ctrl.SetupTagChAndEth(flictalk);
    }
  }//while option
  return bytesent;
}

int OptionBLADE( flic_comm *flictalk , FLICController ctrl ) {
  int bytesent=0;
  int option=1;

  while(option!=0){
    printf("\n");
    printf("\n");
    printf("   0 Exit                                                   \n");
    printf("   1 Send Message                                           \n");
    printf("   2 Read Message                                           \n");
    scanf("%d",&option);

    if (option == 1) bytesent=HelloWorld(flictalk);
    if (option == 2) bytesent=ReadWorld(flictalk);

  }//while option

  //flictalk->closesockets();
  return bytesent;
}
