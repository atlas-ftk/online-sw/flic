#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <assert.h>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//


int main() {
  
  int bytes = 0;

  FLICController ctrl;

  FLIC::Comm::Info b1 = FLIC::Comm::boards[0];
  FLIC::Comm::Info b2 = FLIC::Comm::boards[1];
  flic_comm *flic1 = new flic_comm( b1.boardIP.c_str() , b1.port.c_str() , b1.hostIP.c_str() );
  flic_comm *flic2 = new flic_comm( b2.boardIP.c_str() , b2.port.c_str() , b2.hostIP.c_str() );
  
  int option = 1;
  while( option != 0 ) {
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On" << endl
	 << " 100 Power Off" << endl
         << standalone::line << endl
         << " Basic initialization controls..." << endl
         << "   2 Program FPGAs" << endl
	 << " 300 FlicWare setup" << endl
	 << "   3 Idle boards" << endl
	 << "   4 Setup One Channel Proc" << endl
	 << "  20 Send sync event from SSBe" << endl
	 << "   6 Reset SSBe" << endl
         << standalone::line << endl
         << "  77 Start SSBe data with new configuration" << endl
         << "  78 Stop SSBe data but leave configuration unchanged" << endl
         << standalone::line << endl
         << " Diagnostics..." << endl
         << "  90 Init counters to COUNT MODE" << endl
	 << "  91 Init counters to RATE MODE" << endl
         << "  92 Dump Board 1 Counters" << endl
	 << "  93 Dump Board 2 Counters" << endl
	 << " 222 Check program status" << endl
	 << " 223 Check clock counters" << endl
	 << " 224 Check XOFF counters" << endl
         << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) bytes += ctrl.PICPowerOn(flic1) + ctrl.PICPowerOn(flic2);
    if (option == 100) bytes += ctrl.PICPowerOff(flic1) + ctrl.PICPowerOff(flic2);
    if (option == 2) {
      ctrl.ProgramFPGAs( flic1 );
      ctrl.ProgramFPGAs( flic2 );
      assert( ctrl.CheckClockCounters( flic1 , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic2 , FLIC::targetFpgaU1 ) );
    }
    if (option == 300) {
      /*
      bytes += standalone::SetupSSBeAndProc( flic1 , FLIC::targetFpgaU2 , flic1 , FLIC::targetFpgaU1 );
      bytes += standalone::SetupSSBeAndProc( flic2 , FLIC::targetFpgaU2 , flic2 , FLIC::targetFpgaU1 );
      bytes += standalone::ResetU3U4( flic1 );
      bytes += standalone::ResetU3U4( flic2 );
      */
      FLICSetupTool t;
      t.setFlicComm( flic1 );
      
    }
    if (option == 3) bytes += ctrl.IdleBoard(flic1) + ctrl.IdleBoard(flic2);
    if (option == 4) bytes += ctrl.InitStateMachines(flic1) + ctrl.InitStateMachines(flic2);
    if (option == 20) {
      bytes += ctrl.StartSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
      bytes += ctrl.StartSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    }
    if (option == 6) {
      bytes += ctrl.ResetSSBe( flic1 , FLIC::targetFpgaU2 );
      bytes += ctrl.ResetSSBe( flic2 , FLIC::targetFpgaU2 );
    }
    if (option == 77) {
      bytes += ctrl.StartSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf , 0 , 17 , 450 , false ); // false => don't actually begin sending events
      bytes += ctrl.StartSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf , 0 , 17 , 450 , false ); // false => don't actually begin sending events
      bytes += ctrl.RestartSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf );
      bytes += ctrl.RestartSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 78) {
      bytes += ctrl.StopSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf );
      bytes += ctrl.StopSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 90) bytes += ctrl.InitProcCounters( flic1 , FLIC::targetFpgaU1 , 0x0000 ) + ctrl.InitProcCounters( flic2 , FLIC::targetFpgaU1 , 0x0000 );
    if (option == 91) bytes += ctrl.InitProcCounters( flic1 , FLIC::targetFpgaU1 , 0xFFFF ) + ctrl.InitProcCounters( flic2 , FLIC::targetFpgaU1 , 0xFFFF );
    if (option == 92) bytes += standalone::DumpProcCounters( flic1 , FLIC::targetFpgaU1 );
    if (option == 93) bytes += standalone::DumpProcCounters( flic2 , FLIC::targetFpgaU1 );
    if (option == 222) {
      cout << "Flic1:" << endl << ctrl.CheckStatus( flic1 ) << endl;
      cout << "Flic2:" << endl << ctrl.CheckStatus( flic2 ) << endl;
    }
    if (option == 223) {
      cout << "Flic1:" << endl << ctrl.CheckClockCounters( flic1 , FLIC::targetFpgaU1 ) << endl;
      cout << "Flic2:" << endl << ctrl.CheckClockCounters( flic2 , FLIC::targetFpgaU1 ) << endl;
    }
    if (option == 224) {
      cout << "Flic1:" << endl << ctrl.CheckXOFFCounters( flic1 , FLIC::targetFpgaU1 ) << endl;
      cout << "Flic2:" << endl << ctrl.CheckXOFFCounters( flic2 , FLIC::targetFpgaU1 ) << endl;
    }
    if (option == 225) bytes += ctrl.DumpDataOnTheFloorMode( flic1 , FLIC::targetFpgaU1 ) + ctrl.DumpDataOnTheFloorMode( flic2 , FLIC::targetFpgaU1 );
    //option=0;//One time!
  }//while option

  delete flic1;
  delete flic2;

  return bytes;

}



