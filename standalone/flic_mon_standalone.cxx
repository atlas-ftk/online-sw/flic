//#include <concurrent_queue.h>
#include "tbb/concurrent_queue.h"

#include "flic/FLIC_Mon_comm.h"
#include "src/FLIC_Mon_comm.cxx"

#include "flic/FLIC_Mon_event.h"
#include "src/FLIC_Mon_event.cxx"

#include <inttypes.h>
#include <vector>
#include <deque>
#include <queue>

//#include "flic/FLIC.h"
//#include "flic/StandaloneTools.h"

//#include <TString.h>
#include <TH1F.h>
#include <TH2F.h>
//#include "TH1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"

#define MAXLINE 5000 // Max word received from FLIC: 5000 words

//typedef uint16_t HALF;


using namespace daq::ftk;
/*
  void float32(float* __restrict out, const uint16_t in) {
  uint32_t t1;
  uint32_t t2;
  uint32_t t3;
 
  t1 = in & 0x7fff;                       // Non-sign bits
  t2 = in & 0x8000;                       // Sign bit
  t3 = in & 0x7c00;                       // Exponent
 
  t1 <<= 13;                              // Align mantissa on MSB
  t2 <<= 16;                              // Shift sign bit into position
 
  t1 += 0x38000000;                       // Adjust bias
 
  t1 = (t3 == 0 ? 0 : t1);                // Denormals-as-zero
 
  t1 |= t2;                               // Re-insert sign bit
 
  *((uint32_t*)out) = t1;
  };

*/
int main() {

  FLIC_Mon_comm * flicToBlade = new FLIC_Mon_comm("192.168.2.2", 50000, "192.168.2.1", 50001);

  int sockfd;
  struct sockaddr *pcliaddr;
  socklen_t clilen;


  int n;
  socklen_t len;
  //uint16_t mesg[MAXLINE] = {0};  // uint16_t -> unsigned short int  
  //uint16_t* mesg_spybuffer_end_ptr = mesg + MAXLINE - 3000;
  //uint16_t* mesg_start_ptr = mesg;
  //uint16_t* mesg_end_ptr;


  /*
    struct FLIC_UDP_packet {
    uint16_t mesg[MAXLINE];  // uint16_t -> unsigned short int
    };

    std::vector<FLIC_UDP_packet> flic_udp_buffer;
  
    flic_udp_buffer.resize(5); // 100 UDP packet could be stored

    uint16_t * mesg_ptr = flic_udp_buffer.at(0).mesg;
  */

  struct FLIC_UDP_packet {
    uint16_t mesg_udp[MAXLINE];  // uint16_t -> unsigned short int
    //    int udp_length;
  };

  //  std::deque<FLIC_UDP_packet> flic_udp_buffer;
  std::queue<FLIC_UDP_packet> flic_udp_buffer;
  tbb::concurrent_bounded_queue<FLIC_UDP_packet> flic_udp_buffer_tbb;
  //  uint16_t * mesg_ptr = flic_udp_buffer.at(0).mesg;                                                                                                   
  //unsigned long int i_mesg = 0;

  printf("Before TCanvas definition\n");

  


  //  TCanvas* c1 = new TCanvas("c1","",1000,800);

  printf("Before TH1F definition\n");

  //  c1->Divide(4,2);

  printf("Before TH1F definition\n");

  /*
    std::vector<TH1F *> TrackParameterHistograms;


    TH1F *m_hTrackSector = new TH1F( "hTrackSector" , "TrackSector;Sector;Monitored Ev" , 32 , 0 , 8192 );
    TrackParameterHistograms.push_back(m_hTrackSector);

    TH1F *m_hTrackChi2 = new TH1F( "hTrackChi2" , "TrackChi2;#chi^{2};Monitored Ev" , 25 , 0 , 500 );
    TrackParameterHistograms.push_back(m_hTrackChi2);

    TH1F *m_hTrackD0 = new TH1F( "hTrackD0" , "TrackD0;d_{0} [mm];Monitored Ev" , 30 , -3.0 , 3.0 );
    TrackParameterHistograms.push_back(m_hTrackD0); 

    TH1F *m_hTrackZ0 = new TH1F( "hTrackZ0" , "TrackZ0;z_{0} [mm];Monitored Ev" , 40 , -200.0 , 200.0 );
    TrackParameterHistograms.push_back(m_hTrackZ0); 

    TH1F *m_hTrackCotth = new TH1F( "hTrackCotth" , "TrackCotth;cot#theta;Monitored Ev" , 30 , -6 , 6 );
    TrackParameterHistograms.push_back(m_hTrackCotth);  

    TH1F *m_hTrackPhi = new TH1F( "hTrackPhi" , "TrackPhi;#phi_{0};Monitored Ev" , 32 , -8 , 8 );
    TrackParameterHistograms.push_back(m_hTrackPhi);

    TH1F *m_hTrackCurv = new TH1F( "hTrackCurv" , "TrackCurv;1/2*p_{T} [GeV^{-1}];Monitored Ev" , 25 , -0.5 , 0.5 );
    TrackParameterHistograms.push_back(m_hTrackCurv); 
  */
  
  /*
    TH1F *m_hTrackSector = new TH1F( "hTrackSector" , "TrackSector;Sector;Monitored Ev" , 32 , 0 , 8192 );
    TH1F *m_hTrackChi2 = new TH1F( "hTrackChi2" , "TrackChi2;#chi^{2};Monitored Ev" , 25 , 0 , 500 );
    TH1F *m_hTrackD0 = new TH1F( "hTrackD0" , "TrackD0;d_{0} [mm];Monitored Ev" , 30 , -3.0 , 3.0 );
    TH1F *m_hTrackZ0 = new TH1F( "hTrackZ0" , "TrackZ0;z_{0} [mm];Monitored Ev" , 40 , -200.0 , 200.0 );
    TH1F *m_hTrackCotth = new TH1F( "hTrackCotth" , "TrackCotth;cot#theta;Monitored Ev" , 30 , -6 , 6 );
    TH1F *m_hTrackPhi = new TH1F( "hTrackPhi" , "TrackPhi;#phi_{0};Monitored Ev" , 32 , -8 , 8 );
    TH1F *m_hTrackCurv = new TH1F( "hTrackCurv" , "TrackCurv;1/2*p_{T} [GeV^{-1}];Monitored Ev" , 25 , -0.5 , 0.5 );


  */
  printf("Hello world\n");

  //int temp;

  int udp_packet_counter = 0;
   
  while (true) {
    //  for (i_mesg=0; ; ++i_mesg) {
    printf("going to receive event number %d~~~~~~~\n", ++udp_packet_counter);

    sockfd = flicToBlade->getBladeETHSockfd();
    pcliaddr =  flicToBlade->getSockAddrPtr();
    clilen = flicToBlade->getSocklen_t();

    printf("after get function----\n");

    len = clilen;

    printf("before receive from----\n");

    struct FLIC_UDP_packet received_one_udp_packet;
    
    //return the length of the message in bytes
    n = recvfrom(sockfd, received_one_udp_packet.mesg_udp, MAXLINE, 0, pcliaddr, &len);
    
    printf("after receive %d bytes--->\n", n);

    flic_udp_buffer.push(received_one_udp_packet);
    std::cout << "size flic_udp_buffer: " << flic_udp_buffer.size() << std::endl;
    flic_udp_buffer_tbb.push(received_one_udp_packet);
    std::cout << "size flic_udp_buffer_tbb: " << flic_udp_buffer_tbb.size() << std::endl;


    //    if (i_mesg % 3 == 0) {
    /*
    for (int i = 0; i <1500 ; ++i ) {
      printf("line number: %.4d", i);
      printf("----------->%04x  ", mesg_temp_ptr[i]);
      printf("%u\n", mesg_temp_ptr[i]);
    }
    */
    /*
      FLIC_Mon_event * oneEvent = new FLIC_Mon_event(mesg_temp_ptr);
      oneEvent->getEventProcessingResult();
      printf("printed every one of three event\n");
    */
    //  }


    //temp = 0;

    
  } // end of while

  return 0;
}
