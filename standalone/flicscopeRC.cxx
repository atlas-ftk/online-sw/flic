#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fstream>
#include <getopt.h>

#include "flic/flic_comm.h"
#include "flic/FLICController.h"
#include "flic/FLIC.h"

using namespace daq::ftk;


int main() {

  int bytesent = 0;
  int option   = 1;
  
  flic_comm flic1("10.153.37.18","50000",""); // Prototype board @ USA15
  //flic_comm flic1("10.193.32.17","50000"); // Production boards 4-5 @ ANL
  //flic_comm flic1("10.193.32.18","50000"); // Production board 1 @ Lab4
  
  FLICController ctrl;
  
  while( option != 0 ) {

    printf( "\n" );
    printf( "Select a state transition:\n" );
    printf( "   0 exit\n" );
    printf( "   1 setup\n" );
    printf( "   2 configure\n" );
    printf( "   3 connect\n" );
    printf( "   4 prepareForRun\n" );
    printf( "   5 stopDC\n" );
    //printf( "   6 disconnect\n" );
    printf( "   7 unconfigure\n" );
    //printf( "   8 publish\n" );
    printf( "   9 resynch\n" );
    printf( "   10 send event\n" );
    printf( "   11 program, idle, init\n" );
    printf( "> " );

    scanf( "%d" , &option );
    if( option == 1 ) bytesent += ctrl.setup(&flic1);
    if( option == 2 ) bytesent += ctrl.configure(&flic1);
    if( option == 3 ) bytesent += ctrl.connect(&flic1);
    if( option == 4 ) bytesent += ctrl.prepareForRun(&flic1);
    if( option == 5 ) bytesent += ctrl.stopDC(&flic1);
    //if( option == 6 ) bytesent += ctrl.disconnect(&flic1);
    if( option == 7 ) bytesent += ctrl.unconfigure(&flic1);
    //if( option == 8 ) bytesent += ctrl.publish(&flic1);
    if( option == 9 ) bytesent += ctrl.resynch(&flic1);
    if( option == 10 ) bytesent += ctrl.StartSSBeData( &flic1 , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    if( option == 11 ) {
      ctrl.ProgramFPGAs(&flic1);
      ctrl.IdleBoard(&flic1);
      ctrl.InitStateMachines(&flic1);
    }

  }

  flic1.closesockets();
  return bytesent;
  
}

