#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flictalk , FLICController ctrl );


int main() {
  int bytesent = 0;
  FLICController ctrl;

  while( true ) {
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic1 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    bytesent += Option( flic1 , ctrl );
    delete flic1; // sockets closed in destructor
  }
    
  return bytesent;
}


int Option( flic_comm *flictalk , FLICController ctrl ) {
  int option=1;
  int bytesent=0;
  while(option!=0){
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On" << endl
	 << " 100 Power Off" << endl
	 << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
	 << " Basic initialization controls..." << endl
         << "   2 Program All FPGAs" << endl
	 << "   3 Initialize links and state machines" << endl
	 << "   4 Send single sync event from emulators" << endl
	 << "   5 Reset emulators" << endl
         << standalone::line << endl
	 << "  76 Start SSBe data on U1 with new configuration" << endl
	 << "  77 Start SSBe data on U2 with new configuration" << endl
	 << "  78 Stop SSBe data but leave configuration unchanged" << endl
         << standalone::line << endl
         << "   7 Read a register" << endl
	 << "   8 Write a register" << endl
         << standalone::line << endl
         << "  13 Change Emulation Parameters on U1" << endl
	 << "  14 Change Emulation Parameters on U2" << endl
         << standalone::line << endl
	 << " Diagnostics..." << endl
      	 << " 222 Check program status" << endl
	 << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) bytesent += ctrl.PICPowerOn( flictalk );
    if (option == 100) bytesent += ctrl.PICPowerOff( flictalk );
    if (option == 200) bytesent += ctrl.PICPowerFromIPMC( flictalk );
    if (option == 2) bytesent += ctrl.ProgramFPGAs( flictalk , FLIC::ListProc::DoubleSSBe );
    if (option == 3) {
      bytesent += ctrl.ResetSERDES_SSBe( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.ResetSERDES_SSBe( flictalk , FLIC::targetFpgaU2 );
      bytesent += ctrl.InitStateMachines_SSBe( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.InitStateMachines_SSBe( flictalk , FLIC::targetFpgaU2 );
    }
    if (option == 4) {
      bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
      bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    }
    if (option == 5) {
      bytesent += ctrl.ResetSSBe( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.ResetSSBe( flictalk , FLIC::targetFpgaU2 );
    }
    if (option == 76) ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 );
    if (option == 77) ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU2 );
    if (option == 78) {
      bytesent += ctrl.StopSSBeData( flictalk , FLIC::targetFpgaU1 , 0xf );
      bytesent += ctrl.StopSSBeData( flictalk , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 7) bytesent += standalone::ReadRegister( flictalk );
    if (option == 8) bytesent += standalone::WriteRegister( flictalk );
    if (option == 13) bytesent += standalone::ChangeSSBeRunParam( flictalk , FLIC::targetFpgaU1 );
    if (option == 14) bytesent += standalone::ChangeSSBeRunParam( flictalk , FLIC::targetFpgaU2 );
    if (option == 222) cout << ctrl.CheckStatus( flictalk ) << endl; 
    //option=0;//One time!
  }//while option
  return bytesent;
}




