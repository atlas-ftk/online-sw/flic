#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/StandaloneTools.h"

using namespace daq::ftk;
using namespace std;

int main() {

  FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
  if( ! b.isDefined ) assert( false );

  flic_comm *flic = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() , false );

  ushort d;

  flic->singleread( FLIC::targetFpgaU1 , FLIC::cmdRead , 0x100 , &d );
  printf( "U1 revision = %04x\n" , d );

  flic->singleread( FLIC::targetFpgaU2 , FLIC::cmdRead , 0x100 , &d );
  printf( "U2 revision = %04x\n" , d );

  flic->singleread( FLIC::targetFpgaU3 , FLIC::cmdRead , 0x100 , &d );
  printf( "U3 revision = %04x\n" , d );

  flic->singleread( FLIC::targetFpgaU4 , FLIC::cmdRead , 0x100 , &d );
  printf( "U4 revision = %04x\n" , d );

  delete flic;
  return 0;

}



