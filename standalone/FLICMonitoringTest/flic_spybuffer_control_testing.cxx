// Created by Peilong Wang on June 8, 2017
// Used for testing of setting up FLIC Spybuffer registers
// version 0.0


#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;


int Option( flic_comm *flic_ssbe , flic_comm *flic_proc , FLICController ctrl );
//int Setup( flic_comm *flic_ssbe , flic_comm *flic_proc );

int main() {

  int bytes = 0;
  FLICController ctrl;
  
  while( true ) {

    // build up SSBe connection
    cout << endl;
    cout << "--- SSBe Board ---" << endl;
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic_ssbe = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    cout << endl;


    // build up Proc connection
    cout << "--- Processor Board ---" << endl;
    b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic_proc = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );

    // print out option for controlling
    bytes += Option( flic_ssbe , flic_proc , ctrl );

    // deleting objects
    delete flic_ssbe;
    delete flic_proc;
  }

  return bytes;
}


int Option( flic_comm *flic_ssbe , flic_comm *flic_proc , FLICController ctrl ) {
  int option = 1;
  int bytes  = 0;
  while( option != 0 ) {
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On ALL" << endl
         << " 100 Power Off ALL" << endl
                
         << "--------------------Basic initialization controls--------------------" << endl
         << "   2 Program All FPGAs" << endl
         << "   3 Setup Spybuffer registers for testing" << endl
         << "     - FPGA U3 register 6A is programmed with value 1101" << endl
         << "   4 Change to \"dump data on the floor\" mode" << endl
         << "   5 Initialize counters" << endl
         << "   6 Dump counters" << endl
	 << "   7 Dump spybuffer IP configuration" << endl
	 << "   8 Dump U3/U4 diagnostics" << endl

         << "----------------------------------------" << endl
         << "  76 Send single event" << endl
	 << "  761 Send single event in a while loop" << endl
	 << "  77 Start SSBe data" << endl
         << "  78 Stop SSBe data" << endl
                
         << "--------------------Diagnostics--------------------" << endl
         << "  1008 Read register on SSBe board   1009 Write register on SSBe board" << endl
	 << "  1010 Read register on Proc board   1011 Write register on Proc board" << endl

         << "----------------------------------------" << endl
         << "  1084 Reset and arm monitoring FIFOs" << endl
         << "  1085 Dump monitoring FIFOs on U1" << endl
         << "  1086 Dump monitoring FIFOs on U2" << endl
	 << "  1087 Reset and arm flow control FIFOs" << endl
	 << "  1088 Dump flow control FIFOs on U1" << endl
	 << "  1089 Dump flow control FIFOs on U2" << endl
         << "  1092 Dump global status" << endl
	 << "  1093 Dump raw SLINK status" << endl
         << "  1222 Check program status" << endl
         << "  1223 Check clock counters" << endl
         << "  1224 Check XOFF counters" << endl
         << "----------------------------------------" << endl
         << "Your choice: ";

    scanf("%d",&option);


    //  1 Power On ALL
    if (option == 1) {
      bytes += ctrl.PICPowerOn( flic_ssbe );
      bytes += ctrl.PICPowerOn( flic_proc );
    }

    // 100 Power Off ALL
    if (option == 100) {
      bytes += ctrl.PICPowerOff( flic_ssbe );
      bytes += ctrl.PICPowerOff( flic_proc );
    }

    //  2 Program All FPGAs
    if (option == 2) {
      // Program proc board
      ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::DoubleProc_NoSRAMs );
      //ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::Loopback_Reverse_NoSRAMs ); // FIXME - update name of variable
      // Kick GTX112 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x200 , 0xffff );
      // Kick GTX116 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x208 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x208 , 0xffff );
      // Then check the clocks
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) );
      // Program ssbe board
      ctrl.ProgramFPGAs( flic_ssbe , FLIC::ListProc::DoubleSSBe );
    }


    // 3 Setup Spybuffer registers for testing
    if (option == 3) {

      //  Setup SSBe board
      FLICSetupTool t_ssb;
      t_ssb.setFlicComm( flic_ssbe );
      t_ssb.INIT_U2_U2_U3_U4();

      //  Setup Proc board
      FLICSetupTool t_flic;
      t_flic.setFlicComm( flic_proc );
      t_flic.INIT_U1_U1_U3_U4();

      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );

      //  Send sync event from SSBe board to Proc board
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU2 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );


      //  Reset SSBe
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU2 );


      //  Setup Spy buffers for all events on all channels
      t_flic.INIT_SPYBUFFERS();

    }


    if (option == 4) {
      bytes += ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 5) {
      bytes += ctrl.InitProcCounters( flic_proc , FLIC::targetFpgaU1 , 0x0 );
      bytes += ctrl.InitProcCounters( flic_proc , FLIC::targetFpgaU2 , 0x0 );
    }
    if (option == 6) {
      bytes += standalone::DumpProcCounters( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpProcCounters( flic_proc , FLIC::targetFpgaU2 );
    }
    if( option == 7 ) bytes += standalone::DumpSpybufferIPConfig( flic_proc );
    if( option == 8 ) bytes += standalone::DumpU3U4Diagnostics( flic_proc );


    if (option == 76) {
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 3 , 100 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 100 );
    }
    if (option == 761) {
      ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0x1 , 10 , 0 , 4000 , false );
      int nInput =0;      
      printf("input how many times do you want to loop: \n");
      scanf("%d", &nInput);

      for(int i=0; i<nInput; ++i) {
	ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0x1 );
	// usleep( 1000000 );



	//      bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x201 , 1<<12 );

	// bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0x1 , 1 , 3 , 100 );
	//  bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 100 );
      }
    }


    if (option == 77) {
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 78) {
      bytes += ctrl.StopSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.StopSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }


    

    if (option == 1008) standalone::ReadRegister( flic_ssbe );
    if (option == 1009) standalone::WriteRegister( flic_ssbe );
    if (option == 1010) standalone::ReadRegister( flic_proc );
    if (option == 1011) standalone::WriteRegister( flic_proc );

    if (option == 1084) {
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0 );
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 , 0 );
    }
    if (option == 1085) bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 );
    if (option == 1086) bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 );
    if (option == 1087) {
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 1088) bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
    if (option == 1089) bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );

    if (option == 1092) {
      bytes += standalone::DumpGlobalStatus( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpGlobalStatus( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 1093) {
      bytes += standalone::DumpRawSLINKStatus( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpRawSLINKStatus( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 1222) {
      cout << "--- SSBe Board ---" << endl
           << ctrl.CheckStatus( flic_ssbe ) << endl
           << "--- Processor Board ---" << endl
           << ctrl.CheckStatus( flic_proc ) << endl;
    }
    if (option == 1223) {
      cout << ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 1224) {
      cout << ctrl.CheckXOFFCounters( flic_proc , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckXOFFCounters( flic_proc , FLIC::targetFpgaU2 ) << endl;
    }

    
    //option=0;//One time!
  }//while option
  return bytes;
}

