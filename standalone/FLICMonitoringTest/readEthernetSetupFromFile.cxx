// reading a text file
#include <iostream>
#include <fstream>


int main () {

  char ipaddress[50];
  char port[50];
  char macaddress[50];
  std::ifstream myfile("example.txt");
  if (myfile.is_open()) {
    myfile >> ipaddress >> port >> macaddress;
    std::cout <<"xxx " << ipaddress << "_" << port << "_" << macaddress << std::endl;

    myfile.close();
  }
else
  std::cout << "Unable to open file";

return 0;
}
    
