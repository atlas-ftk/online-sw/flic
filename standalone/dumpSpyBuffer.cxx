#include <iostream>
#include <algorithm>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;

char* getCmdOption(char ** begin, char ** end, const string & option);
bool cmdOptionExists(char** begin, char** end, const string& option);

//
// Spy Buffer Dumping in Multiple Ways 

int main(int argc, char *argv[]) {
  
  FLICController ctrl;
  int bytes = 0;
    
  FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
  if( ! b.isDefined ) exit(0);
  flic_comm *flic1 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
  
  if(cmdOptionExists(argv, argv+argc, "-h")) {
    cout<<" Welcome to AwesomeDump (AW), available options are: "<<endl;
    cout<<" -h       Print this help message  "<<endl;
    cout<<" -m       Running Mode (Default: simgle dump )"<<endl;
    cout<<"          s:   Single Dump; "<<endl;
    cout<<"          l1:  Dump Event with a Given L1ID (May not work at high rates) "<<endl;
    cout<<"               Used together with -i option "<<endl;
    cout<<"               Default ID is 0x029 "<<endl;
    cout<<"          c:   Continously dump n events "<<endl;
    cout<<"               n is given by the -t option "<<endl;
    cout<<"               Deault range is 10 events "<<endl;
    cout<<"          l1r: Continously dump n events starting from L1ID a to b "<<endl;
    cout<<"               a and  is given by the -r option "<<endl;
    cout<<"               Deault range is 0x029 -  0x036 "<<endl;
    cout<<" -i       L1ID  Target event's L1ID (hex) "<<endl;
    cout<<" -t       Tries Number of events to dump "<<endl;
    cout<<" -r       Range Range of L1IDs to dump "<<endl;
    exit(0);
  }

  string mode = "s";
 
  if(cmdOptionExists(argv, argv+argc, "-m")) {
    mode = string(getCmdOption(argv, argv + argc, "-f")); 
  }

  if (mode == "s") {
    cout<<" Arm monitoring FIFO "<<endl;
    bytes += ctrl.ResetArmMonitoringFIFOs( flic1 , FLIC::targetFpgaU1 , 0x0 , 4);
    cout<<" Read monitoring FIFO "<<endl;
    bytes += standalone::ReadMonitoringFIFOs( flic1 , FLIC::targetFpgaU1 );
    cout<<" Event L1 ID:  "<<endl;
    bytes += standalone::DumpCurrentL1ID( flic1 , FLIC::targetFpgaU1 );   
  }
  if (mode == "c") {
    int tries = 10;
    if(cmdOptionExists(argv, argv+argc, "-t")) {
      tries = stoi(string(getCmdOption(argv, argv + argc, "-f")));
    }
    for (int i = 0; i < tries; i++) {
      cout<<" "<<i<<"th try: "<<endl;
      cout<<" Arm monitoring FIFO "<<endl;
      bytes += ctrl.ResetArmMonitoringFIFOs( flic1 , FLIC::targetFpgaU1 , 0x0 , 4);
      cout<<" Read monitoring FIFO "<<endl;
      bytes += standalone::ReadMonitoringFIFOs( flic1 , FLIC::targetFpgaU1 );
      cout<<" Event L1 ID:  "<<endl;
      bytes += standalone::DumpCurrentL1ID( flic1 , FLIC::targetFpgaU1 );   
    }  
  }
  return bytes;
}

char* getCmdOption(char ** begin, char ** end, const string & option)
{
    char ** itr = find(begin, end, option);
    if (itr != end && ++itr != end) {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const string& option)
{
    return find(begin, end, option) != end;
}


