0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x6742 <-- Extended level 1 ID
0x0000
0x023e <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x83fb <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0ffc <-- 
0x0000
0x0000 <-- RoadID[23:0]
0x18a8
0x8023 <-- Track chi2[31:16] | Track d0[15:0]
0x8d61
0x7eed <-- Track z0[31:16] | Track coth[15:0]
0x30c1
0xb911 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015a <-- Res[31:12] | Module L0
0x0000
0x0000 <-- IBL
0x0000
0x027d <-- Res[31:12] | Module L1
0x0000
0x0000 <-- PL0b
0x0000
0x042a <-- Res[31:12] | Module L2
0x1930
0x1160 <-- PL1b
0x0000
0x068d <-- Res[31:12] | Module L3
0x18d0
0x1028 <-- PL2b
0x0355
0x09bd <-- SAx0
0x037c
0x09bc <-- SSt0
0x04a2
0x0d34 <-- SAx1
0x0476
0x0d35 <-- SSt1
0x05d5
0x116d <-- SAx2
0x0024
0x1184 <-- SSt2
0x00e0
0x167c <-- SAx3
0x00ac
0x167d <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
