0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x6a54 <-- Extended level 1 ID
0x0000
0x0161 <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x0910 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0ffe <-- 
0x0000
0x0011 <-- RoadID[23:0]
0x23ad
0x8038 <-- Track chi2[31:16] | Track d0[15:0]
0xb240
0x7e40 <-- Track z0[31:16] | Track coth[15:0]
0x2d6b
0x3cd0 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015c <-- Res[31:12] | Module L0
0x0000
0x0000 <-- IBL
0x0000
0x027f <-- Res[31:12] | Module L1
0x14b4
0x11d8 <-- PL0b
0x0000
0x041f <-- Res[31:12] | Module L2
0x1400
0x28bb <-- PL1b
0x0000
0x0682 <-- Res[31:12] | Module L3
0x1360
0x27ec <-- PL2b
0x0502
0x09bd <-- SAx0
0x04dd
0x09bc <-- SSt0
0x024c
0x0d4c <-- SAx1
0x026a
0x0d4d <-- SSt1
0x0594
0x1185 <-- SAx2
0x0580
0x1184 <-- SSt2
0x0374
0x1694 <-- SAx3
0x0382
0x1695 <-- SSt3
0x0bda
0x2e6f <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0ffe <-- 
0x0000
0x0007 <-- RoadID[23:0]
0x2070
0x7c07 <-- Track chi2[31:16] | Track d0[15:0]
0x9a44
0x8389 <-- Track z0[31:16] | Track coth[15:0]
0x4125
0xbbde <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x016f <-- Res[31:12] | Module L0
0x0000
0x0000 <-- IBL
0x0000
0x028b <-- Res[31:12] | Module L1
0x1770
0x2794 <-- PL0b
0x0000
0x0445 <-- Res[31:12] | Module L2
0x18c0
0x15c8 <-- PL1b
0x0000
0x06b6 <-- Res[31:12] | Module L3
0x1050
0x242d <-- PL2b
0x03e2
0x09ef <-- SAx0
0x040e
0x09ee <-- SSt0
0x0266
0x0d7e <-- SAx1
0x024d
0x0d7f <-- SSt1
0x00c4
0x11cf <-- SAx2
0x00cc
0x11ce <-- SSt2
0x04be
0x16c6 <-- SAx3
0x04c2
0x16c7 <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
