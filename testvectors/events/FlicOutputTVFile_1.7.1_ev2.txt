0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x4d7f <-- Extended level 1 ID
0x0000
0x0520 <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x048e <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x0006 <-- RoadID[23:0]
0x0cc0
0x7ff4 <-- Track chi2[31:16] | Track d0[15:0]
0x6104
0x855a <-- Track z0[31:16] | Track coth[15:0]
0x325b
0xa25d <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x0158 <-- Res[31:12] | Module L0
0x1299
0x235e <-- IBL
0x0000
0x027c <-- Res[31:12] | Module L1
0x14b4
0x254c <-- PL0b
0x0000
0x0429 <-- Res[31:12] | Module L2
0x16c0
0x2453 <-- PL1b
0x0000
0x068c <-- Res[31:12] | Module L3
0x1890
0x1470 <-- PL2b
0x0550
0x09bd <-- SAx0
0x0572
0x09bc <-- SSt0
0x0189
0x0d4c <-- SAx1
0x0184
0x0d4d <-- SSt1
0x0393
0x1185 <-- SAx2
0x0381
0x1184 <-- SSt2
0x0558
0x167c <-- SAx3
0x057f
0x167d <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
