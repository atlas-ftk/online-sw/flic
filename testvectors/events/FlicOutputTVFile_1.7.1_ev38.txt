0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x6a40 <-- Extended level 1 ID
0x0000
0x07e3 <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x0585 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0dff <-- 
0x0000
0x0072 <-- RoadID[23:0]
0x3704
0x7ffe <-- Track chi2[31:16] | Track d0[15:0]
0x84d1
0x800f <-- Track z0[31:16] | Track coth[15:0]
0x254d
0x4432 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x0146 <-- Res[31:12] | Module L0
0x1235
0x1609 <-- IBL
0x0000
0x0270 <-- Res[31:12] | Module L1
0x16b0
0x2424 <-- PL0b
0x0000
0x0410 <-- Res[31:12] | Module L2
0x16c0
0x25bc <-- PL1b
0x0000
0x0673 <-- Res[31:12] | Module L3
0x16c0
0x1038 <-- PL2b
0x0452
0x09a5 <-- SAx0
0x047e
0x09a4 <-- SSt0
0x0592
0x0d1c <-- SAx1
0x0564
0x0d1d <-- SSt1
0x018e
0x116d <-- SAx2
0x0000
0x116c <-- SSt2
0x035a
0x1664 <-- SAx3
0x032c
0x1665 <-- SSt3
0x0bda
0x26de <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0ffd <-- 
0x0000
0x0056 <-- RoadID[23:0]
0x3738
0x8027 <-- Track chi2[31:16] | Track d0[15:0]
0x8673
0x8470 <-- Track z0[31:16] | Track coth[15:0]
0x3330
0xb74e <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015a <-- Res[31:12] | Module L0
0x144d
0x13c9 <-- IBL
0x0000
0x027d <-- Res[31:12] | Module L1
0x0000
0x0000 <-- PL0b
0x0000
0x042b <-- Res[31:12] | Module L2
0x11a0
0x254c <-- PL1b
0x0000
0x068e <-- Res[31:12] | Module L3
0x1330
0x1588 <-- PL2b
0x0551
0x09bd <-- SAx0
0x0524
0x09bc <-- SSt0
0x0134
0x0d4e <-- SAx1
0x0100
0x0d4f <-- SSt1
0x02f8
0x1187 <-- SAx2
0x031a
0x1186 <-- SSt2
0x045e
0x167e <-- SAx3
0x0450
0x167f <-- SSt3
0x0bda
0x001a <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x0011 <-- RoadID[23:0]
0x176b
0x7ff7 <-- Track chi2[31:16] | Track d0[15:0]
0x916b
0x8175 <-- Track z0[31:16] | Track coth[15:0]
0x2da3
0xbe56 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015b <-- Res[31:12] | Module L0
0x2137
0x20d6 <-- IBL
0x0000
0x027e <-- Res[31:12] | Module L1
0x12d0
0x1140 <-- PL0b
0x0000
0x041e <-- Res[31:12] | Module L2
0x1360
0x1658 <-- PL1b
0x0000
0x0681 <-- Res[31:12] | Module L3
0x13e0
0x1350 <-- PL2b
0x00b6
0x09bd <-- SAx0
0x00a2
0x09bc <-- SSt0
0x015e
0x0d34 <-- SAx1
0x0179
0x0d35 <-- SSt1
0x01dd
0x116d <-- SAx2
0x01bb
0x116c <-- SSt2
0x01fc
0x1664 <-- SAx3
0x0226
0x1665 <-- SSt3
0x0bda
0x16f4 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0bff <-- 
0x0000
0x000c <-- RoadID[23:0]
0x1b0c
0x800b <-- Track chi2[31:16] | Track d0[15:0]
0x7f9d
0x8808 <-- Track z0[31:16] | Track coth[15:0]
0x3a3a
0x4c01 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015a <-- Res[31:12] | Module L0
0x22e4
0x27fe <-- IBL
0x0000
0x028a <-- Res[31:12] | Module L1
0x1880
0x12f8 <-- PL0b
0x0000
0x0438 <-- Res[31:12] | Module L2
0x1240
0x283b <-- PL1b
0x0000
0x06a8 <-- Res[31:12] | Module L3
0x24c1
0x244c <-- PL2b
0x0280
0x09ef <-- SAx0
0x029a
0x09ee <-- SSt0
0x01d8
0x0d7e <-- SAx1
0x01e2
0x0d7f <-- SSt1
0x0182
0x11cf <-- SAx2
0x0152
0x11ce <-- SSt2
0x0000
0x16e0 <-- SAx3
0x0130
0x16e1 <-- SSt3
0x0bda
0x0043 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x0000 <-- RoadID[23:0]
0x2b42
0x802a <-- Track chi2[31:16] | Track d0[15:0]
0x8665
0x8148 <-- Track z0[31:16] | Track coth[15:0]
0x3f4d
0x576c <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x016e <-- Res[31:12] | Module L0
0x133b
0x1131 <-- IBL
0x0000
0x028a <-- Res[31:12] | Module L1
0x1800
0x1740 <-- PL0b
0x0000
0x0444 <-- Res[31:12] | Module L2
0x1870
0x15a8 <-- PL1b
0x0000
0x06b4 <-- Res[31:12] | Module L3
0x18e0
0x247c <-- PL2b
0x0028
0x0a05 <-- SAx0
0x0037
0x0a04 <-- SSt0
0x0048
0x0d94 <-- SAx1
0x0040
0x0d95 <-- SSt1
0x00aa
0x11e5 <-- SAx2
0x00ae
0x11e4 <-- SSt2
0x011c
0x16f4 <-- SAx3
0x011c
0x16f5 <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
