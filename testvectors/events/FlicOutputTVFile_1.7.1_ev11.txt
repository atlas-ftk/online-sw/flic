0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x66de <-- Extended level 1 ID
0x0000
0x0aaf <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x4744 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x000c <-- RoadID[23:0]
0x21ff
0x804f <-- Track chi2[31:16] | Track d0[15:0]
0x6d50
0x844d <-- Track z0[31:16] | Track coth[15:0]
0x32b2
0xcc46 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x0159 <-- Res[31:12] | Module L0
0x10af
0x237e <-- IBL
0x0000
0x027c <-- Res[31:12] | Module L1
0x1920
0x257c <-- PL0b
0x0000
0x042a <-- Res[31:12] | Module L2
0x1100
0x13e0 <-- PL1b
0x0000
0x068d <-- Res[31:12] | Module L3
0x1280
0x1360 <-- PL2b
0x042e
0x09bd <-- SAx0
0x0442
0x09bc <-- SSt0
0x057a
0x0d34 <-- SAx1
0x0578
0x0d35 <-- SSt1
0x00b8
0x1185 <-- SAx2
0x009e
0x1184 <-- SSt2
0x0171
0x167c <-- SAx3
0x019e
0x167d <-- SSt3
0x0bda
0x22c9 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x03ff <-- 
0x0000
0x0001 <-- RoadID[23:0]
0x0c8f
0x7f91 <-- Track chi2[31:16] | Track d0[15:0]
0x7bac
0x84d1 <-- Track z0[31:16] | Track coth[15:0]
0x3b48
0xd49c <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015a <-- Res[31:12] | Module L0
0x1037
0x281f <-- IBL
0x0000
0x028a <-- Res[31:12] | Module L1
0x1560
0x22ed <-- PL0b
0x0000
0x0437 <-- Res[31:12] | Module L2
0x1750
0x16c8 <-- PL1b
0x0000
0x06a7 <-- Res[31:12] | Module L3
0x18d0
0x1158 <-- PL2b
0x049d
0x09d5 <-- SAx0
0x0482
0x09d4 <-- SSt0
0x018a
0x0d64 <-- SAx1
0x01c0
0x0d65 <-- SSt1
0x0412
0x119f <-- SAx2
0x0442
0x119e <-- SSt2
0x0000
0x16ae <-- SAx3
0x0000
0x16af <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
