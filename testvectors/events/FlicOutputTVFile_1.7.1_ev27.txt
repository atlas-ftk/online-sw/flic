0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x677a <-- Extended level 1 ID
0x0000
0x07f9 <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x0974 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x0007 <-- RoadID[23:0]
0x6a55
0x8030 <-- Track chi2[31:16] | Track d0[15:0]
0x779e
0x8c99 <-- Track z0[31:16] | Track coth[15:0]
0x2710
0x9e4b <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x0146 <-- Res[31:12] | Module L0
0x1131
0x16d1 <-- IBL
0x0000
0x0270 <-- Res[31:12] | Module L1
0x17b0
0x1518 <-- PL0b
0x0000
0x0411 <-- Res[31:12] | Module L2
0x1310
0x16d8 <-- PL1b
0x0000
0x0674 <-- Res[31:12] | Module L3
0x16e0
0x10a0 <-- PL2b
0x0292
0x09a7 <-- SAx0
0x0266
0x09a6 <-- SSt0
0x0296
0x0d20 <-- SAx1
0x0280
0x0d21 <-- SSt1
0x0288
0x1159 <-- SAx2
0x0260
0x1158 <-- SSt2
0x022d
0x1652 <-- SAx3
0x0212
0x1653 <-- SSt3
0x0bda
0x01c8 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x0004 <-- RoadID[23:0]
0x1469
0x805e <-- Track chi2[31:16] | Track d0[15:0]
0x7724
0x8341 <-- Track z0[31:16] | Track coth[15:0]
0x4008
0xcd91 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x016d <-- Res[31:12] | Module L0
0x1443
0x214e <-- IBL
0x0000
0x028a <-- Res[31:12] | Module L1
0x12e0
0x1720 <-- PL0b
0x0000
0x0444 <-- Res[31:12] | Module L2
0x1410
0x2476 <-- PL1b
0x0000
0x06b4 <-- Res[31:12] | Module L3
0x1520
0x11b8 <-- PL2b
0x0292
0x09ed <-- SAx0
0x029e
0x09ec <-- SSt0
0x007b
0x0d7c <-- SAx1
0x0082
0x0d7d <-- SSt1
0x042f
0x11b5 <-- SAx2
0x041c
0x11b4 <-- SSt2
0x0183
0x16c4 <-- SAx3
0x01a8
0x16c5 <-- SSt3
0x0bda
0x0b3b <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0bff <-- 
0x0000
0x0000 <-- RoadID[23:0]
0x2290
0x7fef <-- Track chi2[31:16] | Track d0[15:0]
0x77bb
0x8a03 <-- Track z0[31:16] | Track coth[15:0]
0x3b53
0x4de0 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x015a <-- Res[31:12] | Module L0
0x1073
0x289d <-- IBL
0x0000
0x028a <-- Res[31:12] | Module L1
0x1660
0x13d0 <-- PL0b
0x0000
0x0438 <-- Res[31:12] | Module L2
0x1100
0x2a0c <-- PL1b
0x0000
0x06a8 <-- Res[31:12] | Module L3
0x1420
0x26a4 <-- PL2b
0x0348
0x09ef <-- SAx0
0x0352
0x09ee <-- SSt0
0x02cc
0x0d7e <-- SAx1
0x02ee
0x0d7f <-- SSt1
0x02a7
0x11d1 <-- SAx2
0x02d2
0x11d0 <-- SSt2
0x0000
0x16e0 <-- SAx3
0x0298
0x16e1 <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
