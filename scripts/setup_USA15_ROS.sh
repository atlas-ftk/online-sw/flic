source /det/tdaq/ftk/ftk_setup.sh FTK-01-00-00
if [ ! -f daq/partitions/FTK_ROS_only.data.xml ]; then
  cp /det/tdaq/ftk/repo/oks/FTK-01-00-00/daq/partitions/FTK_ROS_only.data.xml daq/partitions/
  echo "default FTK_ROS_only.data.xml has been copied to local area"
fi
export TDAQ_DB_DATA=daq/partitions/FTK_ROS_only.data.xml
#export TDAQ_DB_DATA=/det/tdaq/ftk/repo/oks/FTK-01-00-00/daq/partitions/FTK_ROS_only.data.xml 
export TDAQ_PARTITION=FTK_ROS_only
#setup_daq $TDAQ_PARTITION

echo
echo "To start the partition:"
echo "  setup_daq -p $TDAQ_PARTITION"
echo "To check if it is open/closed:"
echo "  ipc_ls -P -l"
echo "To fully close (if there are lingering processes):"
echo "  pmg_kill_partition -p ${TDAQ_PARTITION}"
echo

echo "done."
