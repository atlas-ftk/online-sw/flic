#!/bin/sh

source /afs/cern.ch/user/f/ftk/bin/setup_ftk.sh FTK-00-10-00

export TDAQ_REPOSITORY_ROOT=${PWD}
export TDAQ_DB_DATA=${PWD}/flic/daq/partitions/FTK.data.ustb.xml
export TDAQ_PARTITION=FTK
export TDAQ_DB_PATH=${PWD}/flic:${TDAQ_DB_PATH}
export TDAQ_IPC_INIT_REF=file:${PWD}/ipc/`uname -n | awk -F"." '{print $1}'`/ipc_root.ref

#export CMTPROJECTPATH=`echo $CMTPROJECTPATH | awk -F":" '{print $2":"$1}'`
#export PATH=/opt/tdaq/PMG:$PATH
#ARCH=`uname -i|sed 's?1386?i686?'`
#export LD_LIBRARY_PATH=`pwd`/installed/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
#export PATH=`pwd`/installed/${CMTCONFIG}/bin:$PATH
#export CMTPATH=`pwd`/installed:$CMTPATH

